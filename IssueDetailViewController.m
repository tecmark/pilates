//
//  IssueDetailViewController.m
//  Yoga
//
// Created by Rob on 10-3-18.

//

#import "IssueDetailViewController.h"
#import "AddIssueViewController.h"
#import "JournalViewController.h"
#import "PoseDetailViewController.h"

#define keyboardHeight 160

@implementation IssueDetailViewController


@synthesize issueDict;
@synthesize issueRowNumber;
@synthesize isIssueResolved;
@synthesize isAddIssue;
@synthesize isFromAsana;


- (void)viewDidLoad {
    [super viewDidLoad];
	
	titleLB.text = [self.issueDict objectForKey:@"content"];
	NSString *content = [self.issueDict objectForKey:@"content"];
	contentTV.font = [UIFont fontWithName:@"Helvetica" size:15];
	contentTV.text = (0 == content.length) ? @"" : content;
	poseNameLB.text = [self.issueDict objectForKey:@"poseName"];
	
	CGSize size = [poseNameLB.text sizeWithFont:poseNameLB.font];
	gotoPoseBtn.frameSizeWidth = size.width + 10;
	updateTimeLB.text = [self.issueDict objectForKey:@"updateDate"];
	selectBtn.selected = isIssueResolved;
	[self.view insertSubview:notePage atIndex:1];
	if (isAddIssue) {
		//selected means done
		
		titleLB.text = @"New Issue";
		updateTimeLB.text = todayStr();
		//@TBD if should set the select btn unable
		selectBtn.enabled = NO;
		deleteBtn.enabled = NO;
		
	}
	
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(keyboardWillShow:) 
												 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(keyboardWillHide:) 
												 name:UIKeyboardWillHideNotification object:nil];
}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}


- (void)changedTitle:(NSString*)newTitle {
	poseNameLB.text = newTitle;
}

- (IBAction)addIssue {
	if (isFromAsana) {
		[contentTV becomeFirstResponder];
	}
	else {
		AddIssueViewController *vc = [[AddIssueViewController alloc] init];
		vc.isChangeTitle = YES;
		vc.changeTitleCallbackTarget = self;
		vc.changeTitleCallbackSelector = @selector(changedTitle:);
		[self.navigationController pushViewController:vc animated:YES];
		[vc release];
	}
	
}

- (IBAction)editIssue {
	[contentTV becomeFirstResponder];
	
}

- (IBAction)addIssueDone {
	NSString *realContent = [contentTV.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	if (realContent.length == 0) {
		showAlertViewWithMessage(@"Please input details of the issue.");
		return;
	}
	[contentTV resignFirstResponder];
	if (isAddIssue) {
		[dbHandle() executeUpdate:@"insert into Issues (PoseName, IssueContent, UpdateDate) values (?, ?, ?)", 
		 poseNameLB.text, contentTV.text,todayStr()];
		
	}
	else {
		[dbHandle() executeUpdate:@"update Issues set PoseName = ?, IssueContent = ?, UpdateDate = ? where ID = ?", 
		 poseNameLB.text, contentTV.text, todayStr(), [self.issueDict objectForKey:@"ID"]];
		
	}
	if (isFromAsana) {
		[self.navigationController popViewControllerAnimated:YES];
	}
	else {
		UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:1];
		[self.navigationController popToViewController:vc animated:YES];
	}
}
- (IBAction)setResolved {
	selectBtn.selected = !selectBtn.selected;
	NSNumber * issueId = [self.issueDict objectForKey:@"ID"];
	NSString * isResolvedStr = [NSString stringWithFormat:@"%i", selectBtn.selected];
	[dbHandle() executeUpdate:@"update Issues set IsResolved = ? where ID = ?" ,isResolvedStr, [issueId stringValue]];
}
- (void)doDeleteIssue {
	NSNumber * issueId = [self.issueDict objectForKey:@"ID"];
	[dbHandle() executeUpdate:@"delete from Issues where ID = ?" ,[issueId stringValue]];
	if (isFromAsana) {
		[self.navigationController popViewControllerAnimated:YES];
	}
	else {
		UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:1];
		[self.navigationController popToViewController:vc animated:YES];
	}
	
}
- (IBAction)deleteIssue {
	NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyAlertView" owner:nil options:nil];
	MyAlertView *myAlert = [nib lastObject];
	myAlert.titleLB.text = @"\"Delete Issue\"";
	myAlert.messageLB.text = @"Are you sure you want to delete this issue?";
	myAlert.delegate = self;
	[self.view addSubview:myAlert];
	[myAlert showDialog];
}
- (IBAction)gotoPose {
	id<PLResultSet> result;
	result = [dbHandle() executeQuery: @"SELECT * FROM Poses where Name = ?", poseNameLB.text];
	if ([result next]) {
		NSNumber *poseID = [result objectForColumn:@"ID"];
		NSString *description = [result stringForColumn:@"Description"];
		NSString *picPath = [result stringForColumn:@"PicturePath"];
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:poseID, @"poseID",
							  description, @"description", picPath, @"picPath", poseNameLB.text, @"poseName", nil];
		PoseDetailViewController *vc = [PoseDetailViewController alloc];
		vc.poseDict = dict;
		[self.navigationController pushViewController:vc animated:YES];
		[vc release];
	}
	
}
#pragma mark --- MyAlertViewDelegate ---
- (void)myAlertOk {
	[self doDeleteIssue];
}
#pragma mark --- UITextViewDelegate ---
- (void)textViewDidBeginEditing:(UITextView *)textView {
	NSLog(@"=========== textViewDidBeginEditing ===========");
    
    
}
- (void)textViewDidChange:(UITextView *)textView {
	titleLB.text = contentTV.text;
	//	[contentTV scrollRangeToVisible:NSMakeRange([contentTV.text length], 0)];
	
}
#pragma mark --- Keyboard Delegate ---
- (void)keyboardWillShow:(NSNotification *)aNotification {
	NSLog(@"=========== keyboard will show ===========");
	NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	[UIView beginAnimations:@"ResizeForKeyboardUp" context:nil];
	[UIView setAnimationDuration:animationDuration];

    
    CGFloat maxY = 480 - 216;
    
    CGFloat amountToMove = maxY - contentTV.frame.origin.y;
    
    /*
	int length = contentTV.text.length;
	if (!isAddIssue &&  length > 0) {
		NSRange range = {contentTV.text.length -1, 1};
		[contentTV scrollRangeToVisible:range];
	}
	*/
    CGRect newFrame = contentTV.frame;
    newFrame.size.height -= amountToMove-self.textViewAmountMoved;
    [contentTV setFrame:newFrame];
    
    self.textViewAmountMoved = amountToMove;
    
    
	[UIView commitAnimations];
	NSLog(@"=========== keyboard show finished =========");
}

- (void)keyboardWillHide:(NSNotification *)aNotification {
	
	NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	[UIView beginAnimations:@"ResizeForKeyboardDown" context:nil];
	[UIView setAnimationDuration:animationDuration];
    
    CGRect newFrame = contentTV.frame;
    newFrame.size.height -= -self.textViewAmountMoved;
    [contentTV setFrame:newFrame];
    
    self.textViewAmountMoved = 0;
    
	[UIView commitAnimations];
	
}

@end

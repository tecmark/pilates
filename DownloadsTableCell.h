//
//  DownloadsTableCell.h
//  Yoga
//
// Created by Rob on  11/11/10.
//  
//

#import <Foundation/Foundation.h>
#import "UAProduct.h"
#import "UAStoreFront.h"

@interface DownloadsTableCell : UITableViewCell <UAProductObserverProtocol>{
	IBOutlet UIButton *checkButton;
	IBOutlet UILabel  *classNameLabel;
	IBOutlet UILabel  *dateLabel;
	IBOutlet UILabel  *sizeLabel;
	IBOutlet UIProgressView *downloadProgressView;
	
	UAProduct *product;
}

@property (nonatomic, retain) UIButton *checkButton;
@property (nonatomic, retain) UILabel  *classNameLabel;
@property (nonatomic, retain) UILabel  *dateLabel;
@property (nonatomic, retain) UILabel  *sizeLabel;
@property (nonatomic, retain) UIProgressView *downloadProgressView;
@property (nonatomic, assign) UAProduct *product;

- (void)showDownloadProgress;
- (void)hideDownloadProgress;
- (IBAction)checkBoxCheckStateChanged;

@end

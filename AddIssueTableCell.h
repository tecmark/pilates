//
//  AddIssueTableCell.h
//  Yoga
//
// Created by Rob on 10-3-17.

//

#import <UIKit/UIKit.h>


@interface AddIssueTableCell : UITableViewCell {
	
	IBOutlet UIImageView *poseIV;
	IBOutlet UILabel *titleLB;
	IBOutlet UILabel *subTitleLB;
	IBOutlet UILabel *descLB;

}


@property (nonatomic, retain) UIImageView *poseIV;
@property (nonatomic, retain) UILabel *titleLB;
@property (nonatomic, retain) UILabel *subTitleLB;
@property (nonatomic, retain) UILabel *descLB;


@end

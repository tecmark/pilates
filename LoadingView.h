//
//  LoadingView.h
//
//

#import <UIKit/UIKit.h>


@interface LoadingView : UIView {
	
}

+ (id)loadingViewInView:(UIView *)aSuperview statusMessage:(NSString *)statusMessage;
- (void)removeView;

@end

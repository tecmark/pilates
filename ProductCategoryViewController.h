//
//  ProductListViewController.h
//  Yoga
//
// Created by Rob on 10-4-16.
//  
//

#import <UIKit/UIKit.h>


@interface ProductCategoryViewController : CommonTableViewController {
	IBOutlet UIButton *womenBtn;
	IBOutlet UIButton *menBtn;
	IBOutlet UIButton *kidsBtn;
	IBOutlet UIView *bottomBar;
	int customType;
	int categoryID;
}
@property int categoryID;
@property int customType;

- (IBAction)changeCustomType:(id)sender;

@end

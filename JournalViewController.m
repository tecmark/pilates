//
//  JournalViewController.m
//  Yoga
//
// Created by Rob on 10-3-15.

//

#import "JournalViewController.h"
#import "TableViewHeader.h"
#define kTableMaxHeight 350
#define kTableCellHeight 50

@implementation JournalViewController

@synthesize isForPoseDetailsView;

- (void)loadData {
	id<PLResultSet> result;
	result = [dbHandle() executeQuery: @"SELECT * FROM Issues order by id desc"];
	while ([result next]) {
		NSString *content = [result stringForColumn:@"IssueContent"];
		NSString *poseName = [result stringForColumn:@"PoseName"];
		NSNumber *ID = [result objectForColumn:@"ID"];
//		NSDateFormatter *dateFormatter = [NSDateFormatter new];
		NSString *dateStr = [result stringForColumn:@"UpdateDate"];
		/*
		NSDate *updateDate = [dateFormatter dateFromString:dateStr];
		[dateFormatter setDateFormat:@"EEE MMM dd"];
		dateStr = [dateFormatter stringFromDate:updateDate];
		[NSDateFormatter release];
		 */
		BOOL isResovled = [result boolForColumn:@"IsResolved"];
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:content, @"content",
							  poseName, @"poseName", 
							  ID, @"ID", 
							  dateStr, @"updateDate", nil];
		if (isResovled) {
			[resolvedIssueArray addObject:dict];
		}
		else {
			[notResolvedIssueArray addObject:dict];
		}
	}
	[result close];
}

- (void)setTableHeight {
	if (0 == resolvedIssueArray.count && 0 == notResolvedIssueArray.count) {
		table.hidden = YES;
		noCententInfoView.hidden = NO;
	}
	else {
		table.hidden = NO;
		noCententInfoView.hidden = YES;
		int headerHeight = 0;
		if (resolvedIssueArray.count > 0) headerHeight += 25;
		if (notResolvedIssueArray.count > 0) headerHeight += 25;
		if (isForPoseDetailsView) {
			table.frameSizeHeight = 380;
		}
		else {
			table.frameSizeHeight = 420;
		}

		//table.frameSizeHeight = MIN(kTableMaxHeight+20, 
		//							kTableCellHeight*(resolvedIssueArray.count + notResolvedIssueArray.count) + headerHeight);
	}
	
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	isForPoseDetailsView = NO;
	resolvedIssueArray = [[NSMutableArray alloc] init];
	notResolvedIssueArray = [[NSMutableArray alloc] init];
	table.backgroundColor = [UIColor clearColor];
	[super viewDidLoad];
	
	[self setTableHeight];
	titleLB.text = @"Journal";
}

- (void)viewWillAppear:(BOOL)animated {
	[notResolvedIssueArray removeAllObjects];
	[resolvedIssueArray removeAllObjects];
	[self loadData];
	[table reloadData];
	[self setTableHeight];
	//table.frameSizeHeight = 420;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[resolvedIssueArray release];
	[notResolvedIssueArray release];
    [super dealloc];
}

#pragma mark --- UITableView delegate ---
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSArray * array = (0 == indexPath.section) ?  notResolvedIssueArray:resolvedIssueArray ;
	NSDictionary *dict = [array objectAtIndex:indexPath.row];
	
	IssueDetailViewController *vc = [IssueDetailViewController alloc];
	vc.issueDict = dict;
	vc.issueRowNumber = indexPath.row;
	vc.isIssueResolved = (0 == indexPath.section) ? 0 : 1;
	[vc initWithNibUsingClassName];
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}

#pragma mark --- UITableView Datasource ---
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return kTableCellHeight;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	int arrayCount = (0 == section) ? notResolvedIssueArray.count : resolvedIssueArray.count;
	return (0 == arrayCount) ? 0 : 25;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSString *ret;
	switch (section) {
		case 0:
			ret = @"Unresolved";
			break;
		case 1:
			ret = @"Resolved";
			break;

		default:
			break;
	}
	return ret;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return (0 == section) ? notResolvedIssueArray.count : resolvedIssueArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	JournalHomeTableCell *cell = (JournalHomeTableCell *)[tableView dequeueReusableCellWithIdentifier:@"JournalHomeTableCell"];
	if (nil == cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"JournalHomeTableCell" owner:self options:nil];
		cell = (JournalHomeTableCell *)[nib objectAtIndex:0];
	}

	
	NSString *content; 
	NSString *poseName;
	NSDictionary *dict;
	if (0 == indexPath.section) {
		dict = [notResolvedIssueArray objectAtIndex:indexPath.row];
		
	}
	else {
		dict = [resolvedIssueArray objectAtIndex:indexPath.row];
		
	}
	content = [dict objectForKey:@"content"];
	poseName = [dict objectForKey:@"poseName"];
	cell.titleLB.text = content;
	cell.poseNameLB.text = poseName;
	cell.selectBtn.selected = indexPath.section;
	cell.selectBtn.tag = indexPath.row;
	[cell.selectBtn addTarget:self action:@selector(setResolved:) forControlEvents:UIControlEventTouchUpInside];
//	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	return cell;

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	TableViewHeader *header;
	NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TableViewHeader" owner:nil options:nil];
	for (id oneObject in nib)
		if ([oneObject isKindOfClass:[TableViewHeader class]]){
			header = (TableViewHeader *)oneObject;
			header.textLB.text = (0 == section) ? @"Unresolved" : @"Resolved";
			
		}
	return header;
}
#pragma mark --- IBAction ---
- (IBAction)setResolved:(id)sender {
	UIButton *selectedBtn = (UIButton *)sender;
	if (NO == selectedBtn.selected) {
		selectedBtn.selected = YES;
		int index = selectedBtn.tag;
		
		NSDictionary *dict = [notResolvedIssueArray objectAtIndex:index];
		[resolvedIssueArray addObject:dict];
		[notResolvedIssueArray removeObjectAtIndex:index];
		
		NSNumber * issueId = [dict objectForKey:@"ID"];
		
		[dbHandle() executeUpdate:@"update Issues set IsResolved = 1 where ID = ?", [issueId stringValue]];
		
		/*
		indexPath = [NSIndexPath indexPathForRow:resolvedIssueArray.count -1 inSection:1];
		[table insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
		
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
		[table deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
		*/
	}
	else {
		selectedBtn.selected = NO;
		int index = selectedBtn.tag;
		
		NSDictionary *dict = [resolvedIssueArray objectAtIndex:index];
		[notResolvedIssueArray addObject:dict];
		[resolvedIssueArray removeObjectAtIndex:index];
		
		
		NSNumber * issueId = [dict objectForKey:@"ID"];
		
		[dbHandle() executeUpdate:@"update Issues set IsResolved = 0 where ID = ?", [issueId stringValue]];
		
	}

	[table reloadData];
	[self setTableHeight];
}

- (IBAction)addIssue {
	AddIssueViewController *vc = [[AddIssueViewController alloc] initWithNibUsingClassName];
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}
@end

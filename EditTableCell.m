//
//  EditTableCell.m
//  Yoga
//
// Created by Rob on 10-4-14.

//

#import "EditTableCell.h"


@implementation EditTableCell
@synthesize selection;
@synthesize repeatCountLB;
@synthesize selectBtn;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}


- (void)dealloc {
	[selection release];
	[repeatCountLB release];
	[selectBtn release];
    [super dealloc];
}


@end

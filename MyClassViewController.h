//
//  MyClassViewController.h
//  Yoga
//
// Created by Rob on 10-3-22.

//

#import <UIKit/UIKit.h>
#import "MyClassTableCell.h"
#import "LoadingView.h"


@interface MyClassViewController : CommonTableViewController <UITableViewDelegate, UITableViewDataSource> {

	NSMutableArray *classCatalogArray;

	NSMutableDictionary *classifiedClasses;
	
	LoadingView *loadingView;
}


@property(nonatomic, retain) NSMutableDictionary *classifiedClasses;


@end

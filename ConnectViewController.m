//
//  ConnectViewController.m
//  Yoga
//
// Created by Rob on 10-3-15.

//

#import "ConnectViewController.h"
#import "ProductCategoryViewController.h"
#import "ConnectProductViewController.h"
#import "ThinsWeLikeViewController.h"

@implementation ConnectViewController


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.switchViewContorllerNameArray = [NSArray arrayWithObjects:@"ConnectProductViewController",
										  @"ConnectBooksViewController",
										  @"ConnectOtherViewController",
										  @"ConnectOffTheMat",
										  nil];

}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (IBAction)back {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc {
    [super dealloc];
}

- (IBAction)goToClassLocator:(id)sender {
    ThinsWeLikeViewController *things = [[ThinsWeLikeViewController alloc] initWithDatabaseTable:@"class_locator"];
    [things setTitle:@"Class locator"];
    [self.navigationController pushViewController:things animated:YES];
    [things release];
    
}

- (IBAction)openReading:(id)sender {
    
    ThinsWeLikeViewController *things = [[ThinsWeLikeViewController alloc] initWithDatabaseTable:@"recommended_reading"];
    [things setTitle:@"Recommended reading"];
    [self.navigationController pushViewController:things animated:YES];
    [things release];
}

- (IBAction)gotoPage:(id)sender {
	ProductCategoryViewController *vc = [ProductCategoryViewController alloc];
	vc.categoryID = [sender tag];
	[vc initWithNibUsingClassName];
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}
- (IBAction)gotoOffTheMat {
	

	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.offthematintotheworld.org"]];
}
@end

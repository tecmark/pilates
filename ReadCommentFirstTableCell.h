//
//  ReadCommentFirstTableCell.h
//  Yoga
//
// Created by Rob on  11/26/10.
//  
//

#import <UIKit/UIKit.h>
#import "RatingStars.h"

@interface ReadCommentFirstTableCell : UITableViewCell {
	IBOutlet UILabel *titleL;
	IBOutlet UILabel *reviewerAndDateL;
	IBOutlet RatingStars *reatingV;
	
	IBOutlet UIView *backColorView;
}


@property (nonatomic, assign) IBOutlet UILabel *titleL;
@property (nonatomic, assign) IBOutlet UILabel *reviewerAndDateL;
@property (nonatomic, assign) IBOutlet RatingStars *reatingV;



@end

//
//  ReadCommentsViewController.m
//  Yoga
//
// Created by Rob on  11/26/10.
//  
//

#import "ReadCommentsViewController.h"
#import "ReadCommentTableCell.h"
#import "ReadCommentFirstTableCell.h"
#import "CommentRate.h"
#import "WriteCommentViewController.h"
#import "ClassSectionViewController.h" 

@implementation ReadCommentsViewController


@synthesize yogaClass, writeCommentEnabled, isFromVideo;
UIViewController *thisVc;


#pragma mark -
#pragma mark View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];

	
	NSArray *vcs = self.navigationController.viewControllers;
	thisVc = [vcs objectAtIndex:3];
	NSLog(@"Read Comments view controllers = %i", vcs.count);
	
	
	totalReviews = [self.yogaClass.comments count];
	
	titleLB.text = [NSString stringWithFormat:@"Reviews (%i)", totalReviews, nil];
	
	//writeCommentEnabled = YES;
	
	ReadCommentTableCell *cell = nil;
	NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ReadCommentTableCell" owner:self options:nil];
	for (id oneObject in nib)
		if ([oneObject isKindOfClass:[ReadCommentTableCell class]])
			cell = (ReadCommentTableCell *)oneObject;
	
	if (cell != nil) {
		reviewFont = [cell.commentL.font retain];
	}
	else {
		reviewFont = [UIFont fontWithName:@"HelveticaNeue" size:14];
	}
	
	[self.btn addTarget:self action:@selector(goBackDynamic) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	if (!writeCommentEnabled) {
		writeCommentButton.hidden = YES;
	}
}

#pragma mark -
#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [yogaClass.comments count] + 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.row == 0) {
		return 97;
	}
	
	CommentRate *aCommentRate = [self.yogaClass.comments objectAtIndex:indexPath.row - 1];
	NSString *review = aCommentRate.comment;
	//return 300;
			
	if ([[review stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)
		return 78;
	else {
		reviewFont = [UIFont fontWithName:@"HelveticaNeue" size:16];
		
		
		//CGSize reviewSize = [review sizeWithFont:reviewFont forWidth:294 lineBreakMode:UILineBreakModeWordWrap];
		CGSize reviewSize = [review sizeWithFont:reviewFont constrainedToSize:CGSizeMake(294, 1000) lineBreakMode:UILineBreakModeWordWrap];
		NSLog(@"Row height: %f", reviewSize.height);
		NSLog(@"Review: %@", review);
		return 72 + reviewSize.height+15;
	}
	 
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	if (indexPath.row == 0) {
		static NSString *CellIdentifier = @"readCommentFirstCell";
		
		ReadCommentFirstTableCell *cell = (ReadCommentFirstTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (nil == cell) {
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ReadCommentFirstTableCell" owner:self options:nil];
			for (id oneObject in nib)
				if ([oneObject isKindOfClass:[ReadCommentFirstTableCell class]])
					cell = (ReadCommentFirstTableCell *)oneObject;
		}	
		
		cell.titleL.text = [NSString stringWithFormat:@"Average of %i ratings (current version)", totalReviews, nil];
		
		NSDateFormatter *formater = [[NSDateFormatter alloc] init];
		formater.dateFormat = @"dd MMM yyyy";
		NSString *dateString = [formater stringFromDate:[NSDate date]];
		cell.reviewerAndDateL.text = [NSString stringWithFormat:@"as of %@", dateString, nil];
		[formater release];
		
		[cell.reatingV setRating:self.yogaClass.averageRating];
		
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		return cell;
	}
	else {
		
		static NSString *CellIdentifier = @"readCommentCell";
		
		ReadCommentTableCell *cell = (ReadCommentTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (nil == cell) {
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ReadCommentTableCell" owner:self options:nil];
			for (id oneObject in nib)
				if ([oneObject isKindOfClass:[ReadCommentTableCell class]])
					cell = (ReadCommentTableCell *)oneObject;
		}	
		
		CommentRate *aCommentRate = [self.yogaClass.comments objectAtIndex:indexPath.row - 1];
		
		cell.titleL.text = aCommentRate.title;
		cell.reviewerAndDateL.text = aCommentRate.submittedDate;
		cell.commentL.text = aCommentRate.comment;
				
	//	cell.commentL.text=@"Easy to get into, but hard to master - I keep coming back to this.";
reviewFont = [UIFont fontWithName:@"HelveticaNeue" size:16];
		
		CGSize reviewSize = [aCommentRate.comment sizeWithFont:reviewFont forWidth:294 lineBreakMode:UILineBreakModeWordWrap];
		//CGSize reviewSize = [aCommentRate.comment sizeWithFont:reviewFont constrainedToSize:CGSizeMake(294, 1000) lineBreakMode:UILineBreakModeWordWrap];
		cell.commentL.frame = CGRectMake(cell.commentL.frame.origin.x, cell.commentL.frame.origin.y + 5, 294, reviewSize.height + 15);
		
		[cell.reatingV setRating:aCommentRate.rate];
		
		[cell setBackColorByType:indexPath.row % 2];
		
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		return cell;
	}
}


- (IBAction)goBackDynamic {
	
	NSLog(@"Go back pressed in reviews");
	
//[[UIApplication sharedApplication] setStatusBarHidden:YES];
//	[[UIApplication sharedApplication] setStatusBarOrientation:UIDeviceOrientationLandscapeLeft];
		[self.navigationController popViewControllerAnimated:YES];
}
	
#pragma mark -
#pragma mark Memory management
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}
- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}
- (void)dealloc {
	[reviewFont release];
    [super dealloc];
}


#pragma mark -
#pragma mark IB Action methods
- (IBAction)writeComment {
	WriteCommentViewController *vc = [WriteCommentViewController alloc];
	[vc initWithNibUsingClassName];
	vc.yogaClass = self.yogaClass;
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}

@end


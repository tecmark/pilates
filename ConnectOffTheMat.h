//
//  ConnectOffTheMat.h
//
// Created by Rob on 04/02/2011.
//  Copyright 2011 Bonbol Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface ConnectOffTheMat : CommonViewController  {

	IBOutlet UIScrollView *scrollView;
	IBOutlet UIView *content;
}
- (IBAction)gotoOffTheMat;
@end

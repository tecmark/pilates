//
//  AddIssueViewController.h
//  Yoga
//
// Created by Rob on 10-3-17.

//

#import <UIKit/UIKit.h>


@interface AddIssueViewController : CommonSearchTableViewController <UITableViewDelegate>{
	BOOL isChangeTitle;
	id changeTitleCallbackTarget;
	SEL changeTitleCallbackSelector;
}

@property BOOL isChangeTitle;

@property (nonatomic, assign) id changeTitleCallbackTarget;
@property SEL changeTitleCallbackSelector;

@end
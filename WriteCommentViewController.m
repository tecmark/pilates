//
//  WriteCommentViewController.m
//  Yoga
//
// Created by Rob on  11/10/10.
//  
//

#import "WriteCommentViewController.h"
#import "RateCommentsHelper.h"

@implementation WriteCommentViewController

@synthesize yogaClass;


- (void)viewDidLoad {
    [super viewDidLoad];
	titleLB.text = @"Submit Review";
	
	[raringView setRating:4];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
	
	[titleField becomeFirstResponder];
}
- (void)dealloc {
    [super dealloc];
}

- (IBAction)doneEdit {
	[commentTextView resignFirstResponder];
}
- (IBAction)saveComment {
	[commentTextView resignFirstResponder];
	[[RateCommentsHelper shared] saveRate:raringView.rating title:titleField.text comment:commentTextView.text forClass:yogaClass.classID];
	
	[self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	[commentTextView becomeFirstResponder];
	
	return YES;
}


#pragma mark -
#pragma mark UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
	//keyboardShown = YES;
	
	if([textView.text isEqualToString:@"Review... (Optional)"]){
	textView.text=@"";	
	}
	return YES;
}


#pragma mark -
#pragma mark UIKeyboard Notification methods
- (void)keyboardWillShow:(NSNotification *)aNotification {
	
	NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	[UIView beginAnimations:@"ResizeForKeyboardUp" context:nil];
	[UIView setAnimationDuration:animationDuration];
	doneEditButton.frame = CGRectMake(0, 200, 320, 44);
	[UIView commitAnimations];
}
- (void)keyboardWillHide:(NSNotification *)aNotification {
	
	NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	[UIView beginAnimations:@"ResizeForKeyboardDown" context:nil];
	[UIView setAnimationDuration:animationDuration];
	doneEditButton.frame = CGRectMake(0, 480, 320, 44);
	[UIView commitAnimations];
	
}

@end

//
//  DictViewController.h
//  Yoga
//
// Created by Rob on 10-3-19.

//

#import <UIKit/UIKit.h>
#import "AddIssueTableCell.h"
#import "ArticleDetailViewController.h"
@interface DictViewController : CommonSearchTableViewController {

}
- (IBAction)loadArticle:(id)sender;
@end

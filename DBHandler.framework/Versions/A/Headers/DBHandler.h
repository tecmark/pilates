//
//  DBHandler.h
//
//  Copyright 2010 Tecmark Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

/**
 Database handler class.
 
 Required framework:
 
 -  libsqlite3.0.bylib
 
 Link to framework file is [here](http://redserver.eu/developer/Libraries/iOS/DBHandler/DBHandler.framework.zip)
 
 */
@interface DBHandler : NSObject{
    
}
/**
 @name Main methods
 */
/**
 Use this method to set database name that will be used by default
 
 @param name NSString containing database file name
 */
+ (void) setDefaultDBName:(NSString*)name;

/**
 @return Default database name
 */
+ (NSString*) defaultDBName;

+(NSMutableArray*)sql_exec:(NSString*)sql db_name:(NSString*)dbName error:(NSError**)error;

/**
 Use this method to execute your query on database
 
 @param error Will set error if any occured during query execution
 @param sql NSString containing sql query
 @return Array of dictionaries where each dictionary is a row in database table.
 */
+(NSMutableArray*)sql_exec:(NSString *)sql error:(NSError**)error;

/**
 Use this method to execute string with format.
 
 @param format String with some specified format.
 @param ... Values for your string format.
 @return Array of dictionaries where each dictionary is a row in database table. 
 */
+ (NSArray*) sql_exec_format:(NSString*)format, ... NS_FORMAT_FUNCTION(1, 2);

/**
 Will copy database from resources folder to app cache folder if needed.
 
 Will use defaultDBName parameter.
 */
+(void)prepareDB;

/**
 Will copy database from resources folder to app cache folder if needed.
 @param dbName String with database file name
 */
+(void)prepareDB:(NSString*)dbName;



+(NSMutableArray*)sql_exec_with_distance_func:(NSString*)sql;
+(BOOL)doesTableExists:(NSString*)tableName;
+(NSMutableArray*)returnAllFromTable:(NSString*)tableName;
+(NSMutableArray*)returnAllFromTable:(NSString*)tableName dbName:(NSString*)dbName;
+(void)copyTables:(NSArray*)tables From:(NSString*)db1 to:(NSString*)db2;		
+(void)insertRows:(NSArray*)rows forTable:(NSString*)tableName;
+(void)insertRows:(NSArray*)rows intoDB:(NSString*)db forTable:(NSString*)tableName;
+(int)getLastInsertID;
+(void)resetDatabase;
+(void)resetDatabase:(NSString*)dbName;


@end



@interface DBHandler (DatabaseUpdates)

/**
 Use this methdo to update specific table with provided data.
 
 This method will use "id" as primary key for table. If you want to customise this value use updateTable:withData:primaryKey:
 
 @param tableName String containing table name.
 @param data Array of dictionaries, assume each dictionary is a row with keys as field names.
 @return TRUE if update was successful
 */
+ (BOOL) updateTable:(NSString*)tableName withData:(NSArray*)data;
/**
 Use this methdo to update specific table with provided data.
 
 @param tableName String containing table name.
 @param data Array of dictionaries, assume each dictionary is a row with keys as field names.
 @param primaryKey String with primary key for this table.
 @return TRUE if update was successful
 */
+ (BOOL) updateTable:(NSString*)tableName withData:(NSArray*)data primaryKey:(NSString*)primaryKey;

@end
//
//  ClassesDetailViewController.m
//  Yoga
//
// Created by Rob on 10-3-22.

//

#import "ClassSectionDetailViewController.h"
#import "MoviePlayerViewController.h"

@implementation ClassSectionDetailViewController
@synthesize sectionDict;


- (void)viewDidLoad {
    [super viewDidLoad];
	titleLB.text = [sectionDict objectForKey:@"name"];
	
	NSString *picPath = [sectionDict objectForKey:@"picPath"];
	poseIV.image = [UIImage imageWithContentsOfFile:picPath];
	if (poseIV.image == nil) {
		poseIV.image = [UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:picPath]];
	}
	
	descriptionTV.text = [sectionDict objectForKey:@"name"];

	int duration = [[sectionDict objectForKey:@"duration"] intValue];
	durationLB.text = [NSString stringWithFormat:@"%i:%02i min", duration/60, duration%60];
	
	//[self.view insertSubview:wholeScrollView atIndex:1];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (void)viewWillAppear:(BOOL)animated {
		[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}

- (void)dealloc {
    [super dealloc];
}

- (IBAction)playKrama {
	MoviePlayerViewController *moviePlayer = [MoviePlayerViewController alloc];
	moviePlayer.movieArray = [NSArray arrayWithObject:[sectionDict objectForKey:@"videoPath"]];
	moviePlayer.isPlayingIndividual = YES;
	[moviePlayer initWithNibUsingClassName];
	[self.navigationController pushViewController:moviePlayer animated:NO];
	[moviePlayer release];
}

@end

//
//  ThigsWeLikeCell.m
//  Yoga
//
//  Created by Aleksandr Kelbas on 15/05/2012.
//  Copyright (c) 2012 Tecmark LTD. All rights reserved.
//

#import "ThingsWeLikeCell.h"

@implementation ThingsWeLikeCell
@synthesize imageView;
@synthesize title;
@synthesize subtitle;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [imageView release];
    [title release];
    [subtitle release];
    [super dealloc];
}
@end

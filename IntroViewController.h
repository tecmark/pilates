//
//  IntroViewController.h
//  Yoga
//
// Created by Rob on 10-3-19.

//

#import <UIKit/UIKit.h>
#import "ArticleDetailViewController.h"


@interface IntroViewController : CommonViewController {
	ArticleDetailViewController *articleVC;
}
- (IBAction)loadArticle:(id)sender;


@end

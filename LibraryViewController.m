//
//  LibraryViewController.m
//  Yoga
//
// Created by Rob on 10-3-15.

//

#import "LibraryViewController.h"
#import "IntroViewController.h"
#import "DictViewController.h"
#import "HelpViewController.h"
#import "BasicsViewController.h"

@implementation LibraryViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/




// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.switchViewContorllerNameArray = [NSArray arrayWithObjects:@"IntroViewController",
										  @"DictViewController",
										  @"HelpViewController",
										  @"BasicsViewController",
										  @"ConnectOffTheMat",
										  nil];
	
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

- (IBAction)back {
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)loadArticle:(UIButton*)sender {
	
	NSLog(@"Load article");
    
    int tag = sender.tag;
    NSString *filePath = @"";
    NSString *title = @"";
    
    switch (tag) {
        case 0:
        {
            // Class structure
            filePath = [[NSBundle mainBundle] pathForResource:@"4.a.i.Class structure web" ofType:@"htm"];
            title = @"Class Structure";
        }
            break;
        case 3:
        {
            NSLog(@"Load article");
            
            
            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Definitions and concepts" ofType:@"htm"];
            NSString *title = @"Definitions & Concepts";
            
            ArticleDetailViewController *articleVC= [[ArticleDetailViewController alloc] initWithWebPage:filePath andTitle:title];
            
            
            [self.navigationController pushViewController:articleVC animated:YES];
            return;
        }
            break;
        case 33:// old tag 3
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"4.a.ii.Instructor profile web" ofType:@"htm"];
            title = @"Instructor Profile";
        }
            break;
            
        default:
            break;
    }
	
	ArticleDetailViewController *_articleVC= [[ArticleDetailViewController alloc] initWithWebPage:filePath andTitle:title];
    
	
	[self.navigationController pushViewController:_articleVC animated:YES];
}

@end

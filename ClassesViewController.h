//
//  ClassesViewController.h
//  Yoga
//
// Created by Rob on 10-3-15.

//

#import <UIKit/UIKit.h>
#import "UAInventory.h"
#import "LoadingView.h"

@interface ClassesViewController : CommonHomeViewController {
	IBOutlet UILabel *newClassesNumber;
	IBOutlet UIActivityIndicatorView *newClassesIndicator;
	IBOutlet UILabel *downloadNumber;
	IBOutlet UIActivityIndicatorView *downloadsIndicator;
	
	IBOutlet UIButton *myClassesBtn;
	IBOutlet UIButton *newClassesBtn;
	IBOutlet UIButton *downloadsBtn;
	
	IBOutlet UIImageView *downloadBadge1;
	IBOutlet UIImageView *downloadBadge2;
	
	IBOutlet UILabel *offline1;
	IBOutlet UILabel *offline2;
	
    BOOL inventoryLoaded;
	UAInventory *inventory;
	
	LoadingView *loadingView;
    
    BOOL firstLoadComplete;
	
	
}

- (IBAction)back;
- (void)updateClassNumbers;


@end

//
//  ClassTableViewHeader.h
//  Yoga
//
// Created by Rob on  11/8/10.
//  
//

#import <UIKit/UIKit.h>


@interface ClassTableViewHeader : UIView {

	UILabel *textLB;
	UILabel *textLBSmall;
}

@property (nonatomic, retain) IBOutlet UILabel *textLB;
@property (nonatomic, retain) IBOutlet UILabel *textLBSmall;

@end

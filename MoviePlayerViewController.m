//
//  MoviePlayerViewController.m
//  Yoga
//
// Created by Rob on 10-4-19.
//  
//

#import "MoviePlayerViewController.h"
#import "ReadCommentsViewController.h"

@implementation MoviePlayerViewController


@synthesize moviePlayer;
@synthesize movieArray, classID, yogaClass, repeatArray;
@synthesize isPlayingIndividual;

BOOL readyToPlay;
BOOL canCount;
int repeatNumber;

- (void)setPlayerControlStyle {
    
    self.moviePlayer.controlStyle = MPMovieControlStyleDefault;
}

- (void)playMovie:(NSString *)file {
	
	NSString *movieFilePath = pathForResource(file);
	NSLog(@"Movie Path: %@", movieFilePath);
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:movieFilePath]) {
		movieFilePath = pathOfWritableFile(file);
        NSLog(@"movie not found %@", movieFilePath);
	}
	else{
        NSLog(@"movie found %@", movieFilePath);
    }
	NSURL *movieURL = [NSURL fileURLWithPath:movieFilePath];
	
	self.moviePlayer = [[[MPMoviePlayerController alloc] initWithContentURL:movieURL] autorelease];
	moviePlayer.controlStyle = MPMovieControlStyleFullscreen;
	[moviePlayer setInitialPlaybackTime:0];
	
	if ([moviePlayer respondsToSelector:@selector(view)]) {
		moviePlayer.view.transform = CGAffineTransformMakeRotation(M_PI / 2.0);
		[moviePlayer.view setFrame:CGRectMake(0, 0, 320, 480)];
		[self.view addSubview:moviePlayer.view];
	}
	
	[[UIApplication sharedApplication] setStatusBarOrientation:UIDeviceOrientationLandscapeLeft];
    
    NSLog(@"about to play");
	[self.moviePlayer play];
	
	readyToPlay=NO;	
}


- (void)moviePlayBackDidFinish:(NSNotification*)notification {
    
    if(canCount){
    repeatNumber++;
        canCount=NO;
	}
    
	NSLog(@"movie stopped");
    NSLog(@"movie has played %i times", repeatNumber);
    NSLog(@"repeatsNeeded %i", [[repeatArray objectAtIndex:currentMovieID] intValue]);
   	
	NSInteger reason = [(NSNumber*)[[notification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey] intValue];
	NSLog(@"REASON: %i", reason, nil);
	
	NSInteger state = self.moviePlayer.playbackState;
    
     NSLog(@"STATE: %i", state, nil);
    
	if ((reason == 0 && state == 1)) {
        
        //||(reason == 0 && repeatNumber>0)
        
        
		// Moive Ended
		NSLog(@"Moive Ended");
        
        if(repeatNumber<[[repeatArray objectAtIndex:currentMovieID] intValue]){
            
            //repeatNumber++;
            NSLog(@"repeat same movie");
        }
        else{
            currentMovieID++;
            repeatNumber=0;   
            NSLog(@"play next movie");
        }
        
		if (currentMovieID < movieArray.count)  {
			[moviePlayer.view removeFromSuperview];
			[moviePlayer stop];
			NSString *movieName = [movieArray objectAtIndex:currentMovieID];
			NSLog(@"Playing movie file name: %@", movieName, nil);
            canCount=YES;
			[self playMovie:movieName];
			//[self.moviePlayer setContentURL:[NSURL fileURLWithPath:pathForResource(movieName)]];
			readyToPlay = NO;
		}
		else {
            NSLog(@"finished all movies?");
            
            overlayView.hidden = NO;
            
            currentMovieID = movieArray.count-1;
            repeatNumber=[[repeatArray objectAtIndex:currentMovieID] intValue];
            
            [moviePlayer setControlStyle:MPMovieControlStyleEmbedded];
            [moviePlayer setControlStyle:MPMovieControlStyleNone];
            [self.view bringSubviewToFront:overlayView];
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
            
            
            /*
			currentMovieID = movieArray.count;
			[moviePlayer.view removeFromSuperview];
			[moviePlayer stop];
			overlayView.hidden = NO;		
			 */
		}

	}
	else if (reason == 2) {
		// User exit Moive
		NSLog(@"User exit movie");
	
		overlayView.hidden = NO;
		
		interval = self.moviePlayer.currentPlaybackTime;
		
		[moviePlayer setControlStyle:MPMovieControlStyleEmbedded];
		[moviePlayer setControlStyle:MPMovieControlStyleNone];
		[self.view bringSubviewToFront:overlayView];
	}
	else if (reason == 1) {
		
        NSLog(@"There's an error to play the movie.", nil);
     
        
     
        overlayView.hidden = NO;
		interval = self.moviePlayer.currentPlaybackTime;
		[moviePlayer setControlStyle:MPMovieControlStyleEmbedded];
		[moviePlayer setControlStyle:MPMovieControlStyleNone];
		[self.view bringSubviewToFront:overlayView];
        
	}
    
}


- (void)viewDidLoad {
	
	NSLog(@"view did load");
	if(!reviewLoad){
		
	NSLog(@"init video");
    [super viewDidLoad];
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(moviePlayBackDidFinish:) 
												 name:MPMoviePlayerPlaybackDidFinishNotification 
											   object:nil];
	
	overlayView.transform = CGAffineTransformMakeRotation(M_PI / 2.0);
	overlayView.frameOrigin = CGPointMake(0, 0);
	overlayView.hidden = YES;
	[self.view addSubview:overlayView];
		
		currentMovieID=0;
		NSString *file = [movieArray objectAtIndex:0];
        repeatNumber=0;
        canCount=YES;
		[self playMovie:file];
	}
}

- (void)viewWillAppear:(BOOL)animated {
	NSLog(@"view Will appear");
    
   // showAlertViewWithMessage(@"view will appear");
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];

    
    self.view.frameOrigin=CGPointMake(0, 0);
		  
	if(reviewLoad){
		NSLog(@"reviewLoad = true");
		[self home];
	}

}

- (void)viewDidAppear:(BOOL)animated{
    
	AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.playingVideo=YES;
    
    
   //  showAlertViewWithMessage(@"view did appear");

}

- (void)dealloc {
  //  showAlertViewWithMessage(@"dealloc");
	[classID release];
	[moviePlayer release]; 
	[movieArray release];
	[[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:nil];
	/*
	 [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
	 [[NSNotificationCenter defaultCenter] removeObserver:self
	 name:@"UIDeviceOrientationDidChangeNotification" 
	 object:nil];
	 */
    [super dealloc];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{

	return NO;
}

#pragma mark -
#pragma mark IB Action methods
- (IBAction)playAgain {
	overlayView.hidden = YES;
	currentMovieID = 0;
	NSString *file = [movieArray objectAtIndex:0];
	[self playMovie:file];
}
- (IBAction)back {
    
    NSLog(@"back to video");
    overlayView.hidden = YES;
	NSString *file = [movieArray objectAtIndex:currentMovieID];
	[self playMovie:file];
    /*
    
    
	overlayView.hidden=YES;
	[moviePlayer setControlStyle:MPMovieControlStyleFullscreen];
    [self.moviePlayer setCurrentPlaybackTime:0];
	[self.moviePlayer play];*/
	
}
- (IBAction)home {
	
	reviewLoad=NO;
	AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.playingVideo=NO;
    
	[[UIApplication sharedApplication] setStatusBarHidden:NO];
	
	[[UIApplication sharedApplication] setStatusBarOrientation:UIDeviceOrientationPortrait];
	
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rate {
	
	reviewLoad=YES;
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.playingVideo=NO;
    
    
	[[UIApplication sharedApplication] setStatusBarHidden:NO];
	
	[[UIApplication sharedApplication] setStatusBarOrientation:UIDeviceOrientationPortrait];
	 
	ReadCommentsViewController *vc = [ReadCommentsViewController alloc];
	[vc initWithNibUsingClassName];
	vc.yogaClass = self.yogaClass;
    vc.writeCommentEnabled=YES;
	vc.isFromVideo=YES;
	
	
	
	[self.navigationController pushViewController:vc animated:YES];
	
	
		
}

@end

//
//  ClassInfoViewController.m
//  Yoga
//
// Created by Rob on 10-4-14.
//


#import "BuyClassViewController.h"
#import "WriteCommentViewController.h"
#import "UAStoreFront.h"
#import "UAProduct.h"
#import "DownloadsViewController.h"
#import "ReadCommentsViewController.h"



@implementation BuyClassViewController


@synthesize yogaClass, currentPageNumber;

#pragma mark -
#pragma mark Private methods
- (void)refreshCellWithProductStatus:(UAProductStatus)status {
    
    NSLog(@"PRODUCT STATUS RECEIVED");
    
    switch (status) {
        case UAProductStatusUnpurchased:
            NSLog(@"1");
			[loadingView removeView];
			loadingView = nil;
			buyNowBtn.enabled = YES;
			break;
        case UAProductStatusPurchased:
            NSLog(@"2");
			[loadingView removeView];
			loadingView = nil;
			break;
        case UAProductStatusInstalled:
            NSLog(@"3");
			[yogaClass purchased];
			break;
        case UAProductStatusHasUpdate:
            NSLog(@"4");
            break;
        case UAProductStatusDownloading:
		{
            NSLog(@"5");
			[loadingView removeView];
			loadingView = nil;
			[yogaClass purchased];
			[yogaClass addFileSize];
			
			NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
			
            NSLog(@"Remove viewcontrollers for buyng this class");
            [viewControllers removeObjectAtIndex:[viewControllers count]-1];
             [viewControllers removeObjectAtIndex:[viewControllers count]-1];
            NSLog(@"Add view controller to show downloading");
			downloadViewController = [[DownloadsViewController alloc] init];
			[viewControllers addObject:downloadViewController];
			[self.navigationController setViewControllers:viewControllers animated:YES];
            
            [yogaClass.product removeObserver:self];
            break;
		}
        case UAProductStatusWaiting:
            NSLog(@"6");
            break;
        default:
            break;
    }
}
/*
- (void)refreshCellWithProductStatus:(UAProductStatus)status {
    [self refreshPriceLabel:product.status];
    
    switch (status) {
        case UAProductStatusUnpurchased:
			[loadingView removeView];
			loadingView = nil;
			buyNowBtn.enabled = YES;
			break;
        case UAProductStatusPurchased:
			[loadingView removeView];
			loadingView = nil;
			break;
        case UAProductStatusInstalled:
			[yogaClass purchased];
			break;
        case UAProductStatusHasUpdate:
            break;
        case UAProductStatusDownloading:
		{
			[loadingView removeView];
			loadingView = nil;
			[yogaClass purchased];
			[yogaClass addFileSize];
			
			NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
			[viewControllers removeObjectsInRange:NSMakeRange(2, 2)];
			DownloadsViewController *downloadsView = [[DownloadsViewController alloc] init];
			[viewControllers addObject:downloadsView];
			[self.navigationController setViewControllers:viewControllers animated:YES];
            break;
		}
        case UAProductStatusWaiting:
            break;
        default:
            break;

    }
    [cellView refreshCellViewWithProductStatus:status];
}
*/


#pragma mark -
#pragma mark --- set/get Method ---
- (void)setCurrentPageNumber:(int)num {
	currentPageNumber = num;
	
	//@TODO set the content of the page
	pageControl.currentPage = currentPageNumber;
	[pageControl updateCurrentPageDisplay];
	
}

#pragma mark -
#pragma mark UAProduct observer

- (void)productProgressChanged:(NSNumber*)progress {
      NSLog(@"PRODUCT STATUS CHANGED");
  //  [self refreshProgress:[progress floatValue]];
}

- (void)productStatusChanged:(NSNumber*)status {
      NSLog(@"PRODUCT STATUS OBSERVED");
    
    [self refreshCellWithProductStatus:[status intValue]];
}

/*
#pragma mark -
#pragma mark KVO UAProduct status observer
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object 
                        change:(NSDictionary *)change context:(void *)context {
    
  
    
    if ([keyPath isEqualToString:@"status"]) {
        [self refreshCellWithProductStatus:yogaClass.product.status];
    }
}

*/
#pragma mark -
#pragma mark View Controller methods
- (void)viewDidLoad {
    [super viewDidLoad];
  
    imageScrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 210)];
    [scrollView addSubview:imageScrollView];
    
    [scrollView bringSubviewToFront:buyNowBtn];
    [scrollView bringSubviewToFront:poundBtn];
    
    
	titleLB.text = @"Class info";
	descriptionTV.text = [yogaClass.longDescription stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n\n"];

	pageControl.numberOfPages = 2;
	self.currentPageNumber = 0;
	imageScrollView.showsHorizontalScrollIndicator = NO;
	imageScrollView.contentSize = CGSizeMake(imageScrollView.frameSizeWidth*2, imageScrollView.frameSizeHeight);
	imageScrollView.delegate = self;
	UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageWithData:yogaClass.mainImage]];
	imgView.frameSize = imageScrollView.frameSize;
	[imageScrollView addSubview:imgView];
	[imgView release];
  

	biogView.frameOriginX = imageScrollView.frameSizeWidth;
	[imageScrollView addSubview:biogView];
	biogImageView.image = [UIImage imageWithData:yogaClass.biogImage];
	
    
    biogTextView.text = [yogaClass.biogText stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n\n"];
    NSLog(@"output biog %@", [yogaClass.biogText stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n\n"]);
    
    CGSize constraintSize;
    constraintSize.width = 300.0f;
    constraintSize.height = 1000.0f;
    NSString *theText = descriptionTV.text;
    CGSize theSize = [theText sizeWithFont:descriptionTV.font constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
    
      
    
    NSLog(@"height = %f", theSize.height);
    
	[scrollView setContentSize:CGSizeMake(320, theSize.height+340)];
    
    
	

	className.text = yogaClass.name; //[classDict objectForKey:@"className"];
	[starView setRating:yogaClass.averageRating];
	classDuration.text = yogaClass.classDuration;
	commentCountLB.text = [NSString stringWithFormat:@"%i", [yogaClass.comments count], nil];
	[poundBtn setTitle:yogaClass.product.price forState:UIControlStateNormal];
	[yogaClass.product addObserver:self];
    observerRegistered = YES;
	
	
}
- (void)dealloc {
	[yogaClass release];
	
	
    [super dealloc];
}


#pragma mark -
#pragma mark UIScrollViewDelegate methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView {
	const CGFloat pageWidth = scrollView.frame.size.width;	
	const int num = aScrollView.contentOffset.x / pageWidth;
	if(num  != currentPageNumber) {
		self.currentPageNumber = num;
	}
    
    CGSize constraintSize;
    constraintSize.width = 290.0f;
    constraintSize.height = 1000.0f;
	
	if (num == 1) {
		descriptionTV.alpha=0;
        biogTextView.alpha=1;

        NSString *theText = biogTextView.text;
        CGSize theSize = [theText sizeWithFont:biogTextView.font constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
        [scrollView setContentSize:CGSizeMake(320, theSize.height+340)];
	}
	else {
		descriptionTV.alpha=1;
        biogTextView.alpha=0;
        
        NSString *theText = descriptionTV.text;
        CGSize theSize = [theText sizeWithFont:descriptionTV.font constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
        [scrollView setContentSize:CGSizeMake(320, theSize.height+340)];
	}
}


#pragma mark -
#pragma mark IB Action methods
- (IBAction)poundBtnTouch {
	buyNowBtn.alpha = 0;
	buyNowBtn.hidden = NO;
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.0];
	
	poundBtn.alpha = 0;
	buyNowBtn.alpha = 1;
	
	[UIView commitAnimations];
	
}
- (IBAction)buyNowBtnTouch {
	buyNowBtn.enabled = NO;
	
	//[[UAStoreFront shared] setDownloadDirectory:[yogaClass classVideoPath]];
    
    // commented out for UAFIX
	//yogaClass.product.#import "UA_ASIHTTPRequest.h" = [yogaClass classVideoPath];
	[[UAStoreFront shared].inventory purchase:yogaClass.product.productIdentifier];
	loadingView = [LoadingView loadingViewInView:self.view statusMessage:@"Connecting to store..."];
}
- (IBAction)showComment {
	ReadCommentsViewController *vc = [ReadCommentsViewController alloc];
	[vc initWithNibUsingClassName];
	vc.yogaClass = self.yogaClass;
	vc.writeCommentEnabled = NO;
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}

@end

//
//  MyClassViewController.m
//  Yoga
//
// Created by Rob on 10-3-22.

//

#import "MyClassViewController.h"
#import "ClassSectionViewController.h" 
#import "ClassTableViewHeader.h"
#import "YogaClass.h"
#import "RateCommentsHelper.h"
#define kTableMaxHeight 400
#define kTableCellHeight 65

@implementation MyClassViewController

@synthesize classifiedClasses;

#pragma mark -
#pragma mark Super class methods
- (void)loadData {
	[dataArray removeAllObjects];
	id<PLResultSet> result;
	result = [dbHandle() executeQuery: @"SELECT * FROM ClassCataloges order by ID"];
	while ([result next]) {
		NSString *name = [result stringForColumn:@"Name"];
		[classCatalogArray addObject:name];
	}
	[result close];
	
	for (int i = 0; i < classCatalogArray.count; i++) {
		NSMutableArray *subArray = [NSMutableArray array];
		result = [dbHandle() executeQuery: @"SELECT * FROM Classes where ClassCatalogID = ? and Installed = ? order by id desc", [NSString stringWithFormat:@"%i", i + 1], @"1"];
		
		while ([result next]) {
			YogaClass *aClass = [YogaClass classByLoadingDownloadedFromDatabaseWithClassID:[result stringForColumn:@"ClassID"]];
			if (aClass != nil) {
				[subArray addObject:aClass];
			}
		}
        
		[result close];
		if ([subArray count] > 0) {
			[self.classifiedClasses setObject:subArray forKey:[classCatalogArray objectAtIndex:i]];
		}
	}
}

- (void)showLoading {
	if (loadingView == nil) {
		loadingView = [LoadingView loadingViewInView:self.view statusMessage:@"Updating Comments and Rates..."];
	}
}

- (void)hideLoading {
    [loadingView removeView];
	loadingView = nil;
}

#pragma mark -
#pragma mark View controller methods
- (void)viewDidLoad {
	loadingView = nil;
	classCatalogArray = [NSMutableArray new];
	self.classifiedClasses = [NSMutableDictionary dictionary];
	
	[super viewDidLoad]; // Load data
	
	titleLB.text = @"My Classes";
	
	int cellCount = [self.classifiedClasses.allValues count];
	NSInteger headerHeight = 25 * [self.classifiedClasses.allKeys count];
	table.frameSizeHeight = MIN(kTableMaxHeight, 
								kTableCellHeight* cellCount * 4 + headerHeight);
	
	table.separatorStyle = UITableViewCellSeparatorStyleNone;
	
	/*
	if (!((AppDelegate*)[UIApplication sharedApplication].delegate).isLoadedCommentsRate ) {
		//[self showLoading];
		((AppDelegate*)[UIApplication sharedApplication].delegate).isLoadedCommentsRate = YES;
		[[RateCommentsHelper shared] updateRateCommentsFromServerForAllClasses];
		
		//[self hideLoading];
	}
	 */
}
- (void)viewWillAppear:(BOOL)animated {
	[table reloadData];
}
- (void)dealloc {
	[classCatalogArray release];
	[classifiedClasses release];
    [super dealloc];
}


#pragma mark -
#pragma mark --- UITableView delegate ---
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSMutableDictionary *dict = [[dataArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
	ClassSectionViewController *vc = [ClassSectionViewController alloc];
	//vc.classDict = dict;
	vc.yogaClass = [[self.classifiedClasses objectForKey:[self.classifiedClasses.allKeys objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
	[vc initWithNibUsingClassName];
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}


#pragma mark -
#pragma mark --- UITableView Datasource ---
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	//return (0 == [[self.classifiedClasses objectForKey:[self.classifiedClasses.allKeys objectAtIndex:section]] count]) ? 0 : 24;
	return 24;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return self.classifiedClasses.allKeys.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

	return [[self.classifiedClasses objectForKey:[self.classifiedClasses.allKeys objectAtIndex:section]] count];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	ClassTableViewHeader *header;
	NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ClassTableViewHeader" owner:nil options:nil];
	for (id oneObject in nib)
		if ([oneObject isKindOfClass:[ClassTableViewHeader class]]){
			header = (ClassTableViewHeader *)oneObject;
			header.textLB.text = [[classCatalogArray objectAtIndex:section] capitalizedString];
			header.textLBSmall.text = @"";
		}
	return header;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	MyClassTableCell *cell = (MyClassTableCell *)[tableView dequeueReusableCellWithIdentifier:@"MyClassTableCell"];
	if (nil == cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyClassTableCell" owner:self options:nil];
		for (id oneObject in nib)
			if ([oneObject isKindOfClass:[MyClassTableCell class]])
				cell = (MyClassTableCell *)oneObject;
	}	
	
	YogaClass *aClass = [[self.classifiedClasses objectForKey:[self.classifiedClasses.allKeys objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
	
	cell.classNameLB.text = [aClass name];
	cell.poseIV.image = [UIImage imageWithData:[aClass mainImage]];
	cell.durationLB.text = [aClass classDuration];
	cell.descriptionLB.text = [aClass shortDescription];
	[cell.starView setRating:[aClass averageRating]];
    
    [cell.poseIV layoutSubviews];
	
	NSLog(@"*** average rating = %i", [aClass averageRating]);
	
	
	if([aClass averageRating]>0){
	cell.starView.hidden=NO;
	}
	
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	return cell;
	
}


@end


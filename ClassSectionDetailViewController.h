//
//  ClassesDetailViewController.h
//  Yoga
//
// Created by Rob on 10-3-22.

//

#import <UIKit/UIKit.h>


@interface ClassSectionDetailViewController : CommonViewController {
	NSDictionary *sectionDict;

	IBOutlet UILabel *descriptionTV;
	IBOutlet UIImageView *poseIV;
	IBOutlet UILabel *durationLB;
	IBOutlet UIScrollView *wholeScrollView;
}
@property (nonatomic,retain) NSDictionary *sectionDict;

- (IBAction)playKrama;

@end

//
//  HelpViewController.h
//  Yoga
//
// Created by Rob on 10-3-19.

//

#import <UIKit/UIKit.h>
#import "ArticleDetailViewController.h"

@interface HelpViewController : CommonViewController {

}
- (IBAction)loadArticle:(id)sender;
@end

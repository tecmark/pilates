//
//  ThigsWeLikeCell.h
//  Yoga
//
//  Created by Aleksandr Kelbas on 15/05/2012.
//  Copyright (c) 2012 Tecmark LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThingsWeLikeCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIImageView *imageView;
@property (retain, nonatomic) IBOutlet UILabel *title;
@property (retain, nonatomic) IBOutlet UILabel *subtitle;

@end

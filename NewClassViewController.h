//
//  NewClassViewController.h
//  Yoga
//
// Created by Rob on 10-3-22.

//

#import <UIKit/UIKit.h>
#import "MyClassTableCell.h"


@class LoadingView;

@interface NewClassViewController : CommonTableViewController <UITableViewDelegate, UITableViewDataSource> {

	NSMutableArray *classCatalogArray;

	NSArray *newClassIDs;
	NSMutableArray *unpurchasedProducts;
	
	NSMutableDictionary *newClasses;
	
	LoadingView *loadingView;
	
	NSMutableArray	*newClassLoaders;
    
 
}

@property (nonatomic, retain) NSArray *newClassIDs;
@property (nonatomic, retain) NSMutableArray *unpurchasedProducts;
@property (nonatomic, retain) NSMutableDictionary *newClasses;
@property (nonatomic, retain) NSMutableArray *newClassLoaders;

@end

//
//  ConnectOffTheMat.m
//
// Created by Rob on 04/02/2011.
//  Copyright 2011 Bonbol Limited. All rights reserved.
//

#import "ConnectOffTheMat.h"

@implementation ConnectOffTheMat


- (void)viewDidLoad {
    [super viewDidLoad];
	titleLB.text = @"Off The Mat";
	
	[scrollView addSubview:content];
	scrollView.contentSize=content.frame.size;
}

- (IBAction)gotoOffTheMat {
	
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.offthematintotheworld.org"]];
}

@end

//
//  ProductDetailViewController.m
//  Yoga
//
// Created by Rob on 10-4-16.
//  
//

#import "ProductSubCategoryViewController.h"
#import "ProductDetailViewController.h"
#import "AddIssueTableCell.h"
#define kTableCellHeight 65
#define kTableMaxHeight 325

@implementation ProductSubCategoryViewController
@synthesize subCategoryDict;

- (void)loadData {
	id<PLResultSet> result;
	int subCategoryID = [[subCategoryDict objectForKey:@"subCategoryID"] intValue];
	result = [dbHandle() executeQuery: @"SELECT * FROM Products where CategoryID = ? and SubCategoryID = ? and CustomType = ?",
										stringFromInt(categoryID), stringFromInt(subCategoryID), stringFromInt(customType)];
	while ([result next]) {
		NSNumber *ID = [result objectForColumn:@"ID"];
		NSString *title = [result stringForColumn:@"Title"];
		NSString *description = [result stringForColumn:@"ShortDescription"];
		NSString *fullDescription = [result stringForColumn:@"FullDescription"];
		NSString *brand = [result stringForColumn:@"Brand"];
		NSString *URL = [result stringForColumn:@"URL"];
		NSString *picPath = [NSString stringWithFormat:@"product_%i.png", [ID intValue]];
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
							  ID, @"ID",
							  title, @"title",
							  picPath, @"picPath",
							  description, @"description",
							  fullDescription, @"fullDescription",
							  brand, @"brand",
							  URL, @"URL",
							  nil];
		[dataArray addObject:dict];
	}
	[result close];
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	titleLB.text = [subCategoryDict objectForKey:@"subCategoryName"];
	switch (self.customType) {
		case 1:
			womenBtn.selected = YES;
			break;
		case 2:
			womenBtn.selected = NO;
			menBtn.selected = YES;
			break;
		case 3:
			womenBtn.selected = NO;
			kidsBtn.selected = YES;
			break;
		default:
			break;
	}
	table.frameSizeHeight = MIN(kTableMaxHeight+12, kTableCellHeight*dataArray.count);
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

#pragma mark --- UITableView delegate ---
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *dict = [dataArray objectAtIndex:indexPath.row];
	ProductDetailViewController *vc = [ProductDetailViewController alloc];
	vc.productDict = dict;
	[vc initWithNibUsingClassName];
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}

#pragma mark --- UITableView Datasource ---
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return kTableCellHeight;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	AddIssueTableCell *cell = (AddIssueTableCell *)[tableView dequeueReusableCellWithIdentifier:@"AddIssueTableCell"];
	if (nil == cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddIssueTableCell" owner:nil options:nil];
		cell = (AddIssueTableCell *)[nib objectAtIndex:0];
	}
	
	NSDictionary *dict = [dataArray objectAtIndex:indexPath.row];
	
	NSString *name = [dict objectForKey:@"title"];
	NSString *description = [dict objectForKey:@"description"];
	NSString *picPath = [dict objectForKey:@"picPath"];
	UIImage *poseImg = [UIImage imageNamed:picPath];
	
	cell.poseIV.image = poseImg;
	cell.titleLB.text = name;
	cell.descLB.text = description;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
//	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	return cell;
}

- (IBAction)changeCustomType:(id)sender {
	[super changeCustomType:sender];
	[table reloadData];
}
@end

//
//  DownloadsViewController.m
//  Yoga
//
// Created by Rob on  11/10/10.
//  
//

#import "DownloadsViewController.h"
#import "ClassTableViewHeader.h"
#import "DownloadsTableCell.h"
#import "UAStoreFront.h"
#import "UAStoreKitObserver.h"
#import "UAProduct.h"
#import "YogaClass.h"


@implementation DownloadsViewController


@synthesize downloadingProducts, installedProducts;


#pragma mark -
#pragma mark View Controller methods
- (void)viewDidLoad {
    [super viewDidLoad];
	titleLB.text = @"Downloads";
    
    NSLog(@"Downloads vdl");
	
	self.downloadingProducts = [NSMutableArray array];
	self.installedProducts = [NSMutableArray array];
	
	inventory = [UAStoreFront shared].inventory;
	NSInteger productAmount = [inventory productsForType:ProductTypeAll].count;

	for (int i = 0; i < productAmount; i++) {
		UAProduct *product = [inventory productAtIndex:i];
		if (product.status == UAProductStatusDownloading) {
			[product addObserver:self];
			[self.downloadingProducts addObject:product];
			downloadingSize += product.fileSize;
		}
		else if (product.status == UAProductStatusInstalled && [YogaClass isInstalled:product.productIdentifier]) {
			[self.installedProducts addObject:product];
			downloadedSize += [YogaClass getFileSizeByClassID:product.productIdentifier];
		}
		else if (product.status == UAProductStatusPurchased) {
			[self.downloadingProducts addObject:product];
		}

	}
	
	if ([self.downloadingProducts count] > 0 || [self.installedProducts count] > 0) {
		[self.view addSubview:downloadsView];
	}
	else {
		[self.view addSubview:noDownloadsView];
	}

	noDownloadsView.frameOriginY = 44;
	downloadsView.frameOriginY = 40;
	table.frameOriginY = 0;
	table.frameSizeHeight = 424;
	table.allowsSelection = NO;
    
    
    NSLog(@"Downloads vdl Complete");
}
- (void)viewDidAppear:(BOOL)animated {
	//[[UAStoreFront shared].sfObserver restoreAll];
}

- (void)dealloc {
	for (UAProduct *product in self.downloadingProducts) {
		[product removeObserver:self];
	}
	RELEASE_SAFELY(downloadingProducts);
	RELEASE_SAFELY(installedProducts);
    [super dealloc];
}


- (void)reloadProductStatusWithProduct:(UAProduct*)product {
	[self.downloadingProducts removeObject:product];
	if (![self.installedProducts containsObject:product]) {
		[self.installedProducts addObject:product];
	}
	[table reloadData];
}
#pragma mark -
#pragma mark KVO UAProduct status observer
- (void)refreshCellWithProductStatus:(UAProductStatus)status classID:(NSString*)classID {
    UAProduct *product = [self.downloadingProducts objectAtIndex:0];
    
    switch (status) {
        case UAProductStatusUnpurchased:
			break;
        case UAProductStatusPurchased:
			break;
        case UAProductStatusInstalled:
             
			[YogaClass installedClassWithID:classID];
            [self reloadProductStatusWithProduct:product];
			NSLog(@"=== Class: %@ installed successfully ===", classID, nil);
			break;
        case UAProductStatusHasUpdate:
            break;
        case UAProductStatusDownloading:
			
            break;
        case UAProductStatusWaiting:
            break;
        default:
            break;
    }
}

/*
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object 
                        change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"status"]) {
		UAProduct *product = object;
        [self refreshCellWithProductStatus:product classID:product.productIdentifier];
    }
}*/

- (void)productProgressChanged:(NSNumber*)progress {
    NSLog(@"Dvc PRODUCT PROGRESS CHANGED");

    
}

- (void)productStatusChanged:(NSNumber*)status {
    NSLog(@"Dvc PRODUCT STATUS CHANGED");
   UAProduct *product = [self.downloadingProducts objectAtIndex:0];
     [self refreshCellWithProductStatus:[status intValue] classID:product.productIdentifier];
}



#pragma mark -
#pragma mark --- UITableView Datasource ---
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	if (section == 0) {
		if ([self.downloadingProducts count] > 0)
			return 24;
		else {
			return 0;
		}
	}
	else {
		if ([self.installedProducts count] > 0)
			return 24;
		else {
			return 0;
		}

	}
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 51;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		return [self.downloadingProducts count];
	}
	else {
		return [self.installedProducts count];
	}
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
	ClassTableViewHeader *header;
	NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ClassTableViewHeader" owner:nil options:nil];
	for (id oneObject in nib)
		if ([oneObject isKindOfClass:[ClassTableViewHeader class]]){
			header = (ClassTableViewHeader *)oneObject;
			if (section == 0) {
				header.textLB.text = @"Available for download";
				header.textLBSmall.text = fileSizeToString(downloadingSize);
			}
			else {
				header.textLB.text = @"Downloaded";
				header.textLBSmall.text = fileSizeToString(downloadedSize);
			}
		}
	return header;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	DownloadsTableCell *cell = (DownloadsTableCell *)[tableView dequeueReusableCellWithIdentifier:@"DownloadsTableCell"];
	if (nil == cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DownloadsTableCell" owner:self options:nil];
		for (id oneObject in nib)
			if ([oneObject isKindOfClass:[DownloadsTableCell class]])
				cell = (DownloadsTableCell *)oneObject;
	}
	
	if (indexPath.section == 0) {
		cell.product = [self.downloadingProducts objectAtIndex:indexPath.row];
	}
	else {
		cell.product = [self.installedProducts objectAtIndex:indexPath.row];
		cell.checkButton.selected = YES;
		cell.checkButton.enabled = NO;
	}

	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	return cell;
}

@end

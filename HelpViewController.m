//
//  HelpViewController.m
//  Yoga
//
// Created by Rob on 10-3-19.

//

#import "HelpViewController.h"


@implementation HelpViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	titleLB.text = @"My Yoga Practice";
}
- (IBAction)loadArticle:(id)sender{
	
	NSLog(@"Load article");
	
	ArticleDetailViewController *articleVC= [[ArticleDetailViewController alloc] initWithNibUsingClassName];
	
	//send tag
	//send title
	UIButton *thisButton = [[UIButton alloc] init];
	
	thisButton=sender;
	
	articleVC.articleName=[thisButton titleForState:UIControlStateNormal];
	articleVC.thisTag=[thisButton tag];
	
	[self.navigationController pushViewController:articleVC animated:YES];
	
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end

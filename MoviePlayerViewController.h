//
//  MoviePlayerViewController.h
//  Yoga
//
// Created by Rob on 10-4-19.
//  
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "YogaClass.h"

@interface MoviePlayerViewController : UIViewController {
	MPMoviePlayerController *moviePlayer;
	NSArray *movieArray;
    NSArray *repeatArray;
	IBOutlet UIView *overlayView;
	int currentMovieID;
	BOOL hasRotate;
	YogaClass *yogaClass;
	BOOL isPlayingIndividual;
	BOOL reviewLoad;
	
	NSString *classID;
	
	NSTimeInterval interval;
}

@property (readwrite, retain) MPMoviePlayerController *moviePlayer;
@property (nonatomic, retain) NSArray *movieArray;
@property (nonatomic, retain) NSArray *repeatArray;
@property (nonatomic, retain) NSString *classID;
@property (nonatomic, retain) YogaClass *yogaClass;
@property BOOL isPlayingIndividual;

- (void)playMovie:(NSString *)file;
- (IBAction)playAgain;
- (IBAction)home;
- (IBAction)rate;
- (IBAction)back;

@end

//
//  WriteCommentViewController.h
//  Yoga
//
// Created by Rob on  11/10/10.
//  
//

#import <UIKit/UIKit.h>
#import "LargeRatingView.h"
#import "YogaClass.h"


@interface WriteCommentViewController : CommonViewController <UITextViewDelegate> {
	IBOutlet UITextView *commentTextView;
	IBOutlet LargeRatingView *raringView;
	IBOutlet UIButton *doneEditButton;
	IBOutlet UITextField *titleField;
	
	YogaClass *yogaClass;
}

@property (nonatomic, assign) YogaClass *yogaClass;

- (IBAction)doneEdit;
- (IBAction)saveComment;

@end

//
//  UIImageView+SetNilImage.h
//  Yoga
//
//  Created by Aleksandr Kelbas on 15/05/2012.
//  Copyright (c) 2012 Tecmark LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (SetNilImage)

@end

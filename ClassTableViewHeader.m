//
//  ClassTableViewHeader.m
//  Yoga
//
// Created by Rob on  11/8/10.
//  
//

#import "ClassTableViewHeader.h"


@implementation ClassTableViewHeader

@synthesize textLB, textLBSmall;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        self.textLB.text = @"";
		self.textLBSmall.text = @"";
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (void)dealloc {
	[textLB release];
	[textLBSmall release];
    [super dealloc];
}


@end

//
//  BuyClassViewController.h
//  Yoga
//
// Created by Rob on 10-4-14.

//

#import <UIKit/UIKit.h>
#import "MyPageControl.h"
#import "RatingView.h"
#import "YogaClass.h"
#import "LoadingView.h"
#import "UAProduct.h"
#import "UAStoreFront.h"
#import "DownloadsViewController.h"

@interface BuyClassViewController : CommonViewController <UIScrollViewDelegate, UAProductObserverProtocol>{
    
    DownloadsViewController *downloadViewController;
    
	IBOutlet UIScrollView *imageScrollView;
    IBOutlet UIScrollView *scrollView;
	IBOutlet UITextView *descriptionTV;
	IBOutlet RatingView *starView;
	IBOutlet UILabel *className;
	IBOutlet UILabel *classDuration;
	IBOutlet UILabel *commentCountLB;
	IBOutlet MyPageControl	*pageControl;
	YogaClass *yogaClass;
	int currentPageNumber;
    
	
	IBOutlet UIView *biogView;
	IBOutlet UIImageView *biogImageView;
	IBOutlet UITextView *biogTextView;
	
	IBOutlet UIButton *poundBtn;
	IBOutlet UIButton *buyNowBtn;
	
	BOOL observerRegistered;
	LoadingView *loadingView;
}


@property (nonatomic, retain) YogaClass *yogaClass;
@property int currentPageNumber;

- (IBAction)poundBtnTouch;
- (IBAction)buyNowBtnTouch;
- (IBAction)showComment;


@end

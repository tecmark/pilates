//
//  EditClassViewController.h
//  Yoga
//
// Created by Rob on 10-4-14.

//

#import <UIKit/UIKit.h>
#import "PickerActionSheet.h"
#import "MySegmentControl.h"

@interface EditClassViewController : CommonTableViewController<UIActionSheetDelegate, UITextFieldDelegate, MySegmentControlDelegate> {
	IBOutlet UILabel *timeLB;
	IBOutlet UITextField *classNameTF;
	int tapedRowRepeatCount;
	UILabel *theRepeatLB;
	int selectedRow;
	int totalDuration;
	NSString *durationStr;
	NSString *className;
	NSMutableArray *sectionArray;
	int classID;
	MySegmentControl *selection;
	IBOutlet UIButton *topDoneBtn;
}


@property (nonatomic,retain) NSString *durationStr;
@property int totalDuration;
@property (nonatomic,retain) NSString *className;
@property (nonatomic,retain) NSMutableArray *sectionArray;
@property int classID;


- (IBAction)editDone;
- (IBAction)setRepeatCount:(id)sender;
- (IBAction)setSelected:(id)sender;
- (IBAction)titleInputEnd;


@end

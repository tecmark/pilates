//
//  ReadCommentsViewController.h
//  Yoga
//
// Created by Rob on  11/26/10.
//  
//

#import <UIKit/UIKit.h>
#import "YogaClass.h"

@interface ReadCommentsViewController : CommonViewController <UITableViewDelegate, UITableViewDataSource> {
	IBOutlet UIButton *writeCommentButton;
	IBOutlet UIButton *backBtn;
	
	YogaClass *yogaClass;
	
	BOOL writeCommentEnabled;
	
	BOOL isFromVideo;
	
	NSInteger totalReviews;
	
	UIFont *reviewFont;
}


@property (nonatomic, assign) YogaClass *yogaClass;
@property (nonatomic, assign) BOOL writeCommentEnabled;
@property (nonatomic, assign) BOOL isFromVideo;

- (IBAction)writeComment;
- (IBAction)goBackDynamic;
@end

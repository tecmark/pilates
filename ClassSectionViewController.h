//
//  ClassSectionViewController.h
//  Yoga
//
// Created by Rob on 10-3-22.

//

#import <UIKit/UIKit.h>
#import "MoviePlayerViewController.h"
#import "YogaClass.h"


@interface ClassSectionViewController : CommonTableViewController {
	//NSMutableDictionary *classDict;
	YogaClass *yogaClass;
	
	int totalDuration;
	IBOutlet UILabel *durationLB;
	MoviePlayerViewController *moviePlayer;
	NSString *className;
	NSMutableArray *allPoseArray;
   
}


//@property (nonatomic,retain) NSMutableDictionary *classDict;
@property (nonatomic,retain) YogaClass *yogaClass;
@property int totalDuration;
@property (nonatomic, assign) NSString *className;

- (IBAction)showClassInfo;
- (IBAction)playClassVedio;
- (IBAction)editClass;


@end

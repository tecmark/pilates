//
//  ClassSectionTableCell.m
//  Yoga
//
// Created by Rob on 10-4-13.

//

#import "ClassSectionTableCell.h"


@implementation ClassSectionTableCell
@synthesize durationLB;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:NO animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
	[durationLB release];
    [super dealloc];
}


@end

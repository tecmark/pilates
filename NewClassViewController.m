//
//  NewClassViewController.m
//  Yoga
//
// Created by Rob on 10-3-22.
//

#import "NewClassViewController.h"
#import "BuyClassViewController.h" 
#import "ClassTableViewHeader.h"
#import "UAInventory.h"
#import "UAProduct.h"
#import "UAStoreFront.h"
#import "ClassesServerHelper.h"
#import "LoadingView.h"
#import "NewClassLoader.h"
#import "NewClassLoadQuene.h"

#define kTableMaxHeight 420
#define kTableCellHeight 65



@implementation NewClassViewController


@synthesize newClassIDs, unpurchasedProducts, newClasses;
@synthesize newClassLoaders;

#pragma Mark -
#pragma Mark Private Methods
- (YogaClass*)v:(NSString*)classID {
    NSLog(@"Yoga Class");
	return [YogaClass classByLoadingFromDatabaseWithClassID:classID];
     NSLog(@"End Yoga Class");
}
- (void)loadNewClasses {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	UAInventory *inventory = [UAStoreFront shared].inventory;
	NSInteger productAmount = [inventory productsForType:ProductTypeAll].count;
	
	for (int i = 0; i < productAmount; i++) {
		UAProduct *product = [inventory productAtIndex:i];
		if (product.status == UAProductStatusUnpurchased) {
			[unpurchasedProducts addObject:product];
		}
	}
	
	[[NewClassLoadQuene shared] addObserver:self];
	
	for (int i = 0; i < [unpurchasedProducts count]; i++) {
		UAProduct *product = [unpurchasedProducts objectAtIndex:i];
		
        
        NSLog(@"load new classes loop");
		YogaClass *class = [YogaClass classByLoadingFromDatabaseWithClassID:product.productIdentifier];
		
        NSLog(@"load new classes class set");
		// There's no data for this class in database so laod it from server
		if (class == nil) {
            NSLog(@"class = nil");
			
			// --- Begsin ---
			// New code of using the operation quene
			NewClassLoader *classLoader = [[[NewClassLoader alloc] initWithProduct:product] autorelease];
			
			NewClassLoadQuene *loadQueue = [NewClassLoadQuene shared];
			//[classLoader registerObserver:loadQueue];
			[classLoader addObserver:self];
 			[loadQueue addOperation:classLoader];
			[self.newClassLoaders addObject:classLoader];
			continue;
			// --- End ---
			
			
			//class = [ClassesServerHelper getClassByID:product.productIdentifier];
			//[class saveToDatabase];
		}
        else{
            NSLog(@"class is not nil"); 
 
        }
		
		
		class.classSize = product.fileSize;
		class.product = product;
		 NSLog(@"1");
		NSMutableArray *classes = [self.newClasses objectForKey:class.classCategory];
		if (classes == nil) {
			classes = [NSMutableArray array];
            NSLog(@"4");
			[classes addObject:class];
		}
		else {
            NSLog(@"5");
			[classes addObject:class];
		}
        
        NSLog(@"output category");   
        
        NSLog(@"category=%@", class.classCategory);
        
        
		[self.newClasses setObject:classes forKey:class.classCategory];
        NSLog(@"3");
	}
	
	if ([[NewClassLoadQuene shared].operations count] == 0) {
		int cellCount = [self.newClasses.allValues count];
		NSInteger headerHeight = 25 * [self.newClasses.allKeys count];
		table.frameSizeHeight = MIN(kTableMaxHeight, kTableCellHeight* cellCount * 4 + headerHeight);
		
		[loadingView removeView];
		[table reloadData];
	}
	
	
	//int cellCount = [self.newClasses.allValues count];
	//NSInteger headerHeight = 25 * [self.newClasses.allKeys count];
	//table.frameSizeHeight = MIN(kTableMaxHeight, kTableCellHeight* cellCount * 4 + headerHeight);

	//[loadingView removeView];
	//[table reloadData];
	
    NSLog(@"end load new classes");
    
	[pool release];
}



#pragma mark -
#pragma mark View Controller methods
- (void)viewDidLoad {
	[super viewDidLoad];
	

	[[NewClassLoadQuene shared] setMaxConcurrentOperationCount:6];
	
	self.newClassLoaders = [NSMutableArray array];
	
	self.unpurchasedProducts = [NSMutableArray array];
	self.newClasses = [NSMutableDictionary dictionary];
	
	classCatalogArray = [NSMutableArray new];
	titleLB.text = @"New Classes";
	
	loadingView = [LoadingView loadingViewInView:self.view statusMessage:@"Downloading Classes"];
	
	[NSThread detachNewThreadSelector:@selector(loadNewClasses) toTarget:self withObject:nil];

}
- (void)dealloc {
	[[NewClassLoadQuene shared] removeObserver:self];
	
	for (NewClassLoader *aLoader in self.newClassLoaders) {
    [aLoader removeObserver:self];
	}
	[newClassLoaders release];
	[newClasses release];
	newClasses = nil;
	[unpurchasedProducts release];
	unpurchasedProducts = nil;
	[newClassIDs release];
	newClassIDs = nil;
	[classCatalogArray release];
	classCatalogArray = nil;
    [super dealloc];
}


#pragma mark -
#pragma mark --- UITableView delegate ---
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	BuyClassViewController *vc = [BuyClassViewController alloc];
	YogaClass *aClass = [[self.newClasses objectForKey:[self.newClasses.allKeys objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
	vc.yogaClass = aClass;
	[vc initWithNibUsingClassName];
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}
#pragma mark -
#pragma mark --- UITableView Datasource ---
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 24;//(0 == [[dataArray objectAtIndex:section] count]) ? 0 : 24;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return self.newClasses.allKeys.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [[self.newClasses objectForKey:[self.newClasses.allKeys objectAtIndex:section]] count];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	ClassTableViewHeader *header;
	NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ClassTableViewHeader" owner:nil options:nil];
	for (id oneObject in nib)
		if ([oneObject isKindOfClass:[ClassTableViewHeader class]]){
			header = (ClassTableViewHeader *)oneObject;
			header.textLB.text = [self.newClasses.allKeys objectAtIndex:section];
			
			NSArray *classes = [self.newClasses objectForKey:[self.newClasses.allKeys objectAtIndex:section]];
			double fileSize = 0;
			for (YogaClass *aClass in classes) {
				fileSize += aClass.classSize;
			}
			header.textLBSmall.text = @"";//fileSizeToString(fileSize);
		}
	
	return header;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	MyClassTableCell *cell = (MyClassTableCell *)[tableView dequeueReusableCellWithIdentifier:@"MyClassTableCell"];
	if (nil == cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyClassTableCell" owner:self options:nil];
		for (id oneObject in nib)
			if ([oneObject isKindOfClass:[MyClassTableCell class]])
				cell = (MyClassTableCell *)oneObject;
	}
	
	YogaClass *aClass = [[self.newClasses objectForKey:[self.newClasses.allKeys objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
	
	cell.classNameLB.text = [aClass name];
	cell.poseIV.image = [UIImage imageWithData:[aClass mainImage]];
	cell.durationLB.text = [aClass classDuration];
	cell.descriptionLB.text = [aClass shortDescription];
	[cell.starView setRating:[aClass averageRating]];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	return cell;
	
}

-(void)classesReady{
    
    NSLog(@"Classes ready");
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object 
                        change:(NSDictionary *)change context:(void *)context {
    
    NSLog(@"New Class View Controller Observer");
    if ([keyPath isEqualToString:@"allOperationsFinished"]) {
        NSLog(@"all ops");
        
		NewClassLoadQuene *quene = object;
        
		if (quene.allOperationsFinished) {
			NSLog(@"New Class: allOperationsFinished", nil);
            
      //      [quene removeObserver:self];
			int cellCount = [self.newClasses.allValues count];
			NSInteger headerHeight = 25 * [self.newClasses.allKeys count];
			table.frameSizeHeight = MIN(kTableMaxHeight, kTableCellHeight* cellCount * 4 + headerHeight);
			
			[loadingView removeView];
			loadingView = nil;
			[table reloadData];
            
		}
    }
	
	if ([keyPath isEqualToString:@"isFinished"]) {
        NSLog(@"isFinished");
        
       NewClassLoader *newClassLoader = object;
        [newClassLoader removeObserver:self];
	
        if (![newClassLoader isFinished]) {
              NSLog(@"isFinished set to NO");
			return;
		}
        
		YogaClass *class = newClassLoader.newClass;
		
        NSLog(@"New Class %@ Loaded", class.classID, nil);
		NSMutableArray *classes = [self.newClasses objectForKey:class.classCategory];
		if (classes == nil) {
			classes = [NSMutableArray array];
		}
			[classes addObject:class];
	
		[self.newClasses setObject:classes forKey:class.classCategory];
      
        
    }
}


- (IBAction)back {
	
	[super back];
}

@end

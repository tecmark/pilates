//
//  ThinsWeLikeViewController.m
//  Yoga
//
//  Created by Aleksandr Kelbas on 15/05/2012.
//  Copyright (c) 2012 Tecmark LTD. All rights reserved.
//

#import "ThinsWeLikeViewController.h"
#import "ThingsWeLikeCell.h"


@interface ThinsWeLikeViewController ()
@property (nonatomic, strong) NSArray *tableData;
@property (nonatomic, strong) NSString *tableToLoad;
@end

@implementation ThinsWeLikeViewController
@synthesize myTableView;
@synthesize tableData = _tableData;
@synthesize tableToLoad = _tableToLoad;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id) initWithDatabaseTable:(NSString*)tableName
{
    self = [super initWithNibName:NSStringFromClass([self class]) bundle:[NSBundle mainBundle]];
    if (self)
    {
        self.tableToLoad = tableName;
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.titleLB.text = self.title;
    _tableData = [[NSArray alloc] initWithArray:[DBHandler returnAllFromTable:self.tableToLoad]];
    
}

- (void)viewDidUnload
{
    [self setMyTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [myTableView release];
    [_tableToLoad release];
    [super dealloc];
}


#pragma mark - TableView

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 71;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableData count];
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdent = @"CellIdentifier";
    ThingsWeLikeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdent];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ThingsWeLikeCell" owner:self options:nil];
        for (id object in nib)
        {
            if ([object isKindOfClass:[ThingsWeLikeCell class]])
            {
                cell = object;
                break;
            }
        }
    }
    
    cell.title.text = [[self.tableData objectAtIndex:indexPath.row] objectForKey:@"display_text"];
    cell.subtitle.text = [[self.tableData objectAtIndex:indexPath.row] objectForKey:@"display_subtitle"];
    cell.imageView.image = [UIImage imageNamed:[[self.tableData objectAtIndex:indexPath.row] objectForKey:@"image"]];

    
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    NSDictionary *data = [self.tableData objectAtIndex:indexPath.row];
    NSString *link = [data objectForKey:@"link"];
    NSURL *url = [NSURL URLWithString:link];
    
    [[UIApplication sharedApplication] openURL:url];
    
}

@end

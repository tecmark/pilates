//
//  DownloadsTableCell.m
//  Yoga
//
// Created by Rob on  11/11/10.
//  
//

#import "DownloadsTableCell.h"
#import "UAGlobal.h"
#import "YogaClass.h"



@implementation DownloadsTableCell

@synthesize checkButton, classNameLabel, dateLabel, sizeLabel, downloadProgressView;
NSString *classFilesize;

- (UAProduct*)product {
	return product;
}
- (void)setProduct:(UAProduct*)_product {
	product = _product;
	YogaClass *classInfo = [YogaClass classByLoadingFromDatabaseWithClassIDFromBasicInfo:product.productIdentifier];
	
	self.classNameLabel.text = classInfo.name;
	
	NSDateFormatter *fommater = [[NSDateFormatter alloc] init];
	fommater.dateFormat = @"dddd, dd MMM";
	self.dateLabel.text = [fommater stringFromDate:[NSDate date]];
	[fommater release];
	
	self.sizeLabel.text = [classInfo fileSizeToString];
	
	classFilesize = [[NSString alloc] initWithString:[classInfo fileSizeToString]];
	
	if (product.status == UAProductStatusDownloading) {
		[self showDownloadProgress];
	}
	else {
		[self hideDownloadProgress];
	}

	[product addObserver:self];
}

/*

#pragma mark -
#pragma mark KVO UAProduct status observer
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object 
                        change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"progress"]) {
        downloadProgressView.progress = product.progress;
		float floatMb = [classFilesize floatValue];
		NSLog(@"cell filesize=%@", classFilesize);
		floatMb = floatMb*product.progress;
		sizeLabel.text=[NSString stringWithFormat:@"%.2f MB", floatMb];
    }
}
 */

#pragma mark -
#pragma mark UAProduct observer

- (void)productProgressChanged:(NSNumber*)progress {
    NSLog(@"PRODUCT PROGRESS CHANGED");
    downloadProgressView.progress = product.progress;
    float floatMb = [classFilesize floatValue];
    NSLog(@"cell filesize=%@", classFilesize);
    floatMb = floatMb*product.progress;
    sizeLabel.text=[NSString stringWithFormat:@"%.2f MB", floatMb];
    
    if(product.progress==1){
        NSLog(@"Download COMPLETED");

        
    }

}

- (void)productStatusChanged:(NSNumber*)status {
    NSLog(@"PRODUCT STATUS OBSERVED");
    
   // [self refreshCellWithProductStatus:[status intValue]];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}

- (void)dealloc {
	[product removeObserver:self];
	
	RELEASE_SAFELY(checkButton);
	RELEASE_SAFELY(classNameLabel);
	RELEASE_SAFELY(dateLabel);
	RELEASE_SAFELY(sizeLabel);
	RELEASE_SAFELY(downloadProgressView);
	[super dealloc];
}
- (IBAction)checkBoxCheckStateChanged {
	if (checkButton.selected) {
		checkButton.selected = NO;
		[self hideDownloadProgress];
	}
	else {
		checkButton.selected = YES;
		[self showDownloadProgress];
	}

}

- (void)showDownloadProgress {
	downloadProgressView.hidden = NO;
	downloadProgressView.alpha = 0;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.6];
	classNameLabel.frameOriginY = 5;
	dateLabel.frameOriginY = 6;
	sizeLabel.frameOriginY = 9;
	downloadProgressView.alpha = 1;
	[UIView commitAnimations];
}

- (void)hideDownloadProgress {
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.6];
	classNameLabel.frameOriginY = 13;
	dateLabel.frameOriginY = 15;
	sizeLabel.frameOriginY = 15;
	downloadProgressView.alpha = 0;
	[UIView commitAnimations];
}

@end

//
//  EditTableCell.h
//  Yoga
//
// Created by Rob on 10-4-14.

//

#import <UIKit/UIKit.h>
#import "EditTableCellUneditable.h"
@class EditTableCellUneditable;

@interface EditTableCell : EditTableCellUneditable {
	IBOutlet UIButton *selection;
	IBOutlet UILabel *repeatCountLB;
	IBOutlet UIButton *selectBtn;
}

@property (nonatomic,retain) UIButton *selection;
@property (nonatomic,retain) UILabel *repeatCountLB;
@property (nonatomic,retain) UIButton *selectBtn;

@end

//
//  TextTableCell.h
//  Yoga
//
// Created by Rob on 10-4-16.
//  
//

#import <UIKit/UIKit.h>


@interface TextTableCell : UITableViewCell {
	IBOutlet UILabel *textLB;
}
@property (nonatomic,retain) UILabel *textLB;
@end

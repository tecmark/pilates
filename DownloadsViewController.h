//
//  DownloadsViewController.h
//  Yoga
//
// Created by Rob on  11/10/10.
//  
//

#import <UIKit/UIKit.h>
#import "UAInventory.h"
#import "UAStoreFront.h"

@interface DownloadsViewController : CommonTableViewController <UITableViewDelegate, UITableViewDataSource, UAProductObserverProtocol> {
	IBOutlet UIView *noDownloadsView;
	IBOutlet UIView *downloadsView;
	
	UAInventory *inventory;
	
	NSMutableArray *downloadingProducts;
	NSMutableArray *installedProducts;
	
	
	double downloadingSize;
	double downloadedSize;
}


@property (nonatomic, retain) NSMutableArray *downloadingProducts;
@property (nonatomic, retain) NSMutableArray *installedProducts;


@end

//
//  BasicsViewController.h
//
// Created by Rob on 19/10/2010.
//  Copyright 2010 Bonbol Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "ArticleDetailViewController.h"
@interface BasicsViewController : CommonSearchTableViewController {

}
- (IBAction)loadArticle:(id)sender;
@end

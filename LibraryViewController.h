//
//  LibraryViewController.h
//  Yoga
//
// Created by Rob on 10-3-15.

//

#import <UIKit/UIKit.h>
#import "ArticleDetailViewController.h"


@interface LibraryViewController : CommonHomeViewController {


	IBOutlet UIView *s1;
	IBOutlet UIView *s2;
	IBOutlet UIView *s3;
	IBOutlet UIView *s4;
}

- (IBAction)back;
- (IBAction)loadArticle:(id)sender;
@end

//
//  EditTableCellUneditable.h
//  Yoga
//
// Created by Rob on 10-4-14.

//

#import <UIKit/UIKit.h>


@interface EditTableCellUneditable : UITableViewCell {
	IBOutlet UIImageView *poseIV;
	IBOutlet UILabel *durationLB;
	IBOutlet UILabel *titleLB;
}
@property (nonatomic,retain) UIImageView *poseIV;
@property (nonatomic,retain) UILabel *durationLB;
@property (nonatomic,retain) UILabel *titleLB;

@end

//
//  JournalHomeTableCell.h
//  Yoga
//
// Created by Rob on 10-3-17.

//

#import <Foundation/Foundation.h>


@interface JournalHomeTableCell : UITableViewCell {
	IBOutlet UILabel *titleLB;
	IBOutlet UIButton *selectBtn;
	IBOutlet UILabel *poseNameLB;

}

@property (nonatomic, retain) UILabel *titleLB;
@property (nonatomic, retain) UIButton *selectBtn;
@property (nonatomic, retain) UILabel *poseNameLB;
@end

//
//  ProductDetailViewController.m
//  Yoga
//
// Created by Rob on 10-4-16.
//  
//

#import "ProductDetailViewController.h"


@implementation ProductDetailViewController
@synthesize productDict;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	titleLB.text = [productDict objectForKey:@"title"];
	descriptionTV.text = [productDict objectForKey:@"fullDescription"];
	imageView.image = [UIImage imageNamed:[productDict objectForKey:@"picPath"]];
	[self.view insertSubview:imageView atIndex:1];
	brandLB.text = [productDict objectForKey:@"brand"];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

- (IBAction)addToWishlist {
	NSNumber *ID = [productDict objectForKey:@"ID"];
	[dbHandle() executeUpdate:@"update Products set InWishlist = 1 where ID = ?", [ID stringValue]];
}
- (IBAction)buyIt {
	NSURL *url = [NSURL URLWithString:[productDict objectForKey:@"URL"]];
	[[UIApplication sharedApplication] openURL:url];
}
@end

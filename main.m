//
//  main.m
//  Teen
//
// Created by Rob on 11/5/09.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal;
	
	@try {
		retVal = UIApplicationMain(argc, argv, nil, nil);
	}
	@catch (NSException * e) {
		NSLog(@"-------------------------AppLication exception:\n%@ %@", [e reason], [e description]);
	}
	
    [pool release];
    return retVal;
}

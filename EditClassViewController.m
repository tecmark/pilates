//
//  EditClassViewController.m
//  Yoga
//
// Created by Rob on 10-4-14.
//

#import "EditClassViewController.h"
#import "EditTableCell.h"
#import "EditTableCellUneditable.h"
#import "ClassSectionViewController.h"

@implementation EditClassViewController
@synthesize durationStr;
@synthesize totalDuration;
@synthesize className;
@synthesize sectionArray;
@synthesize classID;


NSString *previousName;
UIButton *btn;
BOOL repeatCountOpen;


- (void)setDurationLB {
	totalDuration = 0;
	for (NSDictionary *dict in dataArray) {
		int duration = [[dict objectForKey:@"duration"] intValue];
		int repeat = [[dict objectForKey:@"repeatCount"] intValue];
		BOOL selected = [[dict objectForKey:@"selected"] boolValue];
		if (selected) {
			totalDuration += duration * repeat;
		}
	}
    
	int hour = totalDuration/3600;
	int min = (totalDuration%3600) / 60;
	int sec = totalDuration%60;
	NSString *hourStr = (0 == hour) ? @"" : [NSString stringWithFormat:@"%i:", hour];
	NSString *minStr = [NSString stringWithFormat:@"%02i", min];
	timeLB.text = [NSString stringWithFormat:@"%@%@:%02i", hourStr, minStr, sec];
}

- (void)loadData {
	dataArray = sectionArray;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	timeLB.text = durationStr;
	table.frameOriginY += 62;
	table.frameSizeHeight=318;
	titleLB.text = [@"Edit " stringByAppendingString:className];
	classNameTF.text=className;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    [super dealloc];
}

#pragma mark --- UITableView Delegate ---
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
}
#pragma mark  --- UITableView Datasource ---
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *dict = [dataArray objectAtIndex:indexPath.row];
	NSString *name = [dict objectForKey:@"name"];
	NSString *picPath = [dict objectForKey:@"picPath"];
	
	NSString *newPicPath = pathForResource(picPath);
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:newPicPath]) {
		newPicPath = picPath;
	}
	
	UIImage *poseImg = [UIImage imageWithContentsOfFile:newPicPath];
	
	//UIImage *poseImg = [UIImage imageNamed:picPath];//[picPath stringByReplacingOccurrencesOfString:@".jpg" withString:@"_thumb.jpg"]];
	tapedRowRepeatCount = [[dict objectForKey:@"repeatCount"] intValue];
	BOOL poseSelected = [[dict objectForKey:@"selected"] boolValue];
	NSNumber *duration = [dict objectForKey:@"duration"];
	
	UITableViewCell *tableCell;

    // show 1st and last krama as uneditable 
    // for 1st two classes also show 2nd last krama (corpse pose) as uneditable
    
    NSLog(@"Class ID = '%i'", classID);
    
	if (0 == indexPath.row || dataArray.count - 1 == indexPath.row) {
		EditTableCellUneditable *cell = (EditTableCellUneditable *)[tableView dequeueReusableCellWithIdentifier:@"EditTableCellUneditable"];
		
        if (nil == cell) {
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EditTableCellUneditable" owner:nil options:nil];
			cell = (EditTableCellUneditable *)[nib objectAtIndex:0];
		}
		
		cell.poseIV.image = poseImg;
		cell.titleLB.text = name;
		
	
		int min = ([duration intValue]%3600) / 60;
		int sec = [duration intValue]%60;
		
		
		NSString *minStr = [NSString stringWithFormat:@"%i", min];
		
		cell.durationLB.text = [NSString stringWithFormat:@"%@:%02i", minStr, sec];
		//cell.durationLB.text = [NSString stringWithFormat:@"%i min", [duration intValue]/60];
		tableCell = cell;
	}
	else {
		EditTableCell *cell = (EditTableCell *)[tableView dequeueReusableCellWithIdentifier:@"EditTableCell"];
		if (nil == cell) {
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EditTableCell" owner:nil options:nil];
			for (id oneObject in nib)
				if ([oneObject isKindOfClass:[EditTableCell class]])
					cell = (EditTableCell *)oneObject;
		}
		cell.selectBtn.tag = indexPath.row;
		cell.selection.tag = indexPath.row;
		cell.selectBtn.selected = poseSelected;
		cell.repeatCountLB.text = [NSString stringWithFormat:@"x%i", tapedRowRepeatCount];
		[cell.selection addTarget:self action:@selector(setRepeatCount:) forControlEvents:UIControlEventTouchUpInside];
		[cell.selectBtn addTarget:self action:@selector(setSelected:) forControlEvents:UIControlEventTouchUpInside];
		cell.poseIV.image = poseImg;
		cell.titleLB.text = name;
		
		int min = ([duration intValue]%3600) / 60;
		int sec = [duration intValue]%60;
		
		NSString *minStr = [NSString stringWithFormat:@"%i", min];
		
		cell.durationLB.text = [NSString stringWithFormat:@"%@:%02i", minStr, sec];
		
		//cell.durationLB.text = [NSString stringWithFormat:@"%i min", [duration intValue]/60];
		tableCell = cell;
	}
	
	tableCell.selectionStyle = UITableViewCellSelectionStyleNone;
	return tableCell;
}

/*
- (IBAction)setRepeatCount:(id)sender {
	selectedRow = [sender tag];
	NSDictionary *dict = [dataArray objectAtIndex:selectedRow];
	tapedRowRepeatCount = [[dict objectForKey:@"repeatCount"] intValue];
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedRow inSection:0];
	EditTableCell *cell = (EditTableCell *)[table cellForRowAtIndexPath:indexPath];
	theRepeatLB = cell.repeatCountLB;
	PickerActionSheet *sheet = [[PickerActionSheet alloc] initWithTitle:@"Please set the repeat count" 
															   delegate:self 
													  cancelButtonTitle:@"Cancel" 
												 destructiveButtonTitle:@"Ok" 
													  otherButtonTitles:nil];
	[sheet showInView:self.view];
	[sheet release];
}
*/

- (void)popupCancelled{
	
	[selection removeFromSuperview];
	[btn removeFromSuperview];
	repeatCountOpen=NO;
	
}


- (IBAction)setRepeatCount:(id)sender {
	if(!repeatCountOpen) {
		repeatCountOpen=YES;
		selectedRow = [sender tag];
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MySegmentControl" owner:nil options:nil];
		selection = [nib objectAtIndex:0];
	//	selection.frameOrigin = CGPointMake(50, 54 + selectedRow*65);
        
        
        CGPoint tableOffSet = table.contentOffset;
		float offSetY = tableOffSet.y;
        
        float yPosition = 158 + selectedRow*65 - offSetY;
        
        if (yPosition < 380) {
            selection.frameOrigin = CGPointMake(73, yPosition);
        }
        else {
            selection.background.image = [UIImage imageNamed:@"mysegmentcontrolRotated.png"];
            selection.frameOrigin = CGPointMake(73, yPosition - 90);
        }
        
        
        
		selection.delegate = self;
		
		//add mask
		btn = [UIButton buttonWithType:UIButtonTypeCustom];
		[btn setImage:[UIImage imageNamed:@"editMask1.png"] forState:UIControlStateNormal];
		btn.frame = CGRectMake(0, 0, 320, 460);
        [btn setAdjustsImageWhenHighlighted:NO];
		[btn addTarget:self action:@selector(popupCancelled) forControlEvents:UIControlEventTouchUpInside];
		btn.alpha=0.6;
        
		[self.view addSubview:btn];
		
		//ad popup
		
        
        
		[self.view addSubview:selection];
	//	[self.view bringSubviewToFront:table];
		
	}
	
}
#pragma mark --- MySegmentControl Delegate ---
- (void)selectedNumber:(int)num {
    
    NSLog(@"repeat count seelcted");
	repeatCountOpen=NO;
	[btn removeFromSuperview];
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedRow inSection:0];
	EditTableCell *cell = (EditTableCell *)[table cellForRowAtIndexPath:indexPath];
	cell.repeatCountLB.text = [NSString stringWithFormat:@"x%i", num];
	NSMutableDictionary *dict = [dataArray objectAtIndex:selectedRow];
	NSNumber *ID = [dict objectForKey:@"ID"];
	[dict setObject:[NSNumber numberWithInt:num] forKey:@"repeatCount"];
	[dbHandle() executeUpdate:@"update ClassSections set RepeatCount = ? where ID = ?", stringFromInt(num), [ID stringValue]];
	[self setDurationLB];
    
    [selection removeFromSuperview];
}

- (IBAction)setSelected:(id)sender {
	
	if(repeatCountOpen){
		[self popupCancelled];
	}
	selectedRow = [sender tag];
	NSMutableDictionary *dict = [dataArray objectAtIndex:selectedRow];
	NSNumber *ID = [dict objectForKey:@"ID"];
	
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedRow inSection:0];
	EditTableCell *cell = (EditTableCell *)[table cellForRowAtIndexPath:indexPath];
	if (cell.selectBtn.selected) {
		cell.selectBtn.selected = NO;
		[dict setValue:[NSNumber numberWithBool:NO] forKey:@"selected"];
		[dbHandle() executeUpdate:@"update ClassSections set Selected = 0 where ID = ?", [ID stringValue]];
	}
	else {
		cell.selectBtn.selected = YES;
		[dict setValue:[NSNumber numberWithBool:YES] forKey:@"selected"];
		[dbHandle() executeUpdate:@"update ClassSections set Selected = 1 where ID = ?", [ID stringValue]];
	}
	[self setDurationLB];
}

- (IBAction)editDone {
	NSArray *vcs = self.navigationController.viewControllers;
	ClassSectionViewController *vc = (ClassSectionViewController *)[vcs objectAtIndex:vcs.count - 2];
//	vc.durationLB.text = timeLB.text;
	vc.totalDuration = self.totalDuration;
	[self.navigationController popViewControllerAnimated:NO];
}
//override

- (IBAction)back {
	NSArray *vcs = self.navigationController.viewControllers;
	UIViewController *vc = [vcs objectAtIndex:vcs.count - 3];
	[self.navigationController popToViewController:vc animated:YES];
}

/*
#pragma mark --- ActionSheet Delegate ---
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	// the user clicked the OK button
	if (buttonIndex == 0)
	{
		theRepeatLB.text = [NSString stringWithFormat:@"x%i", tapedRowRepeatCount];
		[self saveRepeatCount];
		[self setDurationLB];
		
	}
}
*/
#pragma mark --- Picker dataSource ---
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	return 5;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	//	return (0 == component) ? stringFromInt(row + 1) : stringFromInt(row);
	return stringFromInt(row + 1); // the minute is 1 - 5 
}

#pragma mark --- picker delegate ---
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	tapedRowRepeatCount = row + 1;
}

#pragma mark --- UITextFieldDelegate --- 
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	previousName = [[NSString alloc] initWithString:textField.text];
	topDoneBtn.enabled = NO;
	textField.text = @"";
	return YES;
}
- (IBAction)titleInputEnd {
	topDoneBtn.enabled = YES;
	[classNameTF resignFirstResponder];
	
	if([classNameTF.text length]>0){
		titleLB.text = [@"Edit " stringByAppendingString:classNameTF.text];
	[dbHandle() executeUpdate:@"update Classes set Name = ? where ID = ?", classNameTF.text, stringFromInt(classID)];
	ClassSectionViewController *vc = (ClassSectionViewController *)[self viewControllerWithGoBackCount:1];
	vc.className = classNameTF.text;
	}
	else{
		classNameTF.text=previousName;	
	}
}

@end

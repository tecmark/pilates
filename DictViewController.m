//
//  DictViewController.m
//  Yoga
//
// Created by Rob on 10-3-19.

//

#import "DictViewController.h"

@implementation DictViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	titleLB.text = @"History & Philosophy";
}
- (IBAction)loadArticle:(UIButton*)sender{
	
	NSLog(@"Load article");
    
    NSString *fileName = @"";
    NSString *title = @"";
    
    switch (sender.tag) {
        case 4:
        {
            fileName = @"History";
            title = @"History";
        }
            break;
        case 15:
        {
            fileName = @"Principles";
            title = @"Principles";
        }
            break;
        case 5:
        {
            fileName = @"Benefits";
            title = @"Benefits";
        }
            break;
            
        default:
            break;
    }
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"htm"];
	
	ArticleDetailViewController *articleVC= [[ArticleDetailViewController alloc] initWithWebPage:filePath andTitle:title];
	
	
	[self.navigationController pushViewController:articleVC animated:YES];
	
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}




@end

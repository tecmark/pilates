//
//  ArticleDetailViewController.h
//
// Created by Rob on 20/10/2010.
//  Copyright 2010 Bonbol Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface ArticleDetailViewController : CommonViewController {

	IBOutlet UIView *view1;
	IBOutlet UIView *view2;
	IBOutlet UIView *view3;
	IBOutlet UIView *view4;
	IBOutlet UIView *view5;
	IBOutlet UIView *view6;
	IBOutlet UIView *view7;
	IBOutlet UIView *view8;
	IBOutlet UIView *view9;
	IBOutlet UIView *view10;
	IBOutlet UIView *view11;
	IBOutlet UIView *view12;
	IBOutlet UIView *view13;
	IBOutlet UIView *view14;
	IBOutlet UIView *view15;
	IBOutlet UIView *view16;
	IBOutlet UIView *view17;
	
	
	IBOutlet UIScrollView *scrollView;
	IBOutlet UILabel *thisTitleLB;
	
	int thisTag;
	NSString *articleName;
	
	IBOutlet UIWebView *webView;
}
@property (nonatomic, retain) UIWebView *myWebView;
@property (retain, nonatomic) IBOutlet UIView *principlesView;
@property (nonatomic,retain) NSString *articleName;
@property (nonatomic) BOOL hasWebContent;
@property (nonatomic, strong) NSString *titleStr;
@property (nonatomic, strong) NSString *webFilePath;
@property int thisTag;
- (id) initWithWebPage:(NSString*)pagePath andTitle:(NSString*)title;
- (IBAction)back;
@end

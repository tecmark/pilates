//
//  IssueDetailViewController.h
//  Yoga
//
// Created by Rob on 10-3-18.

//

#import <UIKit/UIKit.h>
#import "MyAlertView.h"

@interface IssueDetailViewController : CommonViewController <MyAlertViewDelegate> {
	NSDictionary *issueDict;
	IBOutlet UITextView *contentTV;
	IBOutlet UILabel *poseNameLB;
	IBOutlet UILabel *updateTimeLB;
	IBOutlet UIButton *selectBtn;
	IBOutlet UIButton *deleteBtn;
	IBOutlet UIImageView *notePage;
//	IBOutlet UIButton *addBtn;
	IBOutlet UIButton *doneBtn;
	int issueRowNumber;
	BOOL isIssueResolved;
	BOOL isAddIssue;
	BOOL isFromAsana;
	IBOutlet UIButton *gotoPoseBtn;
}

@property (nonatomic,retain) NSDictionary *issueDict;
@property int issueRowNumber;
@property BOOL isIssueResolved;
@property BOOL isAddIssue;
@property BOOL isFromAsana;
@property CGFloat textViewAmountMoved;

- (IBAction)addIssue;
- (IBAction)editIssue;
- (IBAction)setResolved;
- (IBAction)deleteIssue;
- (IBAction)addIssueDone;
- (IBAction)gotoPose;

@end

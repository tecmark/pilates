//
//  AddIssueTableCell.m
//  Yoga
//
// Created by Rob on 10-3-17.

//

#import "AddIssueTableCell.h"


@implementation AddIssueTableCell
@synthesize poseIV;
@synthesize titleLB;
@synthesize subTitleLB;
@synthesize descLB;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:NO animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
	[poseIV release];
	[titleLB release];
	[subTitleLB release];
	[descLB release];
	
    [super dealloc];
}


@end

//
//  ClassSectionTableCell.h
//  Yoga
//
// Created by Rob on 10-4-13.

//

#import <UIKit/UIKit.h>
#import "AddIssueTableCell.h"

@interface ClassSectionTableCell : AddIssueTableCell {
	IBOutlet UILabel *durationLB;
}

@property (nonatomic, retain) UILabel *durationLB;

@end

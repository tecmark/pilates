//
//  ArticleDetailViewController.m
//
// Created by Rob on 20/10/2010.
//  Copyright 2010 Bonbol Limited. All rights reserved.
//

#import "ArticleDetailViewController.h"
static NSString *titles[] = {
	//TODO add the link
	@"",
	@"What is Yoga?",
	@"Benefits of Yoga",
	@"Common Styles of Yoga",
	@"A Brief History of Yoga",
	@"The Tree of Yoga",
	@"The Yoga Toolkit",
	@"Types of Poses",
	@"Sequencing Principles",
	@"Order of Poses",
	@"Practice Tips",
	@"Energy Locks (Bandha)",
	@"Simple Mudras (Gestures)",
	@"Breathing Exercises",
	@"Glossary",
    @"Principles"
};
@implementation ArticleDetailViewController
@synthesize principlesView;
@synthesize articleName, thisTag;
@synthesize myWebView = _myWebView;
@synthesize hasWebContent = _hasWebContent;
@synthesize titleStr = _titleStr;
@synthesize webFilePath = _webFilePath;

-(void)viewDidLoad{
	
	
	//showAlertViewWithMessage([NSString stringWithFormat:@"tag=%i", thisTag]);
	//				showAlertViewWithMessage([NSString stringWithFormat:@"title=%@", titles[thisTag]])
	[super viewDidLoad];
    
    if (![self hasWebContent])
    {
        titleLB.text = titles[thisTag];
        
        
        
        if(thisTag==1){
            [scrollView addSubview:view1];
            scrollView.contentSize=view1.frame.size;
        }
        else if(thisTag==2){
            [scrollView addSubview:view2];
            scrollView.contentSize=view2.frame.size;
        }
        else if(thisTag==3){
            [scrollView addSubview:view3];
            scrollView.contentSize=view3.frame.size;
        }
        else if(thisTag==4){
            [scrollView addSubview:view4];
            scrollView.contentSize=view4.frame.size;
        }
        else if(thisTag==5){
            [scrollView addSubview:view5];
            scrollView.contentSize=view5.frame.size;
        }
        else if(thisTag==6){
            [scrollView addSubview:view6];
            scrollView.contentSize=view6.frame.size;
        }
        else if(thisTag==7){
            [scrollView addSubview:view7];
            scrollView.contentSize=view7.frame.size;
        }
        else if(thisTag==8){
            [scrollView addSubview:view8];
            scrollView.contentSize=view8.frame.size;
        }
        else if(thisTag==9){
            [scrollView addSubview:view9];
            scrollView.contentSize=view9.frame.size;
        }
        else if(thisTag==10){
            [scrollView addSubview:view10];
            scrollView.contentSize=view10.frame.size;
        }
        else if(thisTag==11){
            [scrollView addSubview:view11];
            scrollView.contentSize=view11.frame.size;
        }
        else if(thisTag==12){
            [scrollView addSubview:view12];
            scrollView.contentSize=view12.frame.size;
        }
        else if(thisTag==13){
            [scrollView addSubview:view13];
            scrollView.contentSize=view13.frame.size;
        }
        else if(thisTag==14){
            [scrollView addSubview:view14];
            scrollView.contentSize=view14.frame.size;
        }
        else if (thisTag == 15)
        {
            [scrollView addSubview:principlesView];
            scrollView.contentSize = principlesView.frame.size;
        }
    }
    else {
        
        titleLB.text = _titleStr;
        [scrollView removeFromSuperview];
        [self.view addSubview:self.myWebView];
        [self.myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:_webFilePath]]];
        for (UIScrollView *webScrollView in self.myWebView.subviews)
        {
            if ([webScrollView isKindOfClass:[UIScrollView class]])
            {
                [webScrollView setBounces:NO];
                break;
            }
        }
        
    }
}


- (id) initWithWebPage:(NSString*)pagePath andTitle:(NSString*)title
{
    self = [super initWithNibUsingClassName];
    if (self)
    {
        [self setHasWebContent:YES];
        _titleStr = title;
        _webFilePath = pagePath;
        self.myWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 40, 320, 420)];
        [self.myWebView setBackgroundColor:[UIColor clearColor]];
        [self.myWebView setOpaque:NO];
        
    }
    
    
    
    return self;
}


- (IBAction)back {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [self.myWebView setDelegate:nil];
}

- (void)dealloc {
    [principlesView release];
    if (self.myWebView)
        [self.myWebView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setPrinciplesView:nil];
    [super viewDidUnload];
}
@end

//
//  ClassInfoViewController.h
//  Yoga
//
// Created by Rob on 10-4-14.

//

#import <UIKit/UIKit.h>
#import "MyPageControl.h"
#import "RatingView.h"
#import "YogaClass.h"

@interface ClassInfoViewController : CommonViewController <UIScrollViewDelegate> {
	IBOutlet UIScrollView *imageScrollView;
    IBOutlet UIScrollView *scrollView;
	IBOutlet UITextView *descriptionTV;
    IBOutlet UITextView *biogTV;
	IBOutlet RatingView *starView;
	IBOutlet UILabel *className;
	IBOutlet UILabel *classDuration;
	IBOutlet UILabel *commentCountLB;
	IBOutlet MyPageControl	*pageControl;
    IBOutlet UIButton *rateAndComment;
	
	
	NSMutableDictionary *classDict;
	YogaClass *yogaClass;
	int currentPageNumber;
	
	IBOutlet UIView *biogView;
	IBOutlet UIImageView *biogImageView;
	//IBOutlet UITextView *biogTextView;
}


@property (nonatomic, retain) NSMutableDictionary *classDict;
@property (nonatomic, retain) YogaClass *yogaClass;
@property int currentPageNumber;


- (IBAction)AddComment;
- (IBAction)showComment;

@end

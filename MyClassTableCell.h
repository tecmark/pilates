//
//  MyClassTableCell.h
//  Yoga
//
// Created by Rob on 10-3-22.

//

#import <UIKit/UIKit.h>
#import "RatingView.h"

@interface MyClassTableCell : UITableViewCell {
	IBOutlet UILabel *classNameLB;
	IBOutlet UILabel *durationLB;
	IBOutlet UILabel *descriptionLB;
	IBOutlet UIImageView *poseIV;
	IBOutlet RatingView *starView;
}

@property (nonatomic, retain) UILabel *classNameLB;
@property (nonatomic, retain) UILabel *durationLB;
@property (nonatomic, retain) UILabel *descriptionLB;
@property (nonatomic, retain) UIImageView *poseIV;
@property (nonatomic, retain) RatingView *starView;

- (void)setRating:(NSInteger)rating;

@end

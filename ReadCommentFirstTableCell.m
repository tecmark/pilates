//
//  ReadCommentFirstTableCell.m
//  Yoga
//
// Created by Rob on  11/26/10.
//  
//

#import "ReadCommentFirstTableCell.h"


@implementation ReadCommentFirstTableCell


@synthesize titleL, reviewerAndDateL, reatingV;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        backColorView.backgroundColor = [UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:209.0/255.0 alpha:1];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	
	if (self != nil) {
		backColorView.backgroundColor = [UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:209.0/255.0 alpha:1];
	}
	
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];
}


- (void)dealloc {
    [super dealloc];
}

@end

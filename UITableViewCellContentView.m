//
//  UITableViewCellContentView.m
//  Yoga
//
//  Created by David on 10-3-17.
//  Copyright 2010 David. All rights reserved.
//

#import "UITableViewCellContentView.h"


@implementation UITableViewCellContentView


+ (id)alloc {
    return [UIView alloc];
}

+ (id)allocWithZone:(NSZone *)zone {
    return [UIView allocWithZone:zone];
}

- (void)dealloc {
    [super dealloc];
}


@end

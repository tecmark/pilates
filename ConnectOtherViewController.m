//
//  ConnectOtherViewController.m
//
// Created by Rob on 29/12/2010.
//  Copyright 2010 Bonbol Limited. All rights reserved.
//

#import "ConnectOtherViewController.h"


static NSString *links[] = {
	//TODO add the link
	@"http://www.bhaktifest.com",//bf
	@"http://www.thethirdspace.com"//tts
};


@implementation ConnectOtherViewController


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	titleLB.text = @"Other Good Things";
}

- (IBAction)gotoLink:(id)sender {
	
//		showAlertViewWithMessage(@"Bonbol note: Please provide a link for this affiliate");
	
	int num = [sender tag];
	
	if(num<10){
		NSLog(@"Goto link %i", num);
		NSURL *URL = [NSURL URLWithString:links[num]];
		[[UIApplication sharedApplication] openURL:URL];
	}
	else{
		
		
	}
	
}

@end

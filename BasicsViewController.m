//
//  BasicsViewController.m
//
// Created by Rob on 19/10/2010.
//  Copyright 2010 Bonbol Limited. All rights reserved.
//

#import "BasicsViewController.h"

@implementation BasicsViewController


/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	titleLB.text = @"Glossary & Articles";
}
- (IBAction)loadArticle:(id)sender{
	
	NSLog(@"Load article");
        
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Definitions and concepts" ofType:@"htm"];
    NSString *title = @"Definitions & Concepts";
    
	ArticleDetailViewController *articleVC= [[ArticleDetailViewController alloc] initWithWebPage:filePath andTitle:title];
	

	[self.navigationController pushViewController:articleVC animated:YES];
	
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}





@end

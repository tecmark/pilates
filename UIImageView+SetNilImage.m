//
//  UIImageView+SetNilImage.m
//  Yoga
//
//  Created by Aleksandr Kelbas on 15/05/2012.
//  Copyright (c) 2012 Tecmark LTD. All rights reserved.
//

#import "UIImageView+SetNilImage.h"

@implementation UIImageView (SetNilImage)


- (void) layoutSubviews
{
    if (self.image == nil)
    {
        [self setImage:[UIImage imageNamed:@"no_image.jpeg"]];
    }
}



@end


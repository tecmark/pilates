//
//  AsanaAddPoseTableCell.h
//  Yoga
//
// Created by Rob on 10-3-29.
//  
//

#import <UIKit/UIKit.h>
#import "AddIssueTableCell.h"

@interface AsanaAddPoseTableCell : AddIssueTableCell {
	IBOutlet UIButton *selectBtn;

}
@property (nonatomic, retain) UIButton *selectBtn;
@end

//
//  InfoHomeViewController.h
//  Yoga
//
// Created by Rob on 10-4-22.
//  
//

#import <UIKit/UIKit.h>


@interface InfoHomeViewController : CommonHomeViewController {
	IBOutlet UILabel *copyrightLB;

}

- (IBAction)gotoLink:(id)sender;
- (IBAction)back;
- (IBAction)sendEmail;
@end

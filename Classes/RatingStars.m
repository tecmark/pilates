//
//  RatingStars.m
//  Yoga
//
// Created by Rob on  11/9/10.
//  
//

#import "RatingStars.h"


@implementation RatingStars

- (void)addStarsImageView {
	self.backgroundColor = [UIColor clearColor];
	
	star1 = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ReadCommentStar1.png"]] autorelease];
	[self addSubview:star1];
	
	star5 = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ReadCommentStar5.png"]] autorelease];

	star5.clipsToBounds = YES;
	star5.contentMode = UIViewContentModeTopLeft;
	[self addSubview:star5];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	
	if (self != nil) {
		self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 105, 16);
		[self addStarsImageView];
	}
	
	return self;
}
- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
		[self addStarsImageView];
    }
    return self;
}

- (void)dealloc {
    [super dealloc];
}

- (void)setRating:(NSInteger)rating {
	if (rating < 0 || rating > 5 || rating == 1) {
		star5.hidden = YES;
	}
	else {
		NSInteger star5Width = rating * 21;
		star5.frame = CGRectMake(star5.frame.origin.x, star5.frame.origin.y, star5Width, 16);
	}
}



@end

//
//  MyPickerView.m
//  Yoga
//
// Created by Rob on 10-4-22.
//  
//

#import "MyPickerView.h"


@implementation MyPickerView
@synthesize pickerDelegate;
@synthesize picker;

- (void)setPickerDelegate:(id)theDelegate {
	picker.delegate = theDelegate;
	picker.dataSource = theDelegate;
	pickerDelegate = theDelegate;
}
- (void)awakeFromNib{
	
}

- (void)dealloc {
    [super dealloc];
}

- (IBAction)done {
	[pickerDelegate pickerDone];
	[self moveOutFrom:kBottom duration:0.5];
}

- (void)resignFirstResponder; {
	[self moveOutFrom:kBottom duration:0.5];
}

@end

//
//  SoundManager.m
//  
//

#import "SoundManager.h"
#import "Sound.h"

static NSArray *soundArray = nil;
static BOOL isMute = NO;

void SGInitSounds(void) {
	NSMutableArray *a = [[NSMutableArray alloc] init];
	Sound *s;
	
	s = [[Sound alloc] initWithResourceFile:@"oneClick.wav"];
	[a addObject:s];
	[s release];
	
	soundArray = a;
}

void SGPlaySound(int soundNum) {
	if(!isMute){
		if(nil == soundArray) {
			SGInitSounds();	
		}	
		
		Sound *sound = [soundArray objectAtIndex:soundNum];
		[sound playRepeat:0];
	}
}

void SGReleaseSounds(void) {
	[soundArray release];
	soundArray = nil;
}

BOOL checkMute(void){
	return isMute;
}
void setMute(BOOL mute){
	isMute = mute;
}

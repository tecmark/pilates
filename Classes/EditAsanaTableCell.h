//
//  EditAsanaTableCell.h
//  Yoga
//
// Created by Rob on 10-4-20.
//  
//

#import <UIKit/UIKit.h>
#import "EditTableCellUneditable.h"

@interface EditAsanaTableCell : EditTableCellUneditable {
	IBOutlet UIButton *selection;
	IBOutlet UILabel *repeatCountLB;
}
@property (nonatomic,retain) UIButton *selection;
@property (nonatomic,retain) UILabel *repeatCountLB;

@end

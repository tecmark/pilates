//
//  Common.m
//  
//
// Created by Rob on 11/5/09.
//  
//

#import "Common.h"

NSString * pathForResource(NSString *file) {
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *resourcePath = [mainBundle resourcePath];
	NSString *filePath = [resourcePath stringByAppendingFormat:@"/%@",file];
	return filePath;
}

void showAlertViewWithMessage (NSString *message ) {
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil 
														message:message 
													   delegate:nil cancelButtonTitle:@"OK" 
											  otherButtonTitles:nil];
	[alertView show];
	[alertView release];
}


NSString * pathOfWritableFile(NSString *file) {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDirectory = [paths objectAtIndex:0];
	
    NSString *filePath = [docDirectory stringByAppendingPathComponent:file];
	return filePath;
}

NSString * dataFilePath() {
    NSString *filePath = pathOfWritableFile(kUserDataFile);
	NSFileManager *fileManager = [NSFileManager defaultManager];
	BOOL isFileExist = [fileManager fileExistsAtPath:filePath];
	if (!isFileExist) {
		NSError *error;
		BOOL success = [fileManager copyItemAtPath:pathForResource(kUserDataFile) toPath:filePath error:&error];
		if (!success) {
			NSString *err = [NSString stringWithFormat:
							 @"Failed to create writable plist file with message '%@'.",
							 [error localizedDescription]];
			[NSException raise:@"Plist create Exception" format:err, nil];
		}
	}
	return filePath;
}

NSString * dataBasePath() {
	NSString *filePath = pathOfWritableFile(kDataBaseFile);
	NSFileManager *fileManager = [NSFileManager defaultManager];
	BOOL isFileExist = [fileManager fileExistsAtPath:filePath];
	if (!isFileExist) {
		NSError *error;
		BOOL success = [fileManager copyItemAtPath:pathForResource(kDataBaseFile) toPath:filePath error:&error];
		if (!success) {
			NSString *err = [NSString stringWithFormat:
							 @"Failed to create writable database file with message '%@'.",
							 [error localizedDescription]];
			[NSException raise:@"Database create Exception" format:err, nil];
		}
	}
	return filePath;
}

NSMutableDictionary* dataDict() {
	NSString *path = dataFilePath();
	NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:path];
	if (nil == dict) {
		dict = [NSMutableDictionary dictionary];
	}
	return dict;
}
BOOL isLandscapeRight() {
	NSMutableDictionary *dict = dataDict();
	BOOL ret = [[dict objectForKey:@"isLandscapeRight"] boolValue];
	return ret;
}
UIImage * imageWithPrefixAndNum (NSString *prefix, int num) {
	NSString *imageName = [NSString stringWithFormat:@"%@%i.png", prefix, num];
	UIImage *image = [UIImage imageNamed:imageName];
	if (nil == image) {
		NSLog(@"image %@ is not exist!", imageName);
	}
	return image;
}

NSString *stringFromInt(int num) {
	return [NSString stringWithFormat:@"%i", num];
}
NSString *stringFromFloat(double num) {
	return [NSString stringWithFormat:@"%f", num];
}
@implementation UIViewController (LBCommon)

- (UIViewController *)viewControllerWithGoBackCount:(int)num {
	NSArray *vcs = self.navigationController.viewControllers;
	UIViewController *vc = [vcs objectAtIndex:vcs.count - 1 - num];
	return vc;
}

@end

#pragma mark --- special methods ---
NSString * todayStr() {
	NSDate *now = [NSDate date];
	NSDateFormatter *formatter = [NSDateFormatter new];
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setDateFormat:@"EEE d MMMM"];
	NSString *todayStr = [formatter stringFromDate:now];
	[formatter release];
	return todayStr;
}
NSDate * todayDate() {
	NSDate *now = [NSDate date];
	NSDateFormatter *formatter = [NSDateFormatter new];
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	//delete the time port
	NSString *dateStr = [formatter stringFromDate:now];
	NSDate *today = [formatter dateFromString:dateStr];
	[formatter release];
	return today;
}


NSString* fileSizeToString(double bytes) {
	if (bytes < 1024)
		return([NSString stringWithFormat:@"%.0f bytes",bytes]);
	
	bytes /= 1024.0;
	if (bytes < 1024)
		return([NSString stringWithFormat:@"%1.2f KB",bytes]);
	
	bytes /= 1024.0;
	if (bytes < 1024)
		return([NSString stringWithFormat:@"%1.2f MB",bytes]);
	
	bytes /= 1024.0;
	if (bytes < 1024)
		return([NSString stringWithFormat:@"%1.2f GB",bytes]);
	
	bytes /= 1024.0;
	return([NSString stringWithFormat:@"%1.2f TB",bytes]);
}

NSURL* smartURLForString(NSString* str)
{
    NSURL *     result;
    NSString *  trimmedStr;
    NSRange     schemeMarkerRange;
    NSString *  scheme;
    
    assert(str != nil);
	
    result = nil;
    
    trimmedStr = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ( (trimmedStr != nil) && (trimmedStr.length != 0) ) {
        schemeMarkerRange = [trimmedStr rangeOfString:@"://"];
        
        if (schemeMarkerRange.location == NSNotFound) {
            result = [NSURL URLWithString:[NSString stringWithFormat:@"ftp://%@", trimmedStr]];
        } else {
            scheme = [trimmedStr substringWithRange:NSMakeRange(0, schemeMarkerRange.location)];
            assert(scheme != nil);
            
            if ( ([scheme compare:@"ftp"  options:NSCaseInsensitiveSearch] == NSOrderedSame) ) {
                result = [NSURL URLWithString:trimmedStr];
            } else {
                // It looks like this is some unsupported URL scheme.
            }
        }
    }
    
    return result;
}
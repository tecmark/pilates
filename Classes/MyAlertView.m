//
//  MyAlertView.m
//  Yoga
//
// Created by Rob on 10-5-6.
//  
//

#import "MyAlertView.h"


@implementation MyAlertView
@synthesize messageLB;
@synthesize titleLB;
@synthesize delegate;

- (void)bounceDialog {
	[dialog scaleTo:1.0 duration:0.2];
}
- (void)showDialog {
	dialog.transform = CGAffineTransformMakeScale(0.1, 0.1);
	dialog.hidden = NO;
	[dialog scaleTo:1.2 duration:0.2];
	[self performSelector:@selector(bounceDialog) withObject:nil afterDelay:0.2];
	
}
- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)dealloc {
    [super dealloc];
}

- (IBAction)cancel {
	[self removeFromSuperview];
}
- (IBAction)ok{
	[self removeFromSuperview];
	[delegate myAlertOk];
}
@end

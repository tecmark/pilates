//
//  TableViewHeader.m
//  Yoga
//
// Created by Rob on 10-4-1.
//  
//

#import "TableViewHeader.h"


@implementation TableViewHeader
@synthesize textLB;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)dealloc {
	[textLB release];
    [super dealloc];
}


@end

//
//  MyPickerView.h
//  Yoga
//
// Created by Rob on 10-4-22.
//  
//

#import <UIKit/UIKit.h>
@protocol MyPikerViewDelegate
- (void)pickerDone;
@end


@interface MyPickerView : UIView {
	IBOutlet UIPickerView *picker;
	id <UIPickerViewDelegate, UIPickerViewDataSource, MyPikerViewDelegate>pickerDelegate;

}
@property (assign) id <UIPickerViewDelegate, UIPickerViewDataSource, MyPikerViewDelegate>pickerDelegate;
@property (nonatomic, readonly) UIPickerView *picker;

- (IBAction)done;

- (void)resignFirstResponder;

@end

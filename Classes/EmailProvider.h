//
//  EmailProvider.h
//  iHaveissues
//
// Created by Rob on  7/17/09.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface EmailProvider : NSObject<MFMailComposeViewControllerDelegate> {
	NSString *subject;
	NSString *emailBody;
	BOOL htmlFormat;
	NSArray *toRecipients;
	NSArray *ccRecipients;
	NSArray *bccRecipients;
	
	NSData *attachments;
	
	UINavigationController *navigationController;
	
	id delegate;
}

@property (nonatomic, copy) NSString *subject;
@property (nonatomic, copy) NSString *emailBody;
@property (nonatomic, retain) NSArray *toRecipients;
@property (nonatomic, retain) NSArray *ccRecipients;
@property (nonatomic, retain) NSArray *bccRecipients;
@property (nonatomic, retain) NSData *attachments;
@property (nonatomic) BOOL htmlFormat;
@property (nonatomic, retain) id delegate;

@property (nonatomic, assign) UINavigationController *navigationController;

+(void)sendEmailFromViewControler:(UIViewController*)viewController 
									to:(NSArray*)to 
									cc:(NSArray*)cc 
								    bcc:(NSArray*)bcc
									attachments:(NSData*)attachs
									subject:(NSString*)subj
									body:(NSString*) body
									htmlFormat:(BOOL)isHtml;

@end

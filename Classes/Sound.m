//
//  Sound.m
//  
//
//  


#import "Sound.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@implementation Sound

@synthesize player;

- (id)initWithContentsOfFile:(NSString *)path {
    self = [super init];
    
    if (self) {
        NSLog(@"File url: %@", path);
		fileURL = [NSURL fileURLWithPath:path isDirectory:NO];
        
        if (fileURL != nil)  {
            NSLog(@"sound file url: %@", fileURL);
            NSError *err = nil;
            AVAudioPlayer *newPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: fileURL
												   error: &err];
            NSLog(@"error: %@", err);
			player = newPlayer;
			if (nil == player)
				NSLog(@"The audio file %@ not exist!Maybe the file name is wrong or the file is not added to the project.", path);
            
            UInt32 doChangeDefaultRoute = 1;
            AudioSessionSetProperty (kAudioSessionProperty_OverrideCategoryDefaultToSpeaker,
                                     sizeof (doChangeDefaultRoute),
                                     &doChangeDefaultRoute);
			[player prepareToPlay];
        }
    }
    return self;
}
    
    
- (id)initWithResourceFile:(NSString *)file {
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *path = [[mainBundle resourcePath] stringByAppendingFormat:@"/%@", file];
	[self initWithContentsOfFile:path];
	return self;
}

- (void)dealloc {
	
#ifdef _DEBUG_METHOD_CALL
	//NSLog(@"Sound::dealloc");
#endif
	if(player.playing){
		[player stop];
	}
	if(nil != player)
		[player release];
	player = nil;
    [super dealloc];
}

- (void)playRepeat:(int)count volume:(float)volume{
	if(player.playing){
		[player pause];
	}
	if (volume > 0) {
		player.volume = volume;
	}
	[player setNumberOfLoops:count];
	//[player setCurrentTime:0.0f];
	NSLog(@"it's Played.");
	[player play];
}
- (void)playRepeat:(int)count {
	[self playRepeat:count volume:0];
}
- (void)playWithVolume:(float)volume {
	[self playRepeat:0 volume:volume];
}
- (void)playOrResumre {
	if (player.currentTime > 0) {
		[player play];
	}
	else {
		[self playRepeat:0 volume:0];
	}
}
- (void)playOnce {
	[self playRepeat:0 volume:0];
}
- (void)resumePlay {
	NSLog(@"it's resumed playing.");
	if (![player isPlaying]) {
		[player play];
	}
}

- (void)stop{
	[player pause];
	[player setCurrentTime:0.0f];
}
- (void)pause {
	NSLog(@"currentTime :%f", player.currentTime, nil);
	[player pause];
}

@end


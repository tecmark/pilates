//
//  AddAsanaPoseViewController.h
//  Yoga
//
// Created by Rob on 10-3-29.
//  
//

#import <UIKit/UIKit.h>


@interface AddAsanaPoseViewController : CommonTableViewController {
	 
	NSMutableArray *asanaNameArray;
	NSMutableArray *selectedPoseArray;
	int asanaID;
	NSString *asanaName;
	BOOL isFromAddAsana;
	IBOutlet UIButton *doneButton;

}

@property int asanaID;
@property (nonatomic, retain) NSMutableArray *selectedPoseArray;
@property (nonatomic, retain) NSString *asanaName;
@property BOOL isFromAddAsana;
- (IBAction)addPoseDone;
- (IBAction)selectPose:(id)sender;

@end

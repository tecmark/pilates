//
//  EditAsanaTableCell.m
//  Yoga
//
// Created by Rob on 10-4-20.
//  
//

#import "EditAsanaTableCell.h"


@implementation EditAsanaTableCell
@synthesize selection;
@synthesize repeatCountLB;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
        
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
	[selection release];
	[repeatCountLB release];
    [super dealloc];
}


@end

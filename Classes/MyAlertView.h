//
//  MyAlertView.h
//  Yoga
//
// Created by Rob on 10-5-6.
//  
//

#import <UIKit/UIKit.h>
@protocol MyAlertViewDelegate
- (void)myAlertOk;
@end


@interface MyAlertView : UIView {
	IBOutlet UIView *dialog;
	IBOutlet UILabel *messageLB;
	IBOutlet UILabel *titleLB;
	id<MyAlertViewDelegate> delegate;
}

@property (nonatomic, retain) UILabel *messageLB;
@property (nonatomic, retain) UILabel *titleLB;
@property (assign) id<MyAlertViewDelegate> delegate;

- (void)showDialog;
- (IBAction)cancel;
- (IBAction)ok;

@end

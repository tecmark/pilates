//
//  ClassesServerHelper.m
//  Yoga
//
// Created by Rob on  11/12/10.
//  
//

#import "ClassesServerHelper.h"
#import "UA_ASIHTTPRequest.h"
#import "RateCommentsHelper.h"

#define kClassContentServer		@"http://www.pilatescustom.com/live_app/"
#define kClassInfoFile			@"info.txt"
#define kClassBiogImageFile		@"biog.jpg"
#define kClassBiogFile			@"biog.txt"
#define kClassKramaFile			@"krama.txt"
#define kClassMainImageFile		@"main.jpg"

#define kClassKramaMainImageServerFolder		@"Main"
#define kClassKramaMainImage					@"MainImage%i.jpg"
#define kClassKramaThumbnailServerFolder		@"Thumbnails"
#define kClassKramaThumbnail					@"Thumbnail%i.jpg"
#define kClassKramaDurationFile					@"duration.txt"
#define kTextSeparater							@"###"

@implementation ClassesServerHelper

+ (NSString*)getClassInfoTxtByID:(NSString*)classID {
    
    NSArray *classComps = [classID componentsSeparatedByString:@"."];
    
    if (classComps.count)
    {
        classID = [classComps lastObject];
    }
    
	NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", kClassContentServer, classID, kClassInfoFile];
    NSLog(@"Loading Class %@", urlString);
    NSURL *url = [NSURL URLWithString:  urlString];
    UA_ASIHTTPRequest *request = [[[UA_ASIHTTPRequest alloc] initWithURL:url] autorelease];
    request.timeOutSeconds = 60;
    request.requestMethod = @"GET";
	
    [request startSynchronous];
	
	return [request responseString];
}

+ (NSData*)getClassBiogImageByID:(NSString*)classID {
	NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", kClassContentServer, classID, kClassBiogImageFile];
    NSURL *url = [NSURL URLWithString:  urlString];
    UA_ASIHTTPRequest *request = [[[UA_ASIHTTPRequest alloc] initWithURL:url] autorelease];
    request.timeOutSeconds = 600;
    request.requestMethod = @"GET";
	
    [request startSynchronous];
	
	return [request responseData];
}

+ (NSString*)getClassBiogTxtByID:(NSString*)classID {
	NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", kClassContentServer, classID, kClassBiogFile];
    NSURL *url = [NSURL URLWithString:  urlString];
    UA_ASIHTTPRequest *request = [[[UA_ASIHTTPRequest alloc] initWithURL:url] autorelease];
    request.timeOutSeconds = 60;
    request.requestMethod = @"GET";
	
    [request startSynchronous];
	
	return [request responseString];
}

+ (NSArray*)getClassKramaByID:(NSString*)classID {
	NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", kClassContentServer, classID, kClassKramaFile];
    NSURL *url = [NSURL URLWithString:  urlString];
    UA_ASIHTTPRequest *request = [[[UA_ASIHTTPRequest alloc] initWithURL:url] autorelease];
    request.timeOutSeconds = 60;
    request.requestMethod = @"GET";
	
    [request startSynchronous];
    
	
	NSArray *karmas = [[request responseString] componentsSeparatedByString:kTextSeparater];
    
    
    NSLog(@"KRAMAS Loaded %i", [karmas count]);
	return [karmas subarrayWithRange:NSMakeRange(1, [karmas count] - 1)];
}
+ (NSArray*)getClassKramaDurationByID:(NSString*)classID {
	NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", kClassContentServer, classID, kClassKramaDurationFile];
    NSURL *url = [NSURL URLWithString:  urlString];
   UA_ASIHTTPRequest *request = [[[UA_ASIHTTPRequest alloc] initWithURL:url] autorelease];
    request.timeOutSeconds = 60;
    request.requestMethod = @"GET";
	
    [request startSynchronous];
	
	NSArray *karmasDuration = [[request responseString] componentsSeparatedByString:kTextSeparater];
	return [karmasDuration subarrayWithRange:NSMakeRange(1, [karmasDuration count] - 1)];
}
+ (NSData*)getClassMainImageByID:(NSString*)classID {
	NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@", kClassContentServer, classID, kClassMainImageFile];
    NSURL *url = [NSURL URLWithString:  urlString];
    UA_ASIHTTPRequest *request = [[[UA_ASIHTTPRequest alloc] initWithURL:url] autorelease];
    request.timeOutSeconds = 600;
    request.requestMethod = @"GET";
	
    [request startSynchronous];
	
	return [request responseData];
}

+ (NSArray*)getClassKramaMainImageByID:(NSString*)classID kramaAmount:(NSInteger)kramaAmount {
	NSMutableArray *imagesArray = [NSMutableArray array];
	
	for (int i = 0; i < kramaAmount; i++) {
		NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@/%@", kClassContentServer, classID, 
							   kClassKramaMainImageServerFolder, [NSString stringWithFormat:kClassKramaMainImage, i + 1, nil], nil];
		NSURL *url = [NSURL URLWithString:  urlString];
		for (int j = 0; j < 3; j++) {
			UA_ASIHTTPRequest *request = [[[UA_ASIHTTPRequest alloc] initWithURL:url] autorelease];
			request.timeOutSeconds = 600;
			request.requestMethod = @"GET";
			
			[request startSynchronous];
			
			NSData *imageData = [request responseData];
			if (imageData != nil) {
				[imagesArray addObject:imageData];
				break;
			}
		}
	}
	
	return [NSArray arrayWithArray:imagesArray];
}

+ (NSArray*)getClassKramaThumbnailByID:(NSString*)classID kramaAmount:(NSInteger)kramaAmount {
	NSMutableArray *imagesArray = [NSMutableArray array];
	
	for (int i = 0; i < kramaAmount; i++) {
		NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@/%@", kClassContentServer, classID, 
							   kClassKramaThumbnailServerFolder, [NSString stringWithFormat:kClassKramaThumbnail, i + 1, nil], nil];
		NSURL *url = [NSURL URLWithString:  urlString];
		for (int j = 0; j < 3; j++) {
			UA_ASIHTTPRequest *request = [[[UA_ASIHTTPRequest alloc] initWithURL:url] autorelease];
			request.timeOutSeconds = 600;
			request.requestMethod = @"GET";
			
			[request startSynchronous];
			
			NSData *imageData = [request responseData];
			if (imageData != nil) {
				[imagesArray addObject:imageData];
				break;
			}
		}
	}
	
	return [NSArray arrayWithArray:imagesArray];
}

+ (YogaClass*)getClassByID:(NSString*)classID {
	NSLog(@"Start to load the class [[%@]] from server.", classID, nil);
	YogaClass *yogaClass = [[YogaClass alloc] initWithClassID:classID];
	NSString *info = [ClassesServerHelper getClassInfoTxtByID:classID];
	NSArray *infoContent = [info componentsSeparatedByString:kTextSeparater];
	yogaClass.name = [infoContent objectAtIndex:1];
	NSLog(@"YogaClass Name: %@\n", yogaClass.name, nil);
	yogaClass.shortDescription = [infoContent objectAtIndex:2];
	NSLog(@"YogaClass shortDescription: %@\n", yogaClass.shortDescription, nil);
	yogaClass.longDescription = [infoContent objectAtIndex:3];
	NSLog(@"YogaClass longDescription: %@\n", yogaClass.longDescription, nil);
	yogaClass.classDuration = [infoContent objectAtIndex:4];
	NSLog(@"YogaClass classDuration: %@\n", yogaClass.classDuration, nil);
	yogaClass.classCategory = [infoContent objectAtIndex:5];
	NSLog(@"YogaClass classCategory: %@\n", yogaClass.classCategory, nil);
	
	yogaClass.biogText = [ClassesServerHelper getClassBiogTxtByID:classID];
	NSLog(@"YogaClass biogText: %@\n", yogaClass.biogText, nil);
	yogaClass.biogImage = [ClassesServerHelper getClassBiogImageByID:classID]; 
	
	yogaClass.mainImage = [ClassesServerHelper getClassMainImageByID:classID]; 
	
    NSLog(@"KRAMAS about to load");
	yogaClass.krama = [ClassesServerHelper getClassKramaByID:classID]; 
	NSInteger karmaAmount = [yogaClass.krama count];
    
    NSLog(@"KRAMAS loaded %i", karmaAmount);
    
	for (int i = 0; i < karmaAmount; i++) {
		NSLog(@"YogaClass krama %i: %@\n", i, [yogaClass.krama objectAtIndex:i], nil);
	}
	
	NSArray *kramaDurationString = [ClassesServerHelper getClassKramaDurationByID:classID];
	NSMutableArray *krameDuration = [NSMutableArray array];
	
	for (int i = 0; i < [kramaDurationString count]; i++) {
		NSString *time = [kramaDurationString objectAtIndex:i];
		NSArray *separatedTime = [time componentsSeparatedByString:@":"];
		
		if ([separatedTime count] == 2) {
			NSInteger mins = [[separatedTime objectAtIndex:0] intValue];
			NSInteger sec = [[separatedTime objectAtIndex:1] intValue];
			
			[krameDuration addObject:[NSNumber numberWithInt:mins * 60 + sec]];
			NSLog(@"YogaClass Krama %i Duration: %@\n", i, [krameDuration objectAtIndex:i], nil);
		}
		else if ([separatedTime count] == 3) {
			NSInteger hours = [[separatedTime objectAtIndex:0] intValue]; 
			NSInteger mins = [[separatedTime objectAtIndex:1] intValue];
			NSInteger sec = [[separatedTime objectAtIndex:2] intValue];
			
			[krameDuration addObject:[NSNumber numberWithInt:hours * 3600 + mins * 60 + sec]];
			NSLog(@"YogaClass Krama %i Duration: %@\n", i, [krameDuration objectAtIndex:i], nil);
		}
	}
	yogaClass.kramaDuration = [NSArray arrayWithArray:krameDuration];
	
	yogaClass.kramaMainImages = [ClassesServerHelper getClassKramaMainImageByID:classID kramaAmount:karmaAmount];
	NSLog(@"YogaClass MainImages %i\n", [yogaClass.kramaMainImages count], nil);
	yogaClass.kramaThumbnails = [ClassesServerHelper getClassKramaThumbnailByID:classID kramaAmount:karmaAmount];
	NSLog(@"YogaClass Thumbnails %i\n", [yogaClass.kramaThumbnails count], nil);
	
	// Load comments and rate
	NSInteger rate;
	yogaClass.comments = [[RateCommentsHelper shared] getCommentsFromServerWithClassID:classID averageRate:&rate];
	yogaClass.averageRating = rate;
	
	return [yogaClass autorelease];
}

@end

//
//  AsanaViewController.h
//  
//
// Created by Rob on 12/15/09.
//  
//

#import <UIKit/UIKit.h>


@interface AsanaViewController : CommonHomeViewController {
	IBOutlet UIScrollView	*scrollView;

}

- (IBAction)back;
- (IBAction)gotoSelectPose:(id)sender;
@end

//
//  CommonViewController.h
//  
//
// Created by Rob on 11/5/09.
//  
//

#import <UIKit/UIKit.h>


@interface CommonHomeViewController : UIViewController {
	NSArray *switchViewContorllerNameArray;
}

@property (nonatomic, retain) NSArray *switchViewContorllerNameArray;

/* the sender's tag identify which view controller should switch to */
- (IBAction)switchViewController:(id)sender;

//overide this method to set the global variables when switch to a view controller
- (void)beforSwitchToViewController:(int)controllerID;

@end

@interface CommonViewController : UIViewController {
	UILabel *titleLB;
	UIButton *btn;
    UIButton *btn2;
}

@property (nonatomic,retain) UILabel *titleLB;
@property (nonatomic,retain) UIButton *btn;
@property (nonatomic,retain) UIButton *btn2;
- (IBAction)back;
- (IBAction)home;


@end

@interface CommonTableViewController : CommonViewController <UITableViewDelegate, UITableViewDataSource> {
	IBOutlet UITableView *table;
	IBOutlet UISearchBar *mySearchBar;
	NSMutableArray *dataArray;
}
@property (nonatomic,retain) NSMutableArray *dataArray;
@end

@interface CommonFavoritesTableViewController : CommonViewController <UITableViewDelegate, UITableViewDataSource> {
	IBOutlet UITableView *table;
	IBOutlet UISearchBar *mySearchBar;
	
	NSMutableArray *dataArray;
}
@property (nonatomic,retain) NSMutableArray *dataArray;

@end

@interface CommonSearchTableViewController : CommonTableViewController <UISearchBarDelegate> {
	IBOutlet UISearchBar *upSearchBar;
	NSMutableDictionary *allPoseDict;
	NSMutableDictionary *poseDict;
	NSMutableArray *keys;
	NSMutableArray *indexArray;
	UIView *mask;
	BOOL isMaskOn;
}

@property (nonatomic, retain) NSMutableDictionary *poseDict;
@property (nonatomic, retain) NSMutableArray *keys;

- (void)resetSearch;

@end
//
//  AddIssueTableCell.h
//  Yoga
//
// Created by Rob on 10-3-17.

//

#import <UIKit/UIKit.h>


@interface FavoritesTableCell : UITableViewCell {
	
	IBOutlet UIImageView *poseIV;
	IBOutlet UILabel *titleLB;
	IBOutlet UILabel *subTitleLB;
	IBOutlet UILabel *descLB;
	IBOutlet UIButton *favouriteStarButton;

}

@property (nonatomic, retain) UIImageView *poseIV;
@property (nonatomic, retain) UILabel *titleLB;
@property (nonatomic, retain) UILabel *subTitleLB;
@property (nonatomic, retain) UILabel *descLB;
@property (nonatomic, retain) UIButton *favouriteStarButton;


@end

//
//  CommentRate.h
//  Yoga
//
// Created by Rob on 11/17/10.
//  
//

#import <Foundation/Foundation.h>


@interface CommentRate : NSObject {
	NSInteger rate;
	NSString* title;
	NSString* comment;
	NSString* submittedDate;
}

@property NSInteger rate;
@property (nonatomic, retain) NSString* title;
@property (nonatomic, retain) NSString* comment;
@property (nonatomic, retain) NSString* submittedDate;

+ (id)commentRateWithRate:(NSInteger)_rate title:(NSString*)_title comment:(NSString*)_comment submittedDate:(NSString*)_submittedDate;
- (id)initWithRate:(NSInteger)_rate title:(NSString*)_title comment:(NSString*)_comment submittedDate:(NSString*)_submittedDate;

@end

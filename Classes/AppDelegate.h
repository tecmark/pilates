//
//  AppDelegate.h
//  
//
// Created by Rob on 03/25/10.
//

#import <CoreLocation/CoreLocation.h>
#import "UAStoreFrontDelegate.h"
#import "UAStoreKitObserver.h"

@class UAStoreKitObserver;

@interface AppDelegate : NSObject <UIApplicationDelegate, UAStoreFrontDelegate> {
    
    UIWindow *window;
    UINavigationController *navigationController;
	
	NSInteger networkingCount;
	
	BOOL isLoadedCommentsRate;
    BOOL playingVideo;
    
  
    
    
  //  NSString *deviceToken;
	//NSString *deviceAlias;
    
}

//@property (nonatomic, retain) NSString *deviceToken;
//@property (nonatomic, retain) NSString *deviceAlias;
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property BOOL isLoadedCommentsRate;
@property BOOL playingVideo;


- (void)didStartNetworking;

@end


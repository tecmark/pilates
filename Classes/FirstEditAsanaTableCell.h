//
//  FirstEditAsanaTableCell.h
//  Yoga
//
// Created by Rob on  12/15/10.
//  
//

#import <UIKit/UIKit.h>


@interface FirstEditAsanaTableCell : UITableViewCell <UITextFieldDelegate> {
	IBOutlet UITextField *sequenceTitle;
	IBOutlet UIButton *titleBtn;
	IBOutlet UIButton *intervalTimeBtn;
	IBOutlet UILabel *intervalLB;
	
	id target;
	SEL titleBtnSelector;
	SEL intervalTimeBtnSelector;
	
	int asanaID;
	
	NSMutableDictionary *asanaDict;
	
	NSString *sequenceTitleCache;
}
@property (nonatomic, retain) UITextField *sequenceTitle;
@property (nonatomic, retain) UIButton *titleBtn;
@property (nonatomic, retain) UIButton *intervalTimeBtn;
@property (nonatomic, retain) UILabel *intervalLB;

@property (nonatomic, assign) id target;
@property (nonatomic) SEL titleBtnSelector;
@property (nonatomic) SEL intervalTimeBtnSelector;

@property int asanaID;
@property (nonatomic, assign) NSMutableDictionary *asanaDict;


- (IBAction)titleBtnTouch;
- (IBAction)intervalTimeBtnTouch;
- (IBAction)titleTexFildInputDone;
- (IBAction)titleTexFildInputStarted;
- (IBAction)titleTexFildInputBegan;
@end

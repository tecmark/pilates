//
//  AppDelegate.m
//  
//
// Created by Rob on 03/25/10.
//  
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "UAirship.h"
#import "UAStoreFront.h"
#import "RateCommentsHelper.h"
#import "YogaClass.h"
#import "UAPush.h"
#import <AVFoundation/AVAudioSession.h>

#import "TestFlight.h"

// Urban Airship Production
//#define  kApplicationKey @"iUkDKamHQsqTcWICI5Svxw"
//#define  kApplicationSecret @"EGWd_WF2SIeI8kIBwDA1dQ"

@implementation AppDelegate

@synthesize window, isLoadedCommentsRate;
//@synthesize deviceToken;
//@synthesize deviceAlias;
@synthesize playingVideo;


- (void)updateClassesCommentsRates {
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	@try {
		if (!self.isLoadedCommentsRate ) {
			self.isLoadedCommentsRate = YES;
			[[RateCommentsHelper shared] updateRateCommentsFromServerForAllClasses];
		}
	}
	@catch (NSException * e) {
		NSLog(@"Update Comments and rates %@ %@", [e reason], [e description], nil);
	}
    
	[pool release];
}


#pragma mark -
#pragma mark Application lifecycle

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
	
    
    if(playingVideo){
        
        NSLog(@"re-opened whilst playing video, go back to class screen");
            //showAlertViewWithMessage(@"loading whilst video");
        	  [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
        
        
    }
    

    NSLog(@"register for notification");
	
	//Register for notifications
    [[UIApplication sharedApplication]
     registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                         UIRemoteNotificationTypeSound |
                                         UIRemoteNotificationTypeAlert)];
    
    
    
    NSLog(@"reset badge");
	// Reset badge number to 0
	[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
	
    }



- (void)applicationDidFinishLaunching:(UIApplication *)application {    
    
    // Override point for customization after app launch    
    [DBHandler setDefaultDBName:@"QuikkaYoga.sqlite"];
    [DBHandler prepareDB];
   
    
	HomeViewController *rootViewController = [[HomeViewController alloc] initWithNibUsingClassName];
	navigationController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
	navigationController.navigationBarHidden = YES;
	[rootViewController release];
	
	[window addSubview:[navigationController view]];
    [window makeKeyAndVisible];
    
    
	
    NSLog(@"Did finish launching");
  
    
    //Init Airship launch options
    NSMutableDictionary *takeOffOptions = [[[NSMutableDictionary alloc] init] autorelease];
    [takeOffOptions setValue:nil forKey:UAirshipTakeOffOptionsLaunchOptionsKey];
    
    // Create Airship singleton that's used to talk to Urban Airhship servers.
    // Please populate AirshipConfig.plist with your info from http://go.urbanairship.com
    [UAirship takeOff:takeOffOptions];
    
    [[UAPush shared] resetBadge];//zero badge on startup
    
    [[UAPush shared] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                         UIRemoteNotificationTypeSound |
                                                         UIRemoteNotificationTypeAlert)];
    
	[NSThread detachNewThreadSelector:@selector(updateClassesCommentsRates) toTarget:self withObject:nil];
}

- (void) startTestFlight
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [TestFlight takeOff:@"054bc19040b75450a9cf3a53b80a206a_NDUyODYyMDExLTEyLTA1IDA2OjIwOjM3LjY0ODcxMQ"];
    [pool drain];
}


- (void)applicationWillTerminate:(UIApplication *)application {
	// Save data if appropriate
	ReleaseGlobalVariables();
	[UAirship land];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    UALOG(@"Application did become active.");
    [[UAPush shared] resetBadge]; //zero badge when resuming from background (iOS 4+)
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    UALOG(@"APN device token: %@", deviceToken);
    // Updates the device token and registers the token with UA
    [[UAPush shared] registerDeviceToken:deviceToken];
    
    
    /*
     * Some example cases where user notifcation may be warranted
     *
     * This code will alert users who try to enable notifications
     * from the settings screen, but cannot do so because
     * notications are disabled in some capacity through the settings
     * app.
     * 
     */
    
    /*
     
     //Do something when notifications are disabled altogther
     if ([application enabledRemoteNotificationTypes] == UIRemoteNotificationTypeNone) {
     UALOG(@"iOS Registered a device token, but nothing is enabled!");
     
     //only alert if this is the first registration, or if push has just been
     //re-enabled
     if ([UAirship shared].deviceToken != nil) { //already been set this session
     NSString* okStr = @"OK";
     NSString* errorMessage =
     @"Unable to turn on notifications. Use the \"Settings\" app to enable notifications.";
     NSString *errorTitle = @"Error";
     UIAlertView *someError = [[UIAlertView alloc] initWithTitle:errorTitle
     message:errorMessage
     delegate:nil
     cancelButtonTitle:okStr
     otherButtonTitles:nil];
     
     [someError show];
     [someError release];
     }
     
     //Do something when some notification types are disabled
     } else if ([application enabledRemoteNotificationTypes] != [UAPush shared].notificationTypes) {
     
     UALOG(@"Failed to register a device token with the requested services. Your notifications may be turned off.");
     
     //only alert if this is the first registration, or if push has just been
     //re-enabled
     if ([UAirship shared].deviceToken != nil) { //already been set this session
     
     UIRemoteNotificationType disabledTypes = [application enabledRemoteNotificationTypes] ^ [UAPush shared].notificationTypes;
     
     
     
     NSString* okStr = @"OK";
     NSString* errorMessage = [NSString stringWithFormat:@"Unable to turn on %@. Use the \"Settings\" app to enable these notifications.", [UAPush pushTypeString:disabledTypes]];
     NSString *errorTitle = @"Error";
     UIAlertView *someError = [[UIAlertView alloc] initWithTitle:errorTitle
     message:errorMessage
     delegate:nil
     cancelButtonTitle:okStr
     otherButtonTitles:nil];
     
     [someError show];
     [someError release];
     }
     }
     
     */
}


- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *) error {
    UALOG(@"Failed To Register For Remote Notifications With Error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    UALOG(@"Received remote notification: %@", userInfo);
    
    // Get application state for iOS4.x+ devices, otherwise assume active
    UIApplicationState appState = UIApplicationStateActive;
    if ([application respondsToSelector:@selector(applicationState)]) {
        appState = application.applicationState;
    }
    
    [[UAPush shared] handleNotification:userInfo applicationState:appState];
    [[UAPush shared] resetBadge]; // zero badge after push received
}



#pragma mark -
#pragma mark Memory management

- (void)dealloc {
	[navigationController release];
	[window release];
	[super dealloc];
}

- (void)didStartNetworking {
    networkingCount += 1;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

#pragma mark -
#pragma mark StoreFrontDelegate

-(void)productPurchased:(UAProduct*)product {
    UALOG(@"[StoreFrontDelegate] Purchased: %@ -- %@", product.productIdentifier, product.title);
	[YogaClass installedClassWithID:product.productIdentifier];
}

-(void)storeFrontDidHide {
    UALOG(@"[StoreFrontDelegate] StoreFront quit, do something with content");
}

-(void)storeFrontWillHide {
    UALOG(@"[StoreFrontDelegate] StoreFront will hide");
}

-(void)productsDownloadProgress:(float)progress count:(int)count {
    UALOG(@"[StoreFrontDelegate] productsDownloadProgress: %f count: %d", progress, count);
    if (count == 0) {
        UALOG(@"Downloads complete");
    }
}

@end


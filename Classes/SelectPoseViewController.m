//
//  StandingPoseViewController.m
//  Yoga
//
// Created by Rob on 10-3-23.
//  
//

#import "SelectPoseViewController.h"
#import "PoseDetailViewController.h"
#import "TableViewHeader.h"
#import "SelectPoseTableCell.h" 

#define kTableCellHeight 65
#define kTableMoveOffset 50
#define kTableMaxHeight	 375

@implementation SelectPoseViewController
@synthesize poseCatalogID;

#pragma mark --- override ---
- (void)loadData {
	id<PLResultSet> asanaResult;
	id<PLResultSet> myAsanaPosesResult;
	id<PLResultSet> result;
	asanaResult = [dbHandle() executeQuery: @"SELECT * FROM Asanas where ID = ?", stringFromInt(poseCatalogID)];
	if ([asanaResult next]) {
		titleLB.text = [asanaResult stringForColumn:@"Name"];
	}
	
	[asanaResult close];
	
	myAsanaPosesResult = [dbHandle() executeQuery: @"SELECT * FROM AsanaDetail where AsanaID = ? ORDER BY PoseIndex", stringFromInt(poseCatalogID)];
	
	while ([myAsanaPosesResult next]) {
		NSNumber *poseId = [myAsanaPosesResult objectForColumn:@"PoseID"];
        NSString *q = [NSString stringWithFormat:@"SELECT * FROM Poses where ID = %d", [poseId intValue]];
        
		result = [dbHandle() executeQuery:q];

		if ([result next]) {
			NSString *picPath = [result safeObjectForColumn:@"PicturePath"];
			
			NSString *poseName = [[[[[result safeObjectForColumn:@"Name"] capitalizedString] stringByReplacingOccurrencesOfString:@"Iii" withString:@"III"]stringByReplacingOccurrencesOfString:@"Ii" withString:@"II"]stringByReplacingOccurrencesOfString:@"Iv" withString:@"IV"];
			//poseName = [NSString stringWithFormat:@"%@ %@", poseId, poseName];
			NSString *sanskrit = [[[[[result safeObjectForColumn:@"Sanskrit"] capitalizedString] stringByReplacingOccurrencesOfString:@"Iii" withString:@"III"]stringByReplacingOccurrencesOfString:@"Ii" withString:@"II"]stringByReplacingOccurrencesOfString:@"Iv" withString:@"IV"];
			NSString *description = [result safeObjectForColumn:@"description"];
			NSString *intro =  [result safeObjectForColumn:@"Intro"];
			
			NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[picPath stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]], @"picPath",
								  poseName, @"poseName", 
								  sanskrit, @"sanskrit",
								  description, @"description", 
								  poseId, @"poseID", 
								  intro, @"Intro",
								  nil];
			NSString *newKey = [[poseName substringToIndex:1] uppercaseString];
			
			newKey = [NSString stringWithFormat:@"%@", poseId];
			
			BOOL keyExist = NO;
		
			for (NSString *key in self.keys) {
				if ([newKey isEqualToString:key]) {
					keyExist = YES;
					break;
				}
			}
			
			if (!keyExist) {
				[self.keys addObject:newKey];
			}
			NSMutableArray *array = [NSMutableArray arrayWithArray:[allPoseDict objectForKey:newKey]];
			[array addObject:dict];
			[allPoseDict setObject:array forKey:newKey];
			
		}
		[result close];
		
	}

	
	self.poseDict = allPoseDict;
	

	
	
	self.keys = (NSMutableArray *)[self.keys sortedArrayUsingSelector:@selector(psuedoNumericCompare:)];
	
	
	
	[indexArray addObjectsFromArray:self.keys];

	[myAsanaPosesResult close];
	
	
	
}




- (void)handleSearchForTerm:(NSString *)searchTerm {

	
	NSMutableArray *sectionsToRemove = [[NSMutableArray	alloc] init];
	[self resetSearch];
	
	
	int rowCount = 0;
	for (NSString *key in self.keys) {
		id dictOfKey = [poseDict valueForKey:key];
		NSMutableArray * array;
		if ([dictOfKey isKindOfClass:[NSArray class]]) 
			array = (NSMutableArray *)dictOfKey;
		else 
			array = [NSMutableArray arrayWithObject:dictOfKey];
		
		
		NSMutableArray *toRemove = [NSMutableArray new];
		for (int i = 0; i < array.count; i++) {
			
			NSDictionary *dict = [array objectAtIndex:i];
			NSString *poseName = [dict objectForKey:@"poseName"];
			NSString *sanskritName = [dict objectForKey:@"sanskrit"];
			NSRange range = [poseName rangeOfString:searchTerm options:NSCaseInsensitiveSearch];
			NSRange rangeSanskrit = [sanskritName rangeOfString:searchTerm options:NSCaseInsensitiveSearch];
			if (range.location == NSNotFound && rangeSanskrit.location == NSNotFound) 
				[toRemove addObject:dict];
			else 
				rowCount++;
		}
		
		if (toRemove.count == array.count) 
			[sectionsToRemove addObject:key];
		
		[array removeObjectsInArray:toRemove];
		[poseDict setObject:array forKey:key];
		[toRemove release];
		
	}
	//[self.keys removeObjectsInArray:sectionsToRemove];
	[sectionsToRemove release];
	table.frameSizeHeight = MIN(kTableMaxHeight+46 , kTableCellHeight*keys.count);
	[table reloadData];
	

	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	SelectPoseTableCell *cell = (SelectPoseTableCell *)[tableView dequeueReusableCellWithIdentifier:@"SelectPoseTableCell"];
	if (nil == cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SelectPoseTabelCell" owner:self options:nil];
		cell = (SelectPoseTableCell *)[nib objectAtIndex:0];
	}
	
	NSString *key = [self.keys objectAtIndex:indexPath.section];
	id poseOfKey = [poseDict valueForKey:key];
	NSDictionary *dict;
	if ([poseOfKey isKindOfClass:[NSArray class]]) {
		NSArray *array = (NSArray *)poseOfKey;
		dict = [array objectAtIndex:indexPath.row];
	}
	else if ([poseOfKey isKindOfClass:[NSDictionary class]]){
		dict = (NSDictionary *)poseOfKey;
	}
	else {
		NSLog(@"The current pose data structure is not dictionary");
	}
	
	//NSNumber *poseId = [dict objectForKey:@"poseID"];
	NSString *poseName = [dict objectForKey:@"poseName"];
	
	//poseName = [poseName substringFromIndex:[[NSString stringWithFormat:@"%@", poseId] length]+1];
	
	NSString *sankritName = [dict objectForKey:@"sanskrit"];
	NSString *picPath = [dict objectForKey:@"picPath"];
	//NSString *description = [dict objectForKey:@"description"];
	NSString *intro = [dict objectForKey:@"Intro"];
	NSString *imageName = [picPath stringByReplacingOccurrencesOfString:@".jpg" withString:@"_thumb.jpg"];
    NSLog(@"image name: %@", imageName);
	UIImage *poseImg = [UIImage imageNamed:imageName];
	
    
    NSLog(@"img view size: %f,%f", cell.poseIV.frame.size.width, cell.frame.size.height);
    NSLog(@"image clips: %d", cell.poseIV.clipsToBounds);
    [cell.poseIV setContentMode:UIViewContentModeScaleAspectFill];
	cell.poseIV.image = poseImg;
	cell.titleLB.text = [[[[poseName capitalizedString] stringByReplacingOccurrencesOfString:@"Iii" withString:@"III"]stringByReplacingOccurrencesOfString:@"Ii" withString:@"II"] stringByReplacingOccurrencesOfString:@"Iv" withString:@"IV"];;

	cell.subTitleLB.text = [[[[sankritName capitalizedString] stringByReplacingOccurrencesOfString:@"Iii" withString:@"III"]stringByReplacingOccurrencesOfString:@"Ii" withString:@"II"] stringByReplacingOccurrencesOfString:@"Iv" withString:@"IV"];;
	cell.descLB.text = intro;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	return cell;
}



- (void)viewDidLoad {
	[super viewDidLoad];
	
	table.frameSizeHeight = 420;
	
	if (self.poseCatalogID == 2) {
		table.contentInset = UIEdgeInsetsMake(60, 0, 30, 0);
	}
	else {
		table.contentInset = UIEdgeInsetsMake(60, 0, 0, 0);
	}
	
	table.contentOffset = CGPointMake(0, 0);
	
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


- (void)dealloc {
    [super dealloc];
}


@end

//
//  MyAsanaSettingViewController.m
//  Yoga
//
// Created by Rob on 10-4-20.
//  
//

#import "MyAsanaSettingViewController.h"
#import "EditAsanaTableCell.h"
#import "MyAsanaPosesViewContorller.h"
#import "MyAlertViewOnlyOkBtn.h"
#import "FirstEditAsanaTableCell.h"

#define kTableMaxHeight 195
#define kTableCellHeight 65
#define kPopupNumberSelectorXPosition	63

@implementation MyAsanaSettingViewController
@synthesize poseArray;
@synthesize asanaDict;
@synthesize durationStr;
//@synthesize allPoseStepCount;
@synthesize totalDuration;

BOOL timeOpen;
BOOL popupOpen;
UIButton *btn;

NSString *nameCache;

- (void)setDurationLB {
	totalDuration = 0;
	for (NSDictionary *dict in dataArray) {
		int duration = [[dict objectForKey:@"duration"] intValue];
		int repeatCount = [[dict objectForKey:@"repeatCount"] intValue];
		totalDuration += (20 * repeatCount);
		totalDuration += (intervalSeconds* repeatCount);
		totalDuration += duration;
	}
	totalDuration-=intervalSeconds;
	int hour = totalDuration/3600;
	int min = (totalDuration%3600) / 60;
	int sec = totalDuration%60;
	NSString *hourStr = (0 == hour) ? @"" : [NSString stringWithFormat:@"%i:", hour];
	NSString *minStr = [NSString stringWithFormat:@"%02i", min];
	durationLB.text = [NSString stringWithFormat:@"%@%@:%02i", hourStr, minStr, sec]; 
}
- (void)loadData {
	self.dataArray = self.poseArray;
}

-(void)zeroCheck{
    
    if(dataArray.count==0){
        
        noPoses.alpha=1;
        l1.alpha=1;
        l2.alpha=1;
    }
    else{
        noPoses.alpha=0;
        l1.alpha=0;
        l2.alpha=0; 
        
    }
    
}
- (void)viewDidLoad {
    
	timeOpen=NO;
	pickerView = nil;
    [super viewDidLoad];
	
    
    //table.frameOriginY += 141;
	table.frameSizeHeight = 380; //MIN(kTableMaxHeight+44, kTableCellHeight*dataArray.count);
	table.contentSize = CGSizeMake(table.contentSize.width, table.contentSize.height + 40);
	table.editing = YES;
	titleLB.text = @"Edit";
	
	durationLB.text = durationStr;
	asanaID = [[asanaDict objectForKey:@"ID"] intValue];
	NSString *asnaName = [asanaDict objectForKey:@"asanaName"];
	titleTF.text = asnaName;
	[titleBtn setTitle:asnaName forState:UIControlStateNormal];
	intervalSeconds = [[asanaDict objectForKey:@"intervalTime"] intValue];
	intervalMin = intervalSeconds / 60;
	intervalSec = intervalSeconds % 60;
	if (intervalMin == 0) {
		//		[intervalTimeBtn setTitleForAllState:[NSString stringWithFormat:@"%isec", intervalSec]];
		intervalLB.text = [NSString stringWithFormat:@"%isec", intervalSec];
	}
	else {
		//		[intervalTimeBtn setTitleForAllState:[NSString stringWithFormat:@"%i:%02imin", intervalSeconds/60, intervalSeconds%60]];
		intervalLB.text = [NSString stringWithFormat:@"%i:%02imin", intervalSeconds/60, intervalSeconds%60];
	}
    
    [self zeroCheck];
     

}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (void)dealloc {
	[pickerView release];
	[poseArray release];
	[asanaDict release];
	//	[[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}


#pragma mark -
#pragma mark --- UITableView dataSource ---
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	 
    if (indexPath.row == 0) {
		return 140;
	}
	else {
		return 65;
	}
}
/*
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 140;
    
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
  /  if(section==0){
    FirstEditAsanaTableCell *cell = (FirstEditAsanaTableCell *)[tableView dequeueReusableCellWithIdentifier:@"FirstEditAsanaTableCell"];
    if (nil == cell) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FirstEditAsanaTableCell" owner:nil options:nil];
        cell = (FirstEditAsanaTableCell *)[nib objectAtIndex:0];
    }
    NSString *asnaName = [asanaDict objectForKey:@"asanaName"];
    cell.sequenceTitle.text = asnaName;
    cell.asanaDict = self.asanaDict;
    cell.asanaID = asanaID;
    cell.target = self;
    //cell.titleBtnSelector = @selector(editTitle);
    cell.intervalTimeBtnSelector = @selector(setIntervalTime);
    intervalSeconds = [[asanaDict objectForKey:@"intervalTime"] intValue];
    intervalMin = intervalSeconds / 60;
    intervalSec = intervalSeconds % 60;
    if (intervalMin == 0) {
        cell.intervalLB.text = [NSString stringWithFormat:@"%isec", intervalSec];
    }
    else {
        cell.intervalLB.text = [NSString stringWithFormat:@"%i:%02imin", intervalSeconds/60, intervalSeconds%60];
    }
    
    
    return cell;
    }
    else{
        
        return nil;
    }
    
}*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
   // if(section>0){
	return dataArray.count+1;	
    //}
    //else{
      //  return 0;
    //}
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.row == 0) {
		FirstEditAsanaTableCell *cell = (FirstEditAsanaTableCell *)[tableView dequeueReusableCellWithIdentifier:@"FirstEditAsanaTableCell"];
		if (nil == cell) {
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FirstEditAsanaTableCell" owner:nil options:nil];
			cell = (FirstEditAsanaTableCell *)[nib objectAtIndex:0];
		}
		
		NSString *asnaName = [asanaDict objectForKey:@"asanaName"];
		cell.sequenceTitle.text = asnaName;
        cell.asanaDict = self.asanaDict;
		cell.asanaID = asanaID;
		cell.target = self;
		//cell.titleBtnSelector = @selector(editTitle);
		cell.intervalTimeBtnSelector = @selector(setIntervalTime);
		 intervalSeconds = [[asanaDict objectForKey:@"intervalTime"] intValue];
		intervalMin = intervalSeconds / 60;
		intervalSec = intervalSeconds % 60;
		if (intervalMin == 0) {
			cell.intervalLB.text = [NSString stringWithFormat:@"%isec", intervalSec];
		}
		else {
			cell.intervalLB.text = [NSString stringWithFormat:@"%i:%02imin", intervalSeconds/60, intervalSeconds%60];
		}
		return cell;
	}
	else {
		EditAsanaTableCell *cell = (EditAsanaTableCell *)[tableView dequeueReusableCellWithIdentifier:@"EditAsanaTableCell"];
		if (nil == cell) {
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EditAsanaTableCell" owner:nil options:nil];
			cell = (EditAsanaTableCell *)[nib objectAtIndex:0];
		}
		
		NSDictionary *dict = [dataArray objectAtIndex:indexPath.row-1];
		
		NSString *name = [dict objectForKey:@"poseName"];
		NSString *picPath = [dict objectForKey:@"picPath"];
		//	NSNumber *stepCount = [dict objectForKey:@"stepCount"];
		NSNumber *repeatCount = [dict objectForKey:@"repeatCount"];
		//	int duration = [[asanaDict objectForKey:@"intervalTime"] intValue] * [stepCount intValue];
		int duration = [[dict objectForKey:@"duration"] intValue];
		int min = duration/60;
		int sec = duration%60;
		
		UIImage *poseImg = [UIImage imageNamed:[picPath stringByReplacingOccurrencesOfString:@".jpg" withString:@"_thumb.jpg"]];
		
		cell.poseIV.image = poseImg;
		cell.titleLB.text = name;
		cell.repeatCountLB.text = [NSString stringWithFormat:@"x%i",[repeatCount intValue]];
        cell.durationLB.text = [NSString stringWithFormat:@"%i:%02i", min, sec];
		cell.selection.tag = indexPath.row-1;
		[cell.selection addTarget:self action:@selector(setRepeatCount:) forControlEvents:UIControlEventTouchUpInside];
    cell.backgroundColor=[UIColor redColor];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
    //	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		return cell;
	}
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
  
  	if (indexPath.row == 0) {
		return NO;
	}
	else {
		return YES;
	}
}


- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
{
    if (proposedDestinationIndexPath.row==0) {
        NSLog(@"Prevent move to row 0");
        
    
        
              return [NSIndexPath indexPathForRow:1 inSection:0];     
    }    
    
    return proposedDestinationIndexPath;
}


- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
	
    NSLog(@"moved cell, %i, %i", sourceIndexPath.row, sourceIndexPath.section);
    NSLog(@"to, %i, %i", destinationIndexPath.row, destinationIndexPath.section);

    
   	if(popupOpen){
		[selection removeFromSuperview];
		[btn removeFromSuperview];
		popupOpen=NO;
	}
    

     NSLog(@"moved cell complete");
    
	
	NSDictionary *dict = [[dataArray objectAtIndex:sourceIndexPath.row-1] retain];
	[dataArray removeObjectAtIndex:sourceIndexPath.row-1];
	[dataArray insertObject:dict atIndex:destinationIndexPath.row-1];
	[dict release];

    NSLog(@"dictionary update complete");
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
	if(popupOpen){
		[selection removeFromSuperview];
		[btn removeFromSuperview];
		popupOpen=NO;
	}
    
    NSLog(@"Commit editing style");
	
	for (int i = indexPath.row; i <= dataArray.count; i++) {
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
		EditAsanaTableCell *cell = (EditAsanaTableCell *)[table cellForRowAtIndexPath:indexPath];
		cell.selection.tag = i-2;
	}
	
	NSDictionary *dict = [dataArray objectAtIndex:indexPath.row-1];
	NSNumber *poseID = [dict objectForKey:@"ID"];
	//	NSNumber *stepCount = [dict objectForKey:@"stepCount"];
	[dbHandle() executeUpdate:@"delete from MyAsanaPoses where ID = ?", [poseID stringValue]];
	[dataArray removeObjectAtIndex:indexPath.row-1];
	//	allPoseStepCount -= [stepCount intValue] * [repeatCount intValue];
	
	[self setDurationLB];
	[table deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
	table.frameSizeHeight = 380;//MIN(kTableMaxHeight+10, kTableCellHeight*dataArray.count);
    
    
    NSTimer *zeroCheckTimer = [NSTimer scheduledTimerWithTimeInterval:0.3
                                                         target:self 
                                                       selector:@selector(zeroCheck) 
                                                       userInfo:nil 
                                                        repeats:NO];
    
    

    
	
}


#pragma mark -
#pragma mark --- UITableView delegate ---
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.row == 0) {
		return UITableViewCellEditingStyleNone;
	}
	else {
		return UITableViewCellEditingStyleDelete;
	}
	
    //return UITableViewCellEditingStyleDelete;
    
}

#pragma mark  --- IBAction ---
- (IBAction)editTitle {
    
    NSLog(@"Edit title local");
	//titleBtn.hidden = YES;
	//titleTF.hidden = NO;
	//nameCache=[[NSString alloc] initWithString:titleTF.text];
	
    

    
    
	//titleTF.text=@"";
	//[titleTF becomeFirstResponder];
	[pickerView resignFirstResponder];
	timeOpen=NO;
	
    NSLog(@"Edit title done");
	//	doneBtn.enabled = NO;
}
- (IBAction)settingDone {
	for (int i = 0; i < dataArray.count; i++) {
		NSDictionary *dict = [dataArray objectAtIndex:i];
		NSNumber *poseID = [dict objectForKey:@"ID"];
		[dbHandle() executeUpdate:@"update MyAsanaPoses set PoseIndex = ? where ID = ?",
		 stringFromInt(i + 1),[poseID stringValue]];
	}
	MyAsanaPosesViewContorller *vc = (MyAsanaPosesViewContorller *)[self viewControllerWithGoBackCount:1];
	//	vc.durationLB.text = durationLB.text;
	//	vc.totalDuration = totalDuration;
	vc.dataArray = self.dataArray;
	[self.navigationController popViewControllerAnimated:NO];
	
}
- (IBAction)texFildInputDone {
	
	if (titleTF.text == nil|| titleTF.text.length < 3) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyAlertViewOnlyOkBtn" owner:nil options:nil];
		MyAlertViewOnlyOkBtn *myAlert = [nib lastObject];
		myAlert.titleLB.text = @"\"Name Needed\"";
		myAlert.messageLB.text = @"Your name needs to contain 3 characters or more.";
		[self.view addSubview:myAlert];
		[myAlert showDialog];
		
	}
	
	}

- (IBAction)setIntervalTime {
	
    NSLog(@"Set interval time here");
	if(!timeOpen){
		timeOpen=YES;
		FirstEditAsanaTableCell *firstCell = (FirstEditAsanaTableCell*)[table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
		[firstCell.sequenceTitle resignFirstResponder];
		
		intervalTimeBtn.selected = YES;
		intervalTimeBtn.userInteractionEnabled = NO;
		
		if (pickerView == nil) {
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyPickerView" owner:nil options:nil];
			pickerView = [[nib objectAtIndex:0] retain];
			pickerView.pickerDelegate = self;
		}
		
		
		[pickerView.picker selectRow:intervalMin inComponent:0 animated:NO];
		[pickerView.picker selectRow:intervalSec/15 inComponent:1 animated:NO];
		[pickerView moveIn:self.view from:kBottom duration:0.5];
	}
}
- (IBAction)setRepeatCount:(id)sender {
    
    NSLog(@"set repeat count");
	if(!popupOpen) {
        
        NSLog(@"open Popup");
	
        UIButton *repeatBtn = sender;
        
        EditAsanaTableCell *cell = (EditAsanaTableCell *)[[repeatBtn superview] superview];
        
        
		
		selectedRow =[table indexPathForCell:cell].row-1;
        
        NSLog(@"POPUP row=%i", selectedRow);
		
		// add mask layer
        
		btn = [UIButton buttonWithType:UIButtonTypeCustom];
		[btn setImage:[UIImage imageNamed:@"editMask2.png"] forState:UIControlStateNormal];
		btn.frame = CGRectMake(0, 0, 320, 460);
		[btn addTarget:self action:@selector(popupCancelled) forControlEvents:UIControlEventTouchUpInside];
		btn.alpha=0.6;
         [btn setAdjustsImageWhenHighlighted:NO];
		[self.view addSubview:btn];

        //add mask
		btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
		[btn2 setImage:[UIImage imageNamed:@"editMask3.png"] forState:UIControlStateNormal];
		btn2.frame = CGRectMake(0, 0, 320, 138);
        [btn2 setAdjustsImageWhenHighlighted:NO];
		[btn2 addTarget:self action:@selector(popupCancelled) forControlEvents:UIControlEventTouchUpInside];
		btn2.alpha=0.6;
		[table addSubview:btn2];
        
        
    
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MySegmentControl" owner:nil options:nil];
		selection = [nib objectAtIndex:0];
		selection.delegate = self;
        
     
   
		CGPoint tableOffSet = table.contentOffset;
		float offSetY = tableOffSet.y;
		
		
			
			// EDIT IT 
			float yPosition = 238+(selectedRow*65) - offSetY;
			
			if (yPosition < 380) {
				selection.frameOrigin = CGPointMake(kPopupNumberSelectorXPosition, yPosition);
			}
			else {
				selection.background.image = [UIImage imageNamed:@"mysegmentcontrolRotated.png"];
				selection.frameOrigin = CGPointMake(kPopupNumberSelectorXPosition, yPosition - 90);
			}
			
			//[table addSubview:selection];
			[self.view addSubview:selection];
			//[self.view bringSubviewToFront:table];
        
        	popupOpen=YES;
    
		
	}
    else{
        
        NSLog(@"Popup already open");
    }
}

- (void)popupCancelled{
	
	[selection removeFromSuperview];
	[btn removeFromSuperview];
    [btn2 removeFromSuperview];
	popupOpen=NO;
	
}
- (IBAction)hideKeyboard {
	[titleTF resignFirstResponder];
}


#pragma mark --- MySegmentControl Delegate ---
- (void)selectedNumber:(int)num {
    NSLog(@"selectedNumber");
	[selection removeFromSuperview];
	[btn removeFromSuperview];
    [btn2 removeFromSuperview];

		popupOpen=NO;
	
	
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedRow+1 inSection:0];
	
    
    EditAsanaTableCell *cell = (EditAsanaTableCell *)[table cellForRowAtIndexPath:indexPath];
	cell.repeatCountLB.text = [NSString stringWithFormat:@"x%i", num];
	
    
    //[cell.selection addTarget:self action:@selector(setRepeatCount:) forControlEvents:UIControlEventTouchUpInside];
    
	
	NSLog(@"update cell");
	
	//selectedRow--;
	NSMutableDictionary *dict = [dataArray objectAtIndex:selectedRow];
    
	
	NSLog(@"selected Row");
	
	NSNumber *ID = [dict objectForKey:@"ID"];
	
	NSLog(@"set ID");
	
	
	[dict setObject:[NSNumber numberWithInt:num] forKey:@"repeatCount"];
	
	
	
	[dbHandle() executeUpdate:@"update MyAsanaPoses set RepeatCount = ? where ID = ?", stringFromInt(num), [ID stringValue]];
	
	NSLog(@"set dict");
	
	
	[self setDurationLB];
	
	NSLog(@"set label");

    
}



#pragma mark --- Picker dataSource ---
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 2;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {

		return (0 == component) ? 10 : 4;
	
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
/*	if (intervalMin == 0) {
		return (0 == component) ? [NSString stringWithFormat:@"%i min",row] : [NSString stringWithFormat:@"%i sec", 45];
	}
	else {*/
		return (0 == component) ? [NSString stringWithFormat:@"%i min",row] : [NSString stringWithFormat:@"%i sec", row*15];
	//}
}


#pragma mark --- picker delegate ---
- (void)pickerView:(UIPickerView *)_pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    NSLog(@"save picker view duration");
	if (0 == component) {
		intervalMin = row;
        intervalSec = 15 * [_pickerView selectedRowInComponent:1];
	}
	else {
		intervalSec = 15 * row;
	}
	
	NSString *intervalString;
	if (intervalMin == 0) {
		intervalSeconds = intervalSec;
		intervalString = [NSString stringWithFormat:@"%i sec", intervalSec];
	}
	else {
		intervalSeconds = intervalMin * 60 + intervalSec;
		intervalString = [NSString stringWithFormat:@"%i:%02i min", intervalSeconds/60, intervalSeconds%60];
	}
	
	FirstEditAsanaTableCell *firstCell = (FirstEditAsanaTableCell*)[table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	firstCell.intervalLB.text = intervalString;
	
	
	[dbHandle() executeUpdate:@"update MyAsana set intervalTime = ? where ID = ?"
	 ,stringFromInt(intervalSeconds) , stringFromInt(asanaID)];
	[asanaDict setObject:[NSNumber numberWithInt:intervalSeconds] forKey:@"intervalTime"];
	
	[self setDurationLB];
	
	[_pickerView reloadComponent:1];
}

- (void)pickerDone {
    
    NSLog(@"picker done");
    
	timeOpen = NO;
	intervalTimeBtn.selected = NO;
	intervalTimeBtn.userInteractionEnabled = YES;
}

@end

//
//  PickerActionSheet.m
//  iAsthma
//
// Created by Rob on  8/7/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "PickerActionSheet.h"


@implementation PickerActionSheet

@synthesize picker;

- (id)initWithTitle:(NSString *)title delegate:(id <UIPickerViewDelegate, UIPickerViewDataSource, UIActionSheetDelegate >)delegate
	cancelButtonTitle:(NSString *)cancelButtonTitle
	destructiveButtonTitle:(NSString *)destructiveButtonTitle
	otherButtonTitles:(NSString *)otherButtonTitles, ... {
	
	self = [super initWithTitle:title delegate:delegate cancelButtonTitle:cancelButtonTitle 
		 destructiveButtonTitle:destructiveButtonTitle otherButtonTitles:otherButtonTitles, nil];
	if (self) {
		UIPickerView *p = [[UIPickerView alloc] initWithFrame:CGRectMake(0, -160, 320, 300)];
		[self addSubview:p];
		p.dataSource = delegate;
		p.delegate = delegate;
		self.picker = p;
		p.showsSelectionIndicator = YES;
		[p release];
	}
	return self;
}

- (void)dealloc {
	[picker release];
    [super dealloc];
}

/*
 *	Determine maximum y-coordinate of UILabel objects. This method assumes that only
 *	following objects are contained in subview list:
 *	- UILabel
 *	- UITextField
 *	- UIThreePartButton (Private Class)
 */
- (CGFloat) maxLabelYCoordinate {
	// Determine maximum y-coordinate of labels
	CGFloat maxY = 0;
	for( UIView *view in self.subviews ){
		if([view isKindOfClass:[UILabel class]]) {
			CGRect viewFrame = [view frame];
			CGFloat lowerY = viewFrame.origin.y + viewFrame.size.height;
			if(lowerY > maxY)
				maxY = lowerY;
		}
	}
	return maxY;
}

- (void)layoutSubviews {
	[super layoutSubviews];
	
	if(!layoutDone) {
		CGRect frame = [self frame];
		CGFloat alertWidth = frame.size.width;
		CGFloat pickerHeight = self.picker.frame.size.height;
		CGFloat labelMaxY = [self maxLabelYCoordinate];
		
		// Insert UIPickerView below labels and move other fields down accordingly
		for(UIView *view in self.subviews){
		    if([view isKindOfClass:[UIPickerView class]]){
				CGRect viewFrame = CGRectMake(0, labelMaxY, alertWidth, pickerHeight);
				[view setFrame:viewFrame];
		    } else if(![view isKindOfClass:[UILabel class]]) {
				CGRect viewFrame = [view frame];
				viewFrame.origin.y += pickerHeight;
				[view setFrame:viewFrame];
			}
		}
		
		// size UIAlertView frame by height of UIPickerView
		frame.size.height += pickerHeight + 2.0;
		frame.origin.y -= pickerHeight + 2.0;
		[self setFrame:frame];
		layoutDone = YES;
	}
}


@end

//
//  FirstEditAsanaTableCell.m
//  Yoga
//
// Created by Rob on  12/15/10.
//  
//

#import "FirstEditAsanaTableCell.h"
#import "MyAlertViewOnlyOkBtn.h"

@implementation FirstEditAsanaTableCell

@synthesize sequenceTitle, titleBtn, intervalTimeBtn, intervalLB;
@synthesize target, titleBtnSelector, intervalTimeBtnSelector, asanaID, asanaDict;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code.
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state.
}


- (void)dealloc {
//	[sequenceTitle release];
	[titleBtn release];
	[intervalTimeBtn release];
	[intervalLB release];
	
    [super dealloc];
}

- (IBAction)titleBtnTouch {
    
    NSLog(@"Title button touch");
	titleBtn.hidden = YES;
	sequenceTitle.hidden = NO;
	
	[sequenceTitle becomeFirstResponder];
	//[intervalTimeBtn resignFirstResponder];
	
	if ([target respondsToSelector:titleBtnSelector]) {
        
        NSLog(@"has slector");   
		[target performSelector:titleBtnSelector];
    }
    else{
        
        NSLog(@"no slector");
    }
    
    NSLog(@"Title button touch DONE");
}

- (IBAction)intervalTimeBtnTouch {
	[self titleTexFildInputDone];
	
	if ([target respondsToSelector:intervalTimeBtnSelector]) 
		[target performSelector:intervalTimeBtnSelector];
}
- (IBAction)titleTexFildInputBegan{
    
    sequenceTitleCache = [[NSString alloc] initWithString:sequenceTitle.text];
    
}



- (IBAction)titleTexFildInputDone {
	
	if (sequenceTitle.text == nil|| sequenceTitle.text.length < 3) {
		
	//	sequenceTitle.hidden = YES;
	//	[titleBtn setTitle:sequenceTitle.text forState:UIControlStateNormal];
	//	titleBtn.hidden = NO;
	
		sequenceTitle.text=sequenceTitleCache;
		
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyAlertViewOnlyOkBtn" owner:nil options:nil];
		MyAlertViewOnlyOkBtn *myAlert = [nib lastObject];
		myAlert.titleLB.text = @"\"Name Needed\"";
		myAlert.messageLB.text = @"Sequence name needs to contain 3 characters or more.";
		[[[self superview] superview] addSubview:myAlert];
		[myAlert showDialog];
		
		
		return;
	}
	else {
		[sequenceTitle resignFirstResponder];
		[dbHandle() executeUpdate:@"update MyAsana set Name = ? where ID = ?"
		 ,sequenceTitle.text , stringFromInt(asanaID)];
		[asanaDict setObject:sequenceTitle.text forKey:@"asanaName"];
	//	sequenceTitle.hidden = YES;
		[titleBtn setTitle:sequenceTitle.text forState:UIControlStateNormal];
	//	titleBtn.hidden = NO;
	}
	 
}

-(IBAction)titleTexFildInputStarted{
    
}


@end

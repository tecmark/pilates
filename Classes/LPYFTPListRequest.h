//
//  LPYFTPListRequest.h
//  Yoga
//
// Created by Rob on 11/17/10.
//  
//

#import <Foundation/Foundation.h>


@interface LPYFTPListRequest : NSObject <NSStreamDelegate> {
	NSString* _urlString;
	
	NSString* ftpUsername;
	NSString* ftpPassword;
	
	NSInputStream *             _networkStream;
    NSMutableData *             _listData;
    NSMutableArray *            _listEntries;           // of NSDictionary as returned by CFFTPCreateParsedResourceListing
    NSString *                  _status;
	
	BOOL shouldKeepRunning; 
}

@property (nonatomic, copy) NSString* urlString;
@property (nonatomic, copy) NSString* ftpUsername;
@property (nonatomic, copy) NSString* ftpPassword;


- (id)initWithURLString:(NSString*)_urlStr;

- (NSArray*)list;

@end

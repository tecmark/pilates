//
//  MyAsanaPosesViewContorller.h
//  Yoga
//
// Created by Rob on 10-3-26.
//  
//

#import <UIKit/UIKit.h>
#import "MyAsanaViewController.h"
@class MyAsanaViewController;

@interface MyAsanaPosesViewContorller : MyAsanaViewController  {
	NSMutableDictionary *asanaDict;
	IBOutlet UIButton *settingBtn;
	UIView *settingView;
	
	//	IBOutlet UIButton	 *musicSwitchBtn;
	IBOutlet UILabel *durationLB;
    
    IBOutlet UIButton *playBtn;
	
	//duration
//	int allPoseStepCount;
	int totalDuration;
}

@property (nonatomic, retain) NSMutableDictionary *asanaDict;
//@property (nonatomic, retain) UILabel *durationLB;
@property int totalDuration;
//- (void)setDurationString:(NSString *)string;
- (IBAction)play; 
- (IBAction)setting;

@end

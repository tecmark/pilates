//
//  SoundManager.h
//  
//

#import <Foundation/Foundation.h>

void SGInitSounds(void);
void SGPlaySound(int soundNum);
void SGReleaseSounds(void);
BOOL checkMute(void);
void setMute(BOOL mute);
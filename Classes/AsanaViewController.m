//
//  AsanaViewController.m
//  
//
// Created by Rob on 12/15/09.
//  
//

#import "AsanaViewController.h"
#import "SelectPoseViewController.h"
#import "MyAsanaViewController.h"
#import "FavoritesViewController.h"

@implementation AsanaViewController


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.switchViewContorllerNameArray = [NSArray arrayWithObjects:
										  @"MyAsanaViewController",
										  @"FavoritesViewController",
										  nil];
	
	scrollView.contentSize=CGSizeMake(320, 550);
	scrollView.scrollEnabled=YES;
	//scrollView.backgroundColor=[UIColor redColor];
	//scrollView.hidden=YES;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

- (IBAction)back {
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)gotoSelectPose:(id)sender {
	SelectPoseViewController *vc = [SelectPoseViewController alloc];
	vc.poseCatalogID = [sender tag];
	[vc initWithNibUsingClassName];
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
	
}
@end

//
//  MyAsanaTableCell.m
//  Yoga
//
// Created by Rob on 10-4-21.
//  
//

#import "MyAsanaTableCell.h"


@implementation MyAsanaTableCell
@synthesize titleTF;
@synthesize poseIV;
@synthesize editBtn;
@synthesize divider;
@synthesize arrow;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:NO animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
	[titleTF release];
	[poseIV release];
	[editBtn release];
	[divider release];
	[arrow release];
	
    [super dealloc];
}

@end

//
//  MyAsanaSettingViewController.h
//  Yoga
//
// Created by Rob on 10-4-20.
//  
//

#import <UIKit/UIKit.h>
#import "MySegmentControl.h"
#import "MyPickerView.h"
@interface MyAsanaSettingViewController : CommonTableViewController 
<MySegmentControlDelegate, UIPickerViewDelegate, UIPickerViewDataSource, MyPikerViewDelegate>{
	//setting
	IBOutlet UITextField *titleTF;
	IBOutlet UIButton	*titleBtn;
	IBOutlet UIButton   *intervalTimeBtn;
	
	NSMutableArray *poseArray;
	NSMutableDictionary *asanaDict;
	int intervalSeconds;
	IBOutlet UILabel *durationLB;
	NSString *durationStr;
//	int allPoseStepCount;
	int totalDuration;
	int asanaID;
	MySegmentControl *selection;
	int selectedRow;
	int intervalMin;
	int intervalSec;
	IBOutlet UIButton *doneBtn;
	IBOutlet UILabel *intervalLB;
	bool keyboardShown;
	IBOutlet UIButton *keyboardDoneBtn;
	
	MyPickerView *pickerView;
    
    IBOutlet UIImageView *noPoses;
    IBOutlet UILabel *l1;
    IBOutlet UILabel *l2;
    
    
} 
@property (nonatomic,retain) NSMutableDictionary *asanaDict;
@property (nonatomic,retain) NSMutableArray *poseArray;
@property (nonatomic,retain) NSString *durationStr;
//@property int allPoseStepCount;
@property int totalDuration;
- (IBAction)setIntervalTime;
//- (IBAction)setMute;
- (IBAction)texFildInputDone;
- (IBAction)settingDone;
- (IBAction)editTitle;
- (IBAction)hideKeyboard;
@end

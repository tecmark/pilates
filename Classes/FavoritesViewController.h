//
//  FavoritesViewController.h
//  Yoga
//
// Created by Rob on 10-3-23.
//  
//

#import <UIKit/UIKit.h>


@interface FavoritesViewController : CommonFavoritesTableViewController {

	
	IBOutlet UIButton *favButton;
}


- (IBAction)favoriteCellTouched:(id)sender;

@end

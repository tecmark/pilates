//
//  Common.h
//  
//
// Created by Rob on 11/5/09.
//  
//

#import <Foundation/Foundation.h>

#define kUserDataFile @"UserData.plist"
#define kDataBaseFile @"QuikkaYoga.sqlite"

NSString * pathForResource (NSString *file);
NSString * pathOfWritableFile(NSString *file);
void showAlertViewWithMessage (NSString *message);
NSString * dataFilePath();
NSString * dataBasePath();
NSMutableDictionary* dataDict();

UIImage * imageWithPrefixAndNum (NSString *prefix, int num);
NSString *stringFromInt(int num);
NSString *stringFromFloat(double num);

@interface UIViewController (LBCommon)
- (UIViewController *)viewControllerWithGoBackCount:(int)num;
@end

#pragma mark --- special methods ---
NSString * todayStr();
NSDate * todayDate();


NSString* fileSizeToString(double bytes); 

NSURL* smartURLForString(NSString* str);
//
//  MyAsanaViewController.h
//  Yoga
//
// Created by Rob on 10-3-23.
//  
//

#import <UIKit/UIKit.h>


@interface MyAsanaViewController : CommonTableViewController {
	IBOutlet UIButton *addBtn;
	IBOutlet UIButton *doneBtn;
//	bool isEditMode;
	IBOutlet UIButton *editBtn;
	
	int previousNameEdit;
    CGPoint preScrollPoint;
    BOOL isNameEdit;
    UITextField *editField;
    BOOL showEditBtn;
    
}

- (IBAction)addRow;
- (IBAction)editTable;
- (IBAction)editTableDone;

@end

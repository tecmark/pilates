//
//  EmailProvider.m
//  iHaveissues
//
// Created by Rob on  7/17/09.
//

#import "EmailProvider.h"

@interface EmailProvider(Private)

-(void)showPicker;
-(void)displayComposerSheet;
-(void)launchMailAppOnDevice;
-(void)setAttachment;

@end


@implementation EmailProvider

@synthesize delegate, subject, emailBody, toRecipients, ccRecipients, bccRecipients, htmlFormat, navigationController, attachments;


#pragma mark -
#pragma mark Memory management

- (void)dealloc {
	[delegate release];
	[subject release];
	[emailBody release];
	[toRecipients release];
	[ccRecipients release];
	[bccRecipients release];
	[attachments release];
	[super dealloc];
}

+(void)sendEmailFromViewControler:(UIViewController *)viewController 
									to:(NSArray*)to 
									cc:(NSArray*)cc 
								   bcc:(NSArray*)bcc
						   attachments:(NSData*)attachs
							   subject:(NSString*)subj
								  body:(NSString*) body
							htmlFormat:(BOOL)isHtml {
    
	
	EmailProvider *provider = [[self alloc] init];
	provider.delegate = viewController;
	provider.navigationController = viewController.navigationController;
	provider.toRecipients = to;
	provider.ccRecipients = cc;
	provider.bccRecipients = bcc;
	provider.attachments = attachs;
	
	provider.subject = subj;
	provider.emailBody = body;
	provider.htmlFormat = isHtml;
	[provider showPicker];
	[provider autorelease];
	
}

-(void)showPicker {
	// This can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheet];
		}
		else
		{
			[self launchMailAppOnDevice];
		}
	}
	else
	{
		[self launchMailAppOnDevice];
	}
}

#pragma mark -
#pragma mark Compose Mail

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet {
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	
//	picker.navigationBar.barStyle = UIBarStyleBlack;
	picker.navigationItem.title = [self subject];
	picker.mailComposeDelegate = self.delegate;
	
	[picker setToRecipients:self.toRecipients];
	
	[picker setCcRecipients:self.ccRecipients];
	
	[picker setBccRecipients:self.bccRecipients];
	
	//Attach the images
	if(self.attachments != nil)
		[picker addAttachmentData:attachments mimeType:@"image/png" fileName:nil];
	
	[picker setSubject:[self subject]];
	
	// Fill out the email body text
	[picker setMessageBody:emailBody isHTML:YES];
	
	[navigationController presentModalViewController:picker animated:YES];
//	[delegate presentModalViewController:picker animated:YES];
    [picker release];
}


#pragma mark -
#pragma mark Workaround

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice {
	NSString *recipients = @"mailto:sample@email.com?&subject=Assessment";
	
	NSString *email = [NSString stringWithFormat:@"%@&body=%@", recipients, emailBody];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

@end

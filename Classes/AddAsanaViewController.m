//
//  AddAsanaViewController.m
//  Yoga
//
// Created by Rob on 10-3-26.
//  
//

#import "AddAsanaViewController.h"
#import "AddAsanaPoseViewController.h"
#import "PickerActionSheet.h"
#import "MyAlertViewOnlyOkBtn.h"

@implementation AddAsanaViewController

BOOL timeOpen;


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	timeOpen=NO;
	pickerView = nil;
    [super viewDidLoad];
	titleLB.text = @"New Sequence";
	//set the default interval time.
	intervalMin = 0;
	intervalSec = 0;
	intervalSeconds = 0;
}

- (void)viewDidAppear:(BOOL)animated {
//	[titleTF becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[pickerView release];
    [super dealloc];
}
/*
- (IBAction)setMute {
	musicSwitchBtn.selected = !musicSwitchBtn.selected;
	
}
 
*/
- (IBAction)editTitle {
	titleBtn.hidden = YES;
	titleTF.hidden = NO;
	[titleTF becomeFirstResponder];
	[pickerView resignFirstResponder];
//	nextBtn.enabled = NO;
}
- (IBAction)addAsanaDone {
	
	id<PLResultSet> result;
	result = [dbHandle() executeQuery:@"select * from MyAsana order by AsanaIndex DESC"];
	int index = 0;
    
	while ([result next]) {
		NSNumber *asanaIndex = [result objectForColumn:@"AsanaIndex"];
		if ([asanaIndex isKindOfClass:[NSNumber class]]) {
			index = [asanaIndex intValue];
		}
        
        // increment AsanaIndex
        
        int newIndex = index+1;
    //    NSLog([NSString stringWithFormat:@"update MyAsana set AsanaIndex = %@ where ID = %@",stringFromInt(newIndex),[[result objectForColumn:@"ID"] stringValue]]);
               
            [dbHandle() executeUpdate:@"update MyAsana set AsanaIndex = ? where ID = ?",
             stringFromInt(newIndex),[result objectForColumn:@"ID"]];
	}
    
	[result close];

    //save to top of list
    index=0;

    
	[dbHandle() executeUpdate:@"insert into MyAsana(Name, intervalTime, AsanaIndex) values(?, ?, ?)"
	 ,titleTF.text , stringFromInt(intervalSeconds), stringFromInt(index + 1)];
	
	int asanaID;
	result = [dbHandle() executeQuery:@"select ID from MyAsana where AsanaIndex = ?", stringFromInt(index + 1)];
	if ([result next]) {
		asanaID = [result intForColumn:@"ID"];
	}
	[result close];
	AddAsanaPoseViewController *vc = [AddAsanaPoseViewController alloc];
	vc.asanaName = titleTF.text;
	vc.selectedPoseArray = [NSMutableArray array];
	vc.isFromAddAsana = YES;
	vc.asanaID = asanaID;
	[vc initWithNibUsingClassName];
	[self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)texFildInputDone {
	if (titleTF.text == nil) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyAlertViewOnlyOkBtn" owner:nil options:nil];
		MyAlertViewOnlyOkBtn *myAlert = [nib lastObject];
		myAlert.titleLB.text = @"\"Name Needed\"";
		myAlert.messageLB.text = @"Your name needs to contain 3 characters or more.";
	
		[self.view addSubview:myAlert];
		[myAlert showDialog];

		//titleTF.hidden = YES;
		//titleBtn.hidden = NO;
		
		//[titleTF becomeFirstResponder];
		
		return;
	}
	else {
		[titleTF resignFirstResponder];
	//	titleTF.hidden = YES;
		[titleBtn setTitle:titleTF.text forState:UIControlStateNormal];
		titleBtn.hidden = NO;
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.6];
		nextBtn.alpha=1;
		[UIView commitAnimations];
	}
}
- (IBAction)setIntervalTime {
	
	if(!timeOpen){
		timeOpen=YES;
		[titleTF resignFirstResponder];
		if (pickerView == nil) {
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyPickerView" owner:nil options:nil];
			pickerView = [[nib objectAtIndex:0] retain];
			pickerView.pickerDelegate = self;
		}
		
		[pickerView.picker selectRow:intervalMin inComponent:0 animated:NO];
		if (intervalMin == 0) {
			[pickerView.picker selectRow:0 inComponent:1 animated:NO];
		}
		else {
			[pickerView.picker selectRow:intervalSec/15 inComponent:1 animated:NO];
		}

		[pickerView moveIn:self.view from:kBottom duration:0.5];
	}
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
	
	if([titleTF.text length] > 0){
	
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.6];
		nextBtn.alpha=1;
		[UIView commitAnimations];
		
	}else{
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.6];
		nextBtn.alpha=0;
		[UIView commitAnimations];
	}
	return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	[pickerView resignFirstResponder];
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.6];
	nextBtn.alpha=0;
	[UIView commitAnimations];
	timeOpen=NO;
	return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	/*
	if (titleTF.text == nil|| titleTF.text.length < 3) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyAlertViewOnlyOkBtn" owner:nil options:nil];
		MyAlertViewOnlyOkBtn *myAlert = [nib lastObject];
		myAlert.titleLB.text = @"\"Name Needed\"";
		myAlert.messageLB.text = @"Your name needs to contain 3 characters or more.";
		
		[self.view addSubview:myAlert];
		[myAlert showDialog];
		
		//titleTF.hidden = YES;
		//titleBtn.hidden = NO;
		
		return YES;
	}
	else {
		[titleTF resignFirstResponder];
		titleTF.hidden = YES;
		[titleBtn setTitle:titleTF.text forState:UIControlStateNormal];
		titleBtn.hidden = NO;
		nextBtn.hidden = NO;
		return YES;
	}
	 */
	return YES;
}


#pragma mark --- Picker dataSource ---
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 2;
}

- (NSInteger)pickerView:(UIPickerView *)_pickerView numberOfRowsInComponent:(NSInteger)component {
	

		return (0 == component) ? 10 : 4;
	

}

- (NSString *)pickerView:(UIPickerView *)_pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {

		return (0 == component) ? [NSString stringWithFormat:@"%i min",row] : [NSString stringWithFormat:@"%i sec", row*15];
	

}


#pragma mark --- picker delegate ---
- (void)pickerView:(UIPickerView *)_pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	if (0 == component) {
		intervalMin = row;
		
	
			intervalSec = 15 * [_pickerView selectedRowInComponent:1];
		
	}
	else {
		intervalSec = 15 * row;
	}
	
	if (intervalMin == 0) {
		intervalSeconds = intervalSec;
//		[intervalTimeBtn setTitleForAllState:[NSString stringWithFormat:@"%isec", intervalSec]];
		intervalLB.text = [NSString stringWithFormat:@"%i sec", intervalSec];
	}
	else {
		intervalSeconds = intervalMin * 60 + intervalSec;
//		[intervalTimeBtn setTitleForAllState:[NSString stringWithFormat:@"%i:%02imin", intervalSeconds/60, intervalSeconds%60]];
		intervalLB.text = [NSString stringWithFormat:@"%i:%02i min", intervalSeconds/60, intervalSeconds%60];
	}
	[_pickerView reloadComponent:1];
	
	
}

- (void)pickerDone {
	timeOpen=NO;
	intervalTimeBtn.selected = NO;
	intervalTimeBtn.userInteractionEnabled = YES;
}

@end

//
//  CommentRate.m
//  Yoga
//
// Created by Rob on 11/17/10.
//  
//

#import "CommentRate.h"


@implementation CommentRate


@synthesize rate, title, comment, submittedDate;


+ (id)commentRateWithRate:(NSInteger)_rate title:(NSString*)_title comment:(NSString*)_comment submittedDate:(NSString*)_submittedDate {
	return [[[CommentRate alloc] initWithRate:_rate title:_title comment:_comment submittedDate:_submittedDate] autorelease];
}


- (id)initWithRate:(NSInteger)_rate title:(NSString*)_title comment:(NSString*)_comment submittedDate:(NSString*)_submittedDate {
	self = [super init];
	
	if (self != nil) {
		self.rate = _rate;
		self.title = _title;
		self.comment = _comment;
		self.submittedDate = _submittedDate;
	}
	
	return self;
}

- (void)dealloc {
	[title release];
	[comment release];
	[submittedDate release];
	[super dealloc];
}

@end

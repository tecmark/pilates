//
//  GlobalVariables.m
//  
//
// Created by Rob on 03/25/10.
//  
//

#import "GlobalVariables.h"
#import "Common.h"
 
static PLSqliteDatabase *g_dbHandle = nil;

PLSqliteDatabase * dbHandle () {
	if (nil == g_dbHandle) {
		NSString *path = dataBasePath();
		g_dbHandle = [[PLSqliteDatabase alloc] initWithPath:path];
		if (![g_dbHandle open]) {
			NSLog(@"Could not open database");
		}
	}

	return g_dbHandle;
}
void ReleaseGlobalVariables() {
	if (nil != g_dbHandle) {
		[g_dbHandle release];
	}
	
}

//
//  NewClassLoader.h
//  Yoga
//
// Created by Rob on  11/30/10.
//  
//

#import <Foundation/Foundation.h>
#import "YogaClass.h"

@interface NewClassLoader : NSOperation {
	UAProduct *product;
	
	BOOL executing; 
	BOOL finished;
	
	YogaClass *newClass;
}

@property (nonatomic, assign) UAProduct *product;
@property (nonatomic, retain) YogaClass *newClass;

- (id)initWithProduct:(UAProduct*)_product;


- (void)addObserver:(id)observer;
- (void)removeObserver:(id)observer;

@end

//
//  RateCommentsHelper.h
//  Yoga
//
// Created by Rob on 11/17/10.
//  
//

#import <Foundation/Foundation.h>
#import "UAGlobal.h"

@interface RateCommentsHelper : NSObject {

}


SINGLETON_INTERFACE(RateCommentsHelper)


- (void)saveCommentsRatesToDataBaseWithClassID:(NSString*)classID commentsRates:(NSArray*)commentsRates;
- (void)loadCommentsRatesFromServerWithClassID:(NSString*)classID;
- (void)updateRateCommentsFromServerForAllClasses;
- (NSArray*)getCommentsFromServerWithClassID:(NSString*)classID averageRate:(NSInteger*)rate;
- (NSArray*)getCommentsFromDatabaseWithClassID:(NSString*)classID averageRate:(NSInteger*)rate;
- (void)saveRate:(NSInteger)rate title:(NSString*)title comment:(NSString*)comment forClass:(NSString*)classID;
- (NSArray*)getCommentRateWithClassID:(NSString*)classID;


@end

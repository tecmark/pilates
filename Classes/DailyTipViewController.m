//
//  DaliTpViewController.m
//  
//
// Created by Rob on 03/25/10.
//  
//

#import "DailyTipViewController.h"
#import "PoseDetailViewController.h"

#define kMaxTipCount 10
@implementation DailyTipViewController
@synthesize currentPageNumber;


#pragma mark -- helper Method ---

-(void) setTipPoseName: (NSString *) name andContent: (NSString *) contentString andHeadline: (NSString *) headlineString andDate:(NSDate *)date
{
	
	headlineLB.text=headlineString;
	poseNameLB.text = [[[[name capitalizedString] stringByReplacingOccurrencesOfString:@"Iii" withString:@"III"]stringByReplacingOccurrencesOfString:@"Ii" withString:@"II"]stringByReplacingOccurrencesOfString:@"Iv" withString:@"IV"];
	tipTV.text = contentString;
	CGSize size = CGSizeMake(tipTV.frameSizeWidth, INT_MAX);
	size = [contentString sizeWithFont:tipTV.font constrainedToSize:size];
	tipTV.frameSizeHeight = size.height;
	int pageHeight = MAX(420, tipTV.frameOriginY+tipTV.frameSizeHeight+10);
	wholeScrollView.contentSize = CGSizeMake(320, pageHeight);
	wholeScrollView.scrollEnabled = YES;
	NSDateFormatter *formatter = [NSDateFormatter new];
	[formatter setDateFormat:@"EEE MMM dd"];
	NSString *dateStr = [formatter stringFromDate:date];
	tipDateLB.text = dateStr;
	[formatter release];
	
}

/*
-(BOOL) resetReadFlag
{
    BOOL bOK = FALSE;
    
    bOK = [dbHandle() executeUpdate: @"Update Tips set IsRead = 0"];
    
    if(! bOK)
    {
        NSLog(@"Update Tips failed, set IsRead = 1");
    }
    
    return bOK;
}
*/
-(BOOL) setTipIsReadWithAccessDate:(NSTimeInterval )since1970
{
	NSString *timeSince1970 = [NSString stringWithFormat:@"%f", since1970];
    
	BOOL bOK = [dbHandle() executeUpdate: @"Update Tips set IsRead = 1, AccessDate = ? where ID = ?", timeSince1970,[tipID stringValue]];
    
    return bOK;
}

-(void) getPoseImageNameWithTipID:(int)thePoseID
{
    poseImgName = nil;
	poseName = nil;
	id<PLResultSet> result;
    result = [dbHandle() executeQuery: @"SELECT * FROM Poses where ID = ?", [NSString stringWithFormat:@"%i", thePoseID]];
    
    if([result next])
    {
		
		
		poseImgName = [result stringForColumn: @"PicturePath"];
		poseName = [result stringForColumn: @"Name"];
		NSLog(@"Retrieved pose name %@", poseName);
		NSLog(@"Retrieved imagename %@", poseImgName);
    }
    
    [result close];
	
	
}

//@TODO : can't get a avialableTip, the bOk always be No
-(void) getAvailableTip 
{
    id<PLResultSet> result;
	

    result = [dbHandle() executeQuery: @"SELECT * FROM Tips where IsRead = 0 ORDER BY RANDOM() LIMIT 1"];
    

    
    if([result next])
    {

        tipID = [result safeObjectForColumn: @"ID"];
		poseID = [result safeObjectForColumn: @"PoseID"];
		tipContent = [result safeObjectForColumn:@"Tip"];
		headline = [result safeObjectForColumn:@"Headline"];
        NSLog(@"New Tip = %@", poseID);
		
    }
    else {
		[result close];
		result = [dbHandle() executeQuery: @"SELECT * FROM Tips ORDER BY AccessDate"];
		if ([result next]) {
			tipID = [result safeObjectForColumn: @"ID"];
			tipContent = [result safeObjectForColumn:@"Tip"];
			headline = [result safeObjectForColumn:@"Headline"];
			poseID = [result safeObjectForColumn: @"PoseID"];
            NSLog(@"New Tip = %@", poseID);
		}
		else {
			NSLog(@"can't get the latest tip");
		}

	}

    [result close];
	
	[self getPoseImageNameWithTipID:[poseID intValue]];
    
	NSDate *today = todayDate();
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:poseID,@"poseID",
						  today, @"Date",
						  tipContent, @"Content",
						  poseName, @"PoseName",
						  poseImgName, @"ImgName",
						  headline, @"Headline",
						  nil];
	
	[tipArray insertObject:dict atIndex:0];
	NSTimeInterval since1970 = [today timeIntervalSince1970];
	[self setTipIsReadWithAccessDate:since1970];
}

#pragma mark --- set/get Method ---
- (void) setCurrentPageNumber:(int)num {
	
	currentPageNumber = num;
	//set the tip info
	NSDictionary *dict = [tipArray objectAtIndex:currentPageNumber];
	NSLog(@"Setting details tip array len=%@", [dict objectForKey:@"Content"] );
	[self setTipPoseName: [dict objectForKey:@"PoseName"] andContent:[dict objectForKey:@"Content"] andHeadline:[dict objectForKey:@"Headline"] andDate:[dict objectForKey:@"Date"]];
	
	pageControl.currentPage = currentPageNumber;
	[pageControl updateCurrentPageDisplay];
	
}

- (void)loadTips {
	id<PLResultSet> result;
    	NSLog(@"loadTips tip array len=%i", tipArray.count);
    
    result = [dbHandle() executeQuery: @"SELECT * FROM Tips where IsRead = 1 ORDER BY AccessDate DESC"];
    
    while([result next])
    {
		NSDate *date = [result dateForColumn: @"AccessDate"];
		
		NSNumber *theTipID = [result safeObjectForColumn:@"ID"];
		NSString *theTipContent = [result safeObjectForColumn:@"Tip"];
		poseID = [result safeObjectForColumn:@"PoseID"];
		headline = [result safeObjectForColumn:@"Headline"];
	
		[self getPoseImageNameWithTipID:[poseID intValue]];
		
		NSLog(@"headline to save %@", headline);
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys: theTipID, @"ID",
							  date, @"Date", 
							  theTipContent, @"Content", 
							  poseName, @"PoseName",
							  poseImgName, @"ImgName",
							  poseID, @"poseID",
							  headline, @"Headline",
							  nil];
		[tipArray addObject:dict];
		
		NSLog(@"DailyTip's date is %@", [date description]);
		if (kMaxTipCount == tipArray.count) {
			break;
		}
		
	}
	[result close];
	
	NSLog(@"loadTips Calculating tip array len=%i", tipArray.count);
	//NSDictionary *dict = [tipArray objectAtIndex:tipArray.count-1];

	//NSLog(@"pose name = %@", [dict objectForKey:@"Headline"]);
    
    NSLog(@"tip count=%i", tipArray.count);
	if (tipArray.count == 0) {
		[self getAvailableTip];
		
	}
	else {
		NSDictionary *lastTip = [tipArray objectAtIndex:0];
		NSDate *lastTipDate = [lastTip objectForKey:@"Date"];
		
		//if the latest daily tip's date is earlier than today, add a new one
		NSDate *today = todayDate();
		if (NSOrderedAscending == [lastTipDate compare:today] || 0 == tipArray.count) {
			[self getAvailableTip];
			if (kMaxTipCount == tipArray.count) {
				[tipArray removeLastObject];
			}
		}
	}

	//reverse the tip array
	int tipArrayCount = tipArray.count;
	if (tipArrayCount > 1) {
		for (int i = 0; i < tipArrayCount/2; i++) {
			[tipArray exchangeObjectAtIndex:i withObjectAtIndex:tipArrayCount - 1 - i];
		}
	}
	
    for (int i = 0; i < tipArray.count ; i++) {
		UIImageView *poseImageView = [UIImageView new];
        [poseImageView setContentMode:UIViewContentModeScaleAspectFill];
		poseImageView.frameSize = poseScrollView.frameSize;
		poseImageView.frameOrigin = CGPointMake(i*poseScrollView.frameSizeWidth, 0 );
		[poseScrollView addSubview:poseImageView];
		[poseImageViewArray addObject:poseImageView];
		[poseImageView release];
	}
    poseScrollView.contentSize = CGSizeMake(tipArray.count * poseScrollView.frameSizeWidth, poseScrollView.frameSizeHeight);
	
	NSLog(@"loadTips FINISHED tip array len=%i", tipArray.count);
	
}



- (void)viewDidLoad {
    [super viewDidLoad];
	[self.view insertSubview:wholeScrollView atIndex:1];
	
	tipArray = [NSMutableArray new];
	poseImageViewArray = [NSMutableArray new];
	[self loadTips];
	//set all the pose images
	for (int i = 0; i < tipArray.count; i++) {
		NSDictionary *dict = [tipArray objectAtIndex:i];
		NSString *imgName = [dict objectForKey:@"ImgName"];
		UIImageView *imageView = [poseImageViewArray objectAtIndex:i];
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
		imageView.image = [UIImage imageNamed:imgName];
		
	}
	pageControl.numberOfPages = tipArray.count;
	self.currentPageNumber = tipArray.count - 1;
	poseScrollView.showsHorizontalScrollIndicator = NO;
	poseScrollView.contentOffset = CGPointMake(currentPageNumber * poseScrollView.frameSizeWidth, 0);
	titleLB.text = @"Daily Inspiration";
	
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)dealloc {
	[poseImageViewArray release];
	[tipArray release];
    [super dealloc];
}


/*
- (void)beforSwitchToViewController:(int)controllerID {
	
}
 */
#pragma mark --- UIScrollViewDelegate ---

- (void)scrollViewDidScroll:(UIScrollView *)sender {
	
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	const CGFloat pageWidth = poseScrollView.frame.size.width;	
	const int num = poseScrollView.contentOffset.x / pageWidth;
	if(num  != currentPageNumber) {
		self.currentPageNumber = num;
	}
}
#pragma mark --- IBAction ---
//@TODO product
- (IBAction)showDetail {
	NSDictionary *dict = [tipArray objectAtIndex:currentPageNumber];
	NSString *theposeID = [dict objectForKey:@"poseID"];
	id<PLResultSet>result;
    NSString *q = [NSString stringWithFormat:@"SELECT * FROM Poses WHERE ID = %@", theposeID];
    NSLog(@"q = %@", q);
	result = [dbHandle() executeQuery:q];
	NSMutableDictionary *newDict;
	if ([result next]) {
		NSString *picPath = [result safeObjectForColumn:@"PicturePath"];
		NSString *description = [result safeObjectForColumn:@"Description"];
		NSString *thePoseName = [result safeObjectForColumn:@"Name"];
		
		thePoseName = [[[thePoseName capitalizedString] stringByReplacingOccurrencesOfString:@"Iii" withString:@"III"]stringByReplacingOccurrencesOfString:@"Ii" withString:@"II"];
		
		NSNumber *isFavortie = [result safeObjectForColumn:@"IsFavorite"];
		newDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
				theposeID, @"poseID",
				picPath,@"picPath",
				description,@"description",
				thePoseName, @"poseName",
				isFavortie,@"isFavortie",
				nil];
	}
	[result close];
	PoseDetailViewController *vc = [PoseDetailViewController alloc];
	vc.poseDict = newDict;
	[vc initWithNibUsingClassName];
	vc.needAutoPlay = YES;
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}
- (IBAction)gotoProduct {
}
@end

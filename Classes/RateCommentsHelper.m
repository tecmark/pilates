//
//  RateCommentsHelper.m
//  Yoga
//
// Created by Rob on 11/17/10.
//  
//

#import "RateCommentsHelper.h"
#import "S7FTPRequest.h"
#import "LPYFTPListRequest.h"
#import "CommentRate.h"
#import "UA_ASIHTTPRequest.h"

/*
#define kFTPServer			@"ftp://bonbol.com"
#define kUploadURLFormat	@"%@/%@/rating/"

#define kFTPUsername		@"bonbol"
#define kFTPPassword		@"B_0nbol7"

#define kClassContentServer		@"http://97.74.95.143/urbanyoga"
#define kRatingURLFormat		@"%@/%@/rating/%@"
 */

#define kFTPServer			@"ftp://urbanyogaapp.com/urbanyogaapp/live_app/live_classes"
#define kUploadURLFormat	@"%@/%@/rating/"

#define kFTPUsername		@"yogapplogger"
#define kFTPPassword		@"quickkLogger"

#define kClassContentServer		@"http://www.urbanyogaapp.com/live_app/live_classes/"
#define kRatingURLFormat		@"%@/%@/rating/%@"

@implementation RateCommentsHelper

NSString *tempPath;

SINGLETON_IMPLEMENTATION(RateCommentsHelper)

- (void)saveCommentsRatesToDataBaseWithClassID:(NSString*)classID commentsRates:(NSArray*)commentsRates {
    
    NSLog(@"saveCommentsRatesToDataBaseWithClassID");
	// Clean the old data
	[dbHandle() executeUpdate:@"delete from ClassRating where ClassID = ?",  classID];
	for (CommentRate *commentRate in commentsRates) {
		//ADD a comment and rate
		[dbHandle() executeUpdate:@"insert into ClassRating(ClassID, Rate, Comment, Title, SubmittedDate) values(?, ?, ?, ?, ?)", 
		 classID, stringFromInt(commentRate.rate), commentRate.comment, commentRate.title, commentRate.submittedDate];
	}
    
     NSLog(@"saveCommentsRatesToDataBaseWithClassID Ended");
}
- (void)loadCommentsRatesFromServerWithClassID:(NSString*)classID {
	NSArray *commentsRates = [[self getCommentRateWithClassID:classID] retain];
	
	[self saveCommentsRatesToDataBaseWithClassID:classID commentsRates:commentsRates];
	
	[commentsRates release];
}
- (void)updateRateCommentsFromServerForAllClasses {
	id<PLResultSet> result = [dbHandle() executeQuery: @"SELECT ClassID FROM Classes"];
	
	while ([result next]) {
		NSString *classID = [result stringForColumn:@"ClassID"];
		NSArray *commentsRates = [[self getCommentRateWithClassID:classID] retain];
		
		// Clean the old data
		[dbHandle() executeUpdate:@"delete from ClassRating where ClassID = ?",  classID];
		for (CommentRate *commentRate in commentsRates) {
			//ADD a comment and rate
			[dbHandle() executeUpdate:@"insert into ClassRating(ClassID, Rate, Comment, Title, SubmittedDate) values(?, ?, ?, ?, ?)", 
			 classID, stringFromInt(commentRate.rate), commentRate.comment, commentRate.title, commentRate.submittedDate];
		}
		
		[commentsRates release];
	}
	
	[result close];
}
- (NSArray*)getCommentsFromServerWithClassID:(NSString*)classID averageRate:(NSInteger*)rate {
	NSArray *commentsRates = [self getCommentRateWithClassID:classID];
	
	if (commentsRates == nil || [commentsRates count] == 0) {
		*rate = 4;
		return nil;
	}
	else {
		NSInteger totalRate = 0;
		for (CommentRate *commentrate in commentsRates) {
			totalRate += commentrate.rate;
		}
		NSInteger commentsAmount = [commentsRates count];
		*rate = totalRate / commentsAmount;
		return commentsRates;
	}

}
- (NSArray*)getCommentsFromDatabaseWithClassID:(NSString*)classID averageRate:(NSInteger*)rate {
	
	NSMutableArray *commentsRates = [NSMutableArray array];
	
	id<PLResultSet> result = [dbHandle() executeQuery: @"SELECT * FROM ClassRating where ClassID = ?", classID];

	NSInteger totalRate = 0;
	while ([result next]) {
		totalRate += [result intForColumn:@"Rate"];
		[commentsRates addObject:[CommentRate commentRateWithRate:[result intForColumn:@"Rate"] 
															title:[result stringForColumn:@"Title"] 
														  comment:[result stringForColumn:@"Comment"] 
													submittedDate:[result stringForColumn:@"SubmittedDate"] ]];
	}
	[result close];
	
	NSInteger commentsAmount = [commentsRates count];
    
    NSLog(@"total rate: %d", totalRate);
    NSLog(@"comments amount: %d", commentsAmount);
    
    if (commentsAmount == 0) {
		*rate = 0;
		return nil;
	}
	
    
	*rate = totalRate / commentsAmount;
	
	
	return commentsRates;
}
- (void)saveRate:(NSInteger)rate title:(NSString*)title comment:(NSString*)comment forClass:(NSString*)classID {
	NSDateFormatter *formater = [[NSDateFormatter alloc] init];
	formater.dateFormat = @"dd MMM yyyy";
	
	NSString *dateString = [formater stringFromDate:[NSDate date]];
	
	NSString *rateComment = [NSString stringWithFormat:@"%i###%@ ###%@ ###%@", rate, title, comment, dateString, nil];
	[formater release];
	
	UIDevice *device = [UIDevice currentDevice];
	NSString *uniqueIdentifier = [device uniqueIdentifier];
	
	tempPath = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@",uniqueIdentifier ,@".txt" ,nil]];
	NSLog(@"Comment tempPath: %@", tempPath, nil);
	[rateComment writeToFile:tempPath atomically:NO encoding:NSUTF8StringEncoding error:NULL];
	
	// Upload to FTP
	NSString *ftpUrl = [NSString stringWithFormat:kUploadURLFormat, kFTPServer, classID, nil];
	NSLog(@"Comment FTP URL: %@", ftpUrl, nil);
	S7FTPRequest *ftpRequest = [[S7FTPRequest alloc] initWithURL:[NSURL URLWithString:ftpUrl] toUploadFile:tempPath];
	
	ftpRequest.username = kFTPUsername;
	ftpRequest.password = kFTPPassword;
	
	ftpRequest.delegate = self;
	ftpRequest.didFinishSelector = @selector(uploadFinished:);
	ftpRequest.didFailSelector = @selector(uploadFailed:);
	ftpRequest.willStartSelector = @selector(uploadWillStart:);
	ftpRequest.didChangeStatusSelector = @selector(requestStatusChanged:);
	ftpRequest.bytesWrittenSelector = @selector(uploadBytesWritten:);
	
	[ftpRequest startRequest];
}
- (NSArray*)getCommentRateWithClassID:(NSString*)classID {
	NSString *urlstring = [NSString stringWithFormat:kUploadURLFormat, kFTPServer, classID, nil];
	LPYFTPListRequest *request = [[LPYFTPListRequest alloc] initWithURLString:urlstring];
	request.ftpUsername = kFTPUsername;
	request.ftpPassword = kFTPPassword;
	
	NSArray *files = [request list];
	[request release];
	
	NSMutableArray *comments = [NSMutableArray array];
	
	for (int i = 0; i < [files count]; i++) {
		NSString *file = [files objectAtIndex:i];
		NSLog(file, nil);
		NSString *urlString = [NSString stringWithFormat:kRatingURLFormat, kClassContentServer, classID, file];
		UA_ASIHTTPRequest *request = [[[UA_ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]] autorelease];
		request.timeOutSeconds = 60;
		request.requestMethod = @"GET";
		[request startSynchronous];
		
		NSString *responseString = [request responseString];
		
		NSLog(responseString, nil);
		
		NSArray *commentArray = [responseString componentsSeparatedByString:@"###"];
		
		if ([commentArray count] == 4) {
			[comments addObject:[CommentRate commentRateWithRate:[[commentArray objectAtIndex:0] intValue] 
														   title:[commentArray objectAtIndex:1]
														 comment:[commentArray objectAtIndex:2] 
												   submittedDate:[commentArray objectAtIndex:3] ]];
		}
	}
	
	return comments;
}


#pragma mark -
#pragma mark S7FTPRequest delegate methods
- (void)uploadFinished:(S7FTPRequest *)request {
	NSFileManager *manageer = [NSFileManager defaultManager];
	if ([manageer fileExistsAtPath:tempPath]) {
		[manageer removeItemAtPath:tempPath error:NULL];
	}
	
	NSLog(@"Upload finished.");
	[request release];
}
- (void)uploadFailed:(S7FTPRequest *)request {
	
	NSLog(@"Upload failed: %@", [request.error localizedDescription]);
	[request release];
}
- (void)uploadWillStart:(S7FTPRequest *)request {
	
	NSLog(@"Will transfer %d bytes.", request.fileSize);
}
- (void)uploadBytesWritten:(S7FTPRequest *)request {
	
	NSLog(@"Transferred: %d", request.bytesWritten);
}
- (void)requestStatusChanged:(S7FTPRequest *)request {
	
	switch (request.status) {
		case S7FTPRequestStatusOpenNetworkConnection:
			NSLog(@"Opened connection.");
			break;
		case S7FTPRequestStatusReadingFromStream:
			NSLog(@"Reading from stream...");
			break;
		case S7FTPRequestStatusWritingToStream:
			NSLog(@"Writing to stream...");
			break;
		case S7FTPRequestStatusClosedNetworkConnection:
			NSLog(@"Closed connection.");
			break;
		case S7FTPRequestStatusError:
			NSLog(@"Error occurred.");
			break;
	}
}

@end

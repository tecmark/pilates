//
//  NewClassLoadQuene.m
//  Yoga
//
// Created by Rob on  11/30/10.
//  
//

#import "NewClassLoadQuene.h"
#import "NewClassLoader.h"



@implementation NewClassLoadQuene

SINGLETON_IMPLEMENTATION(NewClassLoadQuene)

@synthesize allOperationsFinished;

- (void)refreshOperationQueneStatus {
	for (NSOperation *op in self.operations) {
		if (![op isFinished]) {
			self.allOperationsFinished = NO;
			NSLog(@"There're still new classes need to be loaded.", nil);
			return;
		}
	}
	
	NSLog(@"All new classes are loaded.", nil);
	self.allOperationsFinished = YES;
}

- (void)addOperation:(NSOperation *)op {
	if ([op isKindOfClass:[NewClassLoader class]]) {
		
		NewClassLoader *loader = (NewClassLoader*)op;
		
		
		for (NewClassLoader *aClassLoader in self.operations) {
			if ([aClassLoader.product.productIdentifier isEqualToString:loader.product.productIdentifier]) {
				return;
			}
		}
		
		
		[loader addObserver:self];
		//[super addOperations:[NSArray arrayWithObjects:op, nil] waitUntilFinished:YES];
		[super addOperation:op];
	}
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object 
                        change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"isFinished"]) {
		NewClassLoader *classLoader = (NewClassLoader*)object;
		if (![classLoader isFinished]) {
			return;
		}
		else {
			[self refreshOperationQueneStatus];
		}
    }
}
   
- (void)addObserver:(id)observer {
    [self addObserver:observer forKeyPath:@"allOperationsFinished" 
              options:NSKeyValueObservingOptionNew context:NULL];
}

- (void)removeObserver:(id)observer {
    [self removeObserver:observer forKeyPath:@"allOperationsFinished"];
}

@end

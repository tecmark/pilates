//
//  NewClassLoader.m
//  Yoga
//
// Created by Rob on  11/30/10.
//  
//

#import "NewClassLoader.h"
#import "ClassesServerHelper.h"
#import "YogaClass.h"

@implementation NewClassLoader


@synthesize product, newClass;


- (id)initWithProduct:(UAProduct*)_product {
	if (self = [super init]) {
		self.product = _product;
		executing = NO;
		finished = NO;
	}
	
	return self;
}

- (void)dealloc {
	[newClass release];
	
	[super dealloc];
}


- (void)main { 
	@try {
		
		NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
		
		if ([self isCancelled])
		{
			return;	
		}
		
		//[self willChangeValueForKey:@"isExecuting"]; 
		executing = YES;
		//[self didChangeValueForKey:@"isExecuting"];
		
		self.newClass = [ClassesServerHelper getClassByID:self.product.productIdentifier];
		if (self.newClass != nil) {
			[self.newClass saveToDatabase];
			
			self.newClass.classSize = product.fileSize;
			self.newClass.product = self.product;
		}
		
        NSLog(@"new class downloaded");
      
        
		[self willChangeValueForKey:@"isFinished"]; 
		finished = YES;
		[self didChangeValueForKey:@"isFinished"];
		
		[pool release];
	} 
	@catch(...) {
		
	}
}

- (BOOL)isExecuting { 
	return executing;
}

- (BOOL)isFinished { 
	return finished;
}

- (BOOL)isConcurrent {
	return NO;
}

- (void)addObserver:(id)observer {
    [self addObserver:observer forKeyPath:@"isExecuting" 
              options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:observer forKeyPath:@"isFinished" 
              options:NSKeyValueObservingOptionNew context:NULL];
}

- (void)removeObserver:(id)observer {
   // [self removeObserver:observer forKeyPath:@"isExecuting"];
    //[self removeObserver:observer forKeyPath:@"isFinished"];
}

@end

//
//  AddAsanaViewController.h
//  Yoga
//
// Created by Rob on 10-3-26.
//  
//

#import <UIKit/UIKit.h>
#import "MyPickerView.h"

@interface AddAsanaViewController : CommonViewController 
<UIActionSheetDelegate, UIPickerViewDelegate, UIPickerViewDataSource, MyPikerViewDelegate, UITextFieldDelegate>
{
	IBOutlet UITextField *titleTF;
	IBOutlet UIButton    *intervalTimeBtn;
//	IBOutlet UIButton	 *musicSwitchBtn;
	int intervalSeconds;
	IBOutlet UIButton *titleBtn;
	int intervalMin;
	int intervalSec;
	MyPickerView *pickerView;
	IBOutlet UIButton *nextBtn;
	IBOutlet UILabel *intervalLB;
}

- (IBAction)setIntervalTime;
//- (IBAction)setMute;
- (IBAction)texFildInputDone;
- (IBAction)addAsanaDone;
- (IBAction)editTitle;

@end

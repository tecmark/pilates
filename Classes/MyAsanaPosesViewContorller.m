//
//  MyAsanaPosesViewContorller.m
//  Yoga
//
// Created by Rob on 10-3-26.
//  
//

#import "MyAsanaPosesViewContorller.h"
#import "AddIssueTableCell.h"
#import "AddAsanaPoseViewController.h"
#import "PlayAsanaPoseViewController.h"
#import "PoseDetailViewController.h"
#import "MyAsanaSettingViewController.h"
#define kTableCellHeight 65
#define kTableMaxHeight	 345

@implementation MyAsanaPosesViewContorller
@synthesize asanaDict;
@synthesize totalDuration;

/*
- (void)setDurationString:(NSString *)string {
	durationLB.text = string;
}
*/


//override
- (void)loadData {
	id<PLResultSet> result;
	id<PLResultSet> subResult;
	NSNumber *asanaID = [asanaDict objectForKey:@"ID"];
	result = [dbHandle() executeQuery: @"SELECT * FROM MyAsanaPoses where AsanaID = ? order by PoseIndex", [asanaID stringValue]];
//	allPoseStepCount = 0;
	totalDuration = 0;
	int intervalTime = [[asanaDict objectForKey:@"intervalTime"] intValue];
	while ([result next]) {
		NSNumber *ID = [result safeObjectForColumn:@"ID"];
		NSNumber *poseID = [result safeObjectForColumn:@"PoseID"];
		NSNumber *poseIndex = [result safeObjectForColumn:@"PoseIndex"];
		NSNumber *repeatCount = [result safeObjectForColumn:@"RepeatCount"];
		NSString *poseName;
		NSString *intro;
		NSString *picPath;
		NSNumber *duration;
		NSString *sanskrit;
		NSString *desctiption;
		subResult = [dbHandle() executeQuery: @"SELECT * FROM Poses where ID = ?", [poseID stringValue]];
		
		if ([subResult next]) {
			poseName = [[[[[subResult safeObjectForColumn:@"Name"] capitalizedString] stringByReplacingOccurrencesOfString:@"Iii" withString:@"III"]stringByReplacingOccurrencesOfString:@"Ii" withString:@"II"]stringByReplacingOccurrencesOfString:@"Iv" withString:@"IV"];
			sanskrit = [[[[[subResult safeObjectForColumn:@"Sanskrit"] capitalizedString] stringByReplacingOccurrencesOfString:@"Iii" withString:@"III"]stringByReplacingOccurrencesOfString:@"Ii" withString:@"II"]stringByReplacingOccurrencesOfString:@"Iv" withString:@"IV"];
			picPath = [subResult safeObjectForColumn:@"PicturePath"];
			intro = [subResult safeObjectForColumn:@"Intro"];
			duration = [subResult safeObjectForColumn:@"Duration"];
			desctiption = [subResult safeObjectForColumn:@"description"];
		}
		[subResult close];
		//totalDuration += ([duration intValue] + intervalTime) * [repeatCount intValue];
		totalDuration += (20 * [repeatCount intValue]);
		totalDuration += (intervalTime* [repeatCount intValue]);
		totalDuration += [duration intValue];
		// add the step of instruction
		/*
		int stepCount = 1;
		subResult = [dbHandle() executeQuery: @"SELECT * FROM PoseSteps where PoseID = ?", [poseID stringValue]];
		while ([subResult next]) {
			stepCount++;
		}
		[subResult close];
		allPoseStepCount += stepCount;
		NSNumber *stepCountN = [NSNumber numberWithInt:stepCount];
		 */
		NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
							  ID, @"ID",
							  poseID, @"poseID",
							  poseIndex, @"poseIndex",
							  poseName, @"poseName", 
							  picPath, @"picPath",
							  intro, @"intro",
							  sanskrit, @"sanskrit",
//							  stepCountN,@"stepCount",
							  repeatCount, @"repeatCount",
							  duration, @"duration",
							  desctiption, @"description", 
									 nil];
		
		[dataArray addObject:dict];
	}
	
	totalDuration-=intervalTime;
	[result close];
	int hour = totalDuration/3600;
	int min = (totalDuration%3600) / 60;
	int sec = totalDuration%60;
	NSString *hourStr = (0 == hour) ? @"" : [NSString stringWithFormat:@"%i:", hour];
	NSString *minStr = [NSString stringWithFormat:@"%02i", min];
	durationLB.text = [NSString stringWithFormat:@"%@%@:%02i", hourStr, minStr, sec];
}
- (void)viewDidLoad {
 
    [super viewDidLoad];
	titleLB.text = [asanaDict objectForKey:@"asanaName"];
}
-(void)viewDidAppear:(BOOL)animated{
    
    NSLog(@"check if play button should be enabled, %i", dataArray.count);
    if(dataArray.count==0){playBtn.enabled=NO;}
    else{playBtn.enabled=YES;}
    
}
- (void)viewWillAppear:(BOOL)animated {
    
    NSLog(@"rows in section, %i", [table numberOfRowsInSection:0]);
    if(dataArray.count==0){playBtn.enabled=NO;}
    else{playBtn.enabled=YES;}
 
	[super viewWillAppear:YES];
	titleLB.text = [asanaDict objectForKey:@"asanaName"];
}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *dict = [dataArray objectAtIndex:indexPath.row];
	PoseDetailViewController *vc = [PoseDetailViewController alloc];
	//	vc.isFromMyAsana = YES;
	vc.poseDict = dict;
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
   
    AddIssueTableCell *cell = (AddIssueTableCell *)[tableView dequeueReusableCellWithIdentifier:@"AddIssueTableCell"];
	if (nil == cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddIssueTableCell" owner:nil options:nil];
		cell = (AddIssueTableCell *)[nib objectAtIndex:0];
	}
	
     
    
	NSDictionary *dict = [dataArray objectAtIndex:indexPath.row];
	
	NSString *name = [dict objectForKey:@"poseName"];
	NSString *picPath = [dict objectForKey:@"picPath"];
	NSString *intro = [dict objectForKey:@"intro"];
	
	UIImage *poseImg = [UIImage imageNamed:[picPath stringByReplacingOccurrencesOfString:@".jpg" withString:@"_thumb.jpg"]];
	
	cell.poseIV.image = poseImg;
	cell.titleLB.text = name;
	cell.subTitleLB.text = [dict objectForKey:@"sanskrit"];
	cell.descLB.text = intro;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
//	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	
     return cell;
     
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
	NSDictionary *dict = [[dataArray objectAtIndex:sourceIndexPath.row] retain];
	[dataArray removeObjectAtIndex:sourceIndexPath.row];
	[dataArray insertObject:dict atIndex:destinationIndexPath.row];
	[dict release];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *dict = [dataArray objectAtIndex:indexPath.row];
	NSNumber *ID = [dict objectForKey:@"ID"];
	[dbHandle() executeUpdate:@"delete from MyAsanaPoses where ID = ?", [ID stringValue]];
	[dataArray removeObjectAtIndex:indexPath.row];
	[table deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
	table.frameSizeHeight = MIN(kTableMaxHeight, kTableCellHeight*dataArray.count);
}


#pragma mark -
#pragma mark --- IBAction ---
//override
- (IBAction)addRow {
	if (settingBtn.selected || editBtn.selected) return;
	
	AddAsanaPoseViewController *vc = [AddAsanaPoseViewController alloc];
	vc.asanaID = [[asanaDict objectForKey:@"ID"] intValue];
	vc.asanaName = [asanaDict objectForKey:@"asanaName"];
	NSMutableArray *array = [NSMutableArray array];
	for (NSDictionary *dict in dataArray) {
		NSNumber *poseID = [dict objectForKey:@"poseID"];
		[array addObject:poseID];
	}
	vc.selectedPoseArray = array;
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];	
}
- (IBAction)play {
	
	if (editBtn.selected) {
		editBtn.selected = NO;
		table.editing = NO;
	}
	if (settingBtn.selected) {
		settingBtn.selected = NO;
		[settingView removeFromSuperview];
		titleLB.text = [asanaDict objectForKey:@"asanaName"];;
	}
    
    NSLog(@"data array: %@", dataArray);
    
    
    
    
	
	PlayAsanaPoseViewController *vc = [PlayAsanaPoseViewController alloc];
	vc.asanaPoseArray = dataArray;
	vc.asanaName = [asanaDict objectForKey:@"asanaName"];;
	int poseInterval = [[asanaDict objectForKey:@"intervalTime"] intValue];
	vc.poseInterval = poseInterval;
	[vc initWithNibUsingClassName];
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
	
}
- (IBAction)setting {
    
	MyAsanaSettingViewController *vc = [MyAsanaSettingViewController alloc];
	vc.asanaDict = self.asanaDict;
	vc.poseArray = dataArray;
	vc.durationStr = durationLB.text;
//	vc.allPoseStepCount = allPoseStepCount;
	vc.totalDuration = totalDuration;
	[vc initWithNibUsingClassName];
	[self.navigationController pushViewController:vc animated:NO];
	[vc release];
    
}

@end

//
//  InfoHomeViewController.m
//  Yoga
//
// Created by Rob on 10-4-22.
//  
//

#import "InfoHomeViewController.h"
#import "EmailProvider.h"
#define kAppLink @"Hi, take a minute and check out Pilates Custom from Quickka in the App Store. <a href ='http://itunes.apple.com/us/app/urbanyoga/id427127760?mt=8&ls=1'>http://itunes.apple.com/us/app/urbanyoga/id427127760?mt=8&ls=1</a><br><br>For more great apps by Quickka please <a href='http://itunes.apple.com/us/artist/quickka/id342555029'>visit the App Store</a>. "

static NSString *links[] = {
	//TODO add the link
	@"",//gift app 
	@"",//Rate and comment
	@"http://www.pilatescustom.com",
	@"http://itunes.apple.com/gb/artist/quickka/id342555029",
	@"http://www.quickka.com"
};
@implementation InfoHomeViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	copyrightLB.text = @"Copyright \u00A9 2011 Quickka SPV 2 Limited.";
	
	self.switchViewContorllerNameArray = [NSArray arrayWithObjects:@"HomeQuickkaAppsViewController",
										  @"HomeTermsViewController",
										  nil];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}
- (IBAction)gotoLink:(id)sender {
	
	
	int num = [sender tag];
	
	if(num<10){
	NSLog(@"Goto link %i", num);
	NSURL *URL = [NSURL URLWithString:links[num]];
	[[UIApplication sharedApplication] openURL:URL];
	}
	else{
		
		
	}
	
}
- (IBAction)back {
	[self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)sendEmail {
	[EmailProvider sendEmailFromViewControler:self 
										   to:nil
										   cc:nil
										  bcc:nil
								  attachments:nil 
									  subject:@"Check out the Pilates Custom App"
										 body:kAppLink
								   htmlFormat:YES];
	
}
// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {	
	// Notifies users about errors associated with the interface
	
	[self.navigationController dismissModalViewControllerAnimated:YES];
	NSString *alertMsg;
	switch (result)
	{
			
		case MFMailComposeResultSaved:
			alertMsg = @"Email has been saved.";
			break;
		case MFMailComposeResultSent:
			alertMsg = @"Your email has been delivered.";
			break;
		case MFMailComposeResultFailed:
			alertMsg = @"Sent email failed, please try again.";
			break;
		case MFMailComposeResultCancelled:
			return;
			break;
			
		default:
			break;
	}
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil 
													message:alertMsg 
												   delegate:nil 
										  cancelButtonTitle:@"OK" 
										  otherButtonTitles:nil];
	[alert show];
	[alert release];
}

@end

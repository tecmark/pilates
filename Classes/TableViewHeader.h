//
//  TableViewHeader.h
//  Yoga
//
// Created by Rob on 10-4-1.
//  
//

#import <UIKit/UIKit.h>


@interface TableViewHeader : UIView {
	UILabel *textLB;

}

@property (nonatomic, retain) IBOutlet UILabel *textLB;

@end

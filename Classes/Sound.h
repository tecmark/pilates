//
//  Sound.h
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVAudioPlayer.h>

@interface Sound : NSObject {
	AVAudioPlayer *player;
	NSURL *fileURL;
}

@property (nonatomic, retain) AVAudioPlayer *player;
- (id)initWithContentsOfFile:(NSString *)path;
- (id)initWithResourceFile:(NSString *)file;
- (void)playRepeat:(int)count;
- (void)playOnce;
- (void)playWithVolume:(float)volume;
- (void)playRepeat:(int)count volume:(float)volume;

- (void)resumePlay;
- (void)playOrResumre;

- (void)stop;
- (void)pause;
@end

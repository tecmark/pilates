//
//  MySegmentControl.h
//  Yoga
//
// Created by Rob on 10-4-21.
//  
//

#import <UIKit/UIKit.h>

@protocol MySegmentControlDelegate

- (void)selectedNumber:(int)num;

@end

@interface MySegmentControl : UIView {
	IBOutlet UIImageView *background;
	id<MySegmentControlDelegate> delegate;
}

@property (nonatomic, assign)id<MySegmentControlDelegate> delegate;
@property (nonatomic, retain) UIImageView *background;

- (IBAction)selectedNumber:(id)sender;

@end

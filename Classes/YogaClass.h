//
//  YogaClass.h
//  Yoga
//
// Created by Rob on  11/12/10.
//  
//

#import <Foundation/Foundation.h>
#import "UAProduct.h"

@interface YogaClass : NSObject {
	NSInteger classDB_ID;
	NSString *classID;
	NSString *name;
	NSString *classCategory;
	NSString *shortDescription;
	NSString *longDescription;
	NSString *classDuration;
	
	
	NSString *biogText;
	NSData *biogImage;
	
	NSData *mainImage;
	
	NSArray *krama;
	NSArray *kramaDuration;
	NSArray *kramaMainImages;
	NSArray *kramaThumbnails;
	NSArray *video;
	
	NSInteger averageRating;
	NSArray *comments;
	
	double classSize;
	
	UAProduct *product;
}

@property NSInteger classDB_ID;
@property (nonatomic, retain) NSString *classID;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *classCategory;
@property (nonatomic, retain) NSString *shortDescription;
@property (nonatomic, retain) NSString *longDescription;
@property (nonatomic, retain) NSString *classDuration;

@property (nonatomic, retain) NSString *biogText;
@property (nonatomic, retain) NSData *biogImage;
@property (nonatomic, retain) NSData *mainImage;

@property (nonatomic, retain) NSArray *krama;
@property (nonatomic, retain) NSArray *kramaDuration;
@property (nonatomic, retain) NSArray *kramaMainImages;
@property (nonatomic, retain) NSArray *kramaThumbnails;
@property (nonatomic, retain) NSArray *video;

@property NSInteger averageRating;
@property (nonatomic, retain) NSArray *comments;

@property double classSize;
@property (nonatomic, assign) UAProduct *product;


+ (id)classByLoadingDownloadedFromDatabaseWithClassID:(NSString*)ID;
+ (id)classByLoadingFromDatabaseWithClassID:(NSString*)ID;
+ (id)classByLoadingFromDatabaseWithClassIDFromBasicInfo:(NSString*)ID;
- (id)initWithClassID:(NSString*)ID;

+ (void)installedClassWithID:(NSString*)classID;
- (NSString*)fileSizeToString;
- (void)saveToDatabase;
- (void)purchased;
- (void)addFileSize;
+ (double)getFileSizeByClassID:(NSString*)classID;
- (NSString*)classVideoPath;
+ (BOOL)isInstalled:(NSString*)classID;


@end

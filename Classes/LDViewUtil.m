//
//  LDViewUtil.m
//
//


#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>

@implementation UIView (LDViewUtil)

#pragma mark === get/set methods for frame origin ===

- (CGPoint)frameOrigin {
	return self.frame.origin;
}

- (void)setFrameOrigin:(CGPoint)origin {
	CGSize size = self.frame.size;
	self.frame = CGRectMake(origin.x, origin.y, size.width, size.height);
}

- (void)setFrameOrigin:(CGFloat)x :(CGFloat)y {
	CGSize size = self.frame.size;
	self.frame = CGRectMake(x, y, size.width, size.height);
}


- (CGFloat)frameOriginX {
	return self.frame.origin.x;
}

- (void)setFrameOriginX:(CGFloat)x {
	CGRect rect = self.frame;
	
	self.frame = CGRectMake(x, rect.origin.y, rect.size.width, rect.size.height);
}

- (CGFloat)frameOriginY {
	return self.frame.origin.y;
}

- (void)setFrameOriginY:(CGFloat)y {
	CGRect rect = self.frame;
	
	self.frame = CGRectMake(rect.origin.x, y, rect.size.width, rect.size.height);
}

#pragma mark === get/set methods for frame origin ===

- (CGSize) frameSize {
	return self.frame.size;
}

- (void) setFrameSize:(CGSize)size {
	CGPoint origin = self.frame.origin;
	self.frame = CGRectMake(origin.x, origin.y, size.width, size.height);
}

- (void) setFrameSize:(CGFloat)width :(CGFloat)height {
	CGPoint origin = self.frame.origin;
	self.frame = CGRectMake(origin.x, origin.y, width, height);
}


- (CGFloat) frameSizeWidth {
	return self.frame.size.width;
}

- (void) setFrameSizeWidth:(CGFloat)width {
	CGRect rect = self.frame;
	
	self.frame = CGRectMake(rect.origin.x, rect.origin.y, width, rect.size.height);
}

- (CGFloat) frameSizeHeight {
	return self.frame.size.height;
}

- (void) setFrameSizeHeight:(CGFloat)height {
	CGRect rect = self.frame;
	
	self.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, height);
}

- (void)removeAllSubview {
	NSArray *a = [[NSArray alloc] initWithArray:self.subviews];
	for(UIView *v in a) {
		[v removeFromSuperview];	
	}
	[a release];
}



CGImageRef AEViewCreateGradientImage (int pixelsWide,
									  int pixelsHigh)
{
	CGImageRef theCGImage = NULL;
    CGContextRef gradientBitmapContext = NULL;
    CGColorSpaceRef colorSpace;
	CGGradientRef grayScaleGradient;
	CGPoint gradientStartPoint, gradientEndPoint;
	
	// Our gradient is always black-white and the mask
	// must be in the gray colorspace
    colorSpace = CGColorSpaceCreateDeviceGray();
	
	// create the bitmap context
    gradientBitmapContext = CGBitmapContextCreate (NULL, pixelsWide, pixelsHigh,
												   8, 0, colorSpace, kCGImageAlphaNone);
	
	if (gradientBitmapContext != NULL) {
		// define the start and end grayscale values (with the alpha, even though
		// our bitmap context doesn't support alpha the gradient requires it)
		CGFloat colors[] = {0.0, 1.0,1.0, 1.0,};
		
		// create the CGGradient and then release the gray color space
		grayScaleGradient = CGGradientCreateWithColorComponents(colorSpace, colors, NULL, 2);
		
		// create the start and end points for the gradient vector (straight down)
		gradientStartPoint = CGPointZero;
		gradientEndPoint = CGPointMake(0,pixelsHigh);
		
		// draw the gradient into the gray bitmap context
		CGContextDrawLinearGradient (gradientBitmapContext, grayScaleGradient, gradientStartPoint, gradientEndPoint, kCGGradientDrawsAfterEndLocation);
		
		// clean up the gradient
		CGGradientRelease(grayScaleGradient);
		
		// convert the context into a CGImageRef and release the
		// context
		theCGImage=CGBitmapContextCreateImage(gradientBitmapContext);
		CGContextRelease(gradientBitmapContext);		
	}
	
	// clean up the colorspace
	CGColorSpaceRelease(colorSpace);
	
	// return the imageref containing the gradient
    return theCGImage;
}

- (UIImage *)reflectedImageRepresentationWithHeight:(NSUInteger)height
{
	CGContextRef mainViewContentContext;
    CGColorSpaceRef colorSpace;
	
    colorSpace = CGColorSpaceCreateDeviceRGB();
	
	// create a bitmap graphics context the size of the image
    mainViewContentContext = CGBitmapContextCreate (NULL, self.bounds.size.width,height, 8,0, colorSpace, kCGImageAlphaPremultipliedLast);
	
	// free the rgb colorspace
    CGColorSpaceRelease(colorSpace);	
	
	if (mainViewContentContext==NULL)
		return NULL;
	
	// offset the context. This is necessary because, by default, the  layer Ben Copsey Rob on a view for
	// caching its content is flipped. But when you actually access the layer content and have
	// it rendered it is inverted. Since we're only creating a context the size of our 
	// reflection view (a fraction of the size of the main view) we have to translate the context the
	// delta in size, render it, and then translate back (we could have saved/restored the graphics 
	// state
	
	CGFloat translateVertical=self.bounds.size.height-height;
	CGContextTranslateCTM(mainViewContentContext,0,-translateVertical);
	
	// render the layer into the bitmap context
	[self.layer renderInContext:mainViewContentContext];
	
	// translate the context back
	CGContextTranslateCTM(mainViewContentContext,0,translateVertical);
	
	// Create CGImageRef of the main view bitmap content, and then
	// release that bitmap context
	CGImageRef mainViewContentBitmapContext=CGBitmapContextCreateImage(mainViewContentContext);
	CGContextRelease(mainViewContentContext);
	
	// create a 2 bit CGImage containing a gradient that will be used for masking the 
	// main view content to create the 'fade' of the reflection.  The CGImageCreateWithMask
	// function will stretch the bitmap image as required, so we can create a 1 pixel wide
	// gradient
	CGImageRef gradientMaskImage=AEViewCreateGradientImage(1,height);
	
	// Create an image by masking the bitmap of the mainView content with the gradient view
	// then release the  pre-masked content bitmap and the gradient bitmap
	CGImageRef reflectionImage=CGImageCreateWithMask(mainViewContentBitmapContext,gradientMaskImage);
	CGImageRelease(mainViewContentBitmapContext);
	CGImageRelease(gradientMaskImage);
	
	// convert the finished reflection image to a UIImage 
	UIImage *theImage=[UIImage imageWithCGImage:reflectionImage];
	
	// image is retained by the property setting above, so we can 
	// release the original
	CGImageRelease(reflectionImage);
	
	// return the image
	return theImage;
}
#pragma mark --- animation ---
- (void)moveX:(float)x duration:(float)duration{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:duration];
	self.center = CGPointMake(self.center.x + x, self.center.y);
	[UIView commitAnimations];
}
- (void)moveY:(float)y duration:(float)duration{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:duration];
	self.center = CGPointMake(self.center.x, self.center.y + y);
	[UIView commitAnimations];
}
- (void)moveX:(float)x {
	self.center = CGPointMake(self.center.x + x, self.center.y);
}
- (void)moveY:(float)y {
	self.center = CGPointMake(self.center.x, self.center.y + y);
}
- (void)rotateAngleFrom:(float)from to:(float)to duration:(float)duration {
	CABasicAnimation * animation;
	animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    
	[animation setFromValue:[NSNumber numberWithFloat:from]];
    [animation setToValue:[NSNumber numberWithFloat:to]];
	
	animation.duration = duration;
    
    [animation setRemovedOnCompletion:YES];
    [animation setFillMode:kCAFillModeForwards];
    
	[self.layer addAnimation:animation forKey:@"rotateArrow"];
	self.transform = CGAffineTransformMakeRotation(to);

}
- (void) alphaAnimatedFrom:(float) from to:(float) to duration:(float)duration delegate:(id)delegate {
	CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
	theAnimation.removedOnCompletion = YES;
	theAnimation.duration = duration;
	theAnimation.repeatCount = 0;
	theAnimation.autoreverses = NO;
	theAnimation.fromValue = [NSNumber numberWithFloat:from];
	theAnimation.toValue = [NSNumber numberWithFloat:to];
	if (delegate != nil) {
		theAnimation.delegate = delegate;
	}
	
	
	[[self layer] addAnimation:theAnimation forKey:@"alpha"];
	self.alpha = to;
}
- (void)alphaTo:(float)to duration:(float)duration {
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:duration];
	self.alpha = to;
	[UIView commitAnimations];
}
- (void)scaleTo:(float)to positionTo:(CGPoint)dest animationCurve:(UIViewAnimationCurve)curve target:(id)target selector:(SEL)selector duration:(float)duration {
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationCurve:curve];
	[UIView setAnimationDuration:duration];
	[UIView setAnimationDelegate:target];
	[UIView setAnimationDidStopSelector:selector];

	self.center = dest;
	self.transform = CGAffineTransformMakeScale(to, to);
	[UIView commitAnimations];
	
}
- (void)scaleTo:(float)to positionTo:(CGPoint)dest target:(id)target selector:(SEL)selector duration:(float)duration {
	[self scaleTo:to positionTo:dest animationCurve:UIViewAnimationCurveEaseInOut target:target selector:selector duration:duration];
}
- (void)scaleTo:(float)to duration:(float)duration {
	[self scaleTo:to positionTo:self.center animationCurve:UIViewAnimationCurveEaseInOut target:nil selector:nil duration:duration];
}
- (void)scaleTo:(float)scale FadeOutTo:(CGPoint)to duration:(float)duration target:(id)target selector:(SEL)selector {
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:duration];
	[UIView setAnimationDelegate:target];
	[UIView setAnimationDidStopSelector:selector];
	self.alpha = 0;
	self.transform = CGAffineTransformMakeScale(scale, scale);
	self.center = to;
	[UIView commitAnimations];
}

- (void)scaleTo:(float)scale FadeOutTo:(CGPoint)to duration:(float)duration {
	[self scaleTo:scale FadeOutTo:to duration:duration target:nil selector:nil];
}
- (void)moveTo:(CGPoint)to duration:(float)duration target:(id)target selector:(SEL)selector{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:duration];
	[UIView setAnimationDelegate:target];
	[UIView setAnimationDidStopSelector:selector];
	self.center = to;
	[UIView commitAnimations];
}
- (void)moveTo:(CGPoint)to duration:(float)duration {
	[self moveTo:to duration:duration target:nil selector:nil];
}

- (void)moveIn:(UIView *)superView from:(kDirection)direction duration:(float)duration {
	CGPoint to;
	CGPoint origin;
	
	switch (direction) {
		case kLeft:
			origin = CGPointMake(-self.frame.size.width/2, superView.center.y);
			to = CGPointMake(self.frame.size.width/2, superView.center.y);
			break;
		case kRight:
			origin = CGPointMake(superView.frame.size.width + self.frame.size.width/2, superView.center.y );
			to = CGPointMake(superView.frame.size.width - self.frame.size.width/2, superView.center.y);
			break;
		case kTop:
			origin = CGPointMake(superView.center.x, -self.frame.size.height/2);
			to = CGPointMake(superView.center.x, self.frame.size.height/2);
			break;
		case kBottom:
			origin = CGPointMake(superView.center.x, superView.frame.size.height + self.frame.size.height/2);
			to = CGPointMake(superView.center.x, superView.frame.size.height - self.frame.size.height/2);
			break;
		default:
			break;
	}
	self.center = origin;
	[superView addSubview:self];
	[self moveTo:to duration:duration];
}
- (void)moveOutDidEnd:(id)view {
	UIView *theView = (UIView *)view;
	[theView removeFromSuperview];
}
- (void)moveOutFrom:(kDirection)direction duration:(float)duration {
	CGPoint to;
	UIView *superView = self.superview;
	switch (direction) {
		case kLeft:
			to = CGPointMake(-self.frameSizeWidth/2, self.center.y);
			break;
		case kRight:
			to = CGPointMake(superView.frameSizeWidth + self.frameSizeWidth/2, self.center.y);
			break;
		case kTop:
			to = CGPointMake(self.center.x, -self.frameSizeHeight/2);
			break;
		case kBottom:
			to = CGPointMake(self.center.x, superView.frameSizeHeight + self.frameSizeHeight/2);
			break;
		default:
			break;
	}
	[self moveTo:to duration:duration];
	[self performSelector:@selector(moveOutDidEnd:) withObject:self afterDelay:duration];
}
@end

#pragma mark -

@implementation UIButton (LDViewUtil)

- (void)setTitleForAllState:(NSString *) title {
	[self setTitle:title forState:UIControlStateNormal]; 
	[self setTitle:title forState:UIControlStateHighlighted];
	[self setTitle:title forState:UIControlStateDisabled];
	[self setTitle:title forState:UIControlStateSelected];
}

@end

#pragma mark --- NSDictionary MutableDeepCopy ---
@implementation  NSDictionary (MutableDeepCopy)

- (NSMutableDictionary *)mutableDeepCopy {
	NSMutableDictionary *ret = [[NSMutableDictionary alloc] initWithCapacity:self.count];
	NSArray *keys =[self allKeys];
	for (id key in keys) {
		id oneValue = [self valueForKey:key];
		id oneCopy  = nil;
		if ([oneValue respondsToSelector:@selector(mutableDeepCopy)]) 
			oneCopy = [oneValue mutableDeepCopy];
			
		else if ([oneValue respondsToSelector:@selector(mutableCopy)])
			oneCopy = [oneValue mutableCopy];
		if (nil == oneCopy) 
			oneCopy = [oneValue copy];
		[ret setValue:oneCopy forKey:key];
	}
	return ret;
}

@end

#pragma mark --- Corze draw ---
void CGContextFillStrokeRoundRect (CGContextRef context, CGRect rrect, CGFloat radius, CGPathDrawingMode mode) {
	CGFloat minx = CGRectGetMinX(rrect), midx = CGRectGetMidX(rrect), maxx = CGRectGetMaxX(rrect);
	CGFloat miny = CGRectGetMinY(rrect), midy = CGRectGetMidY(rrect), maxy = CGRectGetMaxY(rrect);
	
	CGContextMoveToPoint(context, minx, midy);
	CGContextAddArcToPoint(context, minx, miny, midx, miny, radius);
	CGContextAddArcToPoint(context, maxx, miny, maxx, midy, radius);
	CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
	CGContextAddArcToPoint(context, minx, maxy, minx, midy, radius);
	CGContextClosePath(context);
	CGContextDrawPath(context, mode);
}

void CGContextFillStrokeRoundBounds (CGContextRef context, CGRect bounds, CGPathDrawingMode mode) {
	CGSize size = bounds.size;
	CGRect rrect = CGRectMake(1, 1, size.width-2, size.height-2);
	CGFloat radius = MIN(rrect.size.width, rrect.size.height) * 0.3;
	radius = MIN(radius, 10);
	CGContextFillStrokeRoundRect(context, rrect, radius, mode);
}

@implementation UIViewController (LDViewUtil)

- (id)initWithNibUsingClassName {
	NSString *clazzName = [NSString stringWithCString:class_getName([self class]) encoding:NSUTF8StringEncoding];
#ifdef _DEBUG_METHOD_CALL
	NSLog(@"%@::initWithNibNameUsingClassName", clazzName);
#endif
	return [self initWithNibName:clazzName bundle:nil];	
}


@end

@implementation UILabel (LDViewUtil)
- (void)adjustHeight {
	CGSize size = CGSizeMake(self.frameSizeWidth, INT_MAX);
	size = [self.text sizeWithFont:self.font constrainedToSize:size];
	self.frameSizeHeight = size.height;
}
@end
//
//  AsanaAddPoseTableCell.m
//  Yoga
//
// Created by Rob on 10-3-29.
//  
//

#import "AsanaAddPoseTableCell.h"


@implementation AsanaAddPoseTableCell
@synthesize selectBtn;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:NO animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
	[selectBtn release];
    [super dealloc];
}


@end

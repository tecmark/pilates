//
//  LargeRatingView.m
//  Yoga
//
// Created by Rob on  11/10/10.
//  
//

#import "LargeRatingView.h"

const int StarWidth = 17;
const int SpaceWidth = 19;

@implementation LargeRatingView

- (void)calculateRating:(CGPoint)touchPoint {
	NSInteger touchedX = touchPoint.x;
	NSInteger selectedRating = 1;
	if (touchedX < 27) {
		selectedRating = 1;
	}
	else if (touchedX > StarWidth * 4 + SpaceWidth * 3 + SpaceWidth / 2) {
		selectedRating = 5;
	}
	else {
		selectedRating = (touchedX - 27) / (StarWidth + SpaceWidth) + 2;
	}

	if (selectedRating >= 1 && selectedRating <= 5) {
		[self setRating:selectedRating];
	}
}
- (void)addStarsImageView {
	self.backgroundColor = [UIColor clearColor];
	
	star1 = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"RateStar1_Large.png"]] autorelease];
	[self addSubview:star1];
	
	star5 = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"RateStar5_Large.png"]] autorelease];
	star5.clipsToBounds = YES;
	star5.contentMode = UIViewContentModeTopLeft;
	[self addSubview:star5];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	
	if (self != nil) {
		self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 161, 30);
		[self addStarsImageView];
	}
	
	return self;
}
- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
		[self addStarsImageView];
    }
    return self;
}

- (void)dealloc {
    [super dealloc];
}

- (void)setRating:(NSInteger)ratingVal {
	if (ratingVal < 0 || ratingVal > 5 || ratingVal == 1) {
		star5.hidden = YES;
		currentRating = 1;
	}
	else if (ratingVal == 5) {
		star5.frame = CGRectMake(star5.frame.origin.x, star5.frame.origin.y, 161, 30);
		currentRating = ratingVal;
	}
	else {
		star5.hidden = NO;
		NSInteger star5Width = ratingVal * StarWidth + (ratingVal - 1) * SpaceWidth;
		star5.frame = CGRectMake(star5.frame.origin.x, star5.frame.origin.y, star5Width, 30);
		currentRating = ratingVal;
	}
}
- (NSInteger)rating {
	return currentRating;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    NSLog(@"Rating touch began");
	UITouch *touch = [touches anyObject];
	CGPoint touchPoint = [touch locationInView:[touch view]];
	
	[self calculateRating:touchPoint];
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	CGPoint touchPoint = [touch locationInView:[touch view]];
	
	[self calculateRating:touchPoint];
}

@end

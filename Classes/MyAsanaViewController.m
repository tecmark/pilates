//
//  MyAsanaViewController.m
//  Yoga
//
// Created by Rob on 10-3-23.
//  
//

#import "MyAsanaViewController.h"
#import "AddAsanaViewController.h"
#import "MyAsanaTableCell.h"
#import "MyAsanaPosesViewContorller.h"
#define kTableCellHeight 65
#define kTableMaxHeight 383

@implementation MyAsanaViewController
NSString *titleCache;



- (void)loadData {
	id<PLResultSet> result;
	id<PLResultSet> subResult;
	
//	result = [dbHandle() executeQuery: @"SELECT * FROM MyAsana order by AsanaIndex desc"];
	result = [dbHandle() executeQuery: @"SELECT * FROM MyAsana order by AsanaIndex asc"];
    NSLog(@"result: %@", result);
    
	while ([result next]) {
		NSNumber *ID = [result objectForColumn:@"ID"];
		NSString *name = [result stringForColumn:@"Name"];
		//		NSNumber *isPlayMusic = [result objectForColumn:@"isPlayMusic"];
		NSNumber *intervalTime = [result objectForColumn:@"intervalTime"];
		NSNumber *index = [result objectForColumn:@"AsanaIndex"];
		//		NSString *description = @"";
		NSString *picPath = @"";
		
		subResult = [dbHandle() executeQuery: @"SELECT * FROM MyAsanaPoses where AsanaID = ? order by PoseIndex DESC", [ID stringValue]];
		NSNumber *poseID = nil;
		if ([subResult next]) {
			poseID = [subResult objectForColumn:@"PoseID"];
		}
		
		[subResult close];
		
		if (poseID) {
			subResult = [dbHandle() executeQuery: @"SELECT * FROM Poses where ID = ?", [poseID stringValue]];
			if ([subResult next]) {
				picPath = [subResult stringForColumn:@"PicturePath"];
				//				description = [subResult stringForColumn:@"Description"];
			}
			[subResult close];
		}
		
		NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
									 ID, @"ID",
									 name, @"asanaName", 
									 picPath, @"picPath",
									 //							  description, @"description",
									 //							  isPlayMusic, @"isPlayMusic",
									 intervalTime, @"intervalTime",
									 index, @"index",
									 nil];
		[dataArray addObject:dict];
	}
	[result close];
}
- (void)viewDidLoad {
    [super viewDidLoad];
	
    showEditBtn=NO;
}

- (void)viewWillAppear:(BOOL)animated {
	NSLog(@"View will appear");
	 showEditBtn=NO;
	[dataArray removeAllObjects];
	[self loadData];
	[table reloadData];
	table.frameSizeHeight = MIN(kTableMaxHeight, 380 /*kTableCellHeight*dataArray.count*/);
	
	titleLB.text = @"My Sequences";
}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}
#pragma mark --- UITableView delegate ---
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSMutableDictionary *dict = [dataArray objectAtIndex:indexPath.row];
	MyAsanaPosesViewContorller *vc = [MyAsanaPosesViewContorller alloc];
	vc.asanaDict = dict;
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}

#pragma mark --- UITableView Datasource ---

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    NSDictionary *dict = [[dataArray objectAtIndex:sourceIndexPath.row] retain];
    
    MyAsanaTableCell *cell = (MyAsanaTableCell *)[table cellForRowAtIndexPath:sourceIndexPath];
	cell.editBtn.tag = destinationIndexPath.row;
	cell.titleTF.tag = destinationIndexPath.row;
    
    
    if(destinationIndexPath.row<sourceIndexPath.row){
    //move up screen
    for (int i = destinationIndexPath.row; i < sourceIndexPath.row; i++) {
    
        NSIndexPath *thisIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
        
        
        cell = (MyAsanaTableCell *)[table cellForRowAtIndexPath:thisIndexPath];
        cell.editBtn.tag = i+1;
        cell.titleTF.tag = i+1;
    }
    }
    
    
    if(destinationIndexPath.row>sourceIndexPath.row){
        //move down screen
        for (int i = sourceIndexPath.row+1; i <= destinationIndexPath.row; i++) {
            
            NSIndexPath *thisIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
            
            
            cell = (MyAsanaTableCell *)[table cellForRowAtIndexPath:thisIndexPath];
            cell.editBtn.tag = i-1;
            cell.titleTF.tag = i-1;
        }
    }
    

    

	[dataArray removeObjectAtIndex:sourceIndexPath.row];
	[dataArray insertObject:dict atIndex:destinationIndexPath.row];
	[dict release];
	
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	
    
    for (int i = indexPath.row + 1; i < dataArray.count; i++) {
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
		MyAsanaTableCell *cell = (MyAsanaTableCell *)[table cellForRowAtIndexPath:indexPath];
		cell.editBtn.tag = i - 1;
		cell.titleTF.tag = i - 1;
	}
	
	
	NSDictionary *dict = [dataArray objectAtIndex:indexPath.row];
	NSNumber *ID = [dict objectForKey:@"ID"];
	[dbHandle() executeUpdate:@"delete from MyAsana where ID = ?", [ID stringValue]];
    [dbHandle() executeUpdate:@"delete from MyAsanaPoses where AsanaID = ?", [ID stringValue]];
	[dataArray removeObjectAtIndex:indexPath.row];
	[table deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
	table.frameSizeHeight = MIN(kTableMaxHeight, 380);
    
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	MyAsanaTableCell *cell = (MyAsanaTableCell *)[tableView dequeueReusableCellWithIdentifier:@"MyAsanaTableCell"];
	//if (nil == cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyAsanaTableCell" owner:nil options:nil];
		cell = (MyAsanaTableCell *)[nib objectAtIndex:0];
	//}
	
	NSDictionary *dict = [dataArray objectAtIndex:indexPath.row];
	
	NSString *name = [dict objectForKey:@"asanaName"];
    
   // name = [name stringByAppendingString:[dict objectForKey:@"asanaName"]];
	NSString *picPath = [dict objectForKey:@"picPath"];
	
    
	UIImage *poseImg = [UIImage imageNamed:[picPath stringByReplacingOccurrencesOfString:@".jpg" withString:@"_thumb.jpg"]];
	cell.poseIV.image = poseImg;
	cell.titleTF.text = name;
	cell.titleTF.tag = indexPath.row;
	[cell.titleTF addTarget:self action:@selector(editNameDone:) forControlEvents:UIControlEventEditingDidEndOnExit];
	cell.editBtn.tag = indexPath.row;
    if(showEditBtn){
        cell.editBtn.alpha=1;
        CGRect frame = cell.titleTF.frame;
		frame.size.width=110;
		
		[cell.titleTF setAutocapitalizationType:UITextAutocapitalizationTypeWords];
		cell.titleTF.frame=frame;
        
    }
    else{
   cell.editBtn.alpha=0;
    }
   
	[cell.editBtn addTarget:self action:@selector(editPoseName:) forControlEvents:UIControlEventTouchUpInside];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	//	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	return cell;
}

#pragma mark --- UITextFieldDelegate ---

#pragma mark --- IBAction ---
- (IBAction)addRow {
	//	if (editBtn.selected) return;
	
	AddAsanaViewController *vc = [AddAsanaViewController alloc];
	[vc initWithNibUsingClassName];
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}

- (IBAction)editTable {
	/*
	 editBtn.selected = YES;
	 
	 addBtn.selected = YES;
	 */
    
    showEditBtn=YES;
	addBtn.hidden = YES;
	doneBtn.hidden = NO;
	table.editing = YES;
	editBtn.enabled = NO;
	
	for (int i = 0; i < dataArray.count; i++) {
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
		MyAsanaTableCell *cell = (MyAsanaTableCell *)[table cellForRowAtIndexPath:indexPath];
		
		CGRect frame = cell.titleTF.frame;
		frame.size.width=110;
		
		[cell.titleTF setAutocapitalizationType:UITextAutocapitalizationTypeWords];
		cell.titleTF.frame=frame;
		cell.editBtn.alpha=1;
		cell.divider.hidden = NO;
		cell.arrow.hidden = YES;
	}
	
}

- (IBAction)editPoseName:(id)sender {
    

    
    if(previousNameEdit!=nil){
        
        NSIndexPath *indexPathPrev = [NSIndexPath indexPathForRow:previousNameEdit inSection:0];
        MyAsanaTableCell *cellPrev = (MyAsanaTableCell *)[table cellForRowAtIndexPath:indexPathPrev];
        cellPrev.titleTF.borderStyle = UITextBorderStyleNone;
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
	MyAsanaTableCell *cell = (MyAsanaTableCell *)[table cellForRowAtIndexPath:indexPath];
	titleCache=[[NSString alloc] initWithString:cell.titleTF.text];
	cell.titleTF.userInteractionEnabled = YES;
	cell.titleTF.borderStyle = UITextBorderStyleRoundedRect;
	[cell.titleTF becomeFirstResponder];
    
    if(indexPath.row>2){
        
        preScrollPoint=table.contentOffset;
        [table setContentOffset:CGPointMake(0,(indexPath.row*65)-140) animated:YES];
    }
    
    previousNameEdit=[sender tag];
        
}

- (IBAction)editNameDone:(id)sender {
    

    
	UITextField *tf = (UITextField *)sender;
	[tf resignFirstResponder];
	tf.userInteractionEnabled = NO;
	tf.borderStyle = UITextBorderStyleNone;
    
 
	
	if([tf.text length]>0){
		
		NSMutableDictionary *dict = [dataArray objectAtIndex:[sender tag]];
		[dict setObject:tf.text forKey:@"asanaName"];
		NSNumber *ID = [dict objectForKey:@"ID"];
		[dbHandle() executeUpdate:@"update MyAsana set Name = ? where ID = ?", tf.text, [ID stringValue]];
        
        if([sender tag]>2){
            [table setContentOffset:preScrollPoint animated:YES];
        }
        
	}
	else{
		
		tf.text=titleCache;	
		
	}
}
- (IBAction)editTableDone {
    
     showEditBtn=NO;
    
    NSLog(@"order changed");
	table.editing = NO;
	editBtn.enabled = YES;
    

    
	for (int i = 0; i < dataArray.count; i++) {
        NSDictionary *dict = [dataArray objectAtIndex:i];
		NSNumber *ID = [dict objectForKey:@"ID"];
        
  //  NSLog([NSString stringWithFormat:@"update MyAsana set AsanaIndex = %@ where ID = %@",stringFromInt(i+1),[ID stringValue]]);
        
        
		
    [dbHandle() executeUpdate:@"update MyAsana set AsanaIndex = ? where ID = ?",
		 stringFromInt(i+1),[ID stringValue]];
        
	}
	addBtn.hidden = NO;
	doneBtn.hidden = YES;
	for (int i = 0; i < dataArray.count; i++) {
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
		MyAsanaTableCell *cell = (MyAsanaTableCell *)[table cellForRowAtIndexPath:indexPath];
        
        CGRect frame = cell.titleTF.frame;
		frame.size.width=188;
        cell.titleTF.frame=frame;
        
		cell.editBtn.alpha=0;
		cell.divider.hidden = YES;
		cell.arrow.hidden = NO;
        
          //if editing text then tidy-up as required
        if(cell.titleTF.editing){
            NSLog(@"editing %@", cell.titleTF.text);
            [self editNameDone:cell.titleTF];
        }
	}
}
@end

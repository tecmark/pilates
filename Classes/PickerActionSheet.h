//
//  PickerActionSheet.h
//  iAsthma
//
// Created by Rob on  8/7/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PickerActionSheet : UIActionSheet  {
	UIPickerView *picker;
	BOOL layoutDone;
}

@property (nonatomic, retain) UIPickerView *picker;

@end

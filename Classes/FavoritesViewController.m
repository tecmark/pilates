//
//  FavoritesViewController.m
//  Yoga
//
// Created by Rob on 10-3-23.
//  
//

#import "FavoritesViewController.h"
#define kTableCellHeight 65
#define kTableMaxHeight	 390
@implementation FavoritesViewController



- (void)loadData {
	id<PLResultSet> result;
	
	result = [dbHandle() executeQuery: @"SELECT * FROM Poses where IsFavorite = 1"];
	while ([result next]) {
		/*
		NSNumber *poseID = [result objectForColumn:@"ID"];
		NSString *name = [result stringForColumn:@"Name"];
		NSString *description = [result stringForColumn:@"Description"];
		NSString *picPath = [result stringForColumn:@"PicturePath"];
		
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
							  poseID, @"poseID",
							  name, @"poseName", 
							  picPath, @"picPath",
							  description, @"description",
							  nil];
		*/
		
		NSString *picPath = [result safeObjectForColumn:@"PicturePath"];
		NSNumber *poseID = [result safeObjectForColumn:@"ID"];
		NSString *poseName = [[[[[result safeObjectForColumn:@"Name"] capitalizedString] stringByReplacingOccurrencesOfString:@"Iii" withString:@"III"]stringByReplacingOccurrencesOfString:@"Ii" withString:@"II"]stringByReplacingOccurrencesOfString:@"Iv" withString:@"IV"];
		NSString *sanskrit = [[[[[result safeObjectForColumn:@"Sanskrit"] capitalizedString] stringByReplacingOccurrencesOfString:@"Iii" withString:@"III"]stringByReplacingOccurrencesOfString:@"Ii" withString:@"II"]stringByReplacingOccurrencesOfString:@"Iv" withString:@"IV"];
		NSString *description = [result safeObjectForColumn:@"description"];
		NSString *intro =  [result safeObjectForColumn:@"Intro"];
		
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
							  picPath, @"picPath",
							  poseName, @"poseName", 
							  sanskrit, @"sanskrit",
							  description, @"description", 
							  poseID, @"poseID", 
							  intro, @"Intro",
							  nil];
		
		[dataArray addObject:dict];
	}
	[result close];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	titleLB.text = @"Favorites";
	
}

- (void)viewWillAppear:(BOOL)animated{
	[dataArray removeAllObjects];
	[self loadData];
	[table reloadData];
	
	table.frameSizeHeight = MIN(kTableMaxHeight, 380 /*kTableCellHeight*dataArray.count*/);
    
}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

- (IBAction)favoriteCellTouched:(id)sender {
	
	UIButton *thisBtn = sender;
	NSLog(@"favorites cell touched, %i", thisBtn.tag);
	NSDictionary *dict = [dataArray objectAtIndex:thisBtn.tag];
	NSNumber *poseId = [dict objectForKey:@"poseID"];
	
	
	
	if(!thisBtn.selected){
		
		NSLog(@"fav toggled off");
		
		thisBtn.selected=YES;
		//update database
		
		NSNumber *isFavorite = [NSNumber numberWithBool:NO];
		[dbHandle() executeUpdate:@"update Poses set IsFavorite = ? where ID = ?", [isFavorite stringValue], [poseId stringValue]];
	}
	else{
		
		thisBtn.selected=NO;
		NSLog(@"fav toggled on");
		
		NSNumber *isFavorite = [NSNumber numberWithBool:YES];
		[dbHandle() executeUpdate:@"update Poses set IsFavorite = ? where ID = ?", [isFavorite stringValue], [poseId stringValue]];
		
		
	}
}

@end

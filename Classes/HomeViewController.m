//
//  HomeViewController.m
//  
//
// Created by Rob on 03/25/10.
//  
//

#import "HomeViewController.h"
#import "DailyTipViewController.h"
#import "AsanaViewController.h"
#import "ClassesViewController.h"
#import "JournalViewController.h"
#import "ConnectViewController.h"
#import "LibraryViewController.h"
#import "InfoHomeViewController.h"

@implementation HomeViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.switchViewContorllerNameArray = [NSArray arrayWithObjects:
										  @"DailyTipViewController",
										  @"AsanaViewController",
										  @"ClassesViewController",
										  @"JournalViewController",
										  @"ConnectViewController",
										  @"LibraryViewController",
										  @"InfoHomeViewController",
										  nil];
}



// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end

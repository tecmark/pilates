//
//  LargeRatingView.h
//  Yoga
//
// Created by Rob on  11/10/10.
//  
//

#import <UIKit/UIKit.h>


@interface LargeRatingView : UIView {

	UIImageView *star1;
	UIImageView *star5;
	
	NSInteger currentRating;
}

@property NSInteger rating;

- (void)setRating:(NSInteger)rating;

@end

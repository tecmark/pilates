//
//  RatingView.h
//  Yoga
//
// Created by Rob on  11/9/10.
//  
//

#import <UIKit/UIKit.h>


@interface RatingView : UIView {
	UIImageView *star1;
	UIImageView *star5;
}

- (void)setRating:(NSInteger)rating;

@end

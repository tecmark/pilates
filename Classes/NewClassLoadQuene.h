//
//  NewClassLoadQuene.h
//  Yoga
//
// Created by Rob on  11/30/10.
//  
//

#import <Foundation/Foundation.h>
#import "UAGlobal.h"


@interface NewClassLoadQuene : NSOperationQueue {
	BOOL allOperationsFinished;
}

SINGLETON_INTERFACE(NewClassLoadQuene)


@property BOOL allOperationsFinished;

- (void)addObserver:(id)observer;
- (void)removeObserver:(id)observer;

@end

//
//  MyPageControl.h
//  Yoga
//
// Created by Rob on 10-4-9.
//

#import <Foundation/Foundation.h>


@interface MyPageControl : UIPageControl {
	UIImage* mImageNormal;
	UIImage* mImageCurrent;
}

@property (nonatomic, readwrite, retain) UIImage* imageNormal;
@property (nonatomic, readwrite, retain) UIImage* imageCurrent;

@end

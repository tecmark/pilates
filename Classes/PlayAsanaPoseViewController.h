//
//  PlayAsanaPoseViewController.h
//  Yoga
//
// Created by Rob on 10-4-6.
//  
//

#import <UIKit/UIKit.h>
#import "MyPageControl.h"

@interface PlayAsanaPoseViewController : CommonViewController <UIScrollViewDelegate, AVAudioPlayerDelegate> {
	IBOutlet UIScrollView	*poseScrollView;
	IBOutlet MyPageControl	*pageControl;
	IBOutlet UILabel		*descriptionTV;
	IBOutlet UIButton		*playBtn;
	IBOutlet UILabel		*stepNameLB;
	IBOutlet UILabel		*poseNameLB;
	int						currentPageNumber;

	NSMutableArray			*poseStepArray;
	NSMutableArray			*poseAudioArray;
	int						currentRepeatCount;

	BOOL                    isPlayAudio;
	NSTimer					*poseIntervalTimer;
	NSTimer					*autoTimer;
	NSArray					*asanaPoseArray;
	NSString				*asanaName;
	int poseInterval;
	int currentPoseID;
	BOOL poseIntervaling;
	BOOL isRepeat;
	IBOutlet UIScrollView   *wholeScrollView;

	BOOL isScrollingBack;
	NSInteger previousPageNumber;
}

@property (nonatomic, retain) NSArray				*asanaPoseArray;
@property (nonatomic, retain) NSString				*asanaName;
@property int poseInterval;

- (IBAction)playOrStopAudio:(id)sender;

@end

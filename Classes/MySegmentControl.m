//
//  MySegmentControl.m
//  Yoga
//
// Created by Rob on 10-4-21.
//  
//

#import "MySegmentControl.h"


@implementation MySegmentControl
@synthesize delegate;
@synthesize background;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)dealloc {
    [super dealloc];
}

- (IBAction)selectedNumber:(id)sender {
    NSLog(@"My Segment Control");
	[delegate selectedNumber:[sender tag]];
      NSLog(@"My Segment Control end");
}
@end

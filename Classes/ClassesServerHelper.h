//
//  ClassesServerHelper.h
//  Yoga
//
// Created by Rob on  11/12/10.
//  
//

#import <Foundation/Foundation.h>
#import "YogaClass.h"

@interface ClassesServerHelper : NSObject {

}

+ (NSString*)getClassInfoTxtByID:(NSString*)classID;
+ (NSData*)getClassBiogImageByID:(NSString*)classID;
+ (NSString*)getClassBiogTxtByID:(NSString*)classID;
+ (NSArray*)getClassKramaByID:(NSString*)classID;
+ (NSData*)getClassMainImageByID:(NSString*)classID;

+ (YogaClass*)getClassByID:(NSString*)classID;

@end

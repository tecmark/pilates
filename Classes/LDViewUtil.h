//
//  LDViewUtil.h
//
//

#import <UIKit/UIKit.h>
typedef enum _Direction {
	kLeft = 0,
	kRight,
	kTop,
	kBottom
} kDirection;

void CGContextFillStrokeRoundRect (CGContextRef c, CGRect rrect, CGFloat radius, CGPathDrawingMode mode);
void CGContextFillStrokeRoundBounds (CGContextRef context, CGRect bounds, CGPathDrawingMode mode);

@interface UIView (LDViewUtil)

@property (nonatomic, assign) CGPoint frameOrigin;
- (CGPoint)frameOrigin;
- (void)setFrameOrigin:(CGPoint)origin;
- (void)setFrameOrigin:(CGFloat)x :(CGFloat)y;

@property (nonatomic, assign) CGFloat frameOriginX;
- (CGFloat)frameOriginX;
- (void)setFrameOriginX:(CGFloat)x;

@property (nonatomic, assign) CGFloat frameOriginY;
- (CGFloat)frameOriginY;
- (void)setFrameOriginY:(CGFloat)y;

@property (nonatomic, assign) CGSize frameSize;
- (CGSize) frameSize;
- (void) setFrameSize:(CGSize)size;
- (void) setFrameSize:(CGFloat)width :(CGFloat)height;	

@property (nonatomic, assign) CGFloat frameSizeWidth;
- (CGFloat) frameSizeWidth;
- (void) setFrameSizeWidth:(CGFloat)width;

@property (nonatomic, assign) CGFloat frameSizeHeight;
- (CGFloat) frameSizeHeight;
- (void) setFrameSizeHeight:(CGFloat)height;

- (void)removeAllSubview;

- (UIImage *)reflectedImageRepresentationWithHeight:(NSUInteger)height;
#pragma mark --- animation ---
- (void)moveX:(float)x;
- (void)moveY:(float)y;
- (void)moveX:(float)x duration:(float)duration;
- (void)moveY:(float)y duration:(float)duration;
- (void)rotateAngleFrom:(float)from to:(float)to duration:(float)duration;
- (void)alphaAnimatedFrom:(float) from to:(float) to duration:(float)duration delegate:(id)delegate;
- (void)alphaTo:(float)to duration:(float)duration;
- (void)scaleTo:(float)to positionTo:(CGPoint)dest animationCurve:(UIViewAnimationCurve)curve target:(id)target selector:(SEL)selector duration:(float)duration;
- (void)scaleTo:(float)to positionTo:(CGPoint)dest target:(id)target selector:(SEL)selector duration:(float)duration;
- (void)scaleTo:(float)to duration:(float)duration;
- (void)scaleTo:(float)scale FadeOutTo:(CGPoint)to duration:(float)duration target:(id)target selector:(SEL)selector;
- (void)scaleTo:(float)scale FadeOutTo:(CGPoint)to duration:(float)duration;
- (void)moveTo:(CGPoint)to duration:(float)duration target:(id)target selector:(SEL)selector;
- (void)moveTo:(CGPoint)to duration:(float)duration;
- (void)moveIn:(UIView *)superView from:(kDirection)direction duration:(float)duration;
- (void)moveOutFrom:(kDirection)direction duration:(float)duration;

@end


#pragma mark -

@interface UIButton (LDViewUtil)

- (void)setTitleForAllState:(NSString *) title;

@end

@interface NSDictionary (MutableDeepCopy)

- (NSMutableDictionary *)mutableDeepCopy;

@end

@interface UIViewController (LDViewUtil) 
- (id)initWithNibUsingClassName;

@end

@interface UILabel (LDViewUtil) 
- (void)adjustHeight;

@end
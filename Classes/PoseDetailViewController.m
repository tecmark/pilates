//
//  PoseDetailViewController.m
//  Yoga
//
// Created by Rob on 10-3-23.
//  
//

#import "PoseDetailViewController.h"
#import "JournalHomeTableCell.h"
#import "IssueDetailViewController.h"
#import "TableViewHeader.h"

#define kTableMaxHeight 350
#define kTableCellHeight 50

@implementation PoseDetailViewController

@synthesize poseDict;
@synthesize needAutoPlay;
/*
 @synthesize isFromMyAsana;
 @synthesize asanaPoseArray;
 @synthesize asanaName;
 @synthesize poseInterval;
 */

- (void)loadData {
	id<PLResultSet> result;
	
	NSString *poseName = [poseDict objectForKey:@"poseName"];
	result = [dbHandle() executeQuery: @"SELECT * FROM Issues where PoseName = ? order by id desc", poseName];
	while ([result next]) {
		NSString *content = [result safeObjectForColumn:@"IssueContent"];
		
		NSNumber *ID = [result safeObjectForColumn:@"ID"];
		
		NSString *dateStr = [result safeObjectForColumn:@"UpdateDate"];
		
		BOOL isResovled = [result boolForColumn:@"IsResolved"];
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:poseName, @"poseName",
							  content, @"content",
							  ID, @"ID", 
							  dateStr, @"updateDate", nil];
		if (isResovled) {
			[resolvedIssueArray addObject:dict];
		}
		else {
			[notResolvedIssueArray addObject:dict];
		}
	}
	[result close];
}
- (void)preparePoseAuidoArray {
	for (int i = 0; i < poseStepArray.count; i++) {
		NSNull *null = [NSNull null];
		[poseAudioArray addObject:null];
	}
}
- (void)loadPoseSteps {
	NSNumber *poseId = [poseDict objectForKey:@"poseID"];
	NSString *stepName;
	NSString *picPath;
	NSString *description;
	NSString *audioPath;
	
	picPath = [poseDict objectForKey:@"picPath"];
	description = [poseDict objectForKey:@"description"];
	
	
	stepName = @"Introduction";
	//@wav
	//audioPath = [NSString stringWithFormat:@"instruction_%i.wav", [poseId intValue]];
	audioPath = [NSString stringWithFormat:@"Intro.mp3", [poseId intValue]];
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
						  stepName, @"stepName",
						  picPath, @"picPath",
						  audioPath, @"audioPath",
						  description, @"description",
						  nil];
	[poseStepArray addObject:dict];
	NSString *poseIDStr = [poseId isKindOfClass:[NSNumber class]] ? [poseId stringValue] : (NSString*)poseId;
	id<PLResultSet> result;
	result = [dbHandle() executeQuery: @"SELECT * FROM PoseSteps where PoseID = ? order by StepIndex", poseIDStr];
	
	int stepIndex = 1;
	
	while ([result next]) {
		
		
		//@TODO  is the step name like "Step 1" or some sepcilized name?
		//		stepName = [result stringForColumn:@"Name"];
		
		stepName = [NSString stringWithFormat:@"Step %i", stepIndex];
		picPath = [result stringForColumn:@"PicturePath"];
		//@wav
		//audioPath = [NSString stringWithFormat:@"step%i_%i.wav", stepIndex, [poseId intValue]];
        //audioPath = [NSString stringWithFormat:@"%i_%i.mp3", [poseId intValue], stepIndex];
        audioPath = [result safeObjectForColumn:@"AudioPath"];

		NSLog(@"Playing Audio:%@", audioPath);
		description = [result stringForColumn:@"Description"];
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
							  stepName, @"stepName",
							  picPath, @"picPath",
							  audioPath, @"audioPath",
							  description, @"description",
							  nil];
		[poseStepArray addObject:dict];
		stepIndex++;
		
	}
	[result close];
	
	audioPath = [NSString stringWithFormat:@"Intro.mp3", [poseId intValue]];
	if([poseId intValue]==31){
		dict = [NSDictionary dictionaryWithObjectsAndKeys:
				@"Step 4", @"stepName",
				picPath, @"picPath",
				audioPath, @"audioPath",
				description, @"description",
				nil];
	}
	else{
	dict = [NSDictionary dictionaryWithObjectsAndKeys:
			@"Progression", @"stepName",
			picPath, @"picPath",
			audioPath, @"audioPath",
			description, @"description",
			nil];
	}
	
	[poseStepArray replaceObjectAtIndex:poseStepArray.count-1 withObject:dict];
	
	[self preparePoseAuidoArray];	
}
- (void)setFavoriteBtn {
	NSNumber *poseId = [poseDict objectForKey:@"poseID"];
	id<PLResultSet> result;
    NSString *q = [NSString stringWithFormat:@"SELECT IsFavorite FROM Poses WHERE ID = %@", poseId];
	result = [dbHandle() executeQuery:q];
	if ([result next]) {
		NSNumber *isFavorite = [result safeObjectForColumn:@"IsFavorite"];
        NSLog(@"isFav = %d", [isFavorite boolValue]);
		favoriteBtn.selected = [isFavorite boolValue];
	}
}
- (void)setStepContent {
	NSDictionary *dict = [poseStepArray objectAtIndex:currentPageNumber];
	NSString *description = [dict objectForKey:@"description"];
	NSString *stepName = [dict objectForKey:@"stepName"];
	
    /*
	description = [description stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n\n"];
	description = [description stringByReplacingOccurrencesOfString:@" INHALE:" withString:@"\n\nINHALE:"];
	description = [description stringByReplacingOccurrencesOfString:@" EXHALE:" withString:@"\n\nEXHALE:"];
	description = [description stringByReplacingOccurrencesOfString:@" BREATHING HERE:" withString:@"\n\nBREATHING HERE:"];
	description = [description stringByReplacingOccurrencesOfString:@" Also Try " withString:@"\n\nAlso Try: "];
	description = [description stringByReplacingOccurrencesOfString:@" Repeat to left side" withString:@"\n\nRepeat to left side"];
	
    description = [description stringByReplacingOccurrencesOfString:@" Tips:" withString:@"\n\nTips:"];
    
	description = [description stringByReplacingOccurrencesOfString:@" Relax for a few breaths and then repeat." withString:@"\n\nRelax for a few breaths and then repeat."];
	*/
	
	
	descriptionTV.text = description;
	CGSize size = CGSizeMake(280, INT_MAX);
	descriptionTV.frameSize = [description sizeWithFont:descriptionTV.font constrainedToSize:size];
	int pageHeight = MAX(380, descriptionTV.frameOriginY+descriptionTV.frameSizeHeight+44);
	playPoseView.contentSize = CGSizeMake(320, pageHeight);
	stepNameLB.text = stepName;
	
	
	Sound *s =[poseAudioArray objectAtIndex:currentPageNumber];
	if ([NSNull null] == (NSNull *)s) {
		NSString *file = [dict objectForKey:@"audioPath"];
        NSLog(@"File: %@", file);
        NSLog(@"Dict: %@", dict);
		s = [[Sound alloc] initWithResourceFile:file];
		s.player.delegate = self;
		[poseAudioArray replaceObjectAtIndex:currentPageNumber withObject:s];
		[s release];
	}
	if (isPlayAudio) {
		[[poseAudioArray objectAtIndex:currentPageNumber] playOnce];
	}
	
}

#pragma mark --- set/get Method ---
- (void) setCurrentPageNumber:(int)num {
	currentPageNumber = num;
	//set the tip info
	
	[self setStepContent];
	
	pageControl.currentPage = currentPageNumber;
	[pageControl updateCurrentPageDisplay];
	
}
- (void)setScrollView {
	poseScrollView.contentSize = CGSizeMake(poseStepArray.count*poseScrollView.frameSizeWidth,poseScrollView.frameSizeHeight);
	NSDictionary *dict;
	UIImageView *imageView;
	UIImage *poseImg;
	for (int i = 0; i < poseStepArray.count; i++) {
		dict = [poseStepArray objectAtIndex:i];
		poseImg = [UIImage imageNamed:[dict objectForKey:@"picPath"]];
		imageView = [[UIImageView alloc] initWithImage:poseImg];
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        [imageView setClipsToBounds:YES];
		imageView.frame = CGRectMake(i*poseScrollView.frameSizeWidth, 0, poseScrollView.frameSizeWidth, poseScrollView.frameSizeHeight);
		[poseScrollView addSubview:imageView];
		[imageView release];
	}
	poseScrollView.delegate = self;
	poseScrollView.showsHorizontalScrollIndicator = NO;
	poseScrollView.showsVerticalScrollIndicator = NO;
	self.currentPageNumber = 0;
}
- (void)gotoNextPage {
	[poseScrollView setContentOffset:CGPointMake(poseScrollView.contentOffset.x + poseScrollView.frameSizeWidth,
												 poseScrollView.contentOffset.y) animated:YES];
	self.currentPageNumber = currentPageNumber + 1;
	
}


- (void)viewDidLoad {
	[super viewDidLoad];

	
	self.isForPoseDetailsView = YES;
	
	isIssuesViewShown = NO;
	
	[self.view insertSubview:playPoseView atIndex:1];
	poseStepArray = [NSMutableArray new];
	poseAudioArray = [NSMutableArray new];
	//	playOrStopBtnArray = [NSMutableArray new];
	[self loadPoseSteps];
	pageControl.numberOfPages = poseStepArray.count;
	
	if(pageControl.numberOfPages==5){pageControl.frameOriginX=18;}
	else if(pageControl.numberOfPages==4){pageControl.frameOriginX=8;}
	else  if(pageControl.numberOfPages==3){pageControl.frameOriginX=-1;}
	else  if(pageControl.numberOfPages==2){pageControl.frameOriginX=-12;}
	
	[self setScrollView];
	
	playPoseBtn.selected = YES;
	titleLB.text = [poseDict objectForKey:@"poseName"];
	poseNameLB.text = [poseDict objectForKey:@"poseName"];
	[self setFavoriteBtn];
	if (needAutoPlay) {
		[self playOrStopAudio];
	}
	
	//[self setStepContent];
     
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:YES];
	issueCount = notResolvedIssueArray.count;
	if (0 == notResolvedIssueArray.count) {
		badgeView.hidden = YES;
		issueCountLB.text=@"";
	}
	else {
		badgeView.hidden = NO;
		issueCountLB.text = stringFromInt(notResolvedIssueArray.count);
	}
	
	if (isPlayAudio && !isIssuesViewShown) {
		[[poseAudioArray objectAtIndex:currentPageNumber] resumePlay];
	}
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	//	[playOrStopBtnArray release];
	[poseDict release];
	[poseStepArray release];
	[poseAudioArray release];
    [super dealloc];
}


#pragma mark --- IBAction ---
- (IBAction)gotoProduct {
}
- (IBAction)gotoPlayPoseView {
	if (!playPoseBtn.selected) {
		playPoseBtn.selected = YES;
		//playPoseBtn.enabled = NO;
		
		gotoPoseIssueBtn.selected = NO;
		
		playPoseView.frameOrigin = CGPointMake(0, 40);
		[issueView removeFromSuperview];
		titleLB.text = [poseDict objectForKey:@"poseName"];
		isIssuesViewShown = NO;
		[self.view insertSubview:playPoseView atIndex:1];
		issueCountLB.text = stringFromInt(notResolvedIssueArray.count);
		if (0 < notResolvedIssueArray.count) {
			badgeView.hidden = NO;
		}
		else{
			badgeView.hidden=YES;	
		}
	}
	if (isPlayAudio) {
		[[poseAudioArray objectAtIndex:currentPageNumber] resumePlay];
	}
}
- (IBAction)gotoPoseIssue {
	if (!gotoPoseIssueBtn.selected) {
		gotoPoseIssueBtn.selected = YES;
		
		playPoseBtn.selected = NO;
		//playPoseBtn.enabled = YES;
		[playPoseView removeFromSuperview];
		titleLB.text = @"Issues";
		isIssuesViewShown = YES;
		[self.view insertSubview:issueView atIndex:1];
		//table.frameSizeHeight = 380;
		if (notResolvedIssueArray.count > 0) {
			badgeView.hidden = NO;
		}
		else {
			badgeView.hidden = YES;
		}
		
	}
	
	if (isPlayAudio) {
		[[poseAudioArray objectAtIndex:currentPageNumber] pause];
	}
}
- (IBAction)playOrStopAudio {
	isPlayAudio = !isPlayAudio;
    
    NSLog(@"playOrStopAudio pressed");
	
	playOrStopBtn.selected = isPlayAudio;
	
	if (isPlayAudio) {
		if (currentPageNumber == 0) {
			[self setCurrentPageNumber:1];
			
			[poseScrollView scrollRectToVisible:CGRectMake(320, 0, poseScrollView.frame.size.width, poseScrollView.frame.size.height) animated:YES];
		}
		else {
			[[poseAudioArray objectAtIndex:currentPageNumber] playOrResumre];
		}
	} else {
		
		[[poseAudioArray objectAtIndex:currentPageNumber] pause];
	}
	
}
//override method
- (void)addIssue {
	
	if (isPlayAudio) {
		
		//isPlayAudio = NO;
		//playOrStopBtn.selected = NO;
		[[poseAudioArray objectAtIndex:currentPageNumber] pause];
	}
	
	
	
	
	IssueDetailViewController *vc = [IssueDetailViewController alloc];
	vc.issueDict = self.poseDict;
	vc.isAddIssue = YES;
	vc.isFromAsana = YES;
	[vc initWithNibUsingClassName];
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}
- (IBAction)setFavorite {
	favoriteBtn.selected = !favoriteBtn.selected;
	NSNumber *poseId = [poseDict objectForKey:@"poseID"];
	NSNumber *isFavorite = [NSNumber numberWithBool:favoriteBtn.selected];
	[dbHandle() executeUpdate:@"update Poses set IsFavorite = ? where ID = ?", [isFavorite stringValue], [poseId stringValue]];
}
- (IBAction)setResolved:(id)sender {
	[super setResolved:sender];
	
	if (notResolvedIssueArray.count == 0) {
		badgeView.hidden = YES;
	}
	else {
		badgeView.hidden = NO;
	}
	
	issueCountLB.text = stringFromInt(notResolvedIssueArray.count);
}

#pragma mark --- UITableView	
#pragma mark --- UITableView delegate ---
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSArray * array = (0 == indexPath.section) ?  notResolvedIssueArray:resolvedIssueArray ;
	NSDictionary *dict = [array objectAtIndex:indexPath.row];
	
	IssueDetailViewController *vc = [IssueDetailViewController alloc];
	vc.issueDict = dict;
	vc.issueRowNumber = indexPath.row;
	vc.isIssueResolved = (0 == indexPath.section) ? 0 : 1;
	vc.isFromAsana = YES;
	[vc initWithNibUsingClassName];
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}


#pragma mark --- UITableView Datasource ---
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	JournalHomeTableCell *cell = (JournalHomeTableCell *)[tableView dequeueReusableCellWithIdentifier:@"JournalHomeTableCell"];
	if (nil == cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"JournalHomeTableCell" owner:nil options:nil];
		cell = (JournalHomeTableCell *)[nib objectAtIndex:0];
	}
	
	NSDictionary *dict;
	if (0 == indexPath.section) {
		dict = [notResolvedIssueArray objectAtIndex:indexPath.row];
	}
	else {
		dict = [resolvedIssueArray objectAtIndex:indexPath.row];
	}
	NSString *content = [dict objectForKey:@"content"];
	NSString *updateDate = [dict objectForKey:@"updateDate"];
	
	cell.titleLB.text = content;
	cell.poseNameLB.text = updateDate;
	cell.selectBtn.selected = indexPath.section;
	cell.selectBtn.tag = indexPath.row;
	[cell.selectBtn addTarget:self action:@selector(setResolved:) forControlEvents:UIControlEventTouchUpInside];
	//	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	return cell;
	
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return kTableCellHeight;
}


#pragma mark --- UIScrollViewDelegate ---
/*
 #define CHANGE_PAGE_FRACTION 0.5
 - (void)scrollViewDidScroll:(UIScrollView *)sender {
 
 const CGFloat pageWidth = poseScrollView.frame.size.width;	
 const CGFloat num = poseScrollView.contentOffset.x / pageWidth;
 
 // scroll up to page num + 1
 if(num - currentPageNumber > CHANGE_PAGE_FRACTION) {
 if (!haveUpdateCurrentPage) {
 haveUpdateCurrentPage = YES;
 if (isPlayAudio) {
 [[poseAudioArray objectAtIndex:currentPageNumber] stop];
 }
 self.currentPageNumber = currentPageNumber + 1;
 }
 }
 // scroll down to page num
 else if(currentPageNumber - num > CHANGE_PAGE_FRACTION) {
 if (!haveUpdateCurrentPage) {
 haveUpdateCurrentPage = YES;
 if (isPlayAudio) {
 [[poseAudioArray objectAtIndex:currentPageNumber] stop];
 }
 self.currentPageNumber = currentPageNumber - 1;
 }
 }	
 }
 */

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	const CGFloat pageWidth = poseScrollView.frame.size.width;	
	const int num = poseScrollView.contentOffset.x / pageWidth;
	if(num  != currentPageNumber) {
		
		if (isPlayAudio) {
			[[poseAudioArray objectAtIndex:currentPageNumber] stop];
			
		}
		self.currentPageNumber = num;
	}
	
}


#pragma mark --- audioPlayerDelegate ---
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
	
	NSLog(@"Audio Finished Delegate called");
	if (flag && currentPageNumber < poseStepArray.count - 1) {
		[self gotoNextPage];
	}
	//if the get the final step and the audio is finished, set the button to be play again
	else if (flag && currentPageNumber == poseStepArray.count - 1){
		
		playOrStopBtn.selected = NO;
		isPlayAudio = NO;
		
	}
}


@end

//
//  PoseDetailViewController.h
//  Yoga
//
// Created by Rob on 10-3-23.
//  
//

#import <UIKit/UIKit.h>
#import "JournalViewController.h"
#import "MyPageControl.h"

@interface PoseDetailViewController : JournalViewController <UIScrollViewDelegate, AVAudioPlayerDelegate> {
	IBOutlet UIScrollView	*poseScrollView;
	IBOutlet MyPageControl	*pageControl;
	IBOutlet UILabel		*descriptionTV;
	
	IBOutlet UILabel		*stepNameLB; 
	IBOutlet UILabel		*poseNameLB; 
	IBOutlet UIScrollView	*playPoseView;
	IBOutlet UIView         *issueView;
	IBOutlet UIButton		*playPoseBtn;
	IBOutlet UIButton		*gotoPoseIssueBtn;
	IBOutlet UIButton		*favoriteBtn;
	IBOutlet UIButton		*playOrStopBtn;
	IBOutlet UILabel		*issueCountLB;
	IBOutlet UIView			*badgeView;
	
	NSDictionary			*poseDict;
	
	int						currentPageNumber;
	NSMutableArray			*poseStepArray;
	NSMutableArray			*poseAudioArray;
	BOOL					isPlayAudio;
	IBOutlet UIView         *bottomView;

	int issueCount;
	BOOL needAutoPlay;
	
	BOOL isIssuesViewShown;
}
@property (nonatomic, retain) NSDictionary *poseDict;
@property (assign) BOOL needAutoPlay;

- (IBAction)gotoPlayPoseView;
- (IBAction)gotoPoseIssue;
- (IBAction)playOrStopAudio;
- (IBAction)gotoProduct;
- (IBAction)setFavorite;


@end

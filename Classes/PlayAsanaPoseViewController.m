//
//  PlayAsanaPoseViewController.m
//  Yoga
//
// Created by Rob on 10-4-6.
//  
//

#import "PlayAsanaPoseViewController.h"


@implementation PlayAsanaPoseViewController
@synthesize asanaPoseArray;
@synthesize asanaName;
@synthesize poseInterval;


#pragma mark -
#pragma mark Private methods
- (void)preparePoseAuidoWithPostStepArray:(NSArray*)_poseStepArray index:(NSInteger)index {
	NSMutableArray *audioArray = [NSMutableArray array];
	
    NSLog(@"pose step array: %@", _poseStepArray);
	
    for (int i = 0; i < _poseStepArray.count; i++) {
        
        NSString *fileName = [(NSDictionary*)[_poseStepArray objectAtIndex:i] objectForKey:@"audioPath"];

        
        NSLog(@"fileName: %@", fileName);
        
        if ([fileName length] > 1)
        {
            fileName = [[fileName stringByReplacingOccurrencesOfString:@".mp3" withString:@""] lowercaseString];
            fileName = [fileName stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[fileName substringToIndex:1] uppercaseString]];
                                       
                                      
        }
        NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
        NSLog(@"filePath: %@", filePath);
        
		Sound *myAudio = [[Sound alloc] initWithContentsOfFile:filePath];
		[audioArray addObject:myAudio];
		[myAudio release];
	}
	
	//[poseAudioArray addObject:audioArray];
	[poseAudioArray replaceObjectAtIndex:index withObject:audioArray];
}
- (void)preparePoseAuidoArray {
	for (int i = 0; i < poseStepArray.count; i++) {
		NSNull *null = [NSNull null];
		[poseAudioArray addObject:null];
	}
}
- (NSArray*)loadStepsWithPoseIndex:(NSInteger)poseIndex index:(NSInteger)index {
	
	if ((poseIndex < 0) || (poseIndex > asanaPoseArray.count - 1)) {
		return nil;
	}
	
	
	NSMutableArray *aPoseStepArray = [NSMutableArray array];
	
	
	id<PLResultSet> result;
	
	
	NSNumber *poseId;
	NSString *poseName;
	NSString *stepName;
	NSString *picPath;
	NSString *description;
	NSString *audioPath;
	
	
	NSDictionary *poseDict = [asanaPoseArray objectAtIndex:poseIndex];
	poseId = [poseDict objectForKey:@"poseID"];
	picPath = [poseDict objectForKey:@"picPath"];
	description = [poseDict objectForKey:@"description"];
	poseName =[poseDict objectForKey:@"poseName"];
	stepName = @"Introduction";
	
	//@wav
    NSLog(@"Pose id: %@", poseId);
	audioPath = [NSString stringWithFormat:@"Intro.mp3", [poseId intValue]];
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
						  poseName, @"poseName",
						  stepName, @"stepName",
						  picPath, @"picPath",
						  audioPath, @"audioPath",
						  description, @"description",
						  nil];
	[aPoseStepArray addObject:dict];
	
	NSString *poseIDStr = [poseId stringValue];
	result = [dbHandle() executeQuery: @"SELECT * FROM PoseSteps where PoseID = ? order by StepIndex", poseIDStr];
	int stepIndex = 1;
	while ([result next]) {
		//@TODO  is the step name like "Step 1" or some sepcilized name?
		stepName = [NSString stringWithFormat:@"Step %i", stepIndex];
		picPath = [result stringForColumn:@"PicturePath"];
        audioPath = [result safeObjectForColumn:@"AudioPath"];
		NSLog(@"Playing Audio2:%@", audioPath);
		description = [result stringForColumn:@"Description"];
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
							  stepName, @"stepName",
							  picPath, @"picPath",
							  audioPath, @"audioPath",
							  description, @"description",
							  nil];
		[aPoseStepArray addObject:dict];
		stepIndex++;			
	}
	[result close];
	audioPath = [NSString stringWithFormat:@"Intro.mp3", [poseId intValue]];
	if([poseId intValue]==31){
		dict = [NSDictionary dictionaryWithObjectsAndKeys:
				@"Step 4", @"stepName",
				picPath, @"picPath",
				audioPath, @"audioPath",
				description, @"description",
				nil];
	}
	else{
		dict = [NSDictionary dictionaryWithObjectsAndKeys:
				@"Progression", @"stepName",
				picPath, @"picPath",
				audioPath, @"audioPath",
				description, @"description",
				nil];
	}
	[aPoseStepArray replaceObjectAtIndex:aPoseStepArray.count-1 withObject:dict];
	
	[self preparePoseAuidoWithPostStepArray:aPoseStepArray index:index];
	
	return aPoseStepArray;
}
- (void)loadStepsOfCurrentPose {
	if (isRepeat) return;
	
	[poseStepArray removeAllObjects];
	
	NSArray *poseStepArray1 = [self loadStepsWithPoseIndex:currentPoseID - 1 index:0]; 
	if (poseStepArray1) {
		[poseStepArray addObject:poseStepArray1];
	}
	else {
		[poseStepArray addObject:[NSNull null]];
	}
	
	NSArray *poseStepArray2 = [self loadStepsWithPoseIndex:currentPoseID index:1];
	if (poseStepArray2) {
		[poseStepArray addObject:poseStepArray2];
	}
	else {
		[poseStepArray addObject:[NSNull null]];
	}
	
	NSArray *poseStepArray3 = [self loadStepsWithPoseIndex:currentPoseID + 1 index:2]; 
	if (poseStepArray3) {
		[poseStepArray addObject:poseStepArray3];
	}
	else {
		[poseStepArray addObject:[NSNull null]];
	}
}
- (void)setStepContent {
	NSDictionary *dict;
	
	/*
	 if (currentPoseID != 0) {
	 if (currentPageNumber == 0) {
	 dict = [[poseStepArray objectAtIndex:0] objectAtIndex:((NSArray*)[poseStepArray objectAtIndex:0]).count - 1];
	 }
	 else if (currentPageNumber == ((NSArray*)[poseStepArray objectAtIndex:0]).count + 1) {
	 dict = [[poseStepArray objectAtIndex:2] objectAtIndex:0];
	 }
	 else {
	 dict = [[poseStepArray objectAtIndex:1] objectAtIndex:currentPageNumber - 1];
	 }
	 }
	 else {
	 dict = [[poseStepArray objectAtIndex:1] objectAtIndex:currentPageNumber];
	 }
	 */
	NSInteger stepPageNumber = currentPageNumber;
	
    
    if (currentPoseID != 0) {
		stepPageNumber = currentPageNumber - 1;
	}
	
     
	dict = [[poseStepArray objectAtIndex:1] objectAtIndex:stepPageNumber];
	NSString *description = [dict objectForKey:@"description"];
	NSString *stepName = [dict objectForKey:@"stepName"];
	NSString *poseName = [dict objectForKey:@"poseName"];
	
	description = [description stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n\n"];
	description = [description stringByReplacingOccurrencesOfString:@" INHALE:" withString:@"\n\nINHALE:"];
	description = [description stringByReplacingOccurrencesOfString:@" EXHALE:" withString:@"\n\nEXHALE:"];
	description = [description stringByReplacingOccurrencesOfString:@" BREATHING HERE:" withString:@"\n\nBREATHING HERE:"];
	description = [description stringByReplacingOccurrencesOfString:@" Also Try " withString:@"\n\nAlso Try: "];
	description = [description stringByReplacingOccurrencesOfString:@" Repeat to left side" withString:@"\n\nRepeat to left side"];
	
    description = [description stringByReplacingOccurrencesOfString:@" Tips:" withString:@"\n\nTips:"];
  
	
    description = [description stringByReplacingOccurrencesOfString:@" Relax for a few breaths and then repeat." withString:@"\n\nRelax for a few breaths and then repeat."];
	
	

	
	descriptionTV.text = description;
	//CGSize size = CGSizeMake(descriptionTV.frameSizeWidth, INT_MAX);
	CGSize size = CGSizeMake(280, INT_MAX);
	descriptionTV.frameSize = [description sizeWithFont:descriptionTV.font constrainedToSize:size];
	int pageHeight = MAX(420, descriptionTV.frameOriginY+descriptionTV.frameSizeHeight+10);
	//int pageHeight = MAX(380, descriptionTV.frameOriginY+descriptionTV.frameSizeHeight+44);
	wholeScrollView.contentSize = CGSizeMake(320, pageHeight);
	stepNameLB.text = stepName;
	if([poseName length]>0){
	poseNameLB.text = poseName;
	}
	Sound *s =[[poseAudioArray objectAtIndex:1] objectAtIndex:stepPageNumber];
	if ([NSNull null] == (NSNull *)s) {
		NSString *fileName = [dict objectForKey:@"audioPath"];
        if ([fileName length] > 1)
        {
            fileName = [[fileName stringByReplacingOccurrencesOfString:@".mp3" withString:@""] lowercaseString];
            fileName = [fileName stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[fileName substringToIndex:1] uppercaseString]];
        }
        NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
		s = [[Sound alloc] initWithContentsOfFile:filePath];
		s.player.delegate = self;
		[[poseAudioArray objectAtIndex:1] replaceObjectAtIndex:stepPageNumber withObject:s];
		[s release];
	}
	if (isPlayAudio) {
		s.player.delegate = self;
		[[[poseAudioArray objectAtIndex:1] objectAtIndex:stepPageNumber] playOnce];
	}
}


#pragma mark -
#pragma mark set/get Method
- (void)setCurrentPageNumber:(int)num {
	currentPageNumber = num;
	//set the tip info
	
	[self setStepContent];
	[poseScrollView setContentOffset:CGPointMake(poseScrollView.frameSizeWidth * num,
												 poseScrollView.contentOffset.y) animated:NO];
	
	
	if (currentPoseID != 0) {
		num -= 1;
	}
	
	pageControl.currentPage = num;
	[pageControl updateCurrentPageDisplay];
	
	/*
	 if (poseIntervalTimer) {
	 [poseIntervalTimer invalidate];
	 poseIntervalTimer = nil;
	 }
	 poseIntervalTimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)self.poseInterval 
	 target:self 
	 selector:@selector(gotoNextPage)
	 userInfo:nil
	 repeats:NO];
	 */
}

- (void)setScrollView {
	NSInteger startOffset = 0;
	if (!isRepeat) {
		[poseScrollView removeAllSubview];
		
		NSDictionary *dict;
		UIImageView *imageView;
		UIImage *poseImg;
		
		NSInteger scrollScreenIndex = 0;
		
		
		NSArray *previousPoseStepArray = [poseStepArray objectAtIndex:0];
		if (![previousPoseStepArray isKindOfClass:[NSNull class]]) {
			dict = [previousPoseStepArray objectAtIndex:previousPoseStepArray.count - 1];
			poseImg = [UIImage imageNamed:[dict objectForKey:@"picPath"]];
			imageView = [[UIImageView alloc] initWithImage:poseImg];
			imageView.frame = CGRectMake(0, 0, poseScrollView.frameSizeWidth, poseScrollView.frameSizeHeight);
			UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 200, 30)];
			label.text = @"I'm first one";
			[poseScrollView addSubview:imageView];
			//[poseScrollView addSubview:label];
			[imageView release];
			scrollScreenIndex++;
			startOffset = 1;
		}
		
		NSArray *currentPoseStepArray = [poseStepArray objectAtIndex:1];
		for (int i = 0; i < currentPoseStepArray.count; i++) {
			dict = [currentPoseStepArray objectAtIndex:i];
			poseImg = [UIImage imageNamed:[dict objectForKey:@"picPath"]];
			imageView = [[UIImageView alloc] initWithImage:poseImg];
			imageView.frame = CGRectMake((scrollScreenIndex + i) * poseScrollView.frameSizeWidth, 0, poseScrollView.frameSizeWidth, poseScrollView.frameSizeHeight);
			UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake((scrollScreenIndex + i) * poseScrollView.frameSizeWidth + 50, 0, 200, 30)];
			label.text = [NSString stringWithFormat:@"I'm the one of %i", i, nil];
			
			[poseScrollView addSubview:imageView];
			//[poseScrollView addSubview:label];
			[imageView release];
		}
		
		scrollScreenIndex += currentPoseStepArray.count;
		
		NSArray *nextPoseStepArray = [poseStepArray objectAtIndex:2];
		if (![nextPoseStepArray isKindOfClass:[NSNull class]]) {
			dict = [nextPoseStepArray objectAtIndex:0];
			poseImg = [UIImage imageNamed:[dict objectForKey:@"picPath"]];
			imageView = [[UIImageView alloc] initWithImage:poseImg];
			imageView.frame = CGRectMake(scrollScreenIndex * poseScrollView.frameSizeWidth, 0, poseScrollView.frameSizeWidth, poseScrollView.frameSizeHeight);
			UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(scrollScreenIndex * poseScrollView.frameSizeWidth + 50, 0, 200, 30)];
			label.text = [NSString stringWithFormat:@"I'm last one", nil];
			[poseScrollView addSubview:imageView];
			//[poseScrollView addSubview:label];
			[imageView release];
			scrollScreenIndex++;
		}
		
		poseScrollView.contentSize = CGSizeMake(scrollScreenIndex*poseScrollView.frameSizeWidth,poseScrollView.frameSizeHeight);
		
	}
	
	if (currentPoseID != 0)
	{
		self.currentPageNumber = 1;
	}
	else {
		self.currentPageNumber = 0;	
	}
	
	//return startOffset;
}
- (void)gotoNextPage {
	
	self.currentPageNumber = currentPageNumber + 1;
	
}
- (void)setContentForCurrentPose {
    
    NSLog(@"set content for current pose");
    NSLog(@"pose interval = %i", poseInterval);
	poseIntervaling = NO;
	
	[self loadStepsOfCurrentPose];
	
	pageControl.numberOfPages = ((NSArray*)[poseStepArray objectAtIndex:1]).count;
	if (pageControl.numberOfPages == 5) pageControl.frameOriginX=18;
	else if (pageControl.numberOfPages == 4) pageControl.frameOriginX=8;
	else if (pageControl.numberOfPages == 3) pageControl.frameOriginX=-1;
	else if (pageControl.numberOfPages == 2) pageControl.frameOriginX=-12;
	
	[self setScrollView];
	
	if (currentPoseID != 0) {
		[poseScrollView setContentOffset:CGPointMake(320, 0)];
	}
	
	previousPageNumber = 0;
}



#pragma mark -
#pragma mark View Controller methods
- (void)viewDidLoad {
	[super viewDidLoad];
	
	isScrollingBack = NO;
	isScrollingBack = 0;
	
	
	poseStepArray = [NSMutableArray new];
	poseAudioArray = [NSMutableArray new];
	for (int i = 0; i < 3; i++) {
		[poseAudioArray addObject:[NSNull null]];
	}
	
	
	currentPoseID = 0;
	poseScrollView.showsHorizontalScrollIndicator = NO;
	isPlayAudio = YES;
	playBtn.selected = YES;
	currentRepeatCount = 0;
	isRepeat = NO;
	[self setContentForCurrentPose];
	titleLB.text = asanaName;

	[self.view insertSubview:wholeScrollView atIndex:1];
	//Set the audio play automatically after loading
	
	

/*
	autoTimer = [NSTimer scheduledTimerWithTimeInterval:5.0
														 target:self 
													   selector:@selector(autoScrolling) 
													   userInfo:nil 
														repeats:NO];
	*/
}


- (void)autoScrolling {
	
	NSLog(@"Auto SCrolling");
	poseIntervalTimer = nil;
	
	if (currentPageNumber < poseStepArray.count) {
		
		NSLog(@"Go to next page");
		poseIntervalTimer = [NSTimer scheduledTimerWithTimeInterval:0.0
															 target:self 
														   selector:@selector(gotoNextPage) 
														   userInfo:nil 
															repeats:NO];
	}
	else {
		
		NSLog(@"Go to next pose");
		currentRepeatCount++;
		int totoalRepeatCount = [[[asanaPoseArray objectAtIndex:currentPoseID] objectForKey:@"repeatCount"] intValue];
		if (currentRepeatCount < totoalRepeatCount) {
			NSLog(@"repeat");
			isRepeat = YES;
		}
		else {
            NSLog(@"reset repeats");
			currentPoseID++;
			isRepeat = NO;
			currentRepeatCount = 0;
		}
		if (currentPoseID < asanaPoseArray.count) {
			
            NSLog(@"load setContentForCurrentPose with time %i", poseInterval);
			
			poseIntervalTimer = [NSTimer scheduledTimerWithTimeInterval:poseInterval
																 target:self 
															   selector:@selector(setContentForCurrentPose) 
															   userInfo:nil 
																repeats:NO];
			
			
			poseIntervaling = YES;
			
		}
		else {
			NSLog(@"end of sequence");
			isPlayAudio = NO;
			playBtn.selected = NO;
		}
		
	}
	
		NSLog(@"End Auto SCrolling");
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:YES];
}
- (void)viewWillDisappear:(BOOL)animated{
	if (poseIntervaling) {
		[poseIntervalTimer invalidate];
		poseIntervalTimer = nil;
	}
	
}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (void)dealloc {
	[poseStepArray release];
	[poseAudioArray release];
    [super dealloc];
}


#pragma mark -
#pragma mark scrollView delegate 


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	

	if (autoTimer) {
		[autoTimer invalidate];
		autoTimer = nil;
	}
	
	const CGFloat pageWidth = poseScrollView.frame.size.width;	
	NSInteger num = poseScrollView.contentOffset.x / pageWidth;

	NSLog(@"scrollViewDidEndDecelerating %i, %i", num, currentPoseID);
	if (previousPageNumber > num) {
		NSLog(@"Scrolling backward", nil);
		isScrollingBack = YES;
	}
	else {
		NSLog(@"Scrolling forward", nil);
		isScrollingBack = NO;
	}
	
	previousPageNumber = num;
	
	if (num < 1 && currentPoseID >= 1) {
		NSLog(@"scenario 0");
		
		currentPoseID--;
		[self setContentForCurrentPose];
		return;
	}
	
	if (currentPoseID == 0) {
		NSLog(@"scenario 1, %i", num);
		if (num >= ((NSArray*)[poseStepArray objectAtIndex:1]).count ) {
			
			NSLog(@"scenario 1b");
			currentPoseID++;
			[self setContentForCurrentPose];
			return;
		}
	}
	else {
		NSLog(@"scenario 2");
		if (num > ((NSArray*)[poseStepArray objectAtIndex:1]).count ) {
			currentPoseID++;
			[self setContentForCurrentPose];
			return;
		}
	}
	
	// end audio
	
	if (isPlayAudio && currentPageNumber >= 0) {
		
		NSLog(@"ending audio due to manual scroll");
		
		NSArray *sounds = [poseAudioArray objectAtIndex:1];
		for(int i=0; i<sounds.count; i++){
				[[sounds objectAtIndex:i] stop];
		}
		
		
	
		
	}
	

	
	self.currentPageNumber = num;
	
	NSLog(@"ended scrolling, poseid = %i, step=%i", currentPoseID, currentPageNumber);
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	NSLog(@"scrollViewWillBeginDragging", nil);
}


#pragma mark -
#pragma mark --- audioPlayerDelegate ---
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
	
	NSLog(@"audio finished, PN= %i AND total steps = %i", currentPageNumber, poseStepArray.count);
	//if the get the final step and the audio is finished, set the button to be play again
	if (flag) {
		
		int totalSteps =0;
		
		if(currentPoseID != 0){totalSteps =poseStepArray.count;}
		else{totalSteps = poseStepArray.count-1;}
	
		
		
		if (currentPageNumber < totalSteps) {
			
			NSLog(@"Go to next page");
			poseIntervalTimer = [NSTimer scheduledTimerWithTimeInterval:0.0
																 target:self 
															   selector:@selector(gotoNextPage) 
															   userInfo:nil 
																repeats:NO];
		}
		else {
			
			NSLog(@"Go to next pose");
			currentRepeatCount++;
			int totoalRepeatCount = [[[asanaPoseArray objectAtIndex:currentPoseID] objectForKey:@"repeatCount"] intValue];
			if (currentRepeatCount < totoalRepeatCount) {
				
				isRepeat = YES;
			}
			else {
				currentPoseID++;
				isRepeat = NO;
				currentRepeatCount = 0;
			}
			if (currentPoseID < asanaPoseArray.count) {
				
                NSLog(@"load setContentForCurrentPose with time %i", poseInterval);
				
				poseIntervalTimer = [NSTimer scheduledTimerWithTimeInterval:poseInterval
																	 target:self 
																   selector:@selector(setContentForCurrentPose) 
																   userInfo:nil 
																	repeats:NO];
				
				
				poseIntervaling = YES;
				
			}
			else {
				NSLog(@"end of sequence");
				isPlayAudio = NO;
				playBtn.selected = NO;
				[self.navigationController popViewControllerAnimated:YES];
			}
			
		}
		
	}
}


#pragma mark -
#pragma mark --- IBAction ---
- (IBAction)playOrStopAudio:(id)sender {
    
    if(isPlayAudio){
    NSLog(@"is play audio was TRUE");
    }
    
	isPlayAudio = !isPlayAudio;
	
    UIButton *localBtn = sender;
    
    localBtn.selected = isPlayAudio;
	 
	
    
    NSLog(@"playOrStopAudio MyAsana");
    
    NSLog(@"pose Audio array count = %i", [poseAudioArray count]);
    NSLog(@"poseID = %i", currentPoseID);
    NSLog(@"page number = %i", currentPageNumber);
    
    
    //NSLog(@"count for obj at index %i = %i", currentPoseID, [[poseAudioArray objectAtIndex:currentPoseID] count]);
    
    
	playBtn.selected = isPlayAudio;
    NSLog(@"Current page number: %d", currentPageNumber);
    NSLog(@"pose audio array: %@", poseAudioArray);
    NSArray *object = [poseAudioArray objectAtIndex:1];
    NSLog(@"object: %@", object);
    
	Sound *s = [[poseAudioArray objectAtIndex:1] objectAtIndex:currentPageNumber];
	if ([NSNull null] == (NSNull *)s) {NSLog(@"The audio file is not exisit");return;}
	
	if (isPlayAudio) {
        NSLog(@"play");
		[s playOnce];
	} else {
        NSLog(@"pause");
        //[s pause];
        
		NSArray *sounds = [poseAudioArray objectAtIndex:1];
		for(int i=0; i<sounds.count; i++){
            [[sounds objectAtIndex:i] pause];
		}
		
	}
}


@end

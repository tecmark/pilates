//
//  MyAsanaTableCell.h
//  Yoga
//
// Created by Rob on 10-4-21.
//  
//

#import <UIKit/UIKit.h>


@interface MyAsanaTableCell : UITableViewCell {
	IBOutlet UITextField *titleTF;
	IBOutlet UIImageView *poseIV;
	IBOutlet UIButton *editBtn;
	IBOutlet UIImageView *divider;
	IBOutlet UIImageView *arrow;
	
}

@property (nonatomic, retain) UITextField *titleTF;
@property (nonatomic, retain) UIImageView *poseIV;
@property (nonatomic, retain) UIButton *editBtn;
@property (nonatomic, retain) UIImageView *divider;
@property (nonatomic, retain) UIImageView *arrow;

@end

//
//  SelectPoseTableCell.m
//  Yoga
//
// Created by Rob on 10-8-24.
//  
//

#import "SelectPoseTableCell.h"


@implementation SelectPoseTableCell

//@synthesize subTitleLB;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
    [super dealloc];
}


@end

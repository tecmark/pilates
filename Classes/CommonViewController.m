//
//  CommonViewController.m
//  
//
// Created by Rob on 11/5/09.
// 
//

#import "CommonViewController.h"
#define kTableCellHeight 65
#define kTableMoveOffset 50
#define kTableMaxHeight	 375
#pragma mark --- CommonHomeViewController ---
@implementation CommonHomeViewController
@synthesize switchViewContorllerNameArray;

- (void)viewDidLoad {
	[super viewDidLoad];
	UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background.png"]];
	imgView.frameOrigin = CGPointMake(0, -20);
	[self.view insertSubview:imgView atIndex:0];
	[imgView release];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
	[self.view removeAllSubview];
}


- (void)dealloc {
	[switchViewContorllerNameArray release];
    [super dealloc];
}

- (void)beforSwitchToViewController:(int)controllerID {
}

- (IBAction)switchViewController:(id)sender {
	int senderTag = [sender tag];
	[self beforSwitchToViewController:senderTag];
	NSString *switchViewContorllerName = [switchViewContorllerNameArray objectAtIndex:senderTag];
	Class ViewController = NSClassFromString(switchViewContorllerName);
	UIViewController *vc = [[ViewController alloc] initWithNibName:switchViewContorllerName bundle:nil];
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];	
}

@end

#pragma mark --- CommonViewController ---
@implementation CommonViewController
@synthesize titleLB, btn, btn2;

- (void)viewDidLoad {
	[super viewDidLoad];
	UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background.png"]];
	imgView.frameOrigin = CGPointMake(0, -20);
	[self.view insertSubview:imgView atIndex:0];
	[imgView release];
	
	imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"topBar.png"]];
	imgView.frameOrigin = CGPointZero;
	//imgView.frameOriginY=10;
	imgView.frameSizeHeight = 48;
	imgView.backgroundColor=[UIColor clearColor];
	[self.view insertSubview:imgView atIndex:3];
	//[self.view bringSubviewToFront:imgView];
	
	[imgView release];
	
	NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TitleLB" owner:nil options:nil];
	for (id oneObject in nib)
		if ([oneObject isKindOfClass:[UILabel class]])
			titleLB = (UILabel *)oneObject;
	titleLB.center = CGPointMake(160, titleLB.frameSizeHeight/2);
	[self.view insertSubview:titleLB atIndex:4];
	//[self.view bringSubviewToFront:titleLB];
	
	
	btn = [UIButton buttonWithType:UIButtonTypeCustom];
	[btn setImage:[UIImage imageNamed:@"my Asana Back.png"] forState:UIControlStateNormal];
	btn.frame = CGRectMake(1, 0, 47, 37);
	[btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	[self.view insertSubview:btn atIndex:5];
	//[self.view bringSubviewToFront:btn];
	
}

- (IBAction)back {
	[self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)home {
	[self.navigationController popToRootViewControllerAnimated:YES];
}
@end

#pragma mark --- CommonTableViewController ---
#import "PoseDetailViewController.h"
#import "AddIssueTableCell.h"

@implementation CommonTableViewController
@synthesize dataArray;


- (void)loadData {
}

- (void)viewDidLoad {
	
	
	
	[super viewDidLoad];
	dataArray = [NSMutableArray new];
	[self loadData];
	
//	table.layer.cornerRadius = 12.0f;
	table.backgroundColor = [UIColor clearColor];
	table.separatorStyle = UITableViewCellSeparatorStyleNone;
	//table.separatorColor = [UIColor colorWithRed:121/255.0 green:125/255.0 blue:128/255.0 alpha:1.0];

	table.frameSizeHeight = MIN(kTableMaxHeight+20, kTableCellHeight*dataArray.count);
	table.frame = CGRectMake(0, 40, 320, table.frameSizeHeight);
	table.showsVerticalScrollIndicator = NO;
	table.delegate = self;
	table.dataSource = self;
	 
}

- (void)dealloc {
	[dataArray release];
    [super dealloc];
}


#pragma mark --- UITableView Datasource ---
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return kTableCellHeight;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return dataArray.count;
	
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	
	AddIssueTableCell *cell = (AddIssueTableCell *)[tableView dequeueReusableCellWithIdentifier:@"AddIssueTableCell"];
	if (nil == cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddIssueTableCell" owner:nil options:nil];
		cell = (AddIssueTableCell *)[nib objectAtIndex:0];
	}
	
	NSDictionary *dict = [dataArray objectAtIndex:indexPath.row];
	
	NSString *name = [dict objectForKey:@"poseName"];
	
	NSString *picPath = [dict objectForKey:@"picPath"];
	NSString *description = [dict objectForKey:@"description"];
	UIImage *poseImg = [UIImage imageNamed:[picPath stringByReplacingOccurrencesOfString:@".jpg" withString:@"_thumb.jpg"]];
	
	cell.poseIV.image = poseImg;
	 
 
	cell.titleLB.text = name;
	cell.descLB.text = description;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	return cell;
	 
	 
}
 
 
#pragma mark --- UITableView delegate ---
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
    NSLog(@"selected row");
	NSDictionary *dict = [dataArray objectAtIndex:indexPath.row];
	PoseDetailViewController *vc = [PoseDetailViewController alloc];
	//	vc.isFromMyAsana = YES;
	vc.poseDict = dict;
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}

@end



#pragma mark --- CommonFavoritesTableViewController ---
#import "PoseDetailViewController.h"
#import "FavoritesTableCell.h"

@implementation CommonFavoritesTableViewController
@synthesize dataArray;
- (void)loadData {
}

- (void)viewDidLoad {
	
	[super viewDidLoad];
	dataArray = [NSMutableArray new];
	[self loadData];
	
    //	table.layer.cornerRadius = 12.0f;
	table.backgroundColor = [UIColor clearColor];
	table.separatorStyle = UITableViewCellSeparatorStyleNone;
	//table.separatorColor = [UIColor colorWithRed:121/255.0 green:125/255.0 blue:128/255.0 alpha:1.0];
    
	table.frameSizeHeight = MIN(kTableMaxHeight+20, kTableCellHeight*dataArray.count);
	table.frame = CGRectMake(0, 40, 320, table.frameSizeHeight);
	table.showsVerticalScrollIndicator = NO;
	table.delegate = self;
	table.dataSource = self;
    
    
    
}

- (void)dealloc {
	[dataArray release];
    [super dealloc];
}




#pragma mark --- UITableView Datasource ---
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return kTableCellHeight;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return dataArray.count;
	
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	FavoritesTableCell *cell = (FavoritesTableCell *)[tableView dequeueReusableCellWithIdentifier:@"FavoritesTableCell"];
	if (nil == cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FavoritesTableCell" owner:nil options:nil];
		cell = (FavoritesTableCell *)[nib objectAtIndex:0];
	}
	
	NSDictionary *dict = [dataArray objectAtIndex:indexPath.row];
	
	NSString *name = [dict objectForKey:@"poseName"];
	NSString *sankritName = [dict objectForKey:@"sanskrit"];
	NSString *picPath = [dict objectForKey:@"picPath"];
	//NSString *description = [dict objectForKey:@"description"];
	NSString *intro = [dict objectForKey:@"Intro"];
	NSString *imageName = [picPath stringByReplacingOccurrencesOfString:@".jpg" withString:@"_thumb.jpg"];
	UIImage *poseImg = [UIImage imageNamed:imageName];
	
	cell.poseIV.image = poseImg;
	cell.favouriteStarButton.tag=indexPath.row;
	cell.favouriteStarButton.selected=NO;
	
	
	cell.titleLB.text = [[[[name capitalizedString] stringByReplacingOccurrencesOfString:@"Iii" withString:@"III"]stringByReplacingOccurrencesOfString:@"Ii" withString:@"II"]stringByReplacingOccurrencesOfString:@"Iv" withString:@"IV"];
	cell.subTitleLB.text = [[[[sankritName capitalizedString] stringByReplacingOccurrencesOfString:@"Iii" withString:@"III"]stringByReplacingOccurrencesOfString:@"Ii" withString:@"II"]stringByReplacingOccurrencesOfString:@"Iv" withString:@"IV"];
	cell.descLB.text = intro;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	return cell;
}
#pragma mark --- UITableView delegate ---
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *dict = [dataArray objectAtIndex:indexPath.row];
	PoseDetailViewController *vc = [PoseDetailViewController alloc];
	//	vc.isFromMyAsana = YES;
	vc.poseDict = dict;
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}



@end



#pragma mark --- CommonSearchTableViewController ---
#import "AddIssueTableCell.h"

@implementation CommonSearchTableViewController
@synthesize poseDict;
@synthesize keys;

- (void)resetSearch {
	
	NSLog(@"reset search");
	NSMutableDictionary *allPoseCopy = [allPoseDict mutableDeepCopy];
	self.poseDict = allPoseCopy;
	[allPoseCopy release];
	NSMutableArray *keyArray = [NSMutableArray new];
	[keyArray addObjectsFromArray:[allPoseDict allKeys]];
	self.keys = keyArray;
	[keyArray release];
}
- (void)handleSearchForTerm:(NSString *)searchTerm {
	
	
	NSMutableArray *sectionsToRemove = [[NSMutableArray	alloc] init];
	[self resetSearch];
	
	
	int rowCount = 0;
	for (NSString *key in self.keys) {
		id dictOfKey = [poseDict valueForKey:key];
		NSMutableArray * array;
		if ([dictOfKey isKindOfClass:[NSArray class]]) 
			array = (NSMutableArray *)dictOfKey;
		else 
			array = [NSMutableArray arrayWithObject:dictOfKey];
		
		
		NSMutableArray *toRemove = [NSMutableArray new];
		for (int i = 0; i < array.count; i++) {
			
			NSDictionary *dict = [array objectAtIndex:i];
			NSString *poseName = [dict objectForKey:@"poseName"];
			NSRange range = [poseName rangeOfString:searchTerm options:NSCaseInsensitiveSearch];
			if (range.location == NSNotFound) 
				[toRemove addObject:dict];
			else 
				rowCount++;
		}
		
		if (toRemove.count == array.count) 
			[sectionsToRemove addObject:key];
		
		[array removeObjectsInArray:toRemove];
		[poseDict setObject:array forKey:key];
		[toRemove release];
		
	}
	[self.keys removeObjectsInArray:sectionsToRemove];
	[sectionsToRemove release];
	
	[table reloadData];
	 
	
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
	self.keys = [NSMutableArray new];
	allPoseDict = [NSMutableDictionary new];
	indexArray  = [NSMutableArray new];
	[self loadData];
	

	mySearchBar.backgroundColor = [UIColor clearColor];
	UIView* segment=[mySearchBar.subviews objectAtIndex:0];
	segment.alpha = 0;
	
	UIButton *cancelButton = nil;
	for(UIView *subView in mySearchBar.subviews){
		if([subView isKindOfClass:UIButton.class]){
			cancelButton = (UIButton*)subView;
		}
	}

	[cancelButton setImage:[UIImage imageNamed:@"cancelButton.png"] forState:UIControlStateNormal];
	[cancelButton setBackgroundImage:nil forState:UIControlStateNormal];
	cancelButton.hidden=YES;
	mySearchBar.tintColor=[UIColor colorWithRed:80.0/255.0 green:92.0/255.0 blue:102.0/255.0 alpha:1.0];
	
	//float height = MIN(kTableMaxHeight+46 , kTableCellHeight*keys.count);
	table.frameSizeHeight = MIN(kTableMaxHeight+46 , kTableCellHeight*keys.count);
	table.contentInset = UIEdgeInsetsMake(60, 0, 0, 0);
	
	//table.scrollIndicatorInsets = UIEdgeInsetsMake(-60, 0, 0, 0);
	mySearchBar.frame = CGRectMake(0, -52, 320, 44); 
	[table addSubview:mySearchBar];
	UIImageView *dividor = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"poseDriveLine.png"]];
	dividor.frameOriginY = -5;
	[table addSubview:dividor];
	[dividor release];
	table.contentOffset = CGPointMake(0, 0);
	
	
	/*
	UIView *indexBG = [[UIView alloc] initWithFrame:CGRectMake(288, 65, 10, table.frameSizeHeight-20)];
	indexBG.backgroundColor = [UIColor whiteColor];
	
	[self.view insertSubview:indexBG atIndex:self.view.subviews.count-2];
	[indexBG release];*/
	
	/*
	UIImageView *indexBackground = [[UIImageView alloc] initWithFrame:CGRectMake(281, 61, 25, 363)];
	indexBackground.image=[UIImage imageNamed:@"indexBackground.png"];
	
	[self.view insertSubview:indexBackground atIndex:self.view.subviews.count-2];
	[indexBackground release];
	*/
	/*
	UIImage *img = [UIImage imageNamed:@"descDriveLine.png"]
	UIImageView *driveLine = [[UIImageView alloc] initWithImage:img];
	driveLine.frameOrigin = CGPointMake(0, 88);
	[driveLine release];
	 */
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
	self.poseDict = nil;
	self.keys = nil;
}

- (void)dealloc {
	[poseDict release];
	[allPoseDict release];
	[keys release];
    [super dealloc];
}

#pragma mark === UITableView Datasource ===
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return kTableCellHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return keys.count;
}

/*

 - (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
 NSString *title = [keys objectAtIndex:section];
 return title;
 
 }
 */
 
/*
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
	if (1 != upSearchBar.alpha) {
		return indexArray;
	}
	else {
		return nil;
	}
}
 */

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
	NSString *key = [indexArray objectAtIndex:index];
	if (UITableViewIndexSearch == key) {
		if (0 == upSearchBar.alpha) {
			[upSearchBar alphaTo:1 duration:0.5];
			[table moveY:kTableMoveOffset duration:0.5];
		}
		
		return NSNotFound;
	}
	else {
		
		self.keys = [NSMutableArray arrayWithArray:indexArray];
		[self.keys removeObjectAtIndex:0];
		self.poseDict = allPoseDict;
		[table reloadData];
		table.frameSizeHeight = MIN(kTableMaxHeight+20, kTableCellHeight*keys.count);
		return index;
//		return index - 1;
	}
	
	
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	int ret = 0;
	NSString *key = [keys objectAtIndex:section];
	id poseOfKey = [poseDict valueForKey:key];
	if ([poseOfKey isKindOfClass:[NSArray class]]) 
		ret = [(NSArray *)poseOfKey count];
	else if ([poseOfKey isKindOfClass:[NSDictionary class]]) {
		ret = 1;
	}
	else {
		NSLog(@"pose is not a dictionary or array");
	}
	
	return ret;
	
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	AddIssueTableCell *cell = (AddIssueTableCell *)[tableView dequeueReusableCellWithIdentifier:@"AddIssueTableCell"];
	if (nil == cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddIssueTableCell" owner:self options:nil];
		cell = (AddIssueTableCell *)[nib objectAtIndex:0];
	}
	
	NSString *key = [self.keys objectAtIndex:indexPath.section];
	id poseOfKey = [poseDict valueForKey:key];
	NSDictionary *dict;
	if ([poseOfKey isKindOfClass:[NSArray class]]) {
		NSArray *array = (NSArray *)poseOfKey;
		dict = [array objectAtIndex:indexPath.row];
	}
	else if ([poseOfKey isKindOfClass:[NSDictionary class]]){
		dict = (NSDictionary *)poseOfKey;
	}
	else {
		NSLog(@"The current pose data structure is not dictionary");
	}
	
	
	NSString *poseName = [dict objectForKey:@"poseName"];
	NSString *picPath = [dict objectForKey:@"picPath"];
	//NSString *description = [dict objectForKey:@"description"];
	NSString *sankritName = [dict objectForKey:@"sanskrit"];
	NSString *intro = [dict objectForKey:@"Intro"];
	
	UIImage *poseImg = [UIImage imageNamed:[picPath stringByReplacingOccurrencesOfString:@".jpg" withString:@"_thumb.jpg"]];
	
	cell.poseIV.image = poseImg;
	cell.titleLB.text = poseName;
	cell.subTitleLB.text = sankritName;
	cell.descLB.text = intro;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	return cell;
}

#pragma mark --- UITableView delegate ---
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSString *key = [self.keys objectAtIndex:indexPath.section];
	NSLog(@"section %i", indexPath.section);
	id poseOfKey = [poseDict valueForKey:key];
	NSDictionary *dict;
	if ([poseOfKey isKindOfClass:[NSArray class]]) {
		NSArray *array = (NSArray *)poseOfKey;
		dict = [array objectAtIndex:indexPath.row];
		NSLog(@"row %i", indexPath.row);
	}
	else if ([poseOfKey isKindOfClass:[NSDictionary class]]){
		dict = (NSDictionary *)poseOfKey;
	}
	else {
		NSLog(@"The current pose data structure is not dictionary");
	}
	PoseDetailViewController *vc = [PoseDetailViewController alloc];
	NSLog(@"dict is %@", dict);
	vc.poseDict = dict;
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}


//OLD DELEGATES
/*
#pragma mark --- Textfeild delegate ---
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
   
	[searchBar setShowsCancelButton:YES animated:YES];

    
	mask = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460-44)];
	mask.backgroundColor = [UIColor blackColor];
	mask.alpha = 0.2;
//	[self.view insertSubview:mask aboveSubview:table];

	
    [table addSubview:mask];
	isMaskOn = YES;
	[mask release];
	return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    

    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
	
	NSLog(@"reset search bar");
	[searchBar setShowsCancelButton:NO animated:YES];

}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	
	
	if (searchBar.text.length == 0) {
		[self resetSearch];
		[table reloadData];
		table.frameSizeHeight = MIN(kTableMaxHeight+44, kTableCellHeight*keys.count);
		return;
	}


	[self handleSearchForTerm:searchBar.text];
	 
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
	
    
    NSLog(@"searchBarShouldEndEditing");
	
	[table reloadData];
	isMaskOn = NO;
	[mask removeFromSuperview];
	return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
	
	NSLog(@"searchBarCancelButtonClicked");

}*/

#pragma mark --- Textfeild delegate ---
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:YES animated:YES];
    mask = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460-44)];
    mask.backgroundColor = [UIColor blackColor];
    mask.alpha = 0.4;
    //    [self.view insertSubview:mask aboveSubview:table];
    [table addSubview:mask];
    isMaskOn = YES;
    table.scrollEnabled = NO;
    [mask release];
    return YES;
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    NSLog(@"reset search bar");
    [searchBar setShowsCancelButton:NO animated:YES];
    
    self.searchDisplayController.active = YES;
    
    [searchBar resignFirstResponder];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    
    if (searchBar.text.length == 0) {
        [self resetSearch];
        [table reloadData];
        table.frameSizeHeight = MIN(kTableMaxHeight+44, kTableCellHeight*keys.count);
        return;
    }
    
    /*
     if (isMaskOn) {
     isMaskOn = NO;
     [mask removeFromSuperview];
     }
     
     */
    [self handleSearchForTerm:searchBar.text];
    
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    /*
     [searchBar alphaTo:0 duration:0.5];
     [table moveY:-kTableMoveOffset duration:0.5];
     */
    
    [table reloadData];
    table.scrollEnabled = YES;
    isMaskOn = NO;
    table.scrollEnabled = YES;
    [mask removeFromSuperview];
    return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    
    NSLog(@"cancel");
    
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
}

@end



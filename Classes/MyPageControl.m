//
//  MyPageControl.m
//  Yoga
//
// Created by Rob on 10-4-9.
//

#import "MyPageControl.h"

@interface MyPageControl (Private)
- (void) updateDots;
@end


@implementation MyPageControl

@synthesize imageNormal = mImageNormal;
@synthesize imageCurrent = mImageCurrent;

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        // Initialization code
		self.imageNormal = [UIImage imageNamed:@"pageNormalDot.png"];
		self.imageCurrent = [UIImage imageNamed:@"pageCurrentDot.png"];
		
		
    }
    return self;
}
- (void) dealloc
{
	[mImageNormal release], mImageNormal = nil;
	[mImageCurrent release], mImageCurrent = nil;
	
	[super dealloc];
}

- (void) setNumberOfPages:(NSInteger)numberOfPages {
	[super setNumberOfPages:numberOfPages];
	NSArray* dotViews = self.subviews;
	for(int i = 0; i < dotViews.count; ++i)
	{
		UIImageView* dot = [dotViews objectAtIndex:i];
		dot.frameSize = CGSizeMake(9, 9);
	}
}
/** override to update dots */
- (void) setCurrentPage:(NSInteger)currentPage
{
	[super setCurrentPage:currentPage];
	
	// update dot views
	[self updateDots];
}

/** override to update dots */
- (void) updateCurrentPageDisplay
{
	[super updateCurrentPageDisplay];
	
	// update dot views
	[self updateDots];
}



- (void) setImageNormal:(UIImage*)image
{
	[mImageNormal release];
	mImageNormal = [image retain];
	
	// update dot views
	[self updateDots];
}



- (void) setImageCurrent:(UIImage*)image
{
	[mImageCurrent release];
	mImageCurrent = [image retain];
	
	// update dot views
	[self updateDots];
}

#pragma mark - (Private)

- (void) updateDots
{
	if(mImageCurrent || mImageNormal)
	{
		// Get subviews
		NSArray* dotViews = self.subviews;
		for(int i = 0; i < dotViews.count; ++i)
		{
			UIImageView* dot = [dotViews objectAtIndex:i];
			// Set image
			dot.image = (i == self.currentPage) ? mImageCurrent : mImageNormal;
		}
	}
}

@end

//
//  DailyTipViewController.h
//  
//
// Created by Rob on 03/25/10.
//  
//

#import <UIKit/UIKit.h>
#import "MyPageControl.h"

@interface DailyTipViewController : CommonViewController  {
	IBOutlet UIScrollView	*poseScrollView;
	IBOutlet MyPageControl	*pageControl;
	IBOutlet UILabel		*tipTV;
	IBOutlet UILabel		*tipDateLB;
	IBOutlet UILabel		*poseNameLB;
	IBOutlet UILabel		*headlineLB;
	
	int						currentPageNumber;
	NSMutableArray			*poseImageViewArray;
	
	NSNumber				*tipID;

	NSNumber				*poseID;
	NSString				*tipContent;
	NSString				*headline;
//	NSString				*tipNote;
	NSString				*poseImgName;
	NSString				*poseName;
	NSMutableArray			*tipArray;
	IBOutlet UIScrollView   *wholeScrollView;
	             
}

@property int currentPageNumber;

- (IBAction)showDetail;
- (IBAction)gotoProduct;

@end 

//
//  AddAsanaPoseViewController.m
//  Yoga
//
// Created by Rob on 10-3-29.
//  
//

#import "AddAsanaPoseViewController.h"
#import "AsanaAddPoseTableCell.h"
#import "PoseDetailViewController.h"
#import "TableViewHeader.h"
#define kTableCellHeight 65

@implementation AddAsanaPoseViewController
@synthesize asanaID;
@synthesize selectedPoseArray;
@synthesize asanaName;
@synthesize isFromAddAsana;

- (void)loadData {
	//load asana names
	id<PLResultSet> asanaNameResult;
	id<PLResultSet> poseResult;
	id<PLResultSet> poseDetailResult;
	asanaNameResult = [dbHandle() executeQuery:@"select * from Asanas"];
	while ([asanaNameResult next]) {
		NSString *name = [asanaNameResult safeObjectForColumn:@"Name"];
		NSNumber *ID = [asanaNameResult safeObjectForColumn:@"ID"];
		[asanaNameArray addObject:name];
		poseResult = [dbHandle() executeQuery:@"select * from AsanaDetail where AsanaID = ?", [ID stringValue]];
		NSMutableArray *array = [NSMutableArray array];
		while ([poseResult next]) {
			NSNumber *poseID = [poseResult safeObjectForColumn:@"PoseID"];
			NSString *poseName, *picPath, *intro, *sanskrit, *description;
			poseDetailResult = [dbHandle() executeQuery:@"select * from Poses where ID = ?", [poseID stringValue]];
			if ([poseDetailResult next]) {
				poseName = [[[[[poseDetailResult safeObjectForColumn:@"Name"] capitalizedString] stringByReplacingOccurrencesOfString:@"Iii" withString:@"III"]stringByReplacingOccurrencesOfString:@"Ii" withString:@"II"] stringByReplacingOccurrencesOfString:@"Iv" withString:@"IV"];
				sanskrit = [[[[[poseDetailResult safeObjectForColumn:@"Sanskrit"] capitalizedString] stringByReplacingOccurrencesOfString:@"Iii" withString:@"III"]stringByReplacingOccurrencesOfString:@"Ii" withString:@"II"] stringByReplacingOccurrencesOfString:@"Iv" withString:@"IV"];;
				picPath = [poseDetailResult safeObjectForColumn:@"PicturePath"];
				intro = [poseDetailResult safeObjectForColumn:@"Intro"];
                description = [poseDetailResult safeObjectForColumn:@"Description"];
			}
			[poseDetailResult close];
			NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
								  poseID, @"poseID",
								  poseName, @"poseName",
								  sanskrit, @"sanskrit",
								  picPath, @"picPath",
								  intro, @"intro",
                                  description, @"description",
								  nil];
			[array addObject:dict];
		}
		[poseResult close];
		[dataArray addObject:array];
	}
	[asanaNameResult close];
	
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	asanaNameArray = [NSMutableArray new];
	[super viewDidLoad];
	titleLB.text= asanaName;
	table.frameSizeHeight = 420;
	[self.view bringSubviewToFront:doneButton];
	
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[asanaNameArray release];
    [super dealloc];
}
#pragma mark --- UITableView delegate ---
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *dict = [[dataArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
	PoseDetailViewController *vc = [PoseDetailViewController alloc];
	vc.poseDict = dict;
    [vc initWithNibUsingClassName];
    NSLog(@"view Asana during adding");
	//vc.isFromMyAsana = YES;
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return kTableCellHeight;
}

#pragma mark --- UITableView Datasource ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return asanaNameArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [[dataArray objectAtIndex:section] count];
	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	AsanaAddPoseTableCell *cell = (AsanaAddPoseTableCell *)[tableView dequeueReusableCellWithIdentifier:@"AsanaAddPoseTableCell"];
	if (nil == cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AsanaAddPoseTableCell" owner:self options:nil];
		cell = (AsanaAddPoseTableCell *)[nib objectAtIndex:0];
	}
	
	NSDictionary *dict = [[dataArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
	NSNumber *poseID = [dict objectForKey:@"poseID"];
	NSString *name = [dict objectForKey:@"poseName"];
	NSString *sanskrit = [dict objectForKey:@"sanskrit"];
	NSString *picPath = [dict objectForKey:@"picPath"];
	NSString *intro = [dict objectForKey:@"intro"];
	UIImage *poseImg = [UIImage imageNamed:[picPath stringByReplacingOccurrencesOfString:@".jpg" withString:@"_thumb.jpg"]];
	
	cell.poseIV.image = poseImg;
	cell.titleLB.text = name;
	cell.subTitleLB.text = sanskrit;
	cell.descLB.text = intro;
	cell.selectBtn.tag = [poseID intValue];
	BOOL isSelected = NO;
	for (NSNumber *ID in selectedPoseArray) {
		if ([ID isEqualToNumber:poseID]) {
			isSelected = YES;
			break;
		}
	}
	cell.selectBtn.selected = isSelected;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	TableViewHeader *header;
	NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TableViewHeader" owner:nil options:nil];
	for (id oneObject in nib)
		if ([oneObject isKindOfClass:[TableViewHeader class]]){
			header = (TableViewHeader *)oneObject;
			header.textLB.text = [asanaNameArray objectAtIndex:section];
			
		}
	return header;
}
#pragma mark --- IBAction ---
- (IBAction)addPoseDone {
	if (isFromAddAsana) {
		NSArray *vcs = self.navigationController.viewControllers;
		UIViewController *vc = [vcs objectAtIndex:vcs.count - 3];
		[self.navigationController popToViewController:vc animated:YES];
	}
	else {
		[self.navigationController popViewControllerAnimated:YES];
	}

}

- (IBAction)selectPose:(id)sender {
	UIButton *selectedBtn = (UIButton *)sender;
	int poseID = [selectedBtn tag];
	if (selectedBtn.selected) {
		selectedBtn.selected = NO;
		[dbHandle() executeUpdate:@"Delete from MyAsanaPoses where AsanaID = ? and PoseID = ?", 
				   stringFromInt(asanaID), stringFromInt(poseID)];
		for (NSNumber *ID in selectedPoseArray) {
			if ([ID intValue] == poseID) {
				[selectedPoseArray removeObject:ID];
				break;
			}
		}
		
	}
	else {
		selectedBtn.selected = YES;
		int index = 0;
		id<PLResultSet> result;
		result = [dbHandle() executeQuery:@"select * from MyAsanaPoses order by PoseIndex DESC"];
		if ([result next]) {
			index = [[result objectForColumn:@"PoseIndex"] intValue];
		}
		[dbHandle() executeUpdate:@"insert into MyAsanaPoses(AsanaID, PoseID, PoseIndex) values(?,?,?)", 
				   stringFromInt(asanaID), stringFromInt(poseID), stringFromInt(index + 1)];
		[selectedPoseArray addObject:[NSNumber numberWithInt:poseID]];
		
	}

}

@end

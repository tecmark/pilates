//
//  YogaClass.m
//  Yoga
//
// Created by Rob on  11/12/10.
//  
//

#import "YogaClass.h"
#import "RateCommentsHelper.h"

#define kClassResoureceRootDirectory  	[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] \
										stringByAppendingString:@"/ClassResources"]

#define kMainImagePath					@"%@/%@/main.jpg"
#define kBiogImagePath					@"%@/%@/biog.jpg"

#define kKramaMainImagePath				@"%@/%@/Main/MainImage%i.jpg"
#define kKramaThumbnailsPath			@"%@/%@/Thumbnails/Thumbnail%i.jpg"

#define kClassVideoPath					@"%@/%@/Video"

#define kVideoDatabasePath				@"ClassResources/%@/Video/a%i.m4v"
#define kUAVideoDatabasePath			@"../Library/ua/downloads/%@/a%i.m4v"

@implementation YogaClass


@synthesize classDB_ID, classID, name, shortDescription, longDescription, classDuration, biogText, biogImage, mainImage;
@synthesize krama, kramaDuration, kramaMainImages, kramaThumbnails, video, averageRating, comments, classSize, product;


#pragma mark -
#pragma mark Private methods
- (void)loadClassSize {
	id<PLResultSet> result = [dbHandle() executeQuery: @"SELECT * FROM Classes where ClassID = ?", self.classID];
	if ([result next]) {
		self.classSize = [result doubleForColumn:@"FileSize"];
	}
	[result close];
}
+ (id)loadClassFromDatabaseWidhClassID:(NSString*)ID isDownloaded:(BOOL)downloaded {
	YogaClass *yogaClass = [[YogaClass alloc] initWithClassID:ID];
	
    
    NSLog(@"loadclassfromdb");
    
	NSString *bundlePath = [[NSBundle mainBundle] bundlePath];
	
	if (yogaClass != nil) {
		id<PLResultSet> result = [dbHandle() executeQuery: @"SELECT * FROM Classes where classid = ? and Installed = ?", ID, downloaded ? stringFromInt(1) : stringFromInt(0)];
		
		if ([result next]) {
			yogaClass.name = [result stringForColumn:@"Name"];
			yogaClass.shortDescription = [result stringForColumn:@"ShortDesciption"];
			yogaClass.longDescription = [result stringForColumn:@"Description"];
			yogaClass.classDuration = [result stringForColumn:@"ClassDuration"];
			yogaClass.biogText = [result stringForColumn:@"BiogText"];
            
            NSLog(@"loading class");
			
			NSString *biogImagePath = [result stringForColumn:@"BiogPicturePath"];
			// Had to do the hard code here
			if ([ID isEqualToString:@"0001"]) {
				biogImagePath = [bundlePath stringByAppendingPathComponent:biogImagePath];
			}
			NSLog(@"biogImagePath: %@", biogImagePath, nil);
			yogaClass.biogImage = [NSData dataWithContentsOfFile:biogImagePath];
			
			
			NSString *mainImagePath = [result stringForColumn:@"MainPicturePath"];
			// Had to do the hard code here
			if ([ID isEqualToString:@"0001"]) {
				mainImagePath = [bundlePath stringByAppendingPathComponent:mainImagePath];
			}
			yogaClass.mainImage = [NSData dataWithContentsOfFile:mainImagePath];
			
			
			NSInteger classCategoryID = [result intForColumn:@"ClassCatalogID"];
            
            NSLog(@"cat ID = %i", classCategoryID);
            
            classCategoryID=2;
			id<PLResultSet> classCatalogesResult = [dbHandle() executeQuery: @"SELECT * FROM ClassCataloges where id = ?", stringFromInt(classCategoryID)];
			if ([classCatalogesResult next]) {
				yogaClass.classCategory = [classCatalogesResult stringForColumn:@"Name"];
			}
			[classCatalogesResult close];
			
			
			[yogaClass loadClassSize];
			
			
			yogaClass.classDB_ID = [result intForColumn:@"ID"];
			
			/*
			id<PLResultSet>subResult = [dbHandle() executeQuery: @"SELECT * FROM ClassSections where classid = ?", stringFromInt(yogaClass.classDB_ID)];
			NSMutableArray *tempKrama = [NSMutableArray array];
			NSMutableArray *tempKramaDuration = [NSMutableArray array];
			NSMutableArray *tempKramaMainImages = [NSMutableArray array];
			NSMutableArray *tempKramaThumbnails = [NSMutableArray array];
			while ([subResult next]) {
				[tempKrama addObject:[subResult stringForColumn:@"Name"]];
				[tempKramaDuration addObject:stringFromInt([subResult intForColumn:@"Duration"])];
				
				NSData *kramaMainImageData;
				NSString *kramaMainImagePath = [subResult stringForColumn:@"PicturePath"];
				if ([ID isEqualToString:@"0001"]) {
					kramaMainImagePath = [bundlePath stringByAppendingPathComponent:kramaMainImagePath];
					NSLog(kramaMainImagePath, nil);
				}
				kramaMainImageData = [NSData dataWithContentsOfFile:kramaMainImagePath];
				[tempKramaMainImages addObject:kramaMainImageData];
				
				
				NSData *kramaThumbnailData;
				NSString *kramaThumbnailPath = [subResult stringForColumn:@"ThumbnailPath"];
				if ([ID isEqualToString:@"0001"]) {
					kramaThumbnailPath = [bundlePath stringByAppendingPathComponent:kramaThumbnailPath];
					NSLog(kramaThumbnailPath, nil);
				}
				kramaThumbnailData = [NSData dataWithContentsOfFile:kramaThumbnailPath];
				[tempKramaThumbnails addObject:kramaThumbnailData];
			}
			[subResult close];
			
			yogaClass.krama = [NSArray arrayWithArray:tempKrama];
			yogaClass.kramaDuration = [NSArray arrayWithArray:tempKramaDuration];
			yogaClass.kramaMainImages = [NSArray arrayWithArray:tempKramaMainImages];
			yogaClass.kramaThumbnails = [NSArray arrayWithArray:tempKramaThumbnails];
			*/
			
			// Load comments and rate
			NSInteger rate;
			yogaClass.comments = [[RateCommentsHelper shared] getCommentsFromDatabaseWithClassID:ID averageRate:&rate];
			yogaClass.averageRating = rate;
		}
		else {
			yogaClass = nil;
		}
        
		
		[result close];
	}
	
    NSLog(@"Class loaded from db");
	return [yogaClass autorelease];
}

#pragma mark -
#pragma mark init and dealloc methods
+ (id)classByLoadingDownloadedFromDatabaseWithClassID:(NSString*)ID {
    
    NSLog(@"classByLoadingDownloadedFromDatabaseWithClassID:");
	return [YogaClass loadClassFromDatabaseWidhClassID:ID isDownloaded:YES];
    
    NSLog(@"classByLoadingDownloadedFromDatabaseWithClassID: ended");
}
+ (id)classByLoadingFromDatabaseWithClassID:(NSString*)ID {
    NSLog(@"*** classByLoadingFromDatabaseWithClassID:");
	return [YogaClass loadClassFromDatabaseWidhClassID:ID isDownloaded:NO];
    NSLog(@"*** classByLoadingFromDatabaseWithClassID: end");
}
+ (id)classByLoadingFromDatabaseWithClassIDFromBasicInfo:(NSString*)ID {
	YogaClass *yogaClass = [[YogaClass alloc] initWithClassID:ID];
	
	if (yogaClass != nil) {
		id<PLResultSet> result = [dbHandle() executeQuery: @"SELECT * FROM Classes where classid = ?", ID];
		
		if ([result next]) {
			yogaClass.name = [result stringForColumn:@"Name"];
			yogaClass.shortDescription = [result stringForColumn:@"ShortDesciption"];
			yogaClass.longDescription = [result stringForColumn:@"Description"];
			yogaClass.classDuration = [result stringForColumn:@"ClassDuration"];
			yogaClass.biogText = [result stringForColumn:@"BiogText"];
			
			//NSString *biogImagePath = [result stringForColumn:@"BiogPicturePath"];
			//yogaClass.biogImage = [NSData dataWithContentsOfFile:biogImagePath];
			
			//NSString *mainImagePath = [result stringForColumn:@"MainPicturePath"];
			//yogaClass.mainImage = [NSData dataWithContentsOfFile:mainImagePath];
			
			NSInteger classCategoryID = [result intForColumn:@"ClassCatalogID"];
			id<PLResultSet> classCatalogesResult = [dbHandle() executeQuery: @"SELECT * FROM ClassCataloges where id = ?", stringFromInt(classCategoryID)];
			if ([classCatalogesResult next]) {
				yogaClass.classCategory = [classCatalogesResult stringForColumn:@"Name"];
			}
			[classCatalogesResult close];
			
			[yogaClass loadClassSize];
			
			
			yogaClass.classDB_ID = [result intForColumn:@"ID"];
			/*
			id<PLResultSet>subResult = [dbHandle() executeQuery: @"SELECT * FROM ClassSections where classid = ?", stringFromInt(classDB_ID)];
			NSMutableArray *tempKrama = [NSMutableArray array];
			NSMutableArray *tempKramaDuration = [NSMutableArray array];
			NSMutableArray *tempKramaMainImages = [NSMutableArray array];
			NSMutableArray *tempKramaThumbnails = [NSMutableArray array];
			while ([subResult next]) {
				[tempKrama addObject:[subResult stringForColumn:@"Name"]];
				[tempKramaDuration addObject:stringFromInt([subResult intForColumn:@"Duration"])];
				
				NSData *kramaMainImageData = [NSData dataWithContentsOfFile:[subResult stringForColumn:@"PicturePath"]];
				[tempKramaMainImages addObject:kramaMainImageData];
				
				NSData *kramaThumbnailData = [NSData dataWithContentsOfFile:[subResult stringForColumn:@"ThumbnailPath"]];
				[tempKramaThumbnails addObject:kramaThumbnailData];
			}
			[subResult close];
			
			yogaClass.krama = [NSArray arrayWithArray:tempKrama];
			yogaClass.kramaDuration = [NSArray arrayWithArray:tempKramaDuration];
			yogaClass.kramaMainImages = [NSArray arrayWithArray:tempKramaMainImages];
			yogaClass.kramaThumbnails = [NSArray arrayWithArray:tempKramaThumbnails];
			
			// TODO: Need to load 
			yogaClass.averageRating = 4;
			yogaClass.comments = nil;
			 */
			//[[RateCommentsHelper shared] getCommentRateWithClassID:ID];
		}
		else {
			yogaClass = nil;
		}
		
		[result close];
	}
	
	return [yogaClass autorelease];
}
- (id)initWithClassID:(NSString*)ID {
	self = [super init];
	
	if (self != nil) {
		self.classID = ID;
		self.averageRating = 0;
	}
	
	return self;
}
- (void)dealloc {
	[classID release];
 	[name release];
	[classCategory release];
	[shortDescription release];
	[longDescription release];
	[classDuration release];
	[biogText release];
	[biogImage release];
	[mainImage release];
	[krama release];
	[kramaMainImages release];
	[kramaThumbnails release];
	[comments release];
	[video release];
	
	[super dealloc];	
}



#pragma mark -
#pragma mark Public methods
- (void)saveToDatabase {
	//Save the class info in local storeage and database
	id<PLResultSet> result;
	
	BOOL hasSaved = NO;
	result = [dbHandle() executeQuery: @"SELECT * FROM Classes where ClassID = ?", self.classID];
	if ([result next]) {
		hasSaved = YES;
	}
	[result close];
	
	// There's the class in database so we should not insert again
	//if (hasSaved) return; 
	
	NSLog(@"Save To Database", nil);
	
	NSInteger categoryID;
	result = [dbHandle() executeQuery: @"SELECT * FROM ClassCataloges where name = ?", 
							  [[self.classCategory stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] lowercaseString]];
	if ([result next]) {
		categoryID = [result intForColumn:@"ID"];
	}
	[result close];
	
	BOOL isDir;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	NSString *classResourcesPath = kClassResoureceRootDirectory;
	if (![fileManager fileExistsAtPath:classResourcesPath isDirectory:&isDir]) {
		[fileManager createDirectoryAtPath:classResourcesPath withIntermediateDirectories:YES attributes:nil error:NULL];
		NSLog(@"Create classResourcesPath \n%@", classResourcesPath, nil);
	}
	
	NSString *classPath = [NSString stringWithFormat:@"%@/%@", classResourcesPath, self.classID, nil];
	if (![fileManager fileExistsAtPath:classPath isDirectory:&isDir]) {
		[fileManager createDirectoryAtPath:classPath withIntermediateDirectories:YES attributes:nil error:NULL];
		NSLog(@"Create classPath \n%@", classPath, nil);
	}
	
	NSString *mainPicturePath = [NSString stringWithFormat:kMainImagePath, kClassResoureceRootDirectory, classID, nil]; 
	NSLog(mainPicturePath, nil);
	NSString *biogPicturePath = [NSString stringWithFormat:kBiogImagePath, kClassResoureceRootDirectory, classID, nil];
	NSLog(biogPicturePath, nil);
	
	BOOL status = [self.mainImage writeToFile:mainPicturePath atomically:NO];
	status = [self.biogImage writeToFile:biogPicturePath atomically:NO];
	
	if (hasSaved) {
		[dbHandle() executeUpdate:@"update Classes set ClassCatalogID = ?, Name = ?, ShortDesciption = ?, Description = ?, MainPicturePath = ?, BiogPicturePath = ?, "
		 "ClassDuration = ?, BiogText = ? where ClassID = ?", 
		 stringFromInt(categoryID), name, shortDescription, longDescription, mainPicturePath, biogPicturePath, classDuration, biogText, classID];
	}
	else {
		[dbHandle() executeUpdate:@"insert into Classes(ClassCatalogID, Name, ShortDesciption, Description, MainPicturePath, BiogPicturePath, ClassDuration, BiogText, ClassID)"
		 " values(?, ?, ?, ?, ?, ?, ?, ?, ?)", 
		 stringFromInt(categoryID), name, shortDescription, longDescription, mainPicturePath, biogPicturePath, classDuration, biogText, classID];
	}

	
	result = [dbHandle() executeQuery: @"SELECT * FROM Classes where classid = ?", self.classID];
	NSInteger newClassID;
	if ([result next]) {
		newClassID = [result intForColumn:@"ID"];
	}
	[result close];
	
	NSString *kramaMainPath = [NSString stringWithFormat:@"%@/%@", classPath, @"Main", nil];
	if (![fileManager fileExistsAtPath:kramaMainPath isDirectory:&isDir]) {
		[fileManager createDirectoryAtPath:kramaMainPath withIntermediateDirectories:YES attributes:nil error:NULL];
		NSLog(@"Create kramaMainPath \n%@", kramaMainPath, nil);
	}
	
	NSString *kramaThumbnailsPath = [NSString stringWithFormat:@"%@/%@", classPath, @"Thumbnails", nil];
	if (![fileManager fileExistsAtPath:kramaThumbnailsPath isDirectory:&isDir]) {
		[fileManager createDirectoryAtPath:kramaThumbnailsPath withIntermediateDirectories:YES attributes:nil error:NULL];
		NSLog(@"Create kramaThumbnailsPath \n%@", kramaThumbnailsPath, nil);
	}
	
	// Delete the previous values
	[dbHandle() executeUpdate:@"delete from ClassSections where ClassID = ?", stringFromInt(newClassID)];
	
    
    NSLog(@"KRAMA About to save full details for %i Kramas", [self.krama count]);
	for (int i = 0; i < [self.krama count]; i++) {
		NSString *mainKramaMainImagePath = [NSString stringWithFormat:kKramaMainImagePath, kClassResoureceRootDirectory, classID, i + 1, nil];
		//NSLog(mainKramaMainImagePath, nil);
		NSString *mainKramaThumbnailPath = [NSString stringWithFormat:kKramaThumbnailsPath, kClassResoureceRootDirectory, classID, i + 1, nil];
		//NSLog(mainKramaThumbnailPath, nil);
		
		status = [[self.kramaMainImages objectAtIndex:i] writeToFile:mainKramaMainImagePath atomically:NO];
		status = [[self.kramaThumbnails objectAtIndex:i] writeToFile:mainKramaThumbnailPath atomically:NO];
		
		NSLog(@"KRAMA Save Krama %i");
        NSLog(@"KRAMA Save ID %@", stringFromInt(newClassID));
         NSLog(@"KRAMA Save Title %@", [self.krama objectAtIndex:i]);
         NSLog(@"KRAMA Save PicPath %@", mainKramaMainImagePath);
         NSLog(@"KRAMA Save VidPath %@", [NSString stringWithFormat:kUAVideoDatabasePath, self.classID, i + 1]);
        NSLog(@"KRAMA duration count %i", [self.kramaDuration count]);
         NSLog(@"KRAMA Save Duration %@", [self.kramaDuration objectAtIndex:i] );
         NSLog(@"KRAMA Save ThumbPath %@", mainKramaThumbnailPath);
        
      //  NSLog("DB String = %@", [NSString stringWithFormat:@"insert into ClassSections(ClassID, Name, PicturePath, VideoPath, Duration, ThumbnailPath) values(bb, aa, picturePath, videopath, duration, thumbPath)"]);
        
        
		[dbHandle() executeUpdate:@"insert into ClassSections(ClassID, Name, PicturePath, VideoPath, Duration, ThumbnailPath)"
		 " values(?, ?, ?, ?, ?, ?)", stringFromInt(newClassID), [self.krama objectAtIndex:i], mainKramaMainImagePath, [NSString stringWithFormat:kUAVideoDatabasePath, self.classID, i + 1],[self.kramaDuration objectAtIndex:i], mainKramaThumbnailPath];
        
		NSLog(@"KRAMA Save complete");
	}
	
	// Save comments and rates
	[[RateCommentsHelper shared] saveCommentsRatesToDataBaseWithClassID:self.classID commentsRates:self.comments];
    
    NSLog(@"Save to database completed");
}
- (NSString*)fileSizeToString {
	double bytes = self.classSize;
	if (bytes < 1024)
		return([NSString stringWithFormat:@"%.0f bytes",bytes]);
	
	bytes /= 1024.0;
	if (bytes < 1024)
		return([NSString stringWithFormat:@"%1.2f KB",bytes]);
	
	bytes /= 1024.0;
	if (bytes < 1024)
		return([NSString stringWithFormat:@"%1.2f MB",bytes]);
	
	bytes /= 1024.0;
	if (bytes < 1024)
		return([NSString stringWithFormat:@"%1.2f GB",bytes]);
	
	bytes /= 1024.0;
	return([NSString stringWithFormat:@"%1.2f TB",bytes]);
}
- (void)purchased {
	[dbHandle() executeUpdate:@"update Classes set Purchased = 1 where ClassID = ?", self.classID];
}
- (void)addFileSize {
	[dbHandle() executeUpdate:@"update Classes set FileSize = ? where ClassID = ?", stringFromFloat(product.fileSize), self.classID];
}
+ (double)getFileSizeByClassID:(NSString*)_classID {
	double fileSize;
	id<PLResultSet> result = [dbHandle() executeQuery: @"SELECT * FROM Classes where ClassID = ?", _classID];
	if ([result next]) {
		fileSize = [result doubleForColumn:@"FileSize"];
	}
	[result close];
	
	return fileSize;
}
- (NSString*)classVideoPath {
	return [NSString stringWithFormat:kClassVideoPath, kClassResoureceRootDirectory, self.classID, nil];
}
+ (void)installedClassWithID:(NSString*)classID {
	[dbHandle() executeUpdate:@"update Classes set Installed = ? where classid = ?", stringFromInt(1), classID];
}
+ (BOOL)isInstalled:(NSString*)classID {
	id<PLResultSet> result = [dbHandle() executeQuery: @"SELECT * FROM Classes where ClassID = ?", classID];
	if ([result next]) {
		return [result boolForColumn:@"Installed"];
	}
	[result close];
	
	return NO;
}

#pragma mark -
#pragma mark classCategory implementation
- (NSString*)classCategory {
	return [[classCategory lowercaseString] capitalizedString];
}
- (void)setClassCategory:(NSString *)category {
	[classCategory release];
	classCategory = [category retain];
}


@end

//
//  JournalViewController.h
//  Yoga
//
// Created by Rob on 10-3-15.

//

#import <UIKit/UIKit.h>
#import "JournalHomeTableCell.h"
#import "AddIssueViewController.h"
#import "IssueDetailViewController.h"

@interface JournalViewController : CommonTableViewController <UITableViewDelegate, UITableViewDataSource> {
	NSMutableArray *notResolvedIssueArray;
	NSMutableArray *resolvedIssueArray;
	IBOutlet UIView *noCententInfoView;
	BOOL isForPoseDetailsView;
}


@property BOOL isForPoseDetailsView;


- (IBAction)setResolved:(id)sender;
- (IBAction)addIssue;
/*
- (void)updateResolvedOf:(int)issueRowNumber setResolved:(BOOL)flag;
- (void)deleteIssueAtSection:(int)section row:(int)row;
 */
@end

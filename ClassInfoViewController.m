//
//  ClassInfoViewController.m
//  Yoga
//
// Created by Rob on 10-4-14.
//

#import "ClassInfoViewController.h"
//#import "WriteCommentViewController.h"
#import "ReadCommentsViewController.h"

@implementation ClassInfoViewController


@synthesize classDict, yogaClass;
@synthesize currentPageNumber;


#pragma mark -
#pragma mark --- set/get Method ---
- (void) setCurrentPageNumber:(int)num {
	currentPageNumber = num;
	
	//@TODO set the content of the page
	pageControl.currentPage = currentPageNumber;
	[pageControl updateCurrentPageDisplay];
	
}

-(void)viewWillAppear:(BOOL)animated{
    
}

#pragma mark View Controller methods
- (void)viewDidLoad {
    [super viewDidLoad];
     
    imageScrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 210)];
    [scrollView addSubview:imageScrollView];
   
    [scrollView bringSubviewToFront:rateAndComment];
    
	titleLB.text = @"Class info";
    
    
   //scrollView.contentSize=CGSizeMake(320, descriptionTV.frame.size.height+300);
   
	descriptionTV.text = [yogaClass.longDescription stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n\n"];
    biogTV.text = [yogaClass.biogText stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n\n"];
    
    
    CGSize constraintSize;
    constraintSize.width = 300.0f;
    constraintSize.height = 1000.0f;
    NSString *theText = descriptionTV.text;
    CGSize theSize = [theText sizeWithFont:descriptionTV.font constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
    
    
    
    NSLog(@"height = %f", theSize.height);
    
	[scrollView setContentSize:CGSizeMake(320, theSize.height+340)];
	
    pageControl.numberOfPages = 2;
	self.currentPageNumber = 0;
	imageScrollView.showsHorizontalScrollIndicator = NO;
	imageScrollView.contentSize = CGSizeMake(imageScrollView.frameSizeWidth*2, imageScrollView.frameSizeHeight);
	imageScrollView.delegate = self;
	UIImage *img = [UIImage imageWithData:yogaClass.mainImage]; //[UIImage imageNamed:[classDict objectForKey:@"mainPicture"]];
	UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
	imgView.frameSize = imageScrollView.frameSize;
	[imageScrollView addSubview:imgView];
	[imgView release];
	

	biogView.frameOriginX = imageScrollView.frameSizeWidth;
	[imageScrollView addSubview:biogView];
	biogImageView.image = [UIImage imageWithData:yogaClass.biogImage]; //[UIImage imageNamed:[classDict objectForKey:@"bioPicture"]];

	
	className.text = yogaClass.name; //[classDict objectForKey:@"className"];
	[starView setRating: yogaClass.averageRating /*[[classDict objectForKey:@"rateLevel"] intValue]*/];
	
	
	if(yogaClass.averageRating>0){
	starView.hidden=NO;
		CGRect frame = classDuration.frame;
		frame.origin.x=82;
		classDuration.frame=frame;
	}
	classDuration.text = yogaClass.classDuration; //[classDict objectForKey:@"classDruation"];
	commentCountLB.text = stringFromInt(yogaClass.comments.count);
}
- (void)dealloc {
	[classDict release];
	[yogaClass release];
    [super dealloc];
}


#pragma mark -
#pragma mark UIScrollViewDelegate methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)ascrollView {
	const CGFloat pageWidth = ascrollView.frame.size.width;	
	const int num = ascrollView.contentOffset.x / pageWidth;
	if(num  != currentPageNumber) {
		self.currentPageNumber = num;
	}
    
    CGSize constraintSize;
    constraintSize.width = 290.0f;
    constraintSize.height = 1000.0f;
	
	if (num == 1) {

        
        descriptionTV.alpha=0;
        biogTV.alpha=1;
        
   
        NSString *theText = biogTV.text;
        CGSize theSize = [theText sizeWithFont:biogTV.font constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
        [scrollView setContentSize:CGSizeMake(320, theSize.height+340)];
        
              
       	}
	else {
        NSString *theText = descriptionTV.text;
        CGSize theSize = [theText sizeWithFont:descriptionTV.font constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
        [scrollView setContentSize:CGSizeMake(320, theSize.height+340)];
   
        descriptionTV.alpha=1;
        biogTV.alpha=0;
      
        
	}
    
    
    
}


#pragma mark -
#pragma mark IB Action Methods
- (IBAction)AddComment {
	/*
	WriteCommentViewController *vc = [WriteCommentViewController alloc];
	[vc initWithNibUsingClassName];
	//vc.yogaClass = 
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
	 */
}
- (IBAction)showComment {
	ReadCommentsViewController *vc = [ReadCommentsViewController alloc];
	[vc initWithNibUsingClassName];
	vc.yogaClass = self.yogaClass;
    vc.writeCommentEnabled=YES;
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}

@end

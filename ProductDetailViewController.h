//
//  ProductDetailViewController.h
//  Yoga
//
// Created by Rob on 10-4-16.
//  
//

#import <UIKit/UIKit.h>


@interface ProductDetailViewController : CommonViewController {
	NSDictionary *productDict;
	IBOutlet UIImageView *imageView;
	IBOutlet UILabel *brandLB;
	IBOutlet UITextView *descriptionTV;
}

@property (nonatomic,retain) NSDictionary *productDict;

- (IBAction)addToWishlist;
- (IBAction)buyIt;

@end

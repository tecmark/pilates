//
//  IntroViewController.m
//  Yoga
//
// Created by Rob on 10-3-19.

//

#import "IntroViewController.h"


@implementation IntroViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	titleLB.text = @"Custom Classes";
}
- (IBAction)loadArticle:(UIButton*)sender{
	
	NSLog(@"Load article");
    
    int tag = sender.tag;
    NSString *filePath = @"";
    NSString *title = @"";
    
    switch (tag) {
        case 1:
        {
            // Class structure
            filePath = [[NSBundle mainBundle] pathForResource:@"4.a.i.Class structure web" ofType:@"htm"];
            title = @"Class Structure";
        }
            break;
        case 3:
        {
            filePath = [[NSBundle mainBundle] pathForResource:@"4.a.ii.Instructor profile web" ofType:@"htm"];
            title = @"Instructor Profile";
        }
            break;
            
        default:
            break;
    }
	
	ArticleDetailViewController *_articleVC= [[ArticleDetailViewController alloc] initWithWebPage:filePath andTitle:title];

	
	[self.navigationController pushViewController:_articleVC animated:YES];
	
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end

//
//  MyClassTableCell.m
//  Yoga
//
// Created by Rob on 10-3-22.

//

#import "MyClassTableCell.h"


@implementation MyClassTableCell
@synthesize classNameLB;
@synthesize durationLB;
@synthesize descriptionLB;
@synthesize poseIV;
@synthesize starView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:NO animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
	[classNameLB release];
	[durationLB release];
	[descriptionLB release];
	[poseIV release];
	[starView release];
    [super dealloc];
}

- (void)setRating:(NSInteger)rating {
	[starView setRating:rating];
}

@end

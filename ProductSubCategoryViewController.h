//
//  ProductDetailViewController.h
//  Yoga
//
// Created by Rob on 10-4-16.
//  
//

#import <UIKit/UIKit.h>
#import "ProductCategoryViewController.h"

@interface ProductSubCategoryViewController : ProductCategoryViewController {
	NSDictionary *subCategoryDict;
}

@property (nonatomic, retain) NSDictionary *subCategoryDict;
@end

//
//  AddIssueViewController.m
//  Yoga
//
// Created by Rob on 10-3-17.

//

#import "AddIssueViewController.h"
#import "IssueDetailViewController.h"
#import "AddGeneralIssueTableCell.h"
#import "AddIssueTableCell.h"

#define kTableCellHeight 65
#define kTableMoveOffset 40

@implementation AddIssueViewController


@synthesize isChangeTitle, changeTitleCallbackTarget, changeTitleCallbackSelector;


- (void)loadData {
	id<PLResultSet> result;
	result = [dbHandle() executeQuery: @"SELECT * FROM Poses"];
	while ([result next]) {
		NSString *picPath = [result safeStringForColumn:@"PicturePath"];
		
		
		NSNumber *poseId = [result objectForColumn:@"ID"];
        
        
		NSString *poseName = [[[[[result safeStringForColumn:@"Name"] capitalizedString] stringByReplacingOccurrencesOfString:@"Iii" withString:@"III"]stringByReplacingOccurrencesOfString:@"Ii" withString:@"II"] stringByReplacingOccurrencesOfString:@"Iv" withString:@"IV"];
        
      
		NSString *sanskrit = [[[[[result safeStringForColumn:@"Sanskrit"] capitalizedString] stringByReplacingOccurrencesOfString:@"Iii" withString:@"III"]stringByReplacingOccurrencesOfString:@"Ii" withString:@"II"] stringByReplacingOccurrencesOfString:@"Iv" withString:@"IV"];
        

		NSString *description = [result safeStringForColumn:@"description"];
		
        NSString *intro =  [result safeStringForColumn:@"Intro"];
		
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:picPath, @"picPath",
							  poseName, @"poseName", 
							  sanskrit, @"sanskrit",
							  description, @"description", 
							  poseId, @"poseID", 
							  intro, @"Intro",
							  nil];
		NSString *newKey = [[poseName substringToIndex:1] uppercaseString];
		BOOL keyExist = NO;
		for (NSString *key in self.keys) {
			if ([newKey isEqualToString:key]) {
				keyExist = YES;
				break;
			}
		}
		
		if (!keyExist) {
			[self.keys addObject:newKey];
		}
		NSMutableArray *array = [NSMutableArray arrayWithArray:[allPoseDict objectForKey:newKey]];
		[array addObject:dict];
		[allPoseDict setObject:array forKey:newKey];
		
	}
	self.poseDict = [NSMutableDictionary dictionaryWithDictionary: allPoseDict];
	
	//	[indexArray addObject:UITableViewIndexSearch];
	self.keys = (NSMutableArray *)[self.keys sortedArrayUsingSelector:@selector(compare:)];
	[indexArray addObjectsFromArray:self.keys];
	[result close];
}

- (id)init {
	self = [super init];
	
	if (self != nil) {
		self.isChangeTitle = NO;
		self.changeTitleCallbackTarget = nil;
	}
	return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
	titleLB.text = @"Select a pose";
	
	//table.frameSizeHeight = 420;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
	
}


- (void)dealloc {
    [super dealloc];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	int ret = 0;
	NSString *key = [keys objectAtIndex:section];
	id poseOfKey = [poseDict valueForKey:key];
	if ([poseOfKey isKindOfClass:[NSArray class]]) 
		ret = [(NSArray *)poseOfKey count];
	else if ([poseOfKey isKindOfClass:[NSDictionary class]]) {
		ret = 1;
	}
	else {
		NSLog(@"pose is not a dictionary or array");
	}
	
	NSLog(@"Number Of Rows: %i In Section %i", ret + 1, section, nil);
	
	if (section == 0) {
		return ret + 1;
	}
	else {
		return ret;
	}	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSLog(@"cellForRowAtIndexPath %i", indexPath.row , nil);
	if (indexPath.row == 0 && indexPath.section == 0) {
		AddGeneralIssueTableCell *cell = (AddGeneralIssueTableCell *)[tableView dequeueReusableCellWithIdentifier:@"AddGeneralIssueTableCell"];
		if (nil == cell) {
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddGeneralIssueTableCell" owner:self options:nil];
			cell = (AddGeneralIssueTableCell *)[nib objectAtIndex:0];
			cell.selectionStyle = UITableViewCellSelectionStyleNone;
		}
		
		return cell;
	}
	else {
		AddIssueTableCell *cell = (AddIssueTableCell *)[tableView dequeueReusableCellWithIdentifier:@"AddIssueTableCell"];
		if (nil == cell) {
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddIssueTableCell" owner:self options:nil];
			cell = (AddIssueTableCell *)[nib objectAtIndex:0];
		}
		
		NSString *key = [self.keys objectAtIndex:indexPath.section];
		id poseOfKey = [poseDict valueForKey:key];
		NSDictionary *dict;
		if ([poseOfKey isKindOfClass:[NSArray class]]) {
			NSArray *array = (NSArray *)poseOfKey;
			if (indexPath.section == 0) {
				dict = [array objectAtIndex:indexPath.row - 1];
			}
			else {
				dict = [array objectAtIndex:indexPath.row];
			}
			
		}
		else if ([poseOfKey isKindOfClass:[NSDictionary class]]){
			dict = (NSDictionary *)poseOfKey;
		}
		else {
			NSLog(@"The current pose data structure is not dictionary");
		}
		
		
		NSString *poseName = [dict objectForKey:@"poseName"];
		NSString *picPath = [dict objectForKey:@"picPath"];
		//NSString *description = [dict objectForKey:@"description"];
		NSString *sankritName = [dict objectForKey:@"sanskrit"];
		NSString *intro = [dict objectForKey:@"Intro"];
		
		UIImage *poseImg = [UIImage imageNamed:[picPath stringByReplacingOccurrencesOfString:@".jpg" withString:@"_thumb.jpg"]];
		
		cell.poseIV.image = poseImg;
		cell.titleLB.text = poseName;
		cell.subTitleLB.text = sankritName;
		cell.descLB.text = intro;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		return cell;
	}
}

#pragma mark --- UITableView delegate ---
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (self.isChangeTitle && self.changeTitleCallbackTarget != nil && [self.changeTitleCallbackTarget respondsToSelector:changeTitleCallbackSelector]) 
	{
		NSString *newTitle = @"";
		if (indexPath.row == 0 && indexPath.section == 0) {
			newTitle = @"General Issue";
		}
		else {
			NSDictionary *dict;
			NSString *key = [self.keys objectAtIndex:indexPath.section];
			NSLog(@"section %i", indexPath.section);
			id poseOfKey = [poseDict valueForKey:key];
			
			if ([poseOfKey isKindOfClass:[NSArray class]]) {
				NSArray *array = (NSArray *)poseOfKey;
				if (indexPath.section == 0) {
					dict = [array objectAtIndex:indexPath.row - 1];	
				}
				else {
					dict = [array objectAtIndex:indexPath.row];	
				}
				NSLog(@"row %i", indexPath.row);
			}
			else if ([poseOfKey isKindOfClass:[NSDictionary class]]){
				dict = (NSDictionary *)poseOfKey;
			}
			else {
				NSLog(@"The current pose data structure is not dictionary");
			}
			
			newTitle = [dict objectForKey:@"poseName"];;
		}
		
		[changeTitleCallbackTarget performSelector:changeTitleCallbackSelector withObject:newTitle];
		
		[self.navigationController popViewControllerAnimated:YES];
	}
	else {
		NSDictionary *dict;
		NSString *dateStr = todayStr();
		
		if (indexPath.row == 0 && indexPath.section == 0) {
			
			dict = [NSDictionary dictionaryWithObjectsAndKeys:@"General Issue", @"poseName", dateStr, @"updateDate", nil];
		}
		else {
			NSString *key = [self.keys objectAtIndex:indexPath.section];
			NSLog(@"section %i", indexPath.section);
			id poseOfKey = [poseDict valueForKey:key];
			
			if ([poseOfKey isKindOfClass:[NSArray class]]) {
				NSArray *array = (NSArray *)poseOfKey;
				if (indexPath.section == 0) {
					dict = [array objectAtIndex:indexPath.row - 1];	
				}
				else {
					dict = [array objectAtIndex:indexPath.row];	
				}
				NSLog(@"row %i", indexPath.row);
			}
			else if ([poseOfKey isKindOfClass:[NSDictionary class]]){
				dict = (NSDictionary *)poseOfKey;
			}
			else {
				NSLog(@"The current pose data structure is not dictionary");
			}
			NSString *poseName = [dict objectForKey:@"poseName"];
			dict = [NSDictionary dictionaryWithObjectsAndKeys:poseName, @"poseName", dateStr, @"updateDate", nil];
		}
		
		IssueDetailViewController *vc = [IssueDetailViewController alloc];
		vc.issueDict = dict;
		vc.isAddIssue = YES;
		[vc initWithNibUsingClassName];
		[self.navigationController pushViewController:vc animated:YES];
		[vc release];
	}
}

@end

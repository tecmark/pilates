//
//  HomeQuickkaAppsViewController.m
//
// Created by Rob on 29/12/2010.
//  Copyright 2010 Bonbol Limited. All rights reserved.
//

#import "HomeQuickkaAppsViewController.h"

static NSString *links[] = {
	//TODO add the link
	@"http://itunes.apple.com/pk/app/weight-mate/id351329780?mt=8",//weight mate
	@"http://phobos.apple.com/WebObjects/MZSearch.woa/wa/search?WOURLEncoding=ISO8859_1&lang=1&output=lm&country=US&media=software&term=quickka+calories",//Calories
	@"http://phobos.apple.com/WebObjects/MZSearch.woa/wa/search?WOURLEncoding=ISO8859_1&lang=1&output=lm&country=US&media=software&term=quickka+calories+pro",//Calories PRO
	@"",//pilates
	@"http://itunes.apple.com/pk/app/urban-yoga/id403964391?mt=8"//urban yoga
};



@implementation HomeQuickkaAppsViewController
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	titleLB.text = @"Other Quickka Apps";
}

- (IBAction)gotoLink:(id)sender {
	
	
	int num = [sender tag];
	
	if(num<10){
		NSLog(@"Goto link %i", num);
		NSURL *URL = [NSURL URLWithString:links[num]];
		[[UIApplication sharedApplication] openURL:URL];
	}
	else{
		
		
	}
	
}


@end

//
//  ClassSectionViewController.m
//  Yoga
//
// Created by Rob on 10-3-22.
//

#import "ClassSectionViewController.h"
#import "ClassSectionTableCell.h"
#import "ClassSectionDetailViewController.h"
#import "EditClassViewController.h"
#import "ClassInfoViewController.h"



#define kTableCellHeight 65
#define kTableMaxHeight  325



@implementation ClassSectionViewController


@synthesize yogaClass;
@synthesize totalDuration;
@synthesize className;



- (void)setClassName:(NSString *)name {
	titleLB.text = name;
}
- (void)setTotalDuration:(int)duration {
	totalDuration = duration;
	int hour = totalDuration/3600;
	int min = (totalDuration%3600) / 60;
	int sec = totalDuration%60;
	NSString *hourStr = (0 == hour) ? @"" : [NSString stringWithFormat:@"%i:", hour];
	NSString *minStr = [NSString stringWithFormat:@"%02i", min];
	durationLB.text = [NSString stringWithFormat:@"%@%@:%02i", hourStr, minStr, sec];
}


- (void)loadData {
    
    NSLog(@"Loading Class Krama from ClassSections Table");
	[dataArray removeAllObjects];
	[allPoseArray removeAllObjects];
	totalDuration = 0;
	id<PLResultSet> result;
	result = [dbHandle() executeQuery: @"SELECT * FROM ClassSections where ClassID = ? order by SectionIndex", stringFromInt(yogaClass.classDB_ID) /*[[classDict objectForKey:@"ID"] stringValue]*/];
	NSNumber *ID;
	NSString *name;
	NSString *picPath;
	NSString *videoPath;
	NSNumber *duration;
	NSNumber *repeatCount;
	NSString *description;
	NSNumber *selected;
    
    
    NSLog(@"Loop results for class %@", stringFromInt(yogaClass.classDB_ID));
    
	while ([result next]) {
        
        
        NSLog(@"Loop");
        
		ID = [result objectForColumn:@"ID"];
		name = [result stringForColumn:@"Name"];
		picPath = [result stringForColumn:@"PicturePath"];
		videoPath = [result stringForColumn:@"VideoPath"];
		repeatCount = [result objectForColumn:@"RepeatCount"];
		duration = [result objectForColumn:@"Duration"];
		description = [result stringForColumn:@"Description"];
		selected = [result objectForColumn:@"Selected"];
		NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:ID, @"ID",
							  name, @"name", 
							  picPath, @"picPath", 
							  videoPath, @"videoPath", 
							  repeatCount, @"repeatCount", 
							  duration, @"duration",
							  description, @"description",
							  selected, @"selected",
							  nil];
		[allPoseArray addObject:dict];
		
		if ([selected boolValue]) {
			self.totalDuration += [duration intValue] * [repeatCount intValue];
			[dataArray addObject:dict];
		}
	}
	[result close];
	
    NSLog(@"Loading complete");
}


#pragma mark -
#pragma mark View Controller methods
- (void)viewDidLoad {
	allPoseArray = [NSMutableArray new];
	
    [super viewDidLoad];
	titleLB.text = yogaClass.name; //[classDict objectForKey:@"className"];
}
- (void)viewWillAppear:(BOOL)animated {
	[self loadData];
	[table reloadData];
	table.frameSizeHeight = 381;
	table.frameOriginY=40;
}
- (void)dealloc {
	[yogaClass release];
	[allPoseArray release];
    [super dealloc];
}


#pragma mark -
#pragma mark --- UITableView Delegate ---
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *dict = [dataArray objectAtIndex:indexPath.row];
	ClassSectionDetailViewController *vc = [ClassSectionDetailViewController alloc];
	vc.sectionDict = dict;
	[vc initWithNibUsingClassName];
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}


#pragma mark -
#pragma mark  --- UITableView Datasource ---
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	ClassSectionTableCell *cell = (ClassSectionTableCell *)[tableView dequeueReusableCellWithIdentifier:@"ClassSectionTableCell"];
	if (nil == cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ClassSectionTableCell" owner:self options:nil];
		cell = (ClassSectionTableCell *)[nib objectAtIndex:0];
	}
	
	NSDictionary *dict = [dataArray objectAtIndex:indexPath.row];
	NSString *name = [dict objectForKey:@"name"];
	NSString *picPath = [dict objectForKey:@"picPath"]; 
	NSNumber *duration = [dict objectForKey:@"duration"];
	NSNumber *repeatCount = [dict objectForKey:@"repeatCount"];
	NSString *durationStr = [NSString stringWithFormat:@"%i min",([duration intValue]*[repeatCount intValue])/60];
	NSString *description = [dict objectForKey:@"description"];
	
	cell.titleLB.text = name;
	if ([yogaClass.classID isEqualToString:@"0001"]) {
		cell.poseIV.image = [UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:picPath]];
	}
	else {
		cell.poseIV.image = [UIImage imageWithContentsOfFile:picPath];
	}

    [cell.poseIV layoutSubviews];
	
	cell.durationLB.text = durationStr;
	cell.descLB.text = description;
	
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	return cell;
}


#pragma mark -
#pragma mark IB Action methods
- (IBAction)showClassInfo {
	ClassInfoViewController *vc = [ClassInfoViewController alloc];
	//vc.classDict = self.classDict;
	vc.yogaClass = self.yogaClass;
	[vc initWithNibUsingClassName];
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}
- (IBAction)playClassVedio{
	NSMutableArray *movieArray = [NSMutableArray array];
    NSMutableArray *repeatArray = [NSMutableArray array];
	for (NSDictionary *dict in dataArray) {
		NSLog(@"Attempting to Add video");
		BOOL selected = [[dict objectForKey:@"selected"] boolValue];
		if (selected) {
			NSLog(@"video selected, add to playlist %@",[dict objectForKey:@"videoPath"]);
			NSString *movieName = [dict objectForKey:@"videoPath"];
			[movieArray addObject:movieName];
            NSNumber *repeats = [dict objectForKey:@"repeatCount"];
            
            NSLog(@"adding repeat value %i", [repeats intValue]);
            [repeatArray addObject:repeats];
            
		}
	}
	
	moviePlayer = [MoviePlayerViewController alloc];
	moviePlayer.yogaClass=self.yogaClass;
	moviePlayer.movieArray = movieArray;
    moviePlayer.repeatArray = repeatArray;
	moviePlayer.isPlayingIndividual = NO;
	[moviePlayer initWithNibUsingClassName];
	[self.navigationController pushViewController:moviePlayer animated:NO];
	[moviePlayer release];
}
- (IBAction)editClass {
	EditClassViewController *vc = [EditClassViewController alloc];
	vc.durationStr = durationLB.text;
	vc.totalDuration = totalDuration;
	vc.className = yogaClass.name; //[classDict objectForKey:@"className"];
	vc.sectionArray = allPoseArray;
	vc.classID = yogaClass.classDB_ID; //[[classDict objectForKey:@"ID"] intValue];
	[vc initWithNibUsingClassName];
	
	[self.navigationController pushViewController:vc animated:NO];
	[vc release];
}

@end

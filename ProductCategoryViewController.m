//
//  ProductListViewController.m
//  Yoga
//
// Created by Rob on 10-4-16.
//  
//

#import "ProductCategoryViewController.h"
#import "ProductSubCategoryViewController.h"
#import "TextTableCell.h"
#define kTableCellHeight 50
#define kTableMaxHeight 350

static NSString *categoryName[] = {
	@"Clothing",
	@"Equipment",
	@"Accesssories",
	@"Recommended Books",
	@"Useful Websites",
	@"Wishlist"
};
@implementation ProductCategoryViewController
@synthesize categoryID;
@synthesize customType;

- (void)loadData {
	id<PLResultSet> result;
	
	result = [dbHandle() executeQuery: @"SELECT * FROM ProductCategory where CategoryID = ? and CustomType = 1 order by SubCategoryID",
				  stringFromInt(categoryID)];
	

	while ([result next]) {
		NSString *subCategoryName = [result stringForColumn:@"SubCategoryName"];
		NSNumber *subCategoryID = [result objectForColumn:@"SubCategoryID"];
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
							  subCategoryID, @"subCategoryID",
							  subCategoryName, @"subCategoryName",
							  nil];
		[dataArray addObject:dict];
	}
	[result close];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
		customType = 1;
		womenBtn.selected = YES;
    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	if (1 == categoryID) {
		bottomBar.hidden = NO;
	}
	titleLB.text = categoryName[categoryID - 1];
	table.frameSizeHeight = MIN(kTableMaxHeight+30, kTableCellHeight*dataArray.count);
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

#pragma mark --- UITableView delegate ---
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *dict = [dataArray objectAtIndex:indexPath.row];
	ProductSubCategoryViewController *vc = [ProductSubCategoryViewController alloc];
	vc.categoryID = self.categoryID;
	vc.customType = self.customType;
	vc.subCategoryDict = dict;
	
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
}


#pragma mark --- UITableView Datasource ---
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return kTableCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	TextTableCell *cell = (TextTableCell *)[tableView dequeueReusableCellWithIdentifier:@"TextTableCell"];
	if (nil == cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TextTableCell" owner:nil options:nil];
		cell = (TextTableCell *)[nib objectAtIndex:0];
	}
	
	NSString *name = [[dataArray objectAtIndex:indexPath.row] objectForKey:@"subCategoryName"];
	
	cell.textLB.text = name;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
//	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	return cell;
}

- (IBAction)changeCustomType:(id)sender {
	if (womenBtn == sender) {
		womenBtn.selected = YES;
		menBtn.selected = NO;
		kidsBtn.selected = NO;
		customType = 1;
	}
	else if (menBtn == sender) {
		womenBtn.selected = NO;
		menBtn.selected = YES;
		kidsBtn.selected = NO;
		customType = 2;
	}
	else {
		womenBtn.selected = NO;
		menBtn.selected = NO;
		kidsBtn.selected = YES;
		customType = 3;
	}	
}
@end

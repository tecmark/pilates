//
//  ReadCommentTableCell.h
//  Yoga
//
// Created by Rob on  11/26/10.
//  
//

#import <UIKit/UIKit.h>
#import "RatingStars.h"

@interface ReadCommentTableCell : UITableViewCell {
	IBOutlet UILabel *titleL;
	IBOutlet UILabel *reviewerAndDateL;
	IBOutlet UILabel *commentL;
	IBOutlet RatingStars *reatingV;
	
	IBOutlet UIView *backColorView;
}


@property (nonatomic, assign) IBOutlet UILabel *titleL;
@property (nonatomic, assign) IBOutlet UILabel *reviewerAndDateL;
@property (nonatomic, assign) IBOutlet UILabel *commentL;
@property (nonatomic, assign) IBOutlet RatingStars *reatingV;


- (void)setBackColorByType:(NSInteger)typeIndex;


@end

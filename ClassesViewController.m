//
//  ClassesViewController.m
//  Yoga
//
// Created by Rob on 10-3-15.

//

#import "ClassesViewController.h"
#import "MyClassViewController.h"
#import "UAStoreFront.h"
#import "UAProduct.h"
#import "RateCommentsHelper.h"
#import "UA_Reachability.h"


@implementation ClassesViewController

- (void)showLoading {
	/*
     if (loadingView == nil) {
     loadingView = [LoadingView loadingViewInView:self.view statusMessage:@"Loading New Classes..."];
     }
	 */
	[newClassesIndicator startAnimating];
	newClassesIndicator.hidden=NO;
	newClassesBtn.enabled = NO;
	[downloadsIndicator startAnimating];
	downloadsIndicator.hidden=NO;
	downloadsBtn.enabled = NO;
	
}
- (void)hideLoading {
    //[loadingView removeView];
	//loadingView = nil;
	
	[newClassesIndicator stopAnimating];
	newClassesIndicator.hidden=YES;
	downloadBadge1.alpha=1;
	newClassesBtn.enabled = YES;
	
	[downloadsIndicator stopAnimating];
	downloadsIndicator.hidden=YES;
	downloadBadge2.alpha=1;
	downloadsBtn.enabled = YES;
	
}


#pragma mark -
#pragma mark Private methods
- (void)updateClassNumbers {
    
    NSLog(@"UPDATE CLASS NUMBERS");
	NSInteger productAmount = [inventory productsForType:ProductTypeAll].count;
	NSInteger wartingDownloads = 0;
	NSInteger unpurchased = 0;
	for (int i = 0; i < productAmount; i++) {
		UAProduct *product = [inventory productAtIndex:i];
		if (product.status == 0) {
			unpurchased++;
		}
		else if (product.status == UAProductStatusPurchased || product.status == UAProductStatusDownloading) {
			wartingDownloads++;
		}
	}
		
	newClassesNumber.text = [NSString stringWithFormat:@"%i", unpurchased, nil];
	downloadNumber.text = [NSString stringWithFormat:@"%i", wartingDownloads, nil];
    
    [self hideLoading];
}
- (void)inventoryStatusChanged:(NSNumber *)status {
    
     NSLog(@"INVENTORY CHANGED");
    
    
    NSString *statusString = nil;
    UAInventoryStatus st = [status intValue];
    
     if (st == UAInventoryStatusDownloading) {
     [self showLoading];
     NSLog(@"UA_Status_Downloading");
     
     } else if(st == UAInventoryStatusApple) {
     NSLog(@"UA_Status_Apple");
     
     } else if(st == UAInventoryStatusFailed) {
     NSLog(@"UA_Status_Failed");
     //[self hideLoading];
     //newClassesNumber.text = [NSString stringWithFormat:@"%i", 0, nil];
     //downloadNumber.text = [NSString stringWithFormat:@"%i", 0, nil];
     
     } else if(st == UAInventoryStatusPurchaseDisabled) {
     NSLog(@"UA_Status_Disabled");
     [self hideLoading];
     [self updateClassNumbers];
     
     } else if(st == UAInventoryStatusLoaded) {
         
         
         NSLog(@"UA_Inventory_Status_Loaded");
         [self hideLoading];
         [self updateClassNumbers];
         firstLoadComplete=YES;
         
     /*
     if ([inv countForType:ProductTypeAll] == 0) {
     NSLog(@"UA_No_Content");
     } else {
     //[self hideLoading]
     //[self updateClassNumbers];
     }
     */
} //else if (st == UAInventoryStatusLoaded) {
    /*
     if (!((AppDelegate*)[UIApplication sharedApplication].delegate).isLoadedCommentsRate ) {
     ((AppDelegate*)[UIApplication sharedApplication].delegate).isLoadedCommentsRate = YES;
     [[RateCommentsHelper shared] updateRateCommentsFromServerForAllClasses];
     }
     */
 
    //}


}

-(void)viewWillAppear:(BOOL)animated{
    
    if(firstLoadComplete){
    [self showLoading];
    [self updateClassNumbers];
    }
}

#pragma mark -
#pragma mark View Controller methods
- (void)viewDidLoad {
	loadingView = nil;
    [super viewDidLoad];
	self.switchViewContorllerNameArray = [NSArray arrayWithObjects:
										  @"MyClassViewController",
										  @"NewClassViewController",
										  @"DownloadsViewController",									
										  nil];
	Reachability *reach = [Reachability reachabilityForInternetConnection];
	if ([reach currentReachabilityStatus] == UA_NotReachable) {
		showAlertViewWithMessage(@"To view and download new classes please ensure you have an active cellular or WiFi connection.");
		myClassesBtn.enabled = YES;
		newClassesBtn.enabled = NO;
		downloadsBtn.enabled = NO;
		offline1.hidden=NO;
		offline2.hidden=NO;
		downloadsIndicator.hidden=YES;
		newClassesIndicator.hidden=YES;
		return;
		
	}
	else{
		
		newClassesBtn.enabled = YES;
		downloadsBtn.enabled = YES;
		offline1.hidden=YES;
		offline2.hidden=YES;	
		downloadsIndicator.hidden=NO;
		newClassesIndicator.hidden=NO;
	}
	
          NSLog(@"load inventory");
	inventory = [UAStoreFront shared].inventory;
   //	[inventory registerObserver:self];
    
    [inventory addObserver:self];
	[inventory loadInventory];
    
        
}
- (void)dealloc {
	//[inventory unregisterObserver:self];
    [inventory removeObserver:self];
    inventory = nil;
    [super dealloc];
}


#pragma mark -
#pragma mark KVO UAInventory status observer
- (void)observeValueForKeyPath:(NSString *)keyPath 
					  ofObject:(id)object 
                        change:(NSDictionary *)change 
					   context:(void *)context {
    
    NSLog(@"CHECK INVENTORY");
    if ([keyPath isEqualToString:@"status"]) {
       // [self inventoryStatusChanged:inventory];
         [self inventoryStatusChanged:[NSNumber numberWithInt:UAInventoryStatusLoaded]];
    }
}



#pragma mark -
#pragma mark Ib Action
- (IBAction)back {
	[self.navigationController popViewControllerAnimated:YES];
}

@end

//
//  ConnectViewController.h
//  Yoga
//
// Created by Rob on 10-3-15.

//

#import <UIKit/UIKit.h>


@interface ConnectViewController : CommonHomeViewController {
	IBOutlet UIImageView *matLogo;
	IBOutlet UILabel *whishlistCount;
	IBOutlet UIScrollView *scrollView;

}
- (IBAction)goToClassLocator:(id)sender;
- (IBAction)openReading:(id)sender;

- (IBAction)gotoPage:(id)sender;
- (IBAction)gotoOffTheMat;
- (IBAction)back;
@end

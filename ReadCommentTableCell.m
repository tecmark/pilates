//
//  ReadCommentTableCell.m
//  Yoga
//
// Created by Rob on  11/26/10.
//  
//

#import "ReadCommentTableCell.h"


@implementation ReadCommentTableCell


@synthesize titleL, reviewerAndDateL, commentL, reatingV;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];
}


- (void)dealloc {
    [super dealloc];
}

- (void)setBackColorByType:(NSInteger)typeIndex {
	if (typeIndex == 0) {
		backColorView.backgroundColor = [UIColor colorWithRed:229.0/255.0 green:222.0/255.0 blue:218.0/255.0 alpha:1];
	}
	else {
		backColorView.backgroundColor = [UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:209.0/255.0 alpha:1];
	}
	
}




@end
